# Installation

ModularForms.jl requires binary parts that are provided by the auxiliary package ModularForms_jll. If the required version is registered through the infrastructure of the JuliaPackaging group on GitHub, Julia can install it automatically. If you do not wish to rely on this or if the required version is not available (primarily if you use a development version), you can build and deploy it locally to `Pkg.devdir()`.

``
julia deps/build_tarballs.jl x86_64-linux-gnu --deploy=local
``

This will download the source code for ModularForms_jll via git. If you have provided the source code locally, you can use that one by setting the environment variable `MODULAR_FORMS_JLL_SRC`.

You might need to substitute your own plattform string for the provided one. After deployment you can add the package as a development version to Julia

``
using Pkg
Pkg.develop(path = joinpath(Pkg.devdir(), "ModularForms_jll"))
``
