using Nemo

function fmma!(r::fmpz, a::fmpz, b::fmpz, c::fmpz, d::fmpz)
  ccall((:fmpz_fmma, Nemo.libflint), Nothing,
        (Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}), r, a, b, c, d)
  return r
end

struct SL2ZElemI
  a::fmpz
  b::fmpz
  c::fmpz
  d::fmpz
end

mutable struct SL2ZElemM
  a::fmpz
  b::fmpz
  c::fmpz
  d::fmpz
end

function *(g::SL2ZElemI, h::SL2ZElemI)
  a = ZZ()
  a = fmma!(a, g.a, h.a, g.b, h.c)
  b = ZZ()
  b = fmma!(b, g.a, h.b, g.b, h.d)
  c = ZZ()
  c = fmma!(c, g.c, h.a, g.d, h.c)
  d = ZZ()
  d = fmma!(d, g.c, h.b, g.d, h.d)

  SL2ZElemI(a, b, c, d)
end

function *(g::SL2ZElemM, h::SL2ZElemM)
  a = ZZ()
  a = fmma!(a, g.a, h.a, g.b, h.c)
  b = ZZ()
  b = fmma!(b, g.a, h.b, g.b, h.d)
  c = ZZ()
  c = fmma!(c, g.c, h.a, g.d, h.c)
  d = ZZ()
  d = fmma!(d, g.c, h.b, g.d, h.d)

  SL2ZElemM(a, b, c, d)
end

function set!(g::T, a::fmpz, b::fmpz, c::fmpz, d::fmpz) where {T <: Union{SL2ZElemI, SL2ZElemM}}
  Nemo.set!(g.a, a)
  Nemo.set!(g.b, b)
  Nemo.set!(g.c, c)
  Nemo.set!(g.d, d)
end

function profileI(n::Int)
  g = SL2ZElemI(zero(ZZ), zero(ZZ), zero(ZZ), zero(ZZ))
  h = SL2ZElemI(zero(ZZ), zero(ZZ), zero(ZZ), zero(ZZ))
  for _ in 1:n
    set!(g, rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)),
         rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)))
    set!(h, rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)),
         rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)))
    f = g * h
  end
end

function profileM(n::Int)
  g = SL2ZElemM(zero(ZZ), zero(ZZ), zero(ZZ), zero(ZZ))
  h = SL2ZElemM(zero(ZZ), zero(ZZ), zero(ZZ), zero(ZZ))
  for _ in 1:n
    set!(g, rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)),
         rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)))
    set!(h, rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)),
         rand(ZZ, 1:(BigInt(1) << 63)), rand(ZZ, 1:(BigInt(1) << 63)))
    f = g * h
  end
end
