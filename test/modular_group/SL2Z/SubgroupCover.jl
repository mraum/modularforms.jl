function runtests_modular_group_sl2z_subgroup_cover()
  @testset "Subgroups of covers of SL(2,ZZ)" begin
    @test ngens(Mp1Z) == length(gens(Mp1Z))
    @test isone(one(Mp1Z))

    @testset "creation and comparison" for tx in 1:10
      a = rand(Mp1Z, -20:20)
      @test a == a
      @test copy(a) == a
      @test deepcopy(a) == a
    end

    @test ModularForms._ismp1z(Mp1Z)
    for w in gen_relation_words(Mp1Z)
      @test isone(gen_word(Mp1Z,w))
    end

    @test isa(Gamma0Mp1Z(3), AbstractAlgebra.Group)
    @test isa(Gamma1Mp1Z(3), AbstractAlgebra.Group)
    @test isa(GammaMp1Z(3), AbstractAlgebra.Group)
    @test isa(GammaNSMp1Z(3), AbstractAlgebra.Group)

    for G in [Mp1Z, Gamma0Mp1Z(10), Gamma1Mp1Z(5)]
      for tx in 1:50
        g = rand(G, -20:20)
        @test g == gen_word(G, word_in_gens(g))
      end
  
      for tx in 1:10
        a = rand(G, -20:20)
        b = rand(G, -20:20)
        @test Mp1Z(a*b) == Mp1Z(a)*Mp1Z(b)
  
        a = Mp1Z(a)
        b = Mp1Z(b)
        @test G(a*b) == G(a)*G(b)
      end
    end

    for tx in 1:100
      a = rand(Mp1Z, -10:10)
      b = rand(Mp1Z, -10:10)
      c = rand(Mp1Z, -10:10)
      @test a*(b*c) == (a*b)*c
      @test isone(a*inv(a))
    end

    for N in 1:10
      gp = Gamma0Mp1Z(N)

      cosets = left_coset_representatives(Mp1Z,gp)
      h = rand(Mp1Z, -20:20)
      (hr,hx,gr) = left_coset_transformation_with_index(gp,h)
      @test gr in gp
      @test hr*gr == h
      @test cosets[hx] == hr
      (hr,gr) = left_coset_transformation(gp,h)
      @test gr in gp
      @test hr*gr == h

      cosets = right_coset_representatives(Mp1Z,gp)
      h = rand(Mp1Z, -20:20)
      (gr,hr,hx) = right_coset_transformation_with_index(gp,h)
      @test gr in gp
      @test gr*hr == h
      @test cosets[hx] == hr
      (gr,hr) = right_coset_transformation(gp,h)
      @test gr in gp
      @test gr*hr == h
    end

    @testset "intersection of subgroups of Mp(1,ZZ)" begin
      @test intersect(Gamma0Mp1Z(7), Gamma0Mp1Z(5)) == Gamma0Mp1Z(35)
      # TODO: migrate the level information to
      # AbstractAlgebra.get/set_attribute as in the case of arithmetic
      # types
      @test_skip level(intersect(Gamma0Mp1Z(7), Gamma0Mp1Z(5))) == 35
    end
  end
end
