function runtests_modular_group_psl2z_cusp()
  @testset "PSL2Z cusps" begin
    for tx in 1:10
      q = rand(PSL2ZCusp, -100:100)
      @test moebius(one(SL2Z),q) == q

      a = rand(SL2Z, -10:10)
      @test moebius(a, q) == a*q

      b = rand(SL2Z, -10:10)
      @test a*(b*q) == (a*b)*q

      q = rand(QQ, -100:100)
      @test moebius(a, q) == moebius(a, PSL2ZCusp(q))

      q = rand(PSL2ZCusp, -100:100)
      g = cusp_inv_transformation(q)
      @test isa(g,SL2ZElem)
      @test isinfty(g*q)

      q = rand(QQ, -100:100)
      g = cusp_inv_transformation(q)
      @test isa(g,SL2ZElem)
      @test isinfty(g*PSL2ZCusp(q))
    end
  end
end
