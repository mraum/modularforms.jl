function runtests_modular_group_sl2z_permutation_stabilizer()
  permutation_stabilizer_sl2z_iselement =
      ModularForms.PermStabGL2Z_MODULE.permutation_stabilizer_sl2z_iselement
  @testset "Permutation stablizers in GL2Z" begin
    @testset "Example: Gamma0(6)" begin
      rho = right_induction(TrivialArithmeticType(Gamma0(6)))
      (s,t) = generator_images(rho)
      stab = permutation_stabilizer_sl2z_iselement(
                 [(perms(s),perms(t))], [9])
      stab_epsperm = ModularForms.PermStabGL2Z_MODULE.epsperm(stab)
      stab_negperm = ModularForms.PermStabGL2Z_MODULE.negperm(stab)
      stab_sperm   = ModularForms.PermStabGL2Z_MODULE.sperm(stab)
      stab_tperm   = ModularForms.PermStabGL2Z_MODULE.tperm(stab)
      stab_idperm  = collect(1:ModularForms.PermStabGL2Z_MODULE.permsize(stab))

      # I = (-I)^2
      @test (stab_idperm ==
             stab_negperm[stab_negperm]
           )
  
      # I = \epsilon^2
      @test (stab_idperm ==
             stab_epsperm[stab_epsperm]
           )
  
      # I = (S')^2
      @test (stab_idperm ==
             stab_sperm[stab_sperm]
           )
  
      # I = T^6
      @test (stab_idperm ==
             stab_tperm[stab_tperm][
                 stab_tperm][stab_tperm][
                 stab_tperm][stab_tperm]
           )

      # \epsilon = S' * Tinv * S' * T * S' * Tinv
      @test (stab_epsperm ==
             stab_sperm[invperm(stab_tperm)][
                 stab_sperm][stab_tperm][
                 stab_sperm][invperm(stab_tperm)]
           )
  
      # -I = S' \epsilon S' \epsilon
      @test (stab_negperm ==
             stab_sperm[stab_epsperm][
                 stab_sperm][stab_epsperm]
           )
  
      # -I = (S' epsilon * T)^3
      @test (stab_negperm ==
             stab_sperm[stab_epsperm][stab_tperm][
                 stab_sperm][stab_epsperm][stab_tperm][
                 stab_sperm][stab_epsperm][stab_tperm]
            )
  
    end
  end

  @testset "Comparision with naive computation" begin
    rho1 = GenericType(right_induction(TrivialArithmeticType(Gamma0(3))))
    rho2 = GenericType(right_induction(TrivialArithmeticType(Gamma0(5))))
    rho3 = GenericType(right_induction(TrivialArithmeticType(Gamma0(7))))
    (s1,t1) = map(perms, generator_images(rho1))
    (s2,t2) = map(perms, generator_images(rho2))
    (s3,t3) = map(perms, generator_images(rho3))
    ix1 = rand(1:dim(rho1))
    ix2 = rand(1:dim(rho2))
    ix3 = rand(1:dim(rho3))

    stab1   = permutation_stabilizer_sl2z_iselement(
                  [(s1,t1)], [ix1])
    stab2   = permutation_stabilizer_sl2z_iselement(
                  [(s2,t2)], [ix2])
    stab3   = permutation_stabilizer_sl2z_iselement(
                  [(s3,t3)], [ix3])
    stab12  = permutation_stabilizer_sl2z_iselement(
                  [(s1,t1),(s2,t2)], [ix1,ix2])
    stab123 = permutation_stabilizer_sl2z_iselement(
                  [(s1,t1),(s2,t2),(s3,t3)], [ix1,ix2,ix3])


    # Special cases of c = 0 and a = 0.
    for g in [SL2Z(1,0,0,1)   ,  SL2Z(1,3,0,1)   ,  SL2Z(1,-3,0,1),
              SL2Z(-1,0,0,-1) ,  SL2Z(-1,3,0,-1) ,  SL2Z(-1,-3,0,-1),
              SL2Z(0,-1,1,0)  ,  SL2Z(0,-1,1,3)  ,  SL2Z(0,-1,1,-3),
              SL2Z(0,1,-1,0)  ,  SL2Z(0,1,-1,3)  ,  SL2Z(0,1,-1,-3)]
      gstab1 = (act_perm(rho1(g), ix1) == ix1)
      gstab2 = (act_perm(rho2(g), ix2) == ix2)
      gstab3 = (act_perm(rho3(g), ix3) == ix3)

      gnegstab1 = (act_perm(rho1(-g), ix1) == ix1)
      gnegstab2 = (act_perm(rho2(-g), ix2) == ix2)
      gnegstab3 = (act_perm(rho3(-g), ix3) == ix3)

      @test stab1(g)   == (gstab1, gstab1 || gnegstab1)
      @test stab2(g)   == (gstab2, gstab2 || gnegstab2)
      @test stab3(g)   == (gstab3, gstab3 || gnegstab3)
      @test stab12(g)  ==
            (gstab1 && gstab2,
             gstab1 && gstab2 || gnegstab1 && gnegstab2)
      @test stab123(g) ==
            (gstab1 && gstab2 && gstab3,
             gstab1 && gstab2 && gstab3 ||
                 gnegstab1 && gnegstab2 && gnegstab3)
    end

    for tx in 1:100
      g = rand(SL2Z, -20:20)

      gstab1 = (act_perm(rho1(g), ix1) == ix1)
      gstab2 = (act_perm(rho2(g), ix2) == ix2)
      gstab3 = (act_perm(rho3(g), ix3) == ix3)

      gnegstab1 = (act_perm(rho1(-g), ix1) == ix1)
      gnegstab2 = (act_perm(rho2(-g), ix2) == ix2)
      gnegstab3 = (act_perm(rho3(-g), ix3) == ix3)

      @test stab1(g)   == (gstab1, gstab1 || gnegstab1)
      @test stab2(g)   == (gstab2, gstab2 || gnegstab2)
      @test stab3(g)   == (gstab3, gstab3 || gnegstab3)
      @test stab12(g)  ==
            (gstab1 && gstab2,
             gstab1 && gstab2 || gnegstab1 && gnegstab2)
      @test stab123(g) ==
            (gstab1 && gstab2 && gstab3,
             gstab1 && gstab2 && gstab3 ||
                 gnegstab1 && gnegstab2 && gnegstab3)
    end
  end

  @testset "Use in Farey symbols" begin
    FareySymbolSL2Z = ModularForms.FareySymbolSL2Z_MODULE.FareySymbolSL2Z

    rho1 = GenericType(right_induction(TrivialArithmeticType(Gamma0(3))))
    rho2 = GenericType(right_induction(TrivialArithmeticType(Gamma0(5))))
    (s1,t1) = map(perms, generator_images(rho1))
    (s2,t2) = map(perms, generator_images(rho2))
    ix1 = rand(1:dim(rho1))
    ix2 = rand(1:dim(rho2))

    stab1  = permutation_stabilizer_sl2z_iselement(
                    [(s1,t1)], [ix1])
    stab2  = permutation_stabilizer_sl2z_iselement(
                    [(s2,t2)], [ix2])
    stab12 = permutation_stabilizer_sl2z_iselement(
                    [(s1,t1),(s2,t2)], [ix1,ix2])

    stab1_iselement_via_act(g) = (act_perm(rho1(g), ix1) == ix1)
    stab1_iselement_plusminus_via_act(g) =
        (act_perm(rho1(g), ix1) == ix1 || act_perm(rho1(-g), ix1) == ix1)
    gp = SL2ZSubgroup(FareySymbolSL2Z(
                    stab1_iselement_via_act,
                    stab1_iselement_plusminus_via_act))

    stab1_iselement_via_sl2z_impl(g) = stab1(g)[1]
    stab1_iselement_plusminus_via_sl2z_impl(g) =
        let (a,b) = stab1(g); a || b end
    @test gp == SL2ZSubgroup(FareySymbolSL2Z(
                    stab1_iselement_via_sl2z_impl,
                    stab1_iselement_plusminus_via_sl2z_impl))

    stab12_iselement_via_act(g) =
        (act_perm(rho1(g), ix1) == ix1 && act_perm(rho2(g), ix2) == ix2)
    stab12_iselement_plusminus_via_act(g) =
        (act_perm(rho1(g), ix1) == ix1 && act_perm(rho2(g), ix2) == ix2 ||
         act_perm(rho1(-g), ix1) == ix1 && act_perm(rho2(-g), ix2) == ix2)
    gp = SL2ZSubgroup(FareySymbolSL2Z(
                    stab12_iselement_via_act,
                    stab12_iselement_plusminus_via_act))

    stab12_iselement_via_sl2z_impl(g) = stab12(g)[1]
    stab12_iselement_plusminus_via_sl2z_impl(g) =
        let (a,b) = stab12(g); a || b end
    @test gp == SL2ZSubgroup(FareySymbolSL2Z(
                    stab12_iselement_via_sl2z_impl,
                    stab12_iselement_plusminus_via_sl2z_impl))
  end
end
