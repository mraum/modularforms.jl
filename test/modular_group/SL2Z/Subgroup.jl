function runtests_modular_group_sl2z_subgroup()
  matspace = MatrixSpace(ZZ,2,2)

  @testset "SL2z elements" begin
    for tx in 1:10
      a = rand(ModularForms.SL2zElem)

      b = rand(ModularForms.SL2zElem)
      c = rand(ModularForms.SL2zElem)
      @test a.a*a.d - a.b*a.c == 1
      @test a*(b*c) == (a*b)*c
      @test inv(a)*a == ModularForms.SL2zElem(1,0,0,1)
      @test a*inv(a) == ModularForms.SL2zElem(1,0,0,1)
    end
  end

  @testset "SL2Z elements" begin
    @test isone((-one(SL2Z))^2)

    for ix in 1:10
      a = rand(SL2Z, -10:10)
      b = inv(a)
      Id = one(SL2ZElem)
      tmp = SL2ZElem()

      mul!(tmp, Id, a)
      mul!(a, b, tmp)
      @test a == Id
    end

    for tx in 1:100
      a = rand(SL2Z, -10:10)
      b = rand(SL2Z, -10:10)
      c = rand(SL2Z, -10:10)
      @test a == a
      @test copy(a) == a
      @test deepcopy(a) == a
      @test a*(b*c) == (a*b)*c
      @test mul!(c,a,b) == a*b
      @test isone(a*inv(a))
      @test isone(inv(a)*a)

      @test matspace(a)*matspace(b) == matspace(a*b)
      m = rand(matspace, -10:10)
      @test m*(a*b) == (m*a)*b
      @test tr(a * b * c) == tr(c * a * b) == tr(b * c * a)
    end
  end

  @testset "SL2Z elements from bottom row" begin
    @test_throws DomainError SL2ZElem_from_bottom_row(3,6)
    @test isnothing(SL2ZElem_from_bottom_row(3,6; return_nothing=true))
    a = SL2ZElem_from_bottom_row(3,7)
    @test a.a*a.d - a.b*a.c == 1

    @test_throws DomainError ModularForms.SL2zElem_from_bottom_row(3,6)
    @test isnothing(ModularForms.SL2zElem_from_bottom_row(3,6; return_nothing=true))
    a = ModularForms.SL2zElem_from_bottom_row(3,7)
    @test a.a*a.d - a.b*a.c == 1
  end

  @testset "Subgroups of SL(2,ZZ)" begin
    @test isa(Gamma0SL2Z(3), AbstractAlgebra.Group)
    @test isa(Gamma1SL2Z(3), AbstractAlgebra.Group)
    @test isa(GammaSL2Z(3), AbstractAlgebra.Group)
    @test isa(GammaNSSL2Z(3), AbstractAlgebra.Group)

    # Sanity check: is the level what we think it is?
    for N in [5,7,11,13]
      gp1 = GammaNS(N)
      gp2 = GammaNSPlus(N)
      delete!(AbstractAlgebra._get_attributes(gp1), :level)
      delete!(AbstractAlgebra._get_attributes(gp2), :level)
      @test level(gp1,fallback=true) == N
      @test level(gp2,fallback=true) == N
    end

    for N in 1:10
      gp = Gamma0SL2Z(N)
      @test ngens(gp) == length(gens(gp))
      @test isone(one(gp))

      a = rand(gp, -10:10)
      b = rand(gp, -10:10)
      c = rand(gp, -10:10)
      @test a*(b*c) == (a*b)*c

      @test matspace(a)*matspace(b) == matspace(a*b)
      m = rand(matspace, -10:10)
      @test m*(a*b) == (m*a)*b
    end

    # These are internal functions.
    @test ModularForms._issl2z(SL2Z)
    @test level(SL2Z) == 1
    for N in 1:10
      @test (Gamma0SL2Z(N) == SL2Z) == (N == 1)
      @test ModularForms._isgamma0(Gamma0SL2Z(N))
      @test level(Gamma0SL2Z(N)) == N
    end

    for N1 in 1:10, N2 in 1:5
      gp1 = Gamma0SL2Z(N1)
      gp2 = Gamma0SL2Z(N2)
      @test (gp1 == gp2) == (N1 == N2)
      @test subgroup(SL2Z, g -> g in gp1) == gp1
      @test subgroup(gp1, g -> g in gp2) == Gamma0SL2Z(lcm(N1,N2))
    end

    @test_throws DomainError Gamma0SL2Z(10)(SL2Z(1,0,1,1))

    for G in [SL2Z, Gamma0SL2Z(1), Gamma0SL2Z(10), Gamma1SL2Z(5)]
      for tx in 1:50
        g = rand(G, -20:20)
        @test g == gen_word(G, word_in_gens(g))
      end
  
      for tx in 1:10
        a = rand(G, -20:20)
        b = rand(G, -20:20)
        @test SL2Z(a*b) == SL2Z(a)*SL2Z(b)
  
        a = SL2Z(a)
        b = SL2Z(b)
        @test G(a*b) == G(a)*G(b)
      end
    end

    let
      g = SL2Z(2,3,7,11)

      (s,l,ns) = ModularForms._word_in_stn(g)
      h = SL2Z(1,l,0,1)
      for n in ns  h = h * SL2Z(0,-1,1,n)  end
      if s == -1  h = -h  end
      @test g == h

      (s,ns,r) = ModularForms._word_in_tns(g)
      h = SL2Z(1,r,0,1)
      for n in reverse(ns)  h = SL2Z(n,-1,1,0) * h  end
      if s == -1  h = -h  end
      @test g == h
    end
 
    for w in gen_relation_words(SL2Z)
      @test isone(gen_word(SL2Z,w))
    end

    for N in 1:10
      gp = Gamma0SL2Z(N)

      cosets = left_coset_representatives(gp)
      h = rand(SL2Z, -20:20)
      (hr,hx,gr) = left_coset_transformation_with_index(gp,h)
      @test gr in gp
      @test hr*gr == h
      @test cosets[hx] == hr
      (hr,gr) = left_coset_transformation(gp,h)
      @test gr in gp
      @test hr*gr == h

      cosets = right_coset_representatives(gp)
      h = rand(SL2Z, -20:20)
      (gr,hr,hx) = right_coset_transformation_with_index(gp,h)
      @test gr in gp
      @test gr*hr == h
      @test cosets[hx] == hr
      (gr,hr) = right_coset_transformation(gp,h)
      @test gr in gp
      @test gr*hr == h

      cusps = cusp_representatives(gp)
      c = h*PSL2ZCusp()
      (gr,cr) = cusp_transformation(gp,c)
      @test gr in gp
      @test gr*cr == c
    end

    @testset "conjugates of subgroups" begin
      G = Gamma0SL2Z(7)
      g = SL2Z(2, -1, 1, 0)
      @test left_conjugate(g, right_conjugate(g, G)) == G
    end

    @testset "intersection of subgroups" begin
      @test intersect(Gamma0SL2Z(7), Gamma0SL2Z(5)) == Gamma0SL2Z(35)
    end
  end
end
