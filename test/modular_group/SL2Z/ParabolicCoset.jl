function helperfunction_doublecoset(c0::Int)
  # Generate vectors of coprime numbers
  vecs = Vector{Vector{Bool}}(undef, c0)
  for ix in 1:c0
    vecs[ix] = [gcd(ix, jx) == 1 for jx in 1:ix]
  end

  iter, n = ParabolicDoubleCosetSL2ZIterator(c0)
  ga = Vector{SL2ZElem}(undef, n + 1)
  ix = 1
  ga[1] = SL2ZElem(0, -1, 1, 0)
  for (dlen, alpha) in iter
    ix -= dlen
    for al in alpha
      ix += 1
      ga[ix] = SL2ZElem(0, -1, 1, al) * ga[ix - 1]
    end
    c = abs(Int(ga[ix].c))
    d = abs(Int(ga[ix].d))
    if vecs[c][d]
      vecs[c][d] = false
    else
      return false
    end
  end
  return sum(sum.(vecs)) == 0
end

function runtests_modular_group_sl2z_parabolic_coset()
  @testset "SL2Z double cosets with respect to Γ_∞" begin
    double_cosets = ModularForms.foldl_sl2z_parabolic_double_coset(5,
                       push!, ModularForms.SL2zElem[])
    @test all(g -> isone(g.a*g.d-g.b*g.c), double_cosets)

    double_cosets = ModularForms.foldl_sl2z_parabolic_double_coset(5,
                       (gs,g) -> push!(gs, (g.c,g.d)), NTuple{2,Int}[])
    @test sort!(double_cosets) ==
          vcat([(1,0)], [(c,d) for c in 2:5 for d in 1:c-1 if gcd(c, d) == 1])
  end

  @testset "SL2Z double cosets with respect to Γ_∞" begin
    for ix in 1:10
      @test helperfunction_doublecoset(ix)
    end
    for _ in 1:10
      ix = rand(11:1500)
      @test helperfunction_doublecoset(ix)
    end
  end
end
