# TODO: after implementing non-congruence subgroups, add tests
function runtests_modular_group_sl2z_farey_symbol()
  FareyFraction                        = ModularForms.FareySymbolSL2Z_MODULE.FareyFraction
  FareyFractionList                    = ModularForms.FareySymbolSL2Z_MODULE.FareyFractionList
  FareySymbolSL2Z                      = ModularForms.FareySymbolSL2Z_MODULE.FareySymbolSL2Z
  FareySymbolSL2Z_MODULE               = ModularForms.FareySymbolSL2Z_MODULE
  Unpaired                             = ModularForms.FareySymbolSL2Z_MODULE.Unpaired
  UnpairedList                         = ModularForms.FareySymbolSL2Z_MODULE.UnpairedList
  cusp_transformation                  = ModularForms.FareySymbolSL2Z_MODULE.cusp_transformation
  left_coset_transformation            = ModularForms.FareySymbolSL2Z_MODULE.left_coset_transformation
  left_coset_transformation_with_index = ModularForms.FareySymbolSL2Z_MODULE.left_coset_transformation_with_index
  pairing_matrix_in_group              = ModularForms.FareySymbolSL2Z_MODULE.pairing_matrix_in_group
  word_in_gens                         = ModularForms.FareySymbolSL2Z_MODULE.word_in_gens
  word_in_pairing_matrices             = ModularForms.FareySymbolSL2Z_MODULE.word_in_pairing_matrices


  function _test_word_problem(
      fs::ModularForms.FareySymbolSL2Z_MODULE.FareySymbolSL2Z,
      g::SL2ZElem
     )
    (word, gp) = word_in_pairing_matrices(fs, g)
    if isempty(word)
      @test g == gp
    else
      letter = wx ->
        if wx > 0
          pairing_matrix_in_group(fs,wx)
        else
          inv(pairing_matrix_in_group(fs,-wx))
        end
      @test g == prod(map(letter, word)) * gp
    end

    word = word_in_gens(fs, g)
    if isempty(word)
      @test isone(g)
    else
      @test g == prod(fs.generators[gx]^ge for (gx,ge) in word)
    end
  end

  function _test_farey_symbol(iselement::Function, iselement_plusminus::Function)
    "
    - reduction of cusp according to coset/cusp classes
    "
    fs = FareySymbolSL2Z(iselement, iselement_plusminus)
    S,T = SL2ZElem(0,-1,1,0), SL2ZElem(1,1,0,1)

    for c1 in fs.coset_reps, c2 in fs.coset_reps
      @test c1 == c2 || !iselement(inv(c1)*c2)
    end

    for c in fs.coset_reps, g in [S,T]
      (c2,cx2,g2) = left_coset_transformation_with_index(fs, g*c)
      @test cx2 > 0 ? fs.coset_reps[cx2] == c2 : fs.coset_reps[-cx2] == -c2
      @test iselement(g2)
      @test g*c == c2*g2
      _test_word_problem(fs, g2)
    end
    coset_orbit = left_orbit([S,T], SL2ZElem(1,0,0,1),  m -> left_coset_transformation(fs,m)[1])
    coset_plusminus = union(fs.coset_reps, [left_coset_transformation(fs,-c)[1] for c in fs.coset_reps])
    @test length(coset_orbit) == length(coset_plusminus)
    @test coset_orbit == union(coset_orbit,coset_plusminus)

    @test sum(fs.cusp_class_widths) == length(fs.coset_reps)

    begin
      cw = fs.cusp_class_widths[end]
      @test (  iselement(SL2ZElem(1,cw,0,1))
            || iselement(SL2ZElem(-1,cw,0,-1)) )
      for n in 1:cw-1
        @test !iselement(SL2ZElem(1,n,0,1))
        @test !iselement(SL2ZElem(-1,n,0,-1))
      end
    end
    for (cx,c) in enumerate(fs.cusp_class_finite_reps)
      cw = fs.cusp_class_widths[cx]
      m = ModularForms.cusp_inv_transformation(c)

      @test (  iselement(inv(m) * SL2ZElem(1,cw,0,1) * m)
            || iselement(inv(m) * SL2ZElem(-1,cw,0,-1) * m) )
      for n in 1:cw-1
        @test !iselement(inv(m) * SL2ZElem(1,n,0,1) * m)
        @test !iselement(inv(m) * SL2ZElem(-1,n,0,-1) * m)
      end
    end

    for (xx,x) in enumerate(fs.xs)
      cx = fs.cusp_classes[xx]
      (_,c) = cusp_transformation(fs,x)
      if isinfty(c)
        @test cx == length(fs.cusp_class_finite_reps)+1
      else
        @test get(c) == fs.cusp_class_finite_reps[cx]
      end
    end

    @test fs.even == iselement(SL2ZElem(-1,0,0,-1))
    for g in fs.generators
      @test iselement(g)
    end
    for xx in 1:length(fs.pairings)
      @test iselement(pairing_matrix_in_group(fs,xx))
    end
  end


  @testset "Farey symbols for SL2Z" begin
    @testset "Farey fraction list" begin
      ffl = FareyFractionList(FareyFraction(1,3))
      append!(ffl, FareyFraction(1,2))
      ff = FareyFraction(2,3); append!(ffl, ff)
      FareySymbolSL2Z_MODULE.insert!(ffl, ff, FareyFraction(4,7))

      ffltest = []
      for ff in ffl
        push!(ffltest, (ff.num,ff.den))
      end
      @test ffltest == reverse([(1,3), (1,2), (4,7), (2,3)])
    end

    @testset "Unpaired Farey fraction list" begin
      upl = UnpairedList()
      function test_upl(list)
        upltest = []
        for up in upl
          push!(upltest, (up.ff.num,up.ff.den))
        end
        @test upltest == list
      end

      test_upl([])

      FareySymbolSL2Z_MODULE.append!(upl, FareyFraction(0,1))
      FareySymbolSL2Z_MODULE.append!(upl, FareyFraction(1,1))
      FareySymbolSL2Z_MODULE.append!(upl, FareyFraction(1,0))
      FareySymbolSL2Z_MODULE.delete!(upl, first(upl))
      FareySymbolSL2Z_MODULE.delete!(upl, last(upl))
      FareySymbolSL2Z_MODULE.insert!(upl, first(upl), FareyFraction(1,2))

      test_upl([(1,2),(1,1)])
    end

    @testset "SL2Z" begin
      fssl2z = FareySymbolSL2Z(ModularForms.in_sl2z()...)
      @test fssl2z.pairings == [-2,-3]
      @test fssl2z.pairing_matrices == [SL2ZElem(0,-1,1,0), SL2ZElem(0,-1,1,-1)]
      @test fssl2z.pairing_matrices_are_in_group == [true,true]
      @test fssl2z.pairings_with == [1,2]
      @test fssl2z.nu2 == 1
      @test fssl2z.nu3 == 1

      @test fssl2z.xs == [zero(QQ)]

      @test fssl2z.cusp_classes == [1]
      @test fssl2z.cusp_class_finite_reps == []
      @test fssl2z.cusp_class_widths == [1]
      @test fssl2z.cusp_reductions == [SL2ZElem(1,0,0,1)]

      @test fssl2z.even
      @test fssl2z.generators == [SL2ZElem(0,-1,1,0), SL2ZElem(0,-1,1,-1)]
      @test fssl2z.coset_reps == [SL2ZElem(1,0,0,1)]
    end

    @testset "Gamma0(3)" begin
      fsgamma = FareySymbolSL2Z(ModularForms.in_sl2z_gamma0(3)...)
      @test fsgamma.pairings == [1,-3,1]
      @test fsgamma.pairing_matrices == [SL2ZElem(1,1,0,1),SL2ZElem(1,-1,3,-2),SL2ZElem(-1,1,0,-1)]
      @test fsgamma.pairing_matrices_are_in_group == [true,true,true]
      @test collect(fsgamma.pairings_with) == [3,2,1]
      @test fsgamma.nu2 == 0
      @test fsgamma.nu3 == 1

      _test_word_problem(fsgamma, SL2ZElem(1,7,0,1))
      _test_word_problem(fsgamma, SL2ZElem(1,0,3,1))
    end

    @testset "Gamma0(17)" begin
      _test_farey_symbol(ModularForms.in_sl2z_gamma0(17)...)
    end

    @testset "Gamma1(2)" begin
      _test_farey_symbol(ModularForms.in_sl2z_gamma1(3)...)
    end

    @testset "Gamma(8)" begin
      _test_farey_symbol(ModularForms.in_sl2z_gamma(8)...)
    end
  end
end
