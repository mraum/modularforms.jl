function runtests_modular_group_sl2z_hecke()
  @testset "Hecke matrices for SL2Z" begin
    ms = MatrixSpace(ZZ,2,2)
  
    for tx in 1:100
      m = rand(ms,-20:20)
      g = rand(SL2Z, -20:20)
      (gr,mr) = hecke_transformation(m, g)
      @test gr * mr == m * g
    end

    for n in 1:10
      @test length(hecke_matrices(n, SL2Z)) == sum(divisors(n))
      @test length(hecke_matrices(ZZ(n), SL2Z)) == sum(divisors(n))

      @test issubset(hecke_new_matrices(n, SL2Z), hecke_matrices(n, SL2Z))
    end
  end
end
