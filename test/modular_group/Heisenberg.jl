function runtests_modular_group_heisenberg()
  @testset "Heisenberg groups" begin
    for rk in 0:5
      hgp =  siegel_modular_heisenberg_group(rk, 1)
      @test isone(one(hgp))

      for h in gens(hgp)
        @test h != one(hgp)
      end

      for tx in 1:10
        h = rand(hgp, -10:10)
        @test h == gen_word(hgp, word_in_gens(h))
      end
    end
  end
end
