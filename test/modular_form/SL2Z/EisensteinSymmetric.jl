function runtests_modular_form_sl2z_eisenstein_symmetric()
  @testset "Eisenstein series for SL(2,Z) of type symd" begin
    @testset "Vanishing Fourier expansion components" begin
      CC = AcbField(100)
      k = 10
      d = 6
      j = 2
      prc = 20
      eisenstein_symmetric_fourier_expansion = ModularForms.
          ModularFormSL2ZEisensteinSymmetric_MODULE.
          eisenstein_symmetric_fourier_expansion

      fevec = eisenstein_symmetric_fourier_expansion(k, d, j, prc, CC)
      @test all(iszero, fevec[1:j])
    end

    @testset "Numerical modularity check" begin
      CC = AcbField(100)

      @testset "weight $k, sym^$d" for k in [10,24], d in [0,2,10]
        k > 2 + d || continue
        eisenstein_symmetric_evaluate = ModularForms.
            ModularFormSL2ZEisensteinSymmetric_MODULE.
            eisenstein_symmetric_evaluate

        tau = 2//3 + 7//5*onei(CC)
        y = imag(tau)

        for j in 0:d
          esvec  = eisenstein_symmetric_evaluate(k, d, j, tau)

          esTvec = eisenstein_symmetric_evaluate(k, d, j, tau+1)
          # We need to apply the right action of symd(T); This means replacing X by
          # X+1. esTvec is with respect to the basis
          #   (X-(τ+1))^r (X-(\ov{τ}+1))^(d-r).
          # Replacing X by X+1 in this yields
          #   (X+1-(τ+1))^r (X+1-(\ov{τ}+1))^(d-r) = (X-τ)^r (X-\ov{τ})^(d-r).
          # In other words, we need to test for componentwise equality between esvec
          # and esTvec.
          @test Overlapping.(esvec) == esTvec

          esSvec = eisenstein_symmetric_evaluate(k, d, j, -1//tau)
          # We still need to multiply with τ^-k and apply the right action of symd(S).
          # esSvec is with respect to the basis
          #   (X+1/τ)^r (X+1/\ov{τ})^(d-r).
          # Applying the action of symd(S), this yields
          #   (-1+X/τ)^r (-1+X/\ov{τ})^(d-r)
          # = τ^(-r) \ov{τ}^(r-d)  (-τ+X)^r (-\ov{τ}+X)^(d-r).
          # In other words, we multiply the r-th component by τ^(-r-k) \ov{τ}^(r-d) and
          # then test for componentwise equality with esvec.
          for r in 0:d
            esSvec[r+1] *= tau^(-r-k) * conj(tau)^(r-d)
          end
          @test Overlapping.(esvec) == esSvec
        end
      end
    end
  end
end
