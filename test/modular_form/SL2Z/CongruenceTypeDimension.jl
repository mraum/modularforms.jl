function runtests_modular_form_sl2z_congruence_type_dimension()
  mfspace_dim = ModularForms.ModularFormSL2ZCongruenceType_MODULE.mfspace_dim

  @testset "Dimensions of spaces of modular forms" begin
    @testset "Dimensions for subgroups of SL(2,Z)" begin
      # compare with the results of Sage
      @testset "Gamma0($N), weight 4" for (N,d) in [
          (1,1),(2,2),(3,2),(4,3),(5,3),(6,5),(7,3),(8,5),(9,5)
          ]
        @test mfspace_dim(4,induction(TrivialArithmeticType(Gamma0SL2Z(N)))) == d
      end
   
      # compare with the results of Sage
      @testset "Gamma1($N), weight 4" for (N,d) in [
          (1,1),(2,2),(3,2) ,(4,3),(5,5),(6,5),(7,9),(8,9),(9,13)
          ]
        @test mfspace_dim(4,induction(TrivialArithmeticType(Gamma1SL2Z(N)))) == d
      end
   
      # compare with the results of Sage
      @testset "Gamma1($N), weight 2" for (N,d) in [
          (1,0),(2,1),(3,1),(4,2),(5,3),(6,3),(7,5),(8,5),(9,7)
          ]
        @test mfspace_dim(2,induction(TrivialArithmeticType(Gamma1SL2Z(N)))) == d
      end
   
      # compare with the results of Sage
      @testset "weight $k, Gamma1(7)" for (k,d) in [
          (3,7),(4,9),(5,11),(6,13),(7,15),(8,17),(9,19),
          (10,21),(11,23),(12,25),(13,27),(14,29)
          ]
        @test mfspace_dim(k,induction(TrivialArithmeticType(Gamma1SL2Z(7)))) == d
      end
   
      # compare with the results of Sage
      chi = first(chi for chi in DirichletGroup(7) if chi(3) == MuInf(1//6))
      @test mfspace_dim(11,ArithmeticType(chi)) == 8
   
      # fraction weights
      @testset "weight $k, SL(2,Z)" for k in [5//2, 7//3]
        @test mfspace_dim(k, TrivialArithmeticType(SL2Z)) == 0
      end
    end


    @testset "Dimensions for subgroups of Mp(1,Z)" begin
      # same as for SL2Z
      @testset "Gamma0($N), weight 4" for (N,d) in [
          (1,1),(2,2),(3,2),(4,3),(5,3),(6,5),(7,3),(8,5),(9,5)
          ]
        @test mfspace_dim(4,pullback(Mp1Z,induction(TrivialArithmeticType(Gamma0SL2Z(N))))) == d
      end

      # Dimensions of spaces \rmJ_{k,1}(\Gamma) of Jacobi forms of index 1 are
      # equal to \dim \rmM_k(\Gamma) + \dim \rmS_{k+2}(\Gamma). They can also
      # be computed as the dimension of \rmM_{k-\frac{1}{2}}(\rho), where
      # \rho = \Ind_\Gamma^{\SL{2}(\ZZ)} \rho_{(2)}^\vee. Oberse that the Weil
      # representation is the transformation law for theta series by
      # definition. Jacobi forms there for correspond to vector-valued modular
      # forms for the dual Weil representation.
      rho = dual(weil_type(Mp1Z, fill(2,1,1)))
      @test mfspace_dim(19//2, rho) == 2 # == \dim M_10 + \dim S_12 == 1 + 1
      @test mfspace_dim(19//2, restriction(Gamma0Mp1Z(3), rho)) == 7 # == 4 + 3

      # fractional weights
      @testset "weight $k, Mp(1,Z)" for k in [5//4, 7//3]
        @test mfspace_dim(k, TrivialArithmeticType(Mp1Z)) == 0
      end
    end


    # This is the same test cases as in vvmf.  We convert Weil types to matrix
    # types before computing dimensions to actually test the implementation in
    # ModularForms.jl as opposed to the vvmf wrapper. In vvmf, k is interpreted
    # as k/2, which has to be adjusted when transfering tests from there.
    let
      lattice = [2 1; 1 2]
      @testset "Dimensions for Weil types of $lattice" begin
        rho = weil_type(SL2Z, lattice)

        @test mfspace_dim( 5//2, rho) == 0
        @test mfspace_dim( 6//2, rho) == 1
        @test mfspace_dim( 7//2, rho) == 0
        @test mfspace_dim( 8//2, rho) == 1

        @test mfspace_dim( 9//2, rho) == 0
        @test mfspace_dim(10//2, rho) == 1
        @test mfspace_dim(11//2, rho) == 0
        @test mfspace_dim(12//2, rho) == 0

        @test mfspace_dim(13//2, rho) == 0
        @test mfspace_dim(14//2, rho) == 2
        @test mfspace_dim(15//2, rho) == 0
        @test mfspace_dim(16//2, rho) == 1

        @test mfspace_dim(17//2, rho) == 0
        @test mfspace_dim(18//2, rho) == 2
        @test mfspace_dim(19//2, rho) == 0
        @test mfspace_dim(20//2, rho) == 1

        @test mfspace_dim(21//2, rho) == 0
        @test mfspace_dim(22//2, rho) == 2
        @test mfspace_dim(23//2, rho) == 0
        @test mfspace_dim(24//2, rho) == 1

        @test mfspace_dim(25//2, rho) == 0
        @test mfspace_dim(26//2, rho) == 3
        @test mfspace_dim(27//2, rho) == 0
        @test mfspace_dim(28//2, rho) == 1
      end
                                                      
      lattice = [2 1 1; 1 2 1; 1 1 2]
      @testset "Dimensions for Weil types of $lattice" begin
        rho = weil_type(Mp1Z, lattice)

        @test mfspace_dim( 5//2, rho) == 0
        @test mfspace_dim( 6//2, rho) == 0
        @test mfspace_dim( 7//2, rho) == 1
        @test mfspace_dim( 8//2, rho) == 0

        @test mfspace_dim( 9//2, rho) == 1
        @test mfspace_dim(10//2, rho) == 0
        @test mfspace_dim(11//2, rho) == 2
        @test mfspace_dim(12//2, rho) == 0

        @test mfspace_dim(13//2, rho) == 0
        @test mfspace_dim(14//2, rho) == 0
        @test mfspace_dim(15//2, rho) == 2
        @test mfspace_dim(16//2, rho) == 0

        @test mfspace_dim(17//2, rho) == 1
        @test mfspace_dim(18//2, rho) == 0
        @test mfspace_dim(19//2, rho) == 3
        @test mfspace_dim(20//2, rho) == 0

        @test mfspace_dim(21//2, rho) == 1
        @test mfspace_dim(22//2, rho) == 0
        @test mfspace_dim(23//2, rho) == 3
        @test mfspace_dim(24//2, rho) == 0

        @test mfspace_dim(25//2, rho) == 1
        @test mfspace_dim(26//2, rho) == 0
        @test mfspace_dim(27//2, rho) == 4
        @test mfspace_dim(28//2, rho) == 0
      end

      lattice = [2 1 1 1; 1 2 1 1; 1 1 2 1; 1 1 1 2]
      @testset "Dimensions for Weil types of $lattice" begin
        rho = weil_type(Mp1Z, lattice)

        @test mfspace_dim( 5//2, rho) == 0
        @test mfspace_dim( 6//2, rho) == 0
        @test mfspace_dim( 7//2, rho) == 0
        @test mfspace_dim( 8//2, rho) == 1

        @test mfspace_dim( 9//2, rho) == 0
        @test mfspace_dim(10//2, rho) == 1
        @test mfspace_dim(11//2, rho) == 0
        @test mfspace_dim(12//2, rho) == 2

        @test mfspace_dim(13//2, rho) == 0
        @test mfspace_dim(14//2, rho) == 1
        @test mfspace_dim(15//2, rho) == 0
        @test mfspace_dim(16//2, rho) == 2

        @test mfspace_dim(17//2, rho) == 0
        @test mfspace_dim(18//2, rho) == 1
        @test mfspace_dim(19//2, rho) == 0
        @test mfspace_dim(20//2, rho) == 3

        @test mfspace_dim(21//2, rho) == 0
        @test mfspace_dim(22//2, rho) == 2
        @test mfspace_dim(23//2, rho) == 0
        @test mfspace_dim(24//2, rho) == 3

        @test mfspace_dim(25//2, rho) == 0
        @test mfspace_dim(26//2, rho) == 2
        @test mfspace_dim(27//2, rho) == 0
        @test mfspace_dim(28//2, rho) == 4
      end

      lattice = fill(2, 1, 1)
      @testset "Dimensions for Weil types of $lattice" begin
        rho = weil_type(Mp1Z, lattice)

        @test mfspace_dim( 5//2, rho) == 1
        @test mfspace_dim( 6//2, rho) == 0
        @test mfspace_dim( 7//2, rho) == 0
        @test mfspace_dim( 8//2, rho) == 0

        @test mfspace_dim( 9//2, rho) == 1
        @test mfspace_dim(10//2, rho) == 0
        @test mfspace_dim(11//2, rho) == 0
        @test mfspace_dim(12//2, rho) == 0

        @test mfspace_dim(13//2, rho) == 2
        @test mfspace_dim(14//2, rho) == 0
        @test mfspace_dim(15//2, rho) == 0
        @test mfspace_dim(16//2, rho) == 0

        @test mfspace_dim(17//2, rho) == 2
        @test mfspace_dim(18//2, rho) == 0
        @test mfspace_dim(19//2, rho) == 0
        @test mfspace_dim(20//2, rho) == 0

        @test mfspace_dim(21//2, rho) == 2
        @test mfspace_dim(22//2, rho) == 0
        @test mfspace_dim(23//2, rho) == 0
        @test mfspace_dim(24//2, rho) == 0

        @test mfspace_dim(25//2, rho) == 3
        @test mfspace_dim(26//2, rho) == 0
        @test mfspace_dim(27//2, rho) == 0
        @test mfspace_dim(28//2, rho) == 0
      end

      # TODO: too slow with current implementation
      # lattice = fill(14, 1, 1)
      # @testset "Dimensions for Weil types of $lattice" begin
      #   rho = weil_type(Mp1Z, lattice)

      #   @test mfspace_dim( 5, rho) == 2
      #   @test mfspace_dim( 6, rho) == 0
      #   @test mfspace_dim( 7, rho) == 2
      #   @test mfspace_dim( 8, rho) == 0

      #   @test mfspace_dim( 9, rho) == 3
      #   @test mfspace_dim(10, rho) == 0
      #   @test mfspace_dim(11, rho) == 3
      #   @test mfspace_dim(12, rho) == 0

      #   @test mfspace_dim(13, rho) == 5
      #   @test mfspace_dim(14, rho) == 0
      #   @test mfspace_dim(15, rho) == 4
      #   @test mfspace_dim(16, rho) == 0

      #   @test mfspace_dim(17, rho) == 6
      #   @test mfspace_dim(18, rho) == 0
      #   @test mfspace_dim(19, rho) == 5
      #   @test mfspace_dim(20, rho) == 0

      #   @test mfspace_dim(21, rho) == 7
      #   @test mfspace_dim(22, rho) == 0
      #   @test mfspace_dim(23, rho) == 6
      #   @test mfspace_dim(24, rho) == 0

      #   @test mfspace_dim(25, rho) == 9
      #   @test mfspace_dim(26, rho) == 0
      #   @test mfspace_dim(27, rho) == 7
      #   @test mfspace_dim(28, rho) == 0
      # end

      lattice = [0 2; 2 0]
      @testset "Dimensions for Weil types of $lattice" begin
        rho = weil_type(SL2Z, lattice)

        @test mfspace_dim( 5//2, rho) == 0
        @test mfspace_dim( 6//2, rho) == 0
        @test mfspace_dim( 7//2, rho) == 0
        @test mfspace_dim( 8//2, rho) == 3

        @test mfspace_dim( 9//2, rho) == 0
        @test mfspace_dim(10//2, rho) == 0
        @test mfspace_dim(11//2, rho) == 0
        @test mfspace_dim(12//2, rho) == 3

        @test mfspace_dim(13//2, rho) == 0
        @test mfspace_dim(14//2, rho) == 0
        @test mfspace_dim(15//2, rho) == 0
        @test mfspace_dim(16//2, rho) == 4

        @test mfspace_dim(17//2, rho) == 0
        @test mfspace_dim(18//2, rho) == 0
        @test mfspace_dim(19//2, rho) == 0
        @test mfspace_dim(20//2, rho) == 4

        @test mfspace_dim(21//2, rho) == 0
        @test mfspace_dim(22//2, rho) == 0
        @test mfspace_dim(23//2, rho) == 0
        @test mfspace_dim(24//2, rho) == 6

        @test mfspace_dim(25//2, rho) == 0
        @test mfspace_dim(26//2, rho) == 0
        @test mfspace_dim(27//2, rho) == 0
        @test mfspace_dim(28//2, rho) == 5
      end

      # TODO: too slow with current implementation
      # lattice = [0 24; 24 0]
      # @testset "Dimensions for Weil types of $lattice" begin
      #   rho = weil_type(SL2Z, lattice)

      #   @test mfspace_dim( 5, rho) ==   0
      #   @test mfspace_dim( 6, rho) ==  72
      #   @test mfspace_dim( 7, rho) ==   0
      #   @test mfspace_dim( 8, rho) ==  99

      #   @test mfspace_dim( 9, rho) ==   0
      #   @test mfspace_dim(10, rho) == 119
      #   @test mfspace_dim(11, rho) ==   0
      #   @test mfspace_dim(12, rho) == 147

      #   @test mfspace_dim(13, rho) ==   0
      #   @test mfspace_dim(14, rho) == 167
      #   @test mfspace_dim(15, rho) ==   0
      #   @test mfspace_dim(16, rho) == 195

      #   @test mfspace_dim(17, rho) ==   0
      #   @test mfspace_dim(18, rho) == 215
      #   @test mfspace_dim(19, rho) ==   0
      #   @test mfspace_dim(20, rho) == 243

      #   @test mfspace_dim(21, rho) ==   0
      #   @test mfspace_dim(22, rho) == 262
      #   @test mfspace_dim(23, rho) ==   0
      #   @test mfspace_dim(24, rho) == 293

      #   @test mfspace_dim(25, rho) ==   0
      #   @test mfspace_dim(26, rho) == 310
      #   @test mfspace_dim(27, rho) ==   0
      #   @test mfspace_dim(28, rho) == 339
      # end
    end
  end
end
