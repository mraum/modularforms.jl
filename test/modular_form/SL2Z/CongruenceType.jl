function runtests_modular_form_sl2z_congruence_type()
  @testset "Modular forms for congruence types of SL(2,Z) and Mp(1,Z)" begin

    @testset "Jacobi theta series numerical modularity check" begin
      jacobi_theta_weil_type =
          ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
          jacobi_theta_weil_type

      CC = AcbField(100)
      prec = 200

      th, wt, rho = jacobi_theta_weil_type(CC, prec)

      test_numerically_vector_valued_modular_form(
        th, wt, rho, 2*onei(CC) + 1)
    end


    # In lvl 5 and 7 Dirichlet characters take values in a proper extension.
    testlst = [(5,:gamma1,true, 3), # One Eisenstein series and two products.
               (5,:gamma0,true, 6), # Four Eisenstein series and two products.
               (7,:gamma0,false,3)] # Tree products.
    @testset "Factors in products of Eisenstein series for $eisenstein_groups of level $lvl" for (lvl,eisenstein_groups,single_factor_flag,nmbtests) in testlst
      l = SL2RCoverWeight(2)

      CC = AcbField(100)
      coefftower = cyclotomic_tower(CC)
      sring = ModularForms.ModularFormsSL2ZCongruenceSymbolicRing(coefftower)

      # In level 5 we check all Eisenstein series and two proper products. In
      # level 7 we only check two products.
      for (fs, ks, rhos) in Iterators.take(
          ModularForms.ModularFormSL2ZCongruenceType_MODULE.eisenstein_factors(
              l, lvl;
              single_factor_flag,
              eisenstein_groups),
          nmbtests)
        fs = [map(sring,f) for f in fs]
        for (f,k,rho) in zip(fs,ks,rhos)
          test_numerically_vector_valued_modular_form(
              f, k, rho, 2*onei(CC) + 1)
        end
        f = reduce(kronecker_product, fs)
        k = reduce(⊗, ks)
        @test k == l
        rho = reduce(⊗, rhos)
        test_numerically_vector_valued_modular_form(
            f, k, rho, 2*onei(CC) + 1)
      end
    end


    testlst = [(5,:gamma1,true, 6), # Two homomorphisms from Eisenstein series.
               (5,:gamma0,true, 6), # Two homomorphisms from Eisenstein series.
               (7,:gamma0,false,6)]
    @testset "Products of Eisenstein series for $eisenstein_groups of level $lvl" for (lvl,eisenstein_groups,single_factor_flag,nmbtests) in testlst

      CC = AcbField(100)
      coefftower = cyclotomic_tower(CC)
      sring = ModularForms.ModularFormsSL2ZCongruenceSymbolicRing(coefftower)

      k = SL2RCoverWeight(2)
      rho = induction(TrivialArithmeticType(Gamma0(lvl)))
      for f in Iterators.take(
          ModularForms.ModularFormSL2ZCongruenceType_MODULE.
          symbolic_modular_forms_from_eisenstein_products_and_homomorphisms(
              k, rho, sring;
              single_factor_flag,
              eisenstein_groups,
              return_translation_orbit_representatives = false),
          nmbtests)
        test_numerically_vector_valued_modular_form(
            f, k, rho, 2*onei(CC) + 1)
      end
    end


    testlst = [( 2, Gamma0SL2Z(2), 1),
               ( 2, Gamma0SL2Z(4), 2),
               ( 2, Gamma0SL2Z(9), 3),
               ( 3, Gamma0SL2Z(4), 0),
               ( 4, Gamma0SL2Z(9), 4),

               ( 2, Gamma1SL2Z(4), 2),
               ( 3, Gamma1SL2Z(4), 2),
               ( 4, Gamma1SL2Z(4), 3),
               # TODO: this is extremely slow ( 4, Gamma1SL2Z(9), 8),

               ( 2, DirichletGroup(5)[2], 0),
               ( 3, DirichletGroup(5)[2], 2),
               ( 4, DirichletGroup(5)[2], 0)]
    @testset "Bases of spaces of vector-valued Eisenstein series of weight $k for $grpchi" for (k,grpchi,d) in testlst
      if grpchi isa DirichletCharacter
        rho = induction(ArithmeticType(grpchi))
      elseif grpchi isa SL2ZGroup
        rho = induction(TrivialArithmeticType(grpchi))
      else
        error("incorrect test parameter")
      end
      mfs = EisensteinSeriesSpace(k, rho, QQab)

      @test dim(mfs) == d

      @testset "Numerical modularity check for $fx-th basis element" for (fx,f) in enumerate(basis(mfs))
        CC = AcbField(100)
        test_numerically_vector_valued_modular_form(f, 2*onei(CC) + 1)
      end
    end


    testlst = [( 2, Gamma0SL2Z(1)), # Zero space; special case.
               ( 2, Gamma0SL2Z(2)), # Only one Eisenstein series.
               ( 2, Gamma0SL2Z(4)), # Only Eisenstein series; prime power level.
               ( 2, Gamma0SL2Z(1)), # One cuspform.
               ( 6, Gamma0SL2Z(5)), # One cuspform.
               ( 6, Gamma0SL2Z(7)), # Three cuspforms.
               (11, Gamma0SL2Z(4)), # Zero space; weight condition.
               ( 3, DirichletGroup(5)[2])] # Odd weight and character.
    @testset "Bases of spaces of vector-valued modular forms of weight $k for $grpchi" for (k,grpchi) in testlst
      if grpchi isa DirichletCharacter
        rho = induction(ArithmeticType(grpchi))
      elseif grpchi isa SL2ZGroup
        rho = induction(TrivialArithmeticType(grpchi))
      else
        error("incorrect test parameter")
      end
      mfs = ModularFormsSpace(k, rho, QQab)

      @testset "Numerical modularity check for $fx-th basis element" for (fx,f) in enumerate(basis(mfs))
        CC = AcbField(100)
        test_numerically_vector_valued_modular_form(f, 2*onei(CC) + 1)
      end

      @testset "Inflation of symbolic expression and Fourier expansion for $fx-th basis element"  for (fx,f) in enumerate(basis(mfs))
        prc = 5

        @test [fourier_expansion(es, prc; coefficient_ring = base_ring(mfs))
               for es in ModularForms.symbolic_expression(f)] ==
              fourier_expansion(f, prc)
      end
    end


    # For level 11, we need an 11-th root of unity, so since 11 | (23 - 1),
    # we pick q = 23^1.
    testlst = [(2, Gamma0(11), 23, 1),
               (4, Gamma0(11), 23, 1),
               (4, Gamma0(13), 53, 1),
               (3, DirichletGroup(5)[2], 7, 4)]
    @testset "Modular forms over finite fields, weight $k for $grpchi, modulus $p, degree $d" for (k,grpchi,p,d) in testlst
      k = SL2RCoverWeight(k)
      if grpchi isa DirichletCharacter
        rho = induction(ArithmeticType(grpchi))
      elseif grpchi isa SL2ZGroup
        rho = induction(TrivialArithmeticType(grpchi))
      else
        error("incorrect test parameter")
      end

      QQT = cyclotomic_tower(QQ);
      QQn = muinf_ring(QQT,
                 ModularForms.
                 ModularFormSL2ZCongruenceType_MODULE.
                 fourier_coefficient_muinf_order(k,rho));

      FFT = cyclotomic_tower(GF(p));
      FFn = ModularForms.muinf_ring_by_degree(FFT, d);

      mfsQQ =  ModularFormsSpace(k, rho, QQn, QQT)
      # We set caching to false to avoid that the QQab spaces are merely
      # reduced mod p.
      mfsFF = ModularFormsSpace(k, rho, FFn, FFT; cached = false)

      @test dim(mfsQQ) == dim(mfsFF)

      lvl = level(rho)
      feprc = sturm_bound(k, SL2Z, QQ)
      # FIXME: This is an implementation detail and will very soon not be true
      # anymore. Create a submodule of instead and check containment.
      for (fQQ, fFF) in zip(basis(mfsQQ), basis(mfsFF))
        for (feQQ, feFF) in zip(
            fourier_expansion(fQQ, feprc),
            fourier_expansion(fFF, feprc))
          for n in 0:(lvl*feprc - 1)
            @test FFT(FFn, coeff(feQQ, n//lvl)) == coeff(feFF, n//lvl)
          end
        end
      end
    end
  end
end
