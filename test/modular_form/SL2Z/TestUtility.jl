function test_numerically_vector_valued_modular_form(
     fs::F,
     wt::Union{SL2RCoverWeight, Int},
     rho::ModularForms.ArithType{G,I},
     tau::acb
    ) where {
     G <: SL2ZCoverElement, I,
     C <: Union{fmpz, fmpq, cf_elem, acb},
     A <: PuiseuxSeriesElem{C},
     F <: Vector{A}
    }
  isempty(fs) && return
  isa(wt, Int) && (wt = SL2RCoverWeight(wt))
  CC = parent(tau)
  evaluate(f, tau) = let s = scale(f)
    sum(CC(coeff(f,n//s)) * cispi(2*n*tau//s)
        for n in 0:Int(precision(f)*s)-1)
  end
  fseval = [evaluate(f, tau) for f in fs]
  for g in gens(group(rho))
    gfseval = [Overlapping(cocycle(wt, g, tau) * evaluate(f, moebius(g, tau))) for f in fs]
    @test gfseval == act(rho(g), fseval)
  end
end

function test_numerically_vector_valued_modular_form(
     fs::F,
     wt::Union{SL2RCoverWeight, Int},
     rho::ModularForms.ArithType{G,I},
     tau::acb;
     evaluation_cache::Union{Nothing,Dict{G,Dict}} = nothing
    ) where {
     G <: SL2ZCoverElement, I,
     A <: ModularForms.ModularFormsSL2ZCongruenceSymbolicRingElem,
     F <: Vector{A}
    }
  isempty(fs) && return
  isa(wt, Int) && (wt = SL2RCoverWeight(wt))
  CC = parent(tau)
  if isnothing(evaluation_cache)
    fseval = [evaluate(f, tau) for f in fs]
  else
    fseval = [evaluate(f, tau; evaluation_cache = evaluation_cache[one(group(rho))])
              for f in fs]
  end

  for g in gens(group(rho))
    if isnothing(evaluation_cache)
      gfseval = [cocycle(wt, g, tau) * evaluate(f, moebius(g, tau))
                 for f in fs]
    else
    gfseval = [cocycle(wt, g, tau) *
               evaluate(f, moebius(g, tau);
                        evaluation_cache = evaluation_cache[g])
               for f in fs]
    end
    @test map(Overlapping,gfseval) == act(rho(g), fseval)
  end
end

function test_numerically_vector_valued_modular_form(
     f::ModularForms.ModularFormSL2ZCongruenceType{I, C},
     tau::acb
    ) where {
     G <: SL2ZCoverElement, I,
     C <: Union{fmpz, fmpq, cf_elem, acb},
    }
  wt = weight(f)
  rho = arithtype(f)

  feval = evaluate(f, tau)
  for g in gens(group(rho))
    gfeval = Overlapping.(cocycle(wt, g, tau) .* evaluate(f, moebius(g, tau)))
    @test gfeval == act(rho(g), feval)
  end
end
