function runtests_modular_form_sl2z_hecke()
  @testset "Hecke operators for vector-valued modular forms for SL(2,Z)" begin
    @testset "Jacobi theta series numerical check" begin
      jacobi_theta_weil_type =
          ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.jacobi_theta_weil_type

      CC = AcbField(100)
      prec = 200
      nmax = 10

      th, wt, rho = jacobi_theta_weil_type(CC, nmax*prec)
      @testset "Hecke operator T_$n" for n in 1:nmax
        heckerho = hecke(n,rho)
        hecketh = hecke(n, wt, rho, th)

        test_numerically_vector_valued_modular_form(
          hecketh, wt, heckerho, 2*onei(CC) + 1)
      end
    end
  end
end
