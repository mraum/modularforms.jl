function runtests_modular_form_sl2z_eisenstein_congruence()
  @testset "Eisenstein series for congruence subgroups of SL(2,Z)" begin
    ModularFormsSL2ZCongruenceSymbolicRing =
        ModularForms.ModularFormsSL2ZCongruenceSymbolicRing

    @testset "Eisenstein series for SL2Z" begin
      eisenstein_sl2z_type =
          ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
          eisenstein_sl2z_type

      @testset "Numerical modularity check" begin
        CC = AcbField(100)

        @testset "weight $k, coefficients over $coeff_base_ring" for coeff_base_ring in [QQ,CC], k in [4,6]
          coefftower = cyclotomic_tower(coeff_base_ring)
          sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
          f, rho = eisenstein_sl2z_type(k)
          f = map(sring, f)

          test_numerically_vector_valued_modular_form(
            f,
            SL2RCoverWeight(k), rho,
            2*onei(CC) + 1)
        end
      end
    end

    @testset "Eisenstein series for Gamma0" begin
      @testset "Numerical modularity check" begin
        CC = AcbField(100)

        @testset "weight $k, level $lvl, character $chi, coefficients over $coeff_base_ring" for coeff_base_ring in [QQ,CC], k in [1, 2, 6, 7], lvl in [1,2,4,6,11], chi in (let dgp = DirichletGroup(lvl); [one(dgp), rand(dgp)] end)
          eisenstein_gamma0_type =
              ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
              eisenstein_gamma0_type

          coefftower = cyclotomic_tower(coeff_base_ring)
          sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
          f, rho = eisenstein_gamma0_type(k, chi)
          f = map(sring, f)
          for word in gen_relation_words(SL2Z)
            @test isone(gen_word(rho,word))
          end

          test_numerically_vector_valued_modular_form(
            f,
            SL2RCoverWeight(k), rho,
            2*onei(CC) + 1)
        end
      end

      @testset "Fourier expansions over different fields" for lvl in 1:5
        EisensteinSL2ZGamma0 =
            ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
            EisensteinSL2ZGamma0

        coefftower = cyclotomic_tower(QQ)
        k = 4
        prc = 5
        sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
        for chi in DirichletGroup(lvl)
          coeffring = muinf_ring(coefftower, lcm(lvl,order(chi)))
          for c in 1:lvl, d in 1:lvl
            gcd(c,d) == 1 || continue
            f = sring(EisensteinSL2ZGamma0(k,chi, c,d))
            fe = fourier_expansion(f, prc)
            fe_extended = fourier_expansion(f, prc; coefficient_ring = coeffring)
            coeffconvert = map_from_cyclotomic_tower(coefftower,
                               base_ring(parent(fe)), coeffring)
            @test fe_extended == change_base_ring(fe, coeffconvert, parent(fe_extended))
          end
        end
      end

      @testset "action of translation on Fourier expansions" for lvl in 1:5
        EisensteinSL2ZGamma0 =
            ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
            EisensteinSL2ZGamma0

        coefftower = cyclotomic_tower(QQ)
        k = 4
        prc = 5
        sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
        torbit = TranslationOrbit(collect(1:2*lvl), one(MuInf))

        for chi in DirichletGroup(lvl)
          for c in 1:lvl, d in 1:lvl
            gcd(c,d) == 1 || continue
            f = sring(EisensteinSL2ZGamma0(k,chi, c,d))
            fe = fourier_expansion(f, prc)
            fetwisted = Vector{elem_type(parent(fe))}(undef, length(torbit))
            ModularForms.twist_fourier_expansion!(fetwisted, fe, torbit, coefftower)
            for ox in nonrepresentatives(torbit)
              f = ModularForms.translation_action(f)
              @test fetwisted[ox] == fourier_expansion(f, prc)
            end
          end
        end
      end
    end

    @testset "Eisenstein series for Gamma1" begin

      @testset "Numerical modularity check" begin
        CC = AcbField(100)
        coefftower = cyclotomic_tower(CC)

        @testset "weight $k, level $lvl, coefficients over $coeff_base_ring" for coeff_base_ring in [QQ,CC], k in [1, 2, 6, 7], lvl in [1,2,4,6,11]
          eisenstein_gamma1_type =
              ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
              eisenstein_gamma1_type

          sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
          f, rho = eisenstein_gamma1_type(k, lvl)
          f = map(sring, f)
          for word in gen_relation_words(SL2Z)
            @test isone(gen_word(rho,word))
          end

          test_numerically_vector_valued_modular_form(
            f,
            SL2RCoverWeight(k), rho,
            2*onei(CC) + 1)
        end
      end

      @testset "Fourier expansions over different fields" for lvl in 1:4
        EisensteinSL2ZGamma1 =
            ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
            EisensteinSL2ZGamma1

        coefftower = cyclotomic_tower(QQ)
        coeffring = muinf_ring(coefftower, lcm(lvl,7))
        k = 4
        prc = 5
        sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
        o = TranslationOrbit(collect(1:2*lvl), one(MuInf))

        for c in 1:lvl, d in 1:lvl
          gcd(c,d) == 1 || continue
          f = sring(EisensteinSL2ZGamma1(k,lvl, c,d))
          fe = fourier_expansion(f, prc)
          fe_extended = fourier_expansion(f, prc; coefficient_ring = coeffring)
          coeffconvert = map_from_cyclotomic_tower(coefftower,
                             base_ring(parent(fe)), coeffring)
          @test fe_extended == change_base_ring(fe, coeffconvert, parent(fe_extended))
        end
      end

      @testset "action of translation on Fourier expansions" for lvl in 1:4
        EisensteinSL2ZGamma1 =
            ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
            EisensteinSL2ZGamma1

        coefftower = cyclotomic_tower(QQ)
        k = 4
        prc = 5
        sring = ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
        torbit = TranslationOrbit(collect(1:2*lvl), one(MuInf))

        for c in 1:lvl, d in 1:lvl
          gcd(c,d) == 1 || continue
          f = sring(EisensteinSL2ZGamma1(k,lvl, c,d))
          fe = fourier_expansion(f, prc)
          fetwisted = Vector{elem_type(parent(fe))}(undef, length(torbit))
          ModularForms.twist_fourier_expansion!(fetwisted, fe, torbit, coefftower)
          for ox in nonrepresentatives(torbit)
            f = ModularForms.translation_action(f)
            @test fetwisted[ox] == fourier_expansion(f, prc)
          end
        end
      end
    end

    @testset "polynomials in Eisenstein series" begin
      eisenstein_gamma1_type =
          ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
          eisenstein_gamma1_type

# TODO: move this to symbolic
      @testset "change of coefficient ring tower" begin
        QQtower = cyclotomic_tower(QQ)
        FFtower = cyclotomic_tower(GF(107))
        sringQQ = ModularFormsSL2ZCongruenceSymbolicRing(QQtower)
        sringFF = ModularFormsSL2ZCongruenceSymbolicRing(FFtower)

        FF = muinf_ring(FFtower,1)
        coeffconv(c) = FF(numerator(coeff(c,0)))//FF(denominator(coeff(c,0)))

        @test parent(ModularForms.map_coefficients(coeffconv, one(sringQQ);
                         parent = sringFF, coefficient_ring = FF)) ===
              sringFF
      end

      @testset "Compatibility of Fourier expansion and evaluation" begin
        CC = AcbField(100)

        @testset "tensor product of level $lvl Eisenstein series" for lvl in [3,4]
          feprc = 100

          sring = ModularFormsSL2ZCongruenceSymbolicRing(cyclotomic_tower(CC))

          fs = [eisenstein_gamma1_type(k, lvl)[1] for k in [6,7]]
          fs = [map(sring, f) for f in fs]
          f = reduce(ModularForms.kronecker_product, fs)
        
          tau = 2*onei(CC) + 1
          feval = [evaluate(fc, tau) for fc in f]
          ffeeval = [let
                       fe = fourier_expansion(fc, feprc)
                       s = scale(fe)
                       sum(CC(coeff(fe,n//s)) * cispi(2*n*tau//s)
                           for n in 0:Int(precision(fe)*s)-1)
                     end
                     for fc in f]
          @test Overlapping.(feval) == ffeeval
        end
      end
    end

    @testset "Eisenstein series for Weil type of hyperbolic plane" begin
      eisenstein_weil_hyperbolic_plane_type =
          ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE.
          eisenstein_weil_hyperbolic_plane_type

      @testset "Numerical modularity check" begin
        CC = AcbField(100)

        @testset "weight $k, level $lvl, coefficient ring $coeffring" for coeffring in [QQ, CC], k in [6, 7], lvl in [1,2,4,6]
          feprc = 100

          f, rho = eisenstein_weil_hyperbolic_plane_type(k, lvl, feprc, coeffring)

          test_numerically_vector_valued_modular_form(
            f,
            SL2RCoverWeight(k), rho,
            2*onei(CC) + 1)
        end
      end
    end
  end
end
