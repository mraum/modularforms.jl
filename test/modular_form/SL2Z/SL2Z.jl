function runtests_modular_form_sl2z()
  @testset "Elliptic modular forms for SL(2,Z)" begin
    @testset "copying" begin
      deepcopy(ramanujan_delta()) == ramanujan_delta()
    end

    @testset "spaces of nonpositive weight" begin
      @test dim(ModularFormsSpace(0, SL2Z)) == 1
      @test dim(ModularFormsSpace(-2, SL2Z)) == 0
      @test dim(ModularFormsSpace(SL2RCoverWeight(1//2), SL2Z)) == 0
    end

    @testset "relations in low weight" begin
      @test !iszero(eisenstein_series(12))
      @test eisenstein_series(14) == eisenstein_series(4)^2 * eisenstein_series(6)
      @test eisenstein_series(12) != eisenstein_series(4)^3
      @test eisenstein_series(12) != eisenstein_series(4)^3
      @test eisenstein_series(4)^3 - eisenstein_series(6)^2 == 1728*ramanujan_delta()
    end

    @testset "arithmetic" begin
      mfs = ModularFormsSpace(12, SL2Z, QQab)
      (f,g) = basis(mfs)
      fe(f) = fourier_expansion(f,10)
      @test fe(2*f) == 2 .* fe(f)
      @test (fe(QQab(2) * f + root_of_unity(QQab,7) * g) ==
             QQab(2) .* fe(f) .+ root_of_unity(QQab,7) .* fe(g))
      @test fe(f-g) == fe(f) .- fe(g)
    end

    @testset "matrices of Fourier expansions" begin
      prec = 10
      for k in 24:36
        mfs = ModularFormsSpace(k, SL2Z)
        fes = [fourier_expansion(f, prec) for f in basis(mfs)]

        femat = ModularForms._fourier_expansion_matrix(mfs, prec)
        for i in 1:dim(mfs), j in 1:prec
          @test femat[i,j] == coeff(fes[i],j-1)
        end

        femat = ModularForms._fourier_expansion_matrix(mfs, prec; transposed = true)
        for j in 1:dim(mfs), i in 1:prec
          @test femat[i,j] == coeff(fes[j],i-1)
        end
      end
    end

    @testset "comparison with precomputed Fourier expansions" begin
      mfs = ModularFormsSpace(12, SL2Z, QQ)

      fe0 = fourier_expansion(basis(mfs)[1], 10)
      fe1 = fourier_expansion(basis(mfs)[2], 10)
      q = gen(parent(fe0))
      @test (fe0 - coeff(fe0,1)*fe1 ==
             1 + 196560*q^2 + 16773120*q^3 + 398034000*q^4 +
             4629381120*q^5 + 34417656000*q^6 + 187489935360*q^7 +
             814879774800*q^8 + 2975551488000*q^9 + O(q^10)
            )

      (_,q) = PowerSeriesRing(QQ,5,"q")
      @test ramanujan_delta() == mfs(q - 24*q^2 + 252*q^3 + O(q^4))

      # FIXME: this depends on the ring of modular forms mod 2 and 3
      (_,q) = PowerSeriesRing(GF(2),10,"q")
      @test_skip (fourier_expansion(ramanujan_delta(GF(2)), 10) ==
             q + q^9 + O(q^10))

      (_,q) = PowerSeriesRing(GF(3),10,"q")
      @test_skip (fourier_expansion(ramanujan_delta(GF(3)), 10) ==
             q + q^4 + 2*q^7 + O(q^10))
    end

    @test isone( arithtype(ModularFormsSpace(6, SL2Z)) )

    @testset "numerical modularity check" begin
      mfs = ModularFormsSpace(6, SL2Z)
      f = basis(mfs)[1]

      CC = AcbField(50)
      eps = CC("0 +/- 0.001") + onei(CC)*CC("0 +/- 0.001")
      prec = 50

      tau = onei(CC)
      @test overlaps(eps, evaluate(f,tau))

      S = SL2Z(0,-1,1,0)
      tau = 2*onei(CC) + 1
      @test overlaps(eps,
                evaluate(f,tau) - cocycle(weight(f),S,tau) * evaluate(f,moebius(S,tau)))
    end

  end
end
