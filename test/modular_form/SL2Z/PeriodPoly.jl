function runtests_modular_form_sl2z_period_poly()
  @testset "Period polynomials SL2Z" begin
    @testset "dimensions" begin
      @test dim(PeriodPolynomials(0)) == 0
      for d in 2:2:10
        @test dim(PeriodPolynomials(d)) == 2*dim(ModularFormsSpace(d+2,SL2Z))-1
      end
    end

    @testset "Eichler relations" begin
      _act = ModularForms._act_symmetric_power_polynomial
      SL2zElem = ModularForms.SL2zElem
      for phi in basis(PeriodPolynomials(22))
        cs = ModularForms.coefficients(phi)
        s,r = SL2zElem(0,-1,1,0), SL2zElem(1,-1,1,0)
        @test all(iszero, cs .+ _act(cs,SL2zElem(0,-1,1,0)))
        @test all(iszero, cs .+ _act(cs,r) .+ _act(cs,r^2))
      end
    end

    @testset "symmetric power action" begin
      deg = 12
      cs = [rand(QQ,-10:10) for n in 0:deg]
      a,b,c,d = 2,3,7,11
      ga = ModularForms.SL2zElem(a,b,c,d)
      csga = ModularForms._act_symmetric_power_polynomial(cs,ga)
      R,x = PolynomialRing(QQ,:x)
      @test R(csga) == sum(coeff*(a*x+b)^j*(c*x+d)^(deg-j)
                           for (j,coeff) in zip(0:deg,cs))
    end

    @testset "cocycle reltation" begin
      _act = ModularForms._act_symmetric_power_polynomial
      _value = ModularForms.right_cocycle_value
      SL2zElem = ModularForms.SL2zElem
      ga = SL2zElem(2,3,7,11)
      delta = SL2zElem(-5,-4,19,15)
      for phi in basis(PeriodPolynomials(16))
        @test all(iszero, _value(phi, SL2zElem(-1,0,0,-1)))
        @test coefficients(phi) == _value(phi,SL2zElem(0,-1,1,0))
        @test _value(phi,ga*delta) == _act(_value(phi,ga), delta) .+ _value(phi,delta)
      end
    end
  end
end
