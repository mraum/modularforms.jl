function runtests_modular_form_sl2z_eisenstein_sndorder()
  @testset "Generalized second order Eisenstein series for SL(2,Z)" begin
    @testset "Numerical modularity check" begin
      CC = AcbField(100)

      @testset "weight 150, sym^10" begin
        eisenstein_sndorder_evaluate = ModularForms.
            ModularFormSL2ZEisensteinSndOrder_MODULE.
            eisenstein_sndorder_evaluate
        eisenstein_sym_evaluate_Xτ = ModularForms.
            ModularFormSL2ZEisensteinSymmetric_MODULE.
            eisenstein_symmetric_evaluate_Xτ

        k = 150
        d = 10
        phiS = basis(PeriodPolynomials(d))[2]

        tau = 2//3 + 7//5*onei(CC)
        y = imag(tau)

        for j in 0:d
          es  = eisenstein_sndorder_evaluate(k, phiS, j, tau)

          esT = eisenstein_sndorder_evaluate(k, phiS, j, tau+1)
          @test Overlapping(es) == esT

          esS = eisenstein_sndorder_evaluate(k, phiS, j, -1//tau)
          # We have E |_k S - E + ⟨\phi(S),E_k(τ;D,j)⟩ = 0
          #
          # where
          #
          # ⟨p,q⟩ = ∑_{i=0}^D (-1)^{D-i} \binomial{D}{i}^{-1} p_i q_{D-i}
          #
          esS *= tau^-k #esS ← E|_k S
          PR, X = PolynomialRing(CC, "X")
          es_symd_poly = sum((X-tau)^(rx-1)*z for (rx,z) in enumerate(eisenstein_sym_evaluate_Xτ(k, d, j, tau)))
          phi_contribution = ModularForms.pairing(phiS, es_symd_poly)
          esS += phi_contribution #esS ← E|_k S + ⟨\phi(S), E_k(τ;D,j)⟩
          @test Overlapping(es) == esS
        end
      end
    end
  end
end
