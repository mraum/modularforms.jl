using Distributed

@everywhere begin

using Nemo

using ModularForms
using Test


include("Using.jl")
include("TestUtility.jl")
include("arithmetic_type/TestUtility.jl")
include("modular_form/SL2Z/TestUtility.jl")


## adjustment of external libraries
include("utility/Base.jl")
include("utility/Nemo.jl")

## basic functionality
include("utility/VectorTrie.jl")
include("utility/LinearAlgebra.jl")
include("utility/SubmoduleAcc.jl")
include("utility/SubmoduleAccWithGenMap.jl")
include("utility/Orbit.jl")
include("utility/ContinuedFraction.jl")
include("utility/Hurwitz.jl")

## cyclotomic aritmetic
include("utility/RootOfUnity.jl")
include("utility/QQab.jl")
include("utility/QQMuInfTower.jl")
include("utility/CCMuInfTower.jl")
include("utility/GFpMuInfTower.jl")

## other base objects
include("utility/Dirichlet.jl")

include("modular_group/SL2Z/Cusps.jl")
include("modular_group/SL2Z/FareySymbol.jl")
include("modular_group/SL2Z/PermStab.jl")
include("modular_group/SL2Z/ParabolicCoset.jl")
include("modular_group/SL2Z/Subgroup.jl")
include("modular_group/SL2Z/SubgroupCover.jl")
include("modular_group/SL2Z/Hecke.jl")
include("modular_group/Heisenberg.jl")
# TODO: include("modular_group/Jacobi.jl")

include("arithmetic_type/image/TrivialGroup.jl")
include("arithmetic_type/image/MatrixGroup.jl")
include("arithmetic_type/image/WeilTransformation.jl")
include("arithmetic_type/image/TwistedPerm.jl")

include("arithmetic_type/Trivial.jl")
include("arithmetic_type/Generic.jl")
include("arithmetic_type/Dirichlet.jl")
include("arithmetic_type/Weil.jl")
include("arithmetic_type/Restriction.jl")
include("arithmetic_type/Pullback.jl")
include("arithmetic_type/Pushforward.jl")
include("arithmetic_type/Induction.jl")
include("arithmetic_type/Tensor.jl")
include("arithmetic_type/SL2Z.jl")
include("arithmetic_type/SL2ZCover.jl")
include("arithmetic_type/Hecke.jl")
include("arithmetic_type/TranslationDecomposition.jl")

include("arithmetic_type/homomorphism/Orbit.jl")
include("arithmetic_type/homomorphism/Isotrivial.jl")
include("arithmetic_type/homomorphism/TwistTrivial.jl")
include("arithmetic_type/homomorphism/TwistRootOfUnity.jl")
include("arithmetic_type/homomorphism/TwistMatrix.jl")
include("arithmetic_type/homomorphism/ProperSubgroups.jl")

include("modular_form/SL2Z/SL2Z.jl")
include("modular_form/SL2Z/CongruenceTypeDimension.jl")
include("modular_form/SL2Z/EisensteinCongruence.jl")
include("modular_form/SL2Z/CongruenceType.jl")
include("modular_form/SL2Z/Hecke.jl")
include("modular_form/SL2Z/PeriodPoly.jl")
include("modular_form/SL2Z/EisensteinSymmetric.jl")
include("modular_form/SL2Z/EisensteinSndOrder.jl")


tests_utility = [
  runtests_utility_base,
  runtests_utility_nemo,

  runtests_utility_vector_trie,
  runtests_utility_linear_algebra,
  runtests_utility_submodule_accumulating,
  runtests_utility_submodule_accumulating_with_generator_map,
  runtests_utility_orbit,
  runtests_utility_continued_fraction,
  runtests_utility_hurwitz,

  runtests_utility_root_of_unity,
  runtests_utility_qqab,

  runtests_utility_cc_muinf_tower,
  runtests_utility_qq_muinf_tower,
  runtests_utility_gfp_muinf_tower,

  runtests_utility_dirichlet,
  ]

tests_modular_group = [
  runtests_modular_group_psl2z_cusp,
  runtests_modular_group_sl2z_farey_symbol,
  runtests_modular_group_sl2z_permutation_stabilizer,
  runtests_modular_group_sl2z_parabolic_coset,
  runtests_modular_group_sl2z_subgroup,
  runtests_modular_group_sl2z_subgroup_cover,
  runtests_modular_group_sl2z_hecke,
  runtests_modular_group_heisenberg,
  ]

tests_arithmetic_type_image = [
  runtests_arithmetic_type_image_trivial_group,
  runtests_arithmetic_type_image_matrix_group,
  runtests_arithmetic_type_image_weil_transformation,
  runtests_arithmetic_type_image_twisted_permutation,
  ]

tests_arithmetic_type = [
  runtests_arithmetic_type_trivial,
  runtests_arithmetic_type_generic,
  runtests_arithmetic_type_dirichlet,
  runtests_arithmetic_type_weil,
  runtests_arithmetic_type_restriction,
  runtests_arithmetic_type_pullback,
  runtests_arithmetic_type_pushforward,
  runtests_arithmetic_type_induction,
  runtests_arithmetic_type_tensor,
  runtests_arithmetic_type_sl2z,
  runtests_arithmetic_type_sl2z_cover,
  runtests_arithmetic_type_hecke,
  runtests_arithmetic_type_translation_decomposition,
  ]

tests_arithmetic_type_homomorphism = [
  runtests_arithmetic_type_homomorphism_orbit,
  runtests_arithmetic_type_homomorphism_isotrivial,
  runtests_arithmetic_type_homomorphism_twist_trivial,
  runtests_arithmetic_type_homomorphism_twist_root_of_unity,
  runtests_arithmetic_type_homomorphism_twist_matrix,
  ]

tests_modular_form = [
  runtests_modular_form_sl2z,
  runtests_modular_form_sl2z_congruence_type_dimension,
  runtests_modular_form_sl2z_eisenstein_congruence,
  runtests_modular_form_sl2z_congruence_type,
  runtests_modular_form_sl2z_hecke,
  runtests_modular_form_sl2z_period_poly,
  runtests_modular_form_sl2z_eisenstein_symmetric,
  runtests_modular_form_sl2z_eisenstein_sndorder,
  ]

tests_all = [
  tests_utility,
  tests_modular_group,
  tests_arithmetic_type_image,
  tests_arithmetic_type,
  tests_arithmetic_type_homomorphism,
  tests_modular_form,
  ]

end # @everywhere


function runtests_utility()
  @sync @distributed for runtests in tests_utility
    runtests()
  end
end

function runtests_modular_group()
  @sync @distributed for runtests in tests_modular_group
    runtests()
  end
end

function runtests_arithmetic_type_image()
  @sync @distributed for runtests in tests_arithmetic_type_image
    runtests()
  end
end

function runtests_arithmetic_type()
  @sync @distributed for runtests in tests_arithmetic_type
    runtests()
  end
end

function runtests_arithmetic_type_homomorphism()
  @sync @distributed for runtests in tests_arithmetic_type_homomorphism
    runtests()
  end
end

function runtests_modular_form()
  @sync @distributed for runtests in tests_modular_form
    runtests()
  end
end

function runtests_all()
  tests_flattened = tests_all |> Iterators.flatten |> collect
  @sync @distributed for runtests in tests_flattened
    runtests()
  end
end

function runtests_all_time()
  tests_flattened = tests_all |> Iterators.flatten |> collect

  for runtests in tests_flattened
    twt = @timed @eval $runtests()
    two = @timed @eval $runtests()

    println()
    println(twt.value.description)
    println("  (", runtests, "):")

    print("runtime           : ")
    print(round(twt.time, digits=4))
    println(" seconds")

    print("gc time           : ")
    print(round(twt.gctime, digits=4))
    print(" seconds (")
    print(round(100*(twt.gctime/twt.time), digits=2))
    println("%)")

    print("total allocations : ")
    println(Base.format_bytes(two.bytes))

    print("compilation       : ")
    print(round(twt.time - two.time, digits=4))
    print(" seconds (")
    print(round(100*(1 - two.time/twt.time), digits=2))
    println("%)")
    println()
  end
end
