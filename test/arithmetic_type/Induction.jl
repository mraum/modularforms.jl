function runtests_arithmetic_type_induction()
  @testset "Inductions of arithmetic types" begin
    @testset "induction of the trivial type for Γ_0($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(Gamma0SL2Z(N)))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rho = right_induction(TrivialArithmeticType(Gamma0SL2Z(N)))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))
    end

    @testset "induction of a Dirichlet character for Γ_0($N)" for N in 1:10
      rho = induction(ArithmeticType(DirichletGroup(N)[1]))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rho = right_induction(ArithmeticType(DirichletGroup(N)[1]))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))
    end
    
    @testset "induction of the standard type for Γ_0($N)" for N in 1:10
      rho = induction(standard_type(Gamma0SL2Z(N)))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rho = right_induction(standard_type(Gamma0SL2Z(N)))
      test_subgroup_words(rho, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))

      rhod = dual(rho)
      @test rhod == dual(GenericType(rho))
      @test rhod isa ModularForms.IndArithType
      test_subgroup_words(rhod, Gamma0SL2Z(N+1), Gamma1SL2Z(N+1))
    end
  end
end
