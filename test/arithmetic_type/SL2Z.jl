function runtests_arithmetic_type_sl2z()
  @testset "standard arithmetic type" begin
    rho = standard_type(SL2Z)
    for word in gen_relation_words(SL2Z)
      @test isone(gen_word(rho,word))
    end
  end
end

