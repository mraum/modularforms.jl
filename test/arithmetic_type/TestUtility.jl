function test_subgroup_words(
    rho        :: ModularForms.ArithType{G,I},
    word_group :: AbstractAlgebra.Group,
    gen_group  :: AbstractAlgebra.Group
   ) where {G,I}
  gs = gens(word_group)
  for g in gens(gen_group)
    @test rho(g) == prod((rho(gs[hx]^e)
                          for (hx,e) in word_in_gens(word_group(g))))
  end
end
