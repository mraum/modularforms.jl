function runtests_arithmetic_type_generic()
  @testset "Generic arithmetic types" begin
    @testset "Evaluation and equality" begin
      chi = DirichletGroup(7)[1]

      rho = GenericType(ArithmeticType(chi))
      @test rho == rho
      for g in gens(Gamma1SL2Z(modulus(chi)))
        @test isone(rho(g))
      end

      rho = GenericType(induction(rho))
      @test rho == rho
      test_subgroup_words(rho, Gamma0SL2Z(5), Gamma1SL2Z(5))
    end

    @testset "Tensor products" begin
      rho1 = GenericType(rand_char_perm_type(SL2Z, 10:20))
      rho2 = GenericType(rand_char_perm_type(SL2Z, 10:20))
      rho3 = GenericType(rand_char_perm_type(SL2Z, 10:20))

      @test rho1 ⊗ (rho2 ⊗ rho3) == (rho1 ⊗ rho2) ⊗ rho3
    end

    @testset "Component stabilizers and their restrictions" begin
      chi = DirichletGroup(7)[1]
      sigma = ArithmeticType(chi)
      rho = GenericType(induction(sigma))

      @test component_stabilizer(rho, 1) == Gamma0SL2Z(7)
      @test component_stabilizer(rho ⊗ rho, [1,1]) == Gamma0SL2Z(7)

      @test component_stabilizer(restriction(Gamma0SL2Z(7), rho), 1) ==
            Gamma0SL2Z(7)

      @test generator_images(component_stabilizer_restriction(rho, 1)) ==
            generator_images(sigma)
      @test generator_images(GenericType(
                component_stabilizer_restriction(rho ⊗ rho, [1,1]))) ==
            generator_images(GenericType(sigma ⊗ sigma))
    end
  end

  @testset "Level of generic arithmetic types for SL2Z" begin
    @testset "Permutation type induced from Gamma0($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(Gamma0SL2Z(N)))
      rho = ArithmeticType(SL2Z, image_parent(rho), [rho(g) for g in gens(SL2Z)])
      @test level(rho) == N
    end

    @testset "Permutation type induced from Gamma1($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(Gamma1SL2Z(N)))
      rho = ArithmeticType(SL2Z, image_parent(rho), [rho(g) for g in gens(SL2Z)])
      @test level(rho) == N
    end

    @testset "Permutation type induced from Gamma($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(GammaSL2Z(N)))
      rho = ArithmeticType(SL2Z, image_parent(rho), [rho(g) for g in gens(SL2Z)])
      @test level(rho) == N
    end

    @testset "Weil types" begin
      rho = weil_type(SL2Z, [2 1; 1 2])
      @test level(rho) == 3
    end

    @testset "copy level" begin
      G = subgroup(SL2Z, g -> divisible(g.b,7))
      rho = induction(TrivialArithmeticType(G))
      @test ismissing(level(rho)) || level(rho) == 7
    end
  end

  @testset "Level of generic arithmetic types for Mp1Z" begin
    @testset "Permutation type induced from Gamma0($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(Gamma0Mp1Z(N)))
      rho = ArithmeticType(Mp1Z, image_parent(rho), [rho(g) for g in gens(Mp1Z)])
      @test level(rho) == N
    end

    @testset "Permutation type induced from Gamma1($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(Gamma1Mp1Z(N)))
      rho = ArithmeticType(Mp1Z, image_parent(rho), [rho(g) for g in gens(Mp1Z)])
      @test level(rho) == N
    end

    @testset "Permutation type induced from Gamma($N)" for N in 1:10
      rho = induction(TrivialArithmeticType(GammaMp1Z(N)))
      rho = ArithmeticType(Mp1Z, image_parent(rho), [rho(g) for g in gens(Mp1Z)])
      @test level(rho) == N
    end

    @testset "Weil types" begin
      rho = weil_type(Mp1Z,2)
      @test level(rho) == 4
    end
  end
end
