function runtests_arithmetic_type_hecke()
  @testset "Hecke action on arithmetic types" begin
    @testset "kernel of Hecke image" begin
      for n in 1:10
        rho = hecke(n, TrivialArithmeticType(SL2Z))
        for word in gen_relation_words(SL2Z)
          @test isone(gen_word(rho,word))
        end
  
        rho = hecke(n, rand_char_perm_type(SL2Z, 1:20))
        for word in gen_relation_words(SL2Z)
          @test isone(gen_word(rho,word))
        end

        rho = hecke(n, weil_type(Mp1Z, fill(2,1,1)))
        for word in gen_relation_words(Mp1Z)
          @test isone(gen_word(rho,word))
        end

        rho = hecke_new(n, TrivialArithmeticType(SL2Z))
        for word in gen_relation_words(SL2Z)
          @test isone(gen_word(rho,word))
        end
  
        rho = hecke_new(n, rand_char_perm_type(SL2Z, 1:20))
        for word in gen_relation_words(SL2Z)
          @test isone(gen_word(rho,word))
        end

        rho = hecke_new(n, weil_type(Mp1Z, fill(2,1,1)))
        for word in gen_relation_words(Mp1Z)
          @test isone(gen_word(rho,word))
        end
      end
    end

    @testset "Pullbacks associated with Hecke operators" begin
      # FIXME: implement a separate type of SL2Z, so that elem_type
      # works as required by AbstractAlgebra.
      # rho = TrivialArithmeticType(Gamma0SL2Z(5))
      # @test level(hecke_new_pullback(7, rho)) == 35

      rho = TrivialArithmeticType(Gamma0Mp1Z(5))
      @test level(hecke_new_pullback(7, rho)) == 35
    end
  end
end
