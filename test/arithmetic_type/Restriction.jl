function runtests_arithmetic_type_restriction()
  @testset "Restrictions of arithmetic types" begin
    sigma = TrivialArithmeticType(SL2Z)
    rho = restriction(Gamma0SL2Z(3), sigma)
    @test group(rho) == Gamma0SL2Z(3)
    @test image_parent(rho) == image_parent(sigma)
    @test rho isa ModularForms.ArithType{elem_type(group(rho)),
                                         elem_type(image_parent(sigma))}

    sigma = ArithmeticType(DirichletGroup(5)[1])
    rho = restriction(Gamma0SL2Z(15), sigma)
    for g in gens(group(rho))
      @test sigma(g) == rho(g)
    end
  end
end
