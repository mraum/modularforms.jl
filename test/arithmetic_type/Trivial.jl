function runtests_arithmetic_type_trivial()
  @testset "Trivial arithmetic type" begin
    rho = TrivialArithmeticType(SL2Z, MuInf)

    @test rho == rho
    @test isone(rho)

    @test rho ⊗ (rho ⊗ rho) == (rho ⊗ rho) ⊗ rho

    for tx in 1:10
      g = rand(SL2Z, -10:10)
      @test isone(rho(g))
    end
  end
end
