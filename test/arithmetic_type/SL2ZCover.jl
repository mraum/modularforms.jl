function runtests_arithmetic_type_sl2z_cover()
  @testset "Arithmetic types for covers of SL2Z" begin
    rhosl2z = weil_type(SL2Z, [2 1; 1 2])
    rho = pullback(Mp1Z, rhosl2z)
    for tx in 1:10
      g = rand(Mp1Z, -10:10)
      @test rho(g) == rhosl2z(base_group_element(g))
    end

    rhosl2zres = restriction(Gamma0SL2Z(3), rhosl2z)
    rhores = restriction(Gamma0Mp1Z(3), rho)
    for tx in 1:10
      g = rand(Gamma0Mp1Z(3), -10:10)
      @test rhores(g) == rhosl2zres(base_group_element(g))
    end

    rhosl2zind = induction(SL2Z, rhosl2zres)
    rhoind = induction(Mp1Z, rhores)
    for tx in 1:10
      g = rand(Mp1Z, -10:10)
      @test rhoind(g) == rhosl2zind(base_group_element(g))
    end
  end
end
