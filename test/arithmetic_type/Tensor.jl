function runtests_arithmetic_type_tensor()
  @testset "Tensor products of arithmetic types" begin
    # TODO: add matrix types onces they are implemented in tensor types

    @testset "Tensor products of vectors" begin
      rho1 = rand_char_perm_type(SL2Z, 1:20)
      rho2 = rand_char_perm_type(SL2Z, 1:20)
      rho3 = rand_char_perm_type(SL2Z, 1:20)
      sigma = rho1 ⊗ rho2 ⊗ rho3
  
      for tx in 1:10
        v1 = [rand(QQab, -20:20) for _ in 1:dim(rho1)]
        v2 = [rand(QQab, -20:20) for _ in 1:dim(rho2)]
        v3 = [rand(QQab, -20:20) for _ in 1:dim(rho3)]
        g = rand(SL2Z, -20:20)
        @test sigma(act(rho1(g), v1), act(rho2(g), v2), act(rho3(g), v3)) ==
              act(sigma(g), sigma(v1,v2,v3))
      end
    end

    @testset "Collapsing of tensor products" begin
      rho1 = rand_char_perm_type(SL2Z, 1:20)
      rho2 = rand_char_perm_type(SL2Z, 1:20)
      rho3 = rand_char_perm_type(SL2Z, 1:20)
      sigma = GenericType(rho1 ⊗ rho2 ⊗ rho3)
      @test GenericType(GenericType(rho1 ⊗ rho2) ⊗ rho3) == sigma
      @test GenericType(rho1 ⊗ GenericType(rho2 ⊗ rho3)) == sigma
    end
  end
end

