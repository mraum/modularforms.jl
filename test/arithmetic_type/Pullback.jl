function runtests_arithmetic_type_pullback()
  @testset "Pullback of arithmetic types" begin
    @testset "Pullback along general maps" begin
      G = Gamma0SL2Z(7)
      rho = induction(TrivialArithmeticType(G))
      phi = map_from_func(g -> SL2Z(g.ga.a, 7*g.ga.b,
                                    divexact(g.ga.c,7), g.ga.d),
                          G, SL2Z)
      phirho = pullback(phi, rho)
      test_subgroup_words(phirho, Gamma1SL2Z(7), GammaSL2Z(7))
    end

    @testset "Pullback along inner conjugation" begin
      G = Gamma0SL2Z(7)
      rho = induction(TrivialArithmeticType(G))
      phirho = left_pullback(SL2Z(gens(G)[1]), rho)
      test_subgroup_words(phirho, Gamma1SL2Z(7), GammaSL2Z(7))
    end

    @testset "Pullback along outer conjugation" begin
      G = subgroup(SL2Z, g -> divisible(g.b,7))
      H = Gamma0SL2Z(7)
      rho = induction(TrivialArithmeticType(G))
      phirho = left_pullback(SL2Z(0,-1,1,0), rho)
      test_subgroup_words(phirho, Gamma1SL2Z(7), GammaSL2Z(7))
    end
  end
end
