function runtests_arithmetic_type_weil()
  @testset "Weil representation" begin
    rho = weil_type(Mp1Z, fill(2, 1,1))
    rho = ModularForms.GenericType(rho, MatrixGroup(QQab, dim(rho)))
    for word in gen_relation_words(Mp1Z)
      @test isone(gen_word(rho,word))
    end

    rho = weil_type(SL2Z, [0 1; 1 0])
    rho = ModularForms.GenericType(rho, MatrixGroup(QQab, dim(rho)))
    (s,t) = gens(SL2Z)
    @test isone(rho(s))
    @test isone(rho(t))

    rho = weil_type(SL2Z, [2 1; 1 2])
    rho = ModularForms.GenericType(rho, MatrixGroup(QQab, dim(rho)))
    for word in gen_relation_words(SL2Z)
      @test isone(gen_word(rho,word))
    end

    rho = weil_type(SL2Z, [-2 1; 1 2])
    rho = ModularForms.GenericType(rho, MatrixGroup(QQab, dim(rho)))
    for word in gen_relation_words(SL2Z)
      @test isone(gen_word(rho,word))
    end

    rho = weil_type(Mp1Z, [2 0 1; 0 2 1; 1 1 2])
    rho = ModularForms.GenericType(rho, MatrixGroup(QQab, dim(rho)))
    for word in gen_relation_words(Mp1Z)
      @test isone(gen_word(rho,word))
    end
  end
end
