function runtests_arithmetic_type_translation_decomposition()
  @testset "translation orbits for twisted permutations" begin
    for N in 1:10, gp in [Gamma0SL2Z(N), Gamma1SL2Z(N)]
      rho = induction(TrivialArithmeticType(gp))
      cosets = left_coset_representatives(gp)
      cusps = cusp_representatives(gp)
      tdecomp = translation_decomposition(rho(SL2Z(1,1,0,1)))
      torbit_cusps = [cusp_representative(gp, cosets[cx]*PSL2ZCusp())
                      for cx in representatives(tdecomp)]
      @test Set(cusps) == Set(torbit_cusps)
    end
  end
end
