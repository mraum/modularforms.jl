function runtests_arithmetic_type_homomorphism_twist_root_of_unity()
  @testset "Morphisms of arithmetic permutation types twisted by roots of unity" begin
    rhotriv = TrivialArithmeticType(SL2Z)
    rhoperm = left_induction(TrivialArithmeticType(Gamma0SL2Z(7)))
    rholind = left_induction(ArithmeticType(DirichletGroup(7)[1]))
    rhorind = right_induction(ArithmeticType(DirichletGroup(7)[1]))
    for (rho, sigma) in [(rhotriv, rhoperm),
                         (rhotriv, rholind), (rhotriv, rhorind),
                         (rholind, rhotriv), (rhorind, rhotriv),
                         (rholind, rhorind), (rhorind, rholind)
                        ]
      for hom in basis(homspace(rho,sigma))
        ishomzero = true
        for ix in 1:dim(rho)
          v = ModularForms.unitvec(QQab, dim(rho), ix)
          ishomzero &= all(iszero, hom(v))
          for g in gens(group(rho))
            @test hom(act(rho(g), v)) == act(sigma(g), hom(v))
          end
        end
        @test !ishomzero
      end
    end
  end
end

