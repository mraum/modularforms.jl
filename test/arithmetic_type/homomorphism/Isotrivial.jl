function runtests_arithmetic_type_homomorphism_isotrivial()
  @testset "Isotrivial arithmetic subtypes" begin
    @testset "Orbit chains in products of permutation representations" begin
      sigma = induction(TrivialArithmeticType(Gamma0SL2Z(7)))

      @testset "$n-fold tensor product" for n in 2:4
        rho = reduce(⊗, fill(sigma, n));

        # We check through orbits_total the components are exhausted.
        orbits_total = Set{NTuple{n,Int}}()

        for (ois, ocs, stabs) in ModularForms._orbit_chains(rho)
          for oc in Iterators.product(ocs...)
            p = sigma(prod(oc))
            push!(orbits_total, tuple((act_perm(p, oi) for oi in ois)...))
          end

          # We check that all elements of the stabilizer list actually
          # stabilize the initial element of the orbit.
          for s in stabs
            p = sigma(s)
            @test all(act_perm(p, oi) == oi for oi in ois)
          end
        end

        @test length(orbits_total) == dim(rho)
      end
    end
  end
end
