function runtests_arithmetic_type_homomorphism_orbit()
  @testset "Orbits for homomorphisms" begin
    @testset "extended orbit algorithm over TrivialGroup" for tx in 1:10
      sz = rand(1:20)
      par = TwistedPermGroup(TwistedPermGroup(TrivialGroup(),sz), sz)
      tperms = [rand(twist_parent(par)) for gx in 1:rand(2:5)]
      tperms = [par(perms(g),fill(g,sz)) for g in tperms]

      for (orbit, oi, _, stabilizer_twists, _, orbit_twists) in extended_orbits(tperms)
        oix = searchsortedfirst(orbit, oi)
        for (ox,o) in enumerate(orbit)
          @test o == perms(orbit_twists[ox])[oi]
        end

        for g in stabilizer_twists
          @test perms(g)[oi] == oi
        end
      end
    end
  end
end

