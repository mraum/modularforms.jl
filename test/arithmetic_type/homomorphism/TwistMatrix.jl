function runtests_arithmetic_type_homomorphism_twist_matrix()
  @testset "Morphisms of arithmetic permutation types twisted by matrices" begin
    rhotriv = TrivialArithmeticType(SL2Z)
    rhostd = standard_type(SL2Z)
    rholind = left_induction(standard_type(Gamma0SL2Z(7)))
    rhorind = right_induction(standard_type(Gamma0SL2Z(7)))
    for (rho, sigma) in [(rhotriv, rhostd), (rhostd, rhotriv),
                         (rhostd, rhostd),
                         (rhotriv, rholind), (rholind, rhotriv),
                         (rhostd, rholind), (rhostd, rhorind),
                         (rholind, rhostd), (rhorind, rhostd),
                         (rholind, rholind), (rhorind, rhorind),
                         (rholind, rhorind), (rhorind, rholind)
                        ]
      for hom in basis(homspace(rho,sigma))
        ishomzero = true
        for ix in 1:dim(rho)
          v = ModularForms.unitvec(QQ, dim(rho), ix)
          ishomzero &= all(iszero, hom(v))
          for g in gens(group(rho))
            @test hom(act(rho(g), v)) == act(sigma(g), hom(v))
          end
        end
        @test !ishomzero
      end
    end
  end
end
