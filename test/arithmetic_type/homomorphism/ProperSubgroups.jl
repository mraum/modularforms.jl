function runtests_arithmetic_type_homomorphism_proper_subgroups()
  @testset "Frobenius reciprocity" begin
    triv_7 = TrivialArithmeticType(Gamma0(7))
    rho_7 = induction(triv_7);
    
    rho_11 = induction(TrivialArithmeticType(Gamma0(11)));
    chi_22 = ArithmeticType(DirichletGroup(22)[1]);
    rho_chi_22 = induction(chi_22);

    for (rho,sigma) in [(rho_7,triv_7),(rho_11⊗rho_11,chi_22)]
      sigma_sl2z = induction(sigma)
      basis_big = basis(homspace(rho, sigma_sl2z))
      basis_small = basis(homspace(restriction(group(sigma),rho),sigma))

      l_big = length([b for b in basis_big])
      l_small = length([b for b in basis_small])
      @test l_big == l_small
    end
  end
end
