function runtests_arithmetic_type_homomorphism_twist_trivial()
  @testset "Morphisms of arithmetic permutation types trivally twisted" begin
    rhotriv = TrivialArithmeticType(SL2Z)
    rholind = left_induction(TrivialArithmeticType(Gamma0SL2Z(7)))
    rhorind = right_induction(TrivialArithmeticType(Gamma0SL2Z(7)))
    for (rho, sigma) in [(rhotriv, rholind), (rhotriv, rhorind),
                         (rholind, rhotriv), (rhorind, rhotriv),
                         (rholind, rhorind), (rhorind, rholind)
                        ]
      for hom in basis(homspace(rho,sigma))
        ishomzero = true
        for ix in 1:dim(rho)
          v = ModularForms.unitvec(QQ, dim(rho), ix)
          ishomzero &= all(iszero, hom(v))
          for g in gens(group(rho))
            @test hom(act(rho(g), v)) == act(sigma(g), hom(v))
          end
        @test !ishomzero
        end
      end
    end
  end
end

