function runtests_arithmetic_type_pushforward()
  @testset "Pushforward of arithmetic types" begin
    @testset "Generic arithmetic types" begin
      G = Gamma0SL2Z(7)
      rho = induction(TrivialArithmeticType(G))

      iparent = image_parent(rho)
      h = iparent([2,4,6,1,5,7,8,3])
      phi = map_from_func(p -> inv(h)*p*h, iparent, iparent)
      phirho = pushforward(phi, rho)

      test_subgroup_words(phirho, Gamma1SL2Z(7), GammaSL2Z(7))
    end
  end
end
