function runtests_arithmetic_type_dirichlet()
  @testset "Dirichlet arithmetic types" begin
    chi = DirichletGroup(7)[1]

    rho = ArithmeticType(Gamma0SL2Z(7), chi)
    @test image_parent(rho) == MuInf
    @test level(rho) == 7

    @test conj(conj(rho)) == rho

    rho14 = ArithmeticType(Gamma0SL2Z(14), chi)
    @test level(rho14) == 14
    @test restriction(group(rho14), rho) isa ModularForms.DirichletType
    @test restriction(group(rho14), rho) == rho14

    @test left_pullback(gens(group(rho))[1], rho) isa
          ModularForms.DirichletType
    @test left_pullback(gens(group(rho))[1], rho) == rho
    @test right_pullback(gens(group(rho))[1], rho) isa
          ModularForms.DirichletType
    @test right_pullback(gens(group(rho))[1], rho) == rho

    @test isone(rho(SL2Z(1,0,7,1)))
    @test rho(SL2Z(-1,0,7,-1)) == chi(-1)
  end
end
