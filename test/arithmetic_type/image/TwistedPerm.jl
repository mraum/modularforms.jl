function runtests_arithmetic_type_image_twisted_permutation()
  @testset "Twisted permutations" begin
    @testset "identity in group" for tx in 1:10
      par = TwistedPermGroup(MuInf, rand(10:20))
      @test isone(one(par))

      par = TwistedPermGroup(MatrixGroup(QQab,rand(1:5)), rand(10:20))
      @test isone(one(par))
    end

    @testset "equality and exponents" for tx in 1:20
      par = TwistedPermGroup(MuInf, rand(1:20))

      a = par(collect(1:permsize(par)), fill(one(MuInf),permsize(par)))
      @test isone(a)
      @test isone(par(a))

      a = rand(par, 10:20)
      @test a == a
      @test a == copy(a)
      @test a == deepcopy(a)
      @test a == a*one(par)
      @test a == one(par)*a

      for ix in 1:permsize(a)
        a = a^ix
      end
      a = a^reduce(lcm, map(order, twists(a)); init = 1)
      @test isone(a)
    end

    @testset "associativity and inverses over MuInf" for tx in 1:10
      par = TwistedPermGroup(MuInf, rand(1:20))
      @test (permsize(par) == 1) == isone(cyclic_permutation(par))
      @test isone(cyclic_permutation(par)^permsize(par))
  
      a = rand(par, 10:20)
      b = rand(par, 10:20)
      c = rand(par, 10:20)
      @test (a*b)*c == a*(b*c)
      @test mul!(c,a,b) == a*b
      @test isone(inv(a)*a)
      @test isone(a*inv(a))
    end

    @testset "associativity and inverses over MatrixGroup" for tx in 1:10
      par = TwistedPermGroup(MatrixGroup(QQ,3), rand(1:20))

      a = rand(par, 10:20)
      b = rand(par, 10:20)
      c = rand(par, 10:20)
      @test (a*b)*c == a*(b*c)
      @test isone(inv(a)*a)
      @test isone(a*inv(a))
    end

    @testset "duality" for tx in 1:10
      par = TwistedPermGroup(MuInf, rand(1:20))

      a = rand(par, 10:20)
      @test matrix(QQab, dual(a)) == conj(matrix(QQab, a))
      @test isone(transpose(matrix(QQab, dual(a))) * matrix(QQab, a))
    end

    @testset "action on vectors over MuInf" for tx in 1:5
      par = TwistedPermGroup(MuInf, rand(1:20))
      a = rand(par, 10:20)
      b = rand(par, 10:20)
      v = cf_elem[rand(QQab, -10:10) for ix in 1:dim(par)]
      @test act(a,act(b,v)) == act(a*b,v)
    end

    @testset "action on vectors over matrix groups" for tx in 1:5
      par = TwistedPermGroup(MatrixGroup(QQab,rand(1:5)), rand(1:20))
      a = rand(par, 10:20)
      b = rand(par, 10:20)
      v = cf_elem[rand(QQab, -10:10) for ix in 1:dim(par)]
      @test act(a,act(b,v)) == act(a*b,v)
    end
  end
end
