function runtests_arithmetic_type_image_matrix_group()
  ## the inefficiency of fflu over large cyclotomic fields 
  ## forces us to restrict to small sizes
  @testset "Matrix group over QQab" begin
    @testset "creation and comparison in dimension $n" for n in 1:2
      mg = MatrixGroup(QQab, n)
      a = rand(mg, -5:5)

      @test a == a
      @test isone(one(mg))
      @test copy(a) == a
      @test deepcopy(a) == a
    end

    @testset "arithmetic in dimension $n" for n in 1:2
      mg = MatrixGroup(QQab, n)
      a = rand(mg, -5:5)

      @test (a^2 == a) == isone(a)
      @test (a == inv(a)) == isone(a^2)

      b = rand(mg, -20:20)
      c = rand(mg, -20:20)
      @test a*(b*c) == (a*b)*c

      @test mul!(c,a,b) == a*b

      @test one(mg)^2 == one(mg)
      @test inv(one(mg)) == one(mg)
    end

    @testset "tensor products" begin
      mga = MatrixGroup(QQab, 2)
      mgb = MatrixGroup(QQab, 3)
      a = rand(mga, -20:20)
      b = rand(mgb, -20:20)
      va = [rand(QQab, -20:20) for _ in 1:dim(mga)]
      vb = [rand(QQab, -20:20) for _ in 1:dim(mgb)]
      @test kronecker_product(a*va, b*vb) == (a ⊗ b)*kronecker_product(va,vb)

      b = TrivialGroupElem()
      vb = [rand(QQab, -20:20)]
      @test kronecker_product(act(a,va), act(b,vb)) ==
            act(a ⊗ b, kronecker_product(va,vb))
      @test kronecker_product(act(b,vb), act(a,va)) ==
            act(b ⊗ a, kronecker_product(vb,va))

      b = rand(MuInf, -20:20)
      @test kronecker_product(act(a,va), act(b,vb)) ==
            act(a ⊗ b, kronecker_product(va,vb))
      @test kronecker_product(act(b,vb), act(a,va)) ==
            act(b ⊗ a, kronecker_product(vb,va))
    end
  end
end
