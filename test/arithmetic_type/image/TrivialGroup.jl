function runtests_arithmetic_type_image_trivial_group()
  @testset "Trivial group" begin
    a = one(TrivialGroup())

    @test a == a
    @test isone(a)
    @test a * a == a
    @test inv(a) == a
    for n in -5:5
      @test a^n == a
    end

    @test a ⊗ a == a
  end
end
