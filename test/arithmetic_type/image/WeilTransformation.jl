function runtests_arithmetic_type_image_weil_transformation()
  @testset "Weil transformations" begin
    weila2 = WeilImage(SL2Z, [2 1; 1 2])
    weil2a1 = WeilImage(SL2Z, [2 0; 0 2])
    
    @testset "creation and comparison" for tx in 1:10
      a = weila2(rand(SL2Z, -10:10))

      @test isone(one(weila2))
      @test a == a
      @test copy(a) == a
      @test deepcopy(a) == a
    end

    @testset "homomorphism property" for tx in 1:10
      Ga = GammaSL2Z(3)
    
      g = rand(SL2Z, -10:10)
      h = rand(SL2Z, -10:10)
      a = weila2(one(SL2Z))
    
      @test isone(matrix(QQab, weila2(g))) == (g in Ga)
      @test weila2(g) * weila2(h) == weila2(g*h)
      @test weila2(inv(g)) == inv(weila2(g))
      @test isone(weila2(g) * weila2(inv(g)))
      @test mul!(a,weila2(g),weila2(h)) == weila2(g*h)
    end
    
    @testset "compatibility with tensor products" for tx in 1:10
      g = rand(SL2Z, -10:10)
      @test matrix_unit(weila2(g)) ⊗ matrix_unit(weil2a1(g)) == weila2(g) ⊗ weil2a1(g)

      b = TrivialGroupElem()
      @test b ⊗ weil2a1(g) == weil2a1(g)

      b = rand(MuInf, -20:20)
      @test b ⊗ weil2a1(g) == b ⊗ matrix_unit(weil2a1(g))
    end

    @testset "duality" for tx in 1:10
      g = rand(SL2Z, -10:10)

      @test matrix(QQab, dual(weila2(g))) == conj(matrix(QQab, weila2(g)))
      @test isone(transpose(matrix(QQab, dual(weila2(g)))) * matrix(QQab, weila2(g)))
    end
  end
end
