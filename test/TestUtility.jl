struct Overlapping{O}
  val::O
end

==(a::Overlapping{A}, b::B) where {A,B} = overlaps(a.val, b)
==(a::A, b::Overlapping{B}) where {A,B} = overlaps(a, b.val)
==(a::Overlapping{A}, b::Overlapping{B}) where {A,B} = overlaps(a.val, b.val)

macro test_overlapping(expr)
  if expr.head == :call && length(expr.args) == 3 && expr.args[1] == :(==)
    return quote
        @test(Overlapping($(esc(expr.args[2]))) == Overlapping($(esc(expr.args[3]))))
      end
  else
    throw(DomainError("first argument to @test_overlapping must be an equality test"))
  end
end
