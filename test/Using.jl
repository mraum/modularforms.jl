import Base:
  ==

import AbstractAlgebra
using AbstractAlgebra.Generic:
  PuiseuxSeriesElem,
  scale

using Nemo:
  FiniteField,
  MatrixSpace,
  PolynomialRing,
  QQ,
  ZZ,
  acb,
  fmpq,
  fmpq_poly,
  fmpz,
  mul_classical

using ModularForms:
  GenericType,
  ParabolicDoubleCosetSL2ZIterator,
  SL2ZCoverElement,
  SL2ZElem,
  SL2ZGroup,
  SL2ZSubgroup,
  SubmoduleAcc,
  TrivialGroupElem,
  capacity,
  convert_precision,
  map_generators,
  pivots,
  pivots_upper_bound,
  rank_lower_bound
