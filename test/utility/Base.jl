function runtests_utility_base()
  @testset "Base functionality" begin
    @test ModularForms.unitvec(Int, 4, 2) == [0,1,0,0]
    @test ModularForms.unitvec(Float64, 5, 1) == [1.,0,0,0,0]

    @test ModularForms.unitmat(Int, 4) == [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]
    @test ModularForms.unitmat(Float64, 2) == [1. 0; 0 1.]
  end
end
