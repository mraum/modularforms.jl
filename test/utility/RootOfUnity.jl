function runtests_utility_root_of_unity()
  @testset "MuInfinity" begin
    @test isone(one(MuInf))
    for tx in 1:10
      a = rand(MuInf, 20)
      b = rand(MuInf, 20)
      c = rand(MuInf, 20)

      @test a == copy(a)
      @test a == deepcopy(a)
      @test isone(a^Int(order(a)))
      @test one(MuInf) * a == a
      @test a*(b*c) == (a*b)*c

      @test mul!(c,a,b) == a*b

      CC = AcbField(50)
      @test overlaps(CC(a)*CC(b), CC(a*b))

      q = rand(Rational{Int}, -20:20)
      @test 0 <= exponent(MuInf(q)) < 1
      q = rand(QQ, -20:20)
      @test 0 <= exponent(MuInf(q)) < 1

      @test 0 <= exponent(a*b)
      @test exponent(a*b) < 1
      @test exponent(root_of_unity(rand(1:20))*root_of_unity(rand(1:20))) < 1

      a = rand(1:10)
      b = rand(1:100)
      @test root_of_unity(a*b)^a == root_of_unity(b)

      a = rand(MuInf, 20)
      b = rand(MuInf, 20)

      @test a ⊗ b == a*b
      @test TrivialGroupElem() ⊗ a == a
      @test a ⊗ TrivialGroupElem() == a
    end
  end
end
