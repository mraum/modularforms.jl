function runtests_utility_qqab()
  @testset "QQab" begin
    @testset "equality" begin
      @test iszero(zero(QQab))
      @test isone(one(QQab))
      for tx in 1:10
        a = rand(QQab, 10:20)
        @test a == a
      end
    end
    
    @testset "conversion" begin
      for tx in 1:10
        a = rand(QQ, -20:20)
        @test QQab(a) == a
      end
    end

    @testset "internal conversion" begin
      for tx in 1:10
        a = rand(QQab, 10:20)
        o1 = rand(1:10)
        o2 = rand(1:10)
        @test cf_elem(a.order*o1*o2, a::cf_elem) == cf_elem(a.order*o1*o2, cf_elem(a.order*o1, a))
      end
    end

    @testset "deepcopy" begin
      as = fill(rand(QQab, 1:20), 2)
      bs = deepcopy(as)
      @test bs[1] === bs[2]
      addeq!(bs[1], one(QQab))
      @test as[1] + 1 == bs[1]
      addeq!(as[1], one(QQab))
      @test as[2] == bs[2]
    end
    
    @testset "properties" begin
      for tx in 1:10
        a = rand(QQab, 1:20)
        @test euler_phi(qqab_order(a)) == qqab_degree(a)
      end
    end

    @testset "roots of unity" begin
      for tx in 1:10
        a = rand(1:20)
        @test root_of_unity(QQab,a)^a == one(QQab)
      end

      for tx in 1:10
        a = rand(1:20)
        b = rand(1:20)
        @test QQab(root_of_unity(a)*root_of_unity(b)) == root_of_unity(QQab,a)*root_of_unity(QQab,b)
      end

      for tx in 1:10
        n = rand(0:100)
        d = rand(1:100)
        @test QQab(MuInf(n//d)) == QQab(MuInf(1//d))^n
      end
    end
    
    @testset "complex conjugation" begin
      for tx in 1:10
        n = rand(0:100)
        d = rand(1:100)
        @test conj(QQab(MuInf(n//d))) == QQab(MuInf(-n//d))

        CC = AcbField(20)
        a = rand(QQab,-50:50)
        @test overlaps(conj(CC(a)), CC(conj(a)))
      end
    end

    @testset "arithmetic affects elements" begin
      for tx in 1:100
        a = rand(QQab, -20:20)
        @test a + 1 != a
        @test a - 1 != a
    
        @test 2*zero(QQab) == zero(QQab)
        @test (2*a == a) == iszero(a)
    
        @test zero(QQab)//2 == zero(QQab)
        @test (a//2 == a) == iszero(a)
    
        @test one(QQab)^2 == one(QQab)
        @test (a^2 == a) == (iszero(a) || isone(a))
    
        @test inv(one(QQab)) == one(QQab)
        if !iszero(a)
          @test (a == inv(a)) == (isone(a) || isone(-a))
        end
      end
    end
    
    @testset "algebraic invariants" begin
      for tx in 1:100
        a = rand(QQab, -20:20)
        b = rand(QQab, -20:20)
        c = rand(QQab, -20:20)
        @test a+b == b+a
        @test a*b == b*a
        @test a+(b+c) == (a+b)+c
        @test a*(b*c) == (a*b)*c
        @test a*(b+c) == a*b+a*c
      end
    end
    
    @testset "algebraic relations" begin
      for tx in 1:100
        a = rand(QQab, -20:20)
        b = rand(QQab, -20:20)
        @test a-b == a+(-b)
        if !iszero(b)
          @test a//b == a*inv(b)
        end
      end
    end

    @testset "inplace arithmetic" begin
      for tx in 1:100
        a = rand(QQab, -20:20)
        b = rand(QQab, -20:20)
        c = rand(QQab, -20:20)
        @test a+b == (addeq!(a,b); a)
        @test a*b == (mul!(c,a,b); c)
      end
    end

    @testset "action of roots of unity" begin
      for tx in 1:100
        a = rand(MuInf, 20)
        b = rand(MuInf, 20)
        c = rand(QQab, -20:20)
        @test act(a, act(b,c)) == act(a*b,c)
      end
    end

    @testset "norm of elements" begin
      for tx in 1:10
        a = rand(QQab, -20:20)
        b = rand(QQab, -20:20)
        @test norm(a*b) == norm(a)*norm(b)

        a = rand(QQ, -20:20)
        @test norm(QQab(a)) == a^2
      end
    end

    @testset "square roots" begin
      for a in -20:20
        asqrt = qqab_sqrt(a)
        @test isa(asqrt, cf_elem)
        @test asqrt^2 == QQab(a)
      end
    end
  end
end
