function runtests_utility_linear_algebra()
  @testset "Linear algebra utility" begin

    @testset "pivots" begin
      @test pivots(matrix(ZZ, [1 0 0; 0 0 1])) == [1,3]
      @test pivots(matrix(ZZ, [1 0 0; 0 0 1]), 2) == [1,3]
      
      RR = ArbField(100)
      CC = AcbField(100)
      
      @test_throws DomainError pivots(matrix(CC, [1 0 0; 0 ball(RR(0),RR(1)) 1])) == [1,2]
      
      @test pivots_upper_bound(matrix(CC, [1 0 0; 0 ball(RR(0),RR(1)) 1])) == [1,3]
    end

    @testset "change of basis matrices" begin
      for tx in 1:5
        ms = MatrixSpace(QQ, rand(5:8), rand(2:5))
  
        a = rand(ms, -10:10)
        while rank(a) != ncols(ms)
          a = rand(ms, -10:10)
        end
  
        gp = MatrixGroup(QQ, ncols(ms))
        for tx2 in 1:3
          g = rand(gp, 10:20)
          @test change_of_basis(a, a*matrix(g)) == matrix(g)
        end
      end
    end
  end


  @testset "Lattices" begin
    for ms in [MatrixSpace(ZZ,3), MatrixSpace(ZZ, 4, 7), MatrixSpace(QQ, 13, 8)]
      for tx in 1:2
        m = rand(ms, 10:20)
        s, u, v = smith_normal_form(m)
        r = min(nrows(m), ncols(m))
        @test all(ix == jx || iszero(s[ix,jx]) for ix in 1:r, jx in 1:r)
        @test iszero(sub(s, r+1, 1, nrows(m), ncols(m)))
        @test iszero(sub(s, 1, r+1, nrows(m), ncols(m)))
        @test u*s*v == m
      end
    end
  end
end
