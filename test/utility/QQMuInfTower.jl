function runtests_utility_qq_muinf_tower()
  @testset "tower of cyclotomic fields over $(QQ)" begin
    QQT = cyclotomic_tower(QQ)
    @test base_ring(QQT) == QQ
    @test limit_ring(QQT) == QQab
  
    @testset "arithmetic in cyclotomic field of order $n" for n in 1:10
      @test has_muinf_ring(QQT,n)
      QQn = muinf_ring(QQT, n)
  
      # In the case of of odd n, we have an additional factor 2 in the order.
      @test divisible(muinf_order(QQT, QQn),n)
      if isodd(n)
        @test muinf_order(QQT, QQn) == 2*n
      else
        @test muinf_order(QQT, QQn) == n
      end
      for m in 1:10
        @test has_muinf_order(QQT,QQn,m) == (mod(lcm(2,n),m) == 0)
      end

      @testset "cyclotomic extensions" begin
        @test isnothing(muinf_extension(QQT,QQn,n))
        QQm,cv = muinf_extension(QQT,QQn,13)
        a = rand(QQn,-20:20)
        b = rand(QQn,-20:20)
        @test parent(cv(a)) == QQm
        @test cv(a)*cv(b) == cv(a*b)
      end

      @testset "conversion within of tower" begin
        QQm = muinf_ring(QQT, n*5)

        cv = map_from_cyclotomic_tower(QQT, QQn, QQm)
        a = rand(QQn,-20:20)
        b = rand(QQn,-20:20)
        @test in(a, QQT, QQm)
        @test cv(a)*cv(b) == cv(a*b)

        a = cv(a)
        b = cv(b)
        cv = map_from_cyclotomic_tower(QQT, QQm, QQn)
        @test in(a, QQT, QQn)
        @test cv(a)*cv(b) == cv(a*b)

        @test !in(root_of_unity(QQT, QQm, n*5), QQT, QQn)
      end

      @testset "conversion from $(QQ)" begin
        cv = map_from_cyclotomic_tower(QQT, QQ, QQn)
        a = rand(QQ,-20:20)
        b = rand(QQ,-20:20)
        @test in(cv(a),QQT,QQ)
        @test cv(a)*cv(b) == cv(a*b)

        @test in(root_of_unity(QQT, QQn, n), QQT, QQ) == (n == 1 || n == 2)
      end

      @testset "conversion to $(QQab)" begin
        cv = map_from_cyclotomic_tower(QQT, QQn, QQab)
        a = rand(QQn,-20:20)
        b = rand(QQn,-20:20)
        @test in(a,QQT,QQab)
        @test cv(a)*cv(b) == cv(a*b)
      end

      @testset "roots of unity" begin
        for tx in 1:10
          a = rand(divisors(n))
          @test root_of_unity(QQT,QQn,a)^a == one(QQn)
        end
  
        for tx in 1:10
          a = rand(divisors(n))
          b = rand(divisors(n))
          @test (QQT(QQn,root_of_unity(a)*root_of_unity(b))
                == root_of_unity(QQT,QQn,a)*root_of_unity(QQT,QQn,b))
        end
  
        for a in 0:n-1
          @test QQab(QQT(QQn,MuInf(a//n))) == QQab(MuInf(a//n))
        end

        for a in 0:n-1
          b = rand(QQn,-20:20)
          @test act(MuInf(a//n), b, QQT) == QQT(QQn,MuInf(a//n))*b
        end
      end
    end
  end
end
