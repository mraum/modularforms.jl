function runtests_utility_dirichlet()
  @testset "Dirichlet characters" begin
    @testset "Hashing" begin
      chi1 = first(DirichletGroup(11))
      chi2 = first(DirichletGroup(11))
      @test hash(parent(chi1)) == hash(parent(chi2))
      @test Dict([parent(chi1) => 1])[parent(chi2)] == 1

      @test hash(chi1) == hash(chi2)
      @test Dict([chi1 => 1])[chi2] == 1
    end

    # creation, comparison, and one
    for tx in 1:10
      dg = rand(DirichletGroup, 10:80)
      @test dg == dg
      @test isone(one(dg))
      @test one(dg) == one(dg)
      @test dg[1] == dg[1]

      chi = rand(dg)
      @test chi == copy(chi)
      @test chi == deepcopy(chi)
    end
  
    @test length(DirichletGroup(5)) == 4
    @test length(DirichletGroup(6)) == 2

    # iteration and index access
    for tx in 1:10
      dg = rand(DirichletGroup, 10:80)
  
      @test all(any(dg[i] == chi for chi in dg) for i in 1:length(dg))

      chi_values_iter = [[chi(x) for x in 0:modulus(dg)-1] for chi in dg]
      for ix in 1:length(dg)
        chi = dg[ix]
        chiv = [chi(x) for x in 0:modulus(dg)-1]
        @test length([chivi for chivi in chi_values_iter if chivi == chiv]) == 1
      end
    end

    # creation of subgroups
    for tx in 1:10
      m = rand(10:80)

      dg = DirichletGroup(m)
      sdg = subgroup(DirichletGroup(4*m),m)

      for (chi,schi) in zip(dg,sdg)
        @test [chi(x) for x in 0:m-1] == [schi(x) for x in 0:m-1]
      end
    end

    # conversion
    for tx in 1:10
      m = rand(10:80)
      me = rand(1:20)

      for chi in DirichletGroup(m)
        cm = conductor(chi)*me
        cdg = DirichletGroup(cm)
        cchi = cdg(chi)

        @test parent(cchi) === cdg
        for n in 0:lcm(m,me)-1
          if isone(gcd(n,cm))
            if isone(gcd(n,m))
              @test cchi(n) == chi(n)
            end
          else
            @test iszero(cchi(n))
          end
        end
      end
    end

    # primitive characters
    for tx in 1:10
      m = rand(10:80)
      for chi in DirichletGroup(m)
        chip = primitive(chi)
        @test modulus(chip) == conductor(chi)
        for n in 0:m-1
          if isone(gcd(n,m))
            @test chip(n) == chi(n)
          elseif !isone(gcd(n,conductor(chi)))
            @test iszero(chip(n))
          end
        end
      end
    end

    # iteration of primitive characters
    for tx in 1:10
      dg = rand(DirichletGroup, 1:80)
      dgp = primitive_characters(dg)

      @test length(dgp) == length(collect(dgp))
      @test collect(dgp) == filter(isprimitive,collect(dg))
    end
  
    # parity of characters
    let
      chi = first(primitive_characters(DirichletGroup(3)))
      @test !iseven(chi)
      @test isodd(chi)

      chi = first(primitive_characters(DirichletGroup(15)))
      @test iseven(chi)
      @test !isodd(chi)
    end

    # multiplication inside a group and powers
    for tx in 1:10
      dg = rand(DirichletGroup, 10:80)
      chi = rand(dg)
      n = rand(-100:100)
      m = rand(-20:20)

      @test (chi^m)^n == chi^(m*n)
      m = rand(0:20)
      if gcd(m,modulus(chi)) == 1 && gcd(n,modulus(chi)) == 1
        @test (chi^m)(n) == (chi(n))^m
      end
      @test chi(n) == chi(ZZ(n))

      @test isone(conductor(chi*inv(chi)))

      psi = rand(dg)
      @test isone(mul!(psi, chi, inv(chi)))

      for n in 0:modulus(chi)
        @test conj(chi(n)) == conj(chi)(n)
      end
    end

    # multiplication outside of a group
    for tx in 1:10
      chi1 = rand(DirichletGroup(rand(10:80)))
      chi2 = rand(DirichletGroup(rand(10:80)))
      chi = chi1*chi2
      @test modulus(chi) == lcm(modulus(chi1),modulus(chi2))
      for n in 0:modulus(chi)-1
        gcd(n, modulus(chi)) == 1 || continue
        @test chi1(n)*chi2(n) == chi(n)
      end
    end

    @testset "Gauss sums" begin
      CC = AcbField(50)
      for N in 15:20
        for chi in primitive_characters(DirichletGroup(N))
          @test norm(gauss_sum(chi)) == N

          for a in 0:N-1
            if isone(gcd(a,N))
              @test norm(gauss_sum(chi,a)) == N
            else
              @test iszero(gauss_sum(chi,a))
            end

            @test overlaps(CC(gauss_sum(chi,a)),gauss_sum(chi,a,CC))
          end
        end
      end
    end
  end
end
