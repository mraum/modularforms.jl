function runtests_utility_continued_fraction()
  @testset "minus continued fractions" begin
    RR = ArbField(200)
    @test minus_continued_fraction((1 + sqrt(RR(5)))/2, 20) == vcat([2], fill(3,19))
  end
end
