function runtests_utility_gfp_muinf_tower()
  @testset "tower of cyclotomic fields over $(GF(3))" begin
    FFT = cyclotomic_tower(GF(3))
    @test base_ring(FFT) == GF(3)
    @test_throws ErrorException limit_ring(FFT)
  
    @testset "arithmetic in cyclotomic field of order $n" for n in 1:10
      @test has_muinf_ring(FFT,n) == !divisible(n,3)
      divisible(n,3) && continue
      FFn = muinf_ring(FFT, n)

      @test divisible(muinf_order(FFT, FFn),n)
      for m in 1:10
        @test has_muinf_order(FFT,FFn,m) == (mod(order(FFn)-1,m) == 0)
      end
  
      @testset "cyclotomic extensions" begin
        if has_muinf_order(FFT,FFn,13)
          @test isnothing(muinf_extension(FFT,FFn,13))
        else
          FFm,cv = muinf_extension(FFT,FFn,13)
          a = rand(FFn)
          b = rand(FFn)
          @test parent(cv(a)) == FFm
          @test cv(a)*cv(b) == cv(a*b)
        end
      end

      @testset "conversion within of tower" begin
        FFm = muinf_ring(FFT, n*5)

        cv = map_from_cyclotomic_tower(FFT, FFn, FFm)
        a = rand(FFn)
        b = rand(FFn)
        @test in(a, FFT, FFm)
        @test cv(a)*cv(b) == cv(a*b)

        a = cv(a)
        b = cv(b)
        cv = map_from_cyclotomic_tower(FFT, FFm, FFn)
        @test in(a, FFT, FFn)
        @test cv(a)*cv(b) == cv(a*b)

        @test in(root_of_unity(FFT, FFm, n*5), FFT, FFn) == (mod(order(FFn),ZZ(5*n))==1)
      end

      @testset "conversion from $(GF(3))" begin
        cv = map_from_cyclotomic_tower(FFT, GF(3), FFn)
        a = rand(GF(3))
        b = rand(GF(3))
        @test in(cv(a),FFT,GF(3))
        @test cv(a)*cv(b) == cv(a*b)
      end

      @testset "roots of unity" begin
        for tx in 1:10
          a = rand(divisors(n))
          @test root_of_unity(FFT,FFn,a)^a == one(FFn)
        end
  
        for tx in 1:10
          a = rand(divisors(n))
          b = rand(divisors(n))
          @test (FFT(FFn,root_of_unity(a)*root_of_unity(b))
                == root_of_unity(FFT,FFn,a)*root_of_unity(FFT,FFn,b))
        end
  
        for a in 0:n-1
          b = rand(FFn)
          @test act(MuInf(a//n), b, FFT) == FFT(FFn,MuInf(a//n))*b
        end
      end
    end
  end
end
