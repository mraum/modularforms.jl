function runtests_utility_submodule_accumulating()
  @testset "Accumulating submodule" begin
    @testset "special 3-dimensional case" begin
      fm = FreeModule(QQ, 3)
      m = ModularForms.SubmoduleAcc(QQ, dim(fm), 4)
  
      for v in [fmpq[1,2,0], fmpq[1,0,0], fmpq[0,1,0]]
        insert!(m, v)
      end
  
      @test rank_lower_bound(m) == 0
  
      reduce!(m)

      @test rank_lower_bound(m) == 2
      @test collect(m)[1] == fmpq[1,0,0]
      @test collect(m)[2] == fmpq[0,1,0]
  
      reduce!(m)
  
      @test rank_lower_bound(m) == 2
      @test collect(m)[1] == fmpq[1,0,0]
      @test collect(m)[2] == fmpq[0,1,0]
    end

    @testset "random cases" for tx in 1:5
      fm = FreeModule(QQ, rand(1:10))
      m = ModularForms.SubmoduleAcc(QQ, dim(fm), rand(1:20))
      mg = ModularForms.SubmoduleAccWithGenMap(QQ, fm, dim(fm), capacity(m))

      vs = Vector{fmpq}[]
      for _ in 1:3
        for vx in 1:rand(1:20)
          v = [rand(QQ,-5:5) for _ in 1:dim(fm)]
          try
            insert!(m, v)
            insert!(mg, v, fm(v))
          catch DomainError
            break
          end
          push!(vs,v)
        end

        reduce!(m)
        reduce!(mg)
  
        @test rank_lower_bound(m) == rank_lower_bound(mg)
        for (c,(cg,vg)) in zip(collect(m), collect(mg))
          @test c == cg
        end
      end
    end

    @testset "change of base ring" begin
      sm = SubmoduleAcc(QQ, 3, 5)
      insert!(sm, fmpq[1, 2, 4])
      insert!(sm, fmpq[3, 1, 3])
      reduce!(sm)
      sm = change_base_ring(QQab, sm)
      @test rank_lower_bound(sm) == 0
      reduce!(sm)
      @test rank_lower_bound(sm) == 2
    end
  end
end
