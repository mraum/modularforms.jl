function runtests_utility_hurwitz()
  @testset "Hurwitz numbers" begin
    @testset "length of list of Hurwitz numbers for precision $prc" for prc in 0:20
      @test precision(hurwitz_numbers(prc)) == prc
    end

    # generating function of 12*H(n) given in OIES
    function hurwitz_genfct(prc::Int; ring::R = ZZ) where R
      _,x = PowerSeriesRing(ring,prc,"x")
      divexact(
          divexact(1
                   + sum(divexact(4*(-1)^k * x^(k^2 + k), (1 + (-x)^k)^2)
                         for k in 1:isqrt(prc))
                   + sum(divexact(4*(-1)^k * x^(k^2 - k), (x^(-k) + (-1)^k)^2)
                         for k in -isqrt(prc)-1:-1),
                   sum(x^(k^2) for k in -isqrt(prc):isqrt(prc))
                  )
        + divexact(1
                   + sum(divexact(4*(-1)^k * x^(k^2 + 2*k), (1 + x^(2*k))^2)
                         for k in 1:isqrt(prc))
                   + sum(divexact(4*(-1)^k * x^(k^2 - 2*k), (x^(-2*k) + 1)^2)
                         for k in -isqrt(prc)-2:-1),
                   sum((-x)^(k^2) for k in -isqrt(prc):isqrt(prc))
                  ),
        -2)
    end
    
    prc = 40
    hw = hurwitz_numbers(prc)
    hf = hurwitz_genfct(prc)
    @testset "compare $n-th coefficients with generating series" for n in 0:prc-1
      @test coeff(hw,n) == coeff(hf,n)
    end
  end
end
