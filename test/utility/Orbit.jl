function runtests_utility_orbit()
  @testset "Orbit utility" begin
    @testset "general left orbits" begin
      (K,a) = FiniteField(3,2,"a")
      @test length(left_orbit([a],one(K))) == 8
      @test left_orbit([one(K)],a) == [a]
      orbit = left_orbit([K(2)],a)
      @test a in orbit
      @test 2*a in orbit
    end
  end
end
