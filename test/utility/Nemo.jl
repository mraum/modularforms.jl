function runtests_utility_nemo()
  @testset "Nemo utility" begin
    @testset "arb to fmpz conversion" begin
      RR = ArbField(100)
      @test upper_bound_integer(RR(2)) == 2
      @test upper_bound_integer(ball(RR(1),RR(2))) == 4
    end

    @testset "multiplication Nemo matrix and Julia vector" begin
      ms = MatrixSpace(QQ, rand(5:15), rand(10:20))
      vs = MatrixSpace(QQ, ncols(ms), 1)
      for tx in 1:10
        m = rand(ms, 10:20)
        v = rand(vs, 10:20)
        @test Vector{fmpq}(m*v) == mul_classical(m,Vector{fmpq}(v))
      end
    end

    @testset "conversion among Power series of different precision over $C" for C in [CyclotomicField(7,"a")[1], ZZ, QQ, ResidueRing(ZZ,10), ResidueRing(ZZ,ZZ("36893488147419103363")), FiniteField(7,3,"a")[1], FiniteField(ZZ("36893488147419103363"),3,"a")[1]]
      R,xR = PowerSeriesRing(C,10,"x")
      S,xS = PowerSeriesRing(C,15,"x")
    
      @test convert_precision(S,xR^2) == xS^2
      @test precision(convert_precision(S,xR^2)) == 12
    
      @test convert_precision(R,xS^2) == xR^2
      @test precision(convert_precision(R,xS^2)) == 17
    end
    
    @testset "conversion among Laurent series of different precision over $C" for C in [CyclotomicField(7,"a")[1],ZZ,QQ]
      R,xR = LaurentSeriesRing(C,10,"x")
      S,xS = LaurentSeriesRing(C,15,"x")
    
      @test convert_precision(S,xR^2) == xS^2
      @test precision(convert_precision(S,xR^2)) == 12
    
      @test convert_precision(R,xS^2) == xR^2
      @test precision(convert_precision(R,xS^2)) == 17
    end
    
    @testset "conversion among Puiseux series of different precision over $C" for C in [CyclotomicField(7,"a")[1],ResidueRing(ZZ,10),ZZ]
      R,xR = PuiseuxSeriesRing(C,10,"x")
      S,xS = PuiseuxSeriesRing(C,15,"x")
    
      @test convert_precision(S,xR^2) == xS^2
      @test precision(convert_precision(S,xR^2)) == 12
    
      @test convert_precision(R,xS^2) == xR^2
      @test precision(convert_precision(R,xS^2)) == 17
    end

    let domaincodomains = []
      push!(domaincodomains, (QQ,QQab,QQab))
      
      D = ResidueRing(ZZ,10)
      C = ResidueRing(ZZ,5)
      f = a->C(a.data)
      push!(domaincodomains, (D,C,f))
      
      @testset "change of base ring from $D to $C" for (D,C,f) in domaincodomains
         # @testset "Power series" begin
         #   RD,xD = PowerSeriesRing(D,10,"x")
         #   RC,xC = PowerSeriesRing(C,10,"x")
         #   @test change_base_ring(xD, f, RC) == xC
         # end
      
        @testset "Laurent series" begin
          RD,xD = LaurentSeriesRing(D,10,"x")
          RC,xC = LaurentSeriesRing(C,10,"x")
          @test change_base_ring(xD, f, RC) == xC
        end
        
        @testset "Puiseux series" begin
          RD,xD = PuiseuxSeriesRing(D,10,"x")
          RC,xC = PuiseuxSeriesRing(C,10,"x")
          @test change_base_ring(xD, f, RC) == xC
        end
      end
    end
  end
end
