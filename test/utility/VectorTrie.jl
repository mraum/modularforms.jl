function runtests_utility_vector_trie()
  @testset "Vector tries" begin
    VectorTrie = ModularForms.VectorTrie
    tokey(a::Union{Int,BigInt}) = BitVector(map(Bool, digits(a, base=2)))

    @test_throws TypeError VectorTrie{UInt8,Nothing}()

    trie = VectorTrie{Bool,Int}()

    trie[tokey(17)] = 19
    @test collect(trie) == [tokey(17) => 19]

    trie[] = 2
    trie[tokey(19)] = 21
    @test collect(trie) == [[] => 2, tokey(17) => 19, tokey(19) => 21]

    for (k,v) in trie
      @test k isa AbstractVector{Bool}
      @test v isa Int
      @test v == 2 + reduce(+, (d*2^(n-1) for (n,d) in enumerate(k)), init=0)
    end


    delete!(trie, tokey(17))
    @test collect(trie) == [Bool[] => 2, tokey(19) => 21]
    @test keys(trie) == [Bool[], tokey(19)]

    @test haskey(trie, tokey(19))
    @test !haskey(trie, tokey(17))

    @test trie == VectorTrie([Bool[] => 2, tokey(19) => 21])

    @test trie == copy(trie)
    @test trie == deepcopy(trie)

    trie = VectorTrie{Bool,Int}()
    for k in 0:20
      trie[tokey(k)] = 3*k+1
    end
    for k in 16:30
      delete!(trie,tokey(k))
    end
    @test Set(collect(trie)) == Set(tokey(k) => 3*k+1 for k in 0:15)

    # Breadth first seach through all subtries to check integrety of the date
    # structure.
    all_subtries = [trie]
    while !isempty(all_subtries)
      subtrie = pop!(all_subtries)
      @test !isnothing(ModularForms.value(subtrie)) ||
            !isempty(ModularForms.subtries(subtrie))
      prepend!(all_subtries, values(ModularForms.subtries(subtrie)))
    end

    trie = VectorTrie{Bool,Int}()
    trie[tokey(19)] = 23
    @test collect(map(c -> c isa Int && 2*c, trie)) == [tokey(19) => 46]
    @test collect(map(c -> 2. * c, trie; value_type = Float64)) ==
          [tokey(19) => 2. * 23]

    map!(c -> c isa Int && 2*c, trie)
    @test collect(trie) == [tokey(19) => 46]

    @test collect(mergewith(+, trie, trie)) == [Vector(tokey(19)) => 92]

    trie = VectorTrie{Bool,Int}()
    for k in 20:30
      trie[tokey(k)] = 3*k+1
    end

    @test ModularForms.map_reduce(trie,
              identity,
              it->reduce(+, map(kv->kv[2],it); init = 0),
              +) ==
          sum(3*k+1 for k in 20:30)

    @test ModularForms.map_reduce(trie,
              v -> 7*v,
              it->reduce(+, map(kv->(kv[1]+1)*kv[2],it); init = 0),
              +) ==
          sum(prod(1 .+ tokey(k))*7*(3*k+1) for k in 20:30)

    trie = VectorTrie{Bool,Int}()
    @test ModularForms.map_reduce(trie,
              _ -> error(),
              _ -> error(),
              _ -> error();
              init = :init) == :init
  end
end
