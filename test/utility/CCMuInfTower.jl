function runtests_utility_cc_muinf_tower()
  CC = AcbField(100)
  @testset "tower of cyclotomic fields in $CC" begin
    CCT = cyclotomic_tower(CC)
    @test base_ring(CCT) == CC
    @test limit_ring(CCT) == CC
  
    @testset "conversion from and to cyclotomic field of order $n" for n in 1:10
      @test has_muinf_ring(CCT,n)
      CCn = muinf_ring(CCT, n)
  
      # In the special case of CCMuInfTower, all roots of unity are contained
      # in the base ring.
      @test divisible(muinf_order(CCT, CCn),n)
      @test muinf_order(CCT, CCn) == 0
      @test has_muinf_order(CCT,CC,n)

      @testset "cyclotomic extensions" begin
        # No conversion needed
        @test isnothing(muinf_extension(CCT,CCn,13))
        @test parent(root_of_unity(CCT,CCn,13)) == CCn
      end
  
      @testset "conversion within of tower" begin
        CCm = muinf_ring(CCT, n*5)

        cv = map_from_cyclotomic_tower(CCT, CCn, CCm)
        a = rand(CCn,-20:20)
        b = rand(CCn,-20:20)
        @test in(a, CCT, CCm)
        @test_overlapping cv(a)*cv(b) == cv(a*b)

        a = cv(a)
        b = cv(b)
        cv = map_from_cyclotomic_tower(CCT, CCm, CCn)
        @test in(a, CCT, CCn)
        @test_overlapping cv(a)*cv(b) == cv(a*b)
      end

      @testset "conversion from $(CC)" begin
        cv = map_from_cyclotomic_tower(CCT, CC, CCn)
        a = rand(CC,-20:20)
        b = rand(CC,-20:20)
        @test_overlapping cv(a)*cv(b) == cv(a*b)
      end

      @testset "conversion to $(CC)" begin
        cv = map_from_cyclotomic_tower(CCT, CCn, CC)
        a = rand(CCn,-20:20)
        b = rand(CCn,-20:20)
        @test_overlapping cv(a)*cv(b) == cv(a*b)
      end

      @testset "roots of unity" begin
        for tx in 1:10
          a = rand(divisors(n))
          @test_overlapping root_of_unity(CCT,CCn,a)^a == one(CCn)
        end
  
        for tx in 1:10
          a = rand(divisors(n))
          b = rand(divisors(n))
          @test_overlapping(
              CCT(CCn,root_of_unity(a)*root_of_unity(b))
              == root_of_unity(CCT,CCn,a)*root_of_unity(CCT,CCn,b))
        end
  
        for a in 0:n-1
          @test_overlapping CC(CCT(CCn,MuInf(a//n))) == CC(MuInf(a//n))
        end

        for a in 0:n-1
          b = rand(CCn,-20:20)
          c = rand(CCn,-20:20)
          @test_overlapping act(MuInf(a//n), b, CCT) == CCT(CCn,MuInf(a//n))*b
        end
      end
    end
  end
end
