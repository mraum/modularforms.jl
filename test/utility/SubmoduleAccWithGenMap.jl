function runtests_utility_submodule_accumulating_with_generator_map()
  @testset "Accumulating submodule with generator map" begin
    @testset "special 3-dimensional case" begin
      fm = FreeModule(QQ, 3)
      m = ModularForms.SubmoduleAccWithGenMap(QQ, fm, dim(fm), 4)
  
      for v in [fmpq[1,2,0], fmpq[1,0,0], fmpq[0,1,0]]
        insert!(m, v, fm(v))
      end
  
      @test rank_lower_bound(m) == 0
  
      reduce!(m)
  
      @test rank_lower_bound(m) == 2
      for (c,vs) in m
        @test fm(c) == vs
      end

      # Check that generators are that are added first are preferred over those
      # added later. The order here is the reverse one of the insertion order.
      @test m.gens[1] == fm(fmpq[1,2,0])
      @test m.gens[2] == fm(fmpq[1,0,0])
  
      # Check that generators are stable when adding new vectors.
      for v in [fmpq[2,2,0], fmpq[1,-1,0], fmpq[1,1,0]]
        insert!(m, v, fm(v))
      end
      reduce!(m)
  
      @test rank_lower_bound(m) == 2
      @test m.gens[1] == fm(fmpq[1,2,0])
      @test m.gens[2] == fm(fmpq[1,0,0])

      for v in [fmpq[1,0,1], fmpq[0,0,1]]
        insert!(m, v, fm(v))
      end
      reduce!(m)

      # Check that generators are that are added first are preferred over those
      # added later. The order here is the reverse one of the insertion order.
      @test rank_lower_bound(m) == 3
      @test m.gens[1] == fm(fmpq[1,2,0])
      @test m.gens[2] == fm(fmpq[1,0,0])
      @test m.gens[3] == fm(fmpq[1,0,1])
    end

    @testset "random cases" for tx in 1:5
      fm = FreeModule(QQ, rand(1:10))
      m = ModularForms.SubmoduleAccWithGenMap(QQ, fm, dim(fm), rand(1:20))

      vs = Vector{fmpq}[]
      for _ in 1:3
        for vx in 1:rand(1:20)
          v = [rand(QQ,-5:5) for _ in 1:dim(fm)]
          try
            insert!(m, v, fm(v))
          catch DomainError
            break
          end
          push!(vs,v)
        end

        reduce!(m)
        mref, _ = sub(fm, map(fm,vs))
  
        @test rank_lower_bound(m) == dim(mref)
        for (c,vs) in m
          @test fm(c) == vs
        end
      end
    end

    @testset "change of base ring" begin
      sm = ModularForms.SubmoduleAccWithGenMap(QQ, fmpq, 3, 5)
      insert!(sm, fmpq[1, 2, 4], QQ(17,1))
      insert!(sm, fmpq[3, 1, 3], QQ(1,13))
      reduce!(sm)
      gs = [deepcopy(g) for (_,g) in sm]
      sm = change_base_ring(QQab, sm)
      @test rank_lower_bound(sm) == 0
      reduce!(sm)
      @test rank_lower_bound(sm) == 2
    
      @test gs == [g for (_,g) in map_generators(QQab, cf_elem, sm)]
      @test gs == [g for (_,g) in map_generators(map_from_func(g->QQab(g),QQ,QQab), sm)]
    end
  end
end
