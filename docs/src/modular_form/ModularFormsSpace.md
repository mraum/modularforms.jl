# Modular forms space

Space                                         | Element type                                 | Parent type                                        |
----------------------------------------------|----------------------------------------------|----------------------------------------------------|
$\mathcal{M}_k(\mathrm{SL}_2(\mathbb{Z}))$    | `ModularForms.ModularFormSL2Z`               | `ModularForms.ModularFormsSpaceSL2Z`               |
$\mathcal{M}_k(\mathrm{SL}_2(\mathbb{Z}); R)$ | `ModularForms.ModularFormSL2Z`               | `ModularForms.ModularFormsSpaceSL2Z`               |
$\mathcal{M}_k(\rho)$                         | `ModularForms.ModularFormSL2ZCongruenceType` | `ModularForms.ModularFormsSpaceSL2ZCongruenceType` |
$\mathcal{M}_k(\rho; R)$                      | `ModularForms.ModularFormSL2ZCongruenceType` | `ModularForms.ModularFormsSpaceSL2ZCongruenceType` |
$\mathcal{E}_k(\rho)$                         | `ModularForms.ModularFormSL2ZCongruenceType` | `ModularForms.ModularFormsSpaceSL2ZCongruenceType` |
$\mathcal{E}_k(\rho; R)$                      | `ModularForms.ModularFormSL2ZCongruenceType` | `ModularForms.ModularFormsSpaceSL2ZCongruenceType` |

## Constructors

To create modular forms spaces $\mathcal{M}_k$, we can write

```@docs
ModularFormsSpace
```

If we instead want to create Eisenstein space $\mathcal{E}_k$, we can write
```@docs
ModularForms.EisensteinSpace
```

## Interface

```@docs
weight
```

```@docs
ModularForms.coeffring
```

```@docs
basis(::ModularForms.ModularFormsSpaceSL2Z{R}) where R
```

```@docs
dim(::ModularForms.ModularFormsSpaceSL2Z{R}) where R
dim(::ModularForms.ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}
```

```@docs
ModularForms.sturm_bound
```

## Functionality

```@docs
fourier_expansion_matrix(::ModularForms.ModularFormsSpaceSL2Z{R}, ::Int) where R
```

## Arithmetic operations

```@docs
+(a::ModularForms.ModularFormsSpaceSL2Z, b::ModularForms.ModularFormsSpaceSL2Z)
```

```@docs
-(a::ModularForms.ModularFormsSpaceSL2Z, b::ModularForms.ModularFormsSpaceSL2Z)
```

```@docs
*(a::ModularForms.ModularFormsSpaceSL2Z, b::ModularForms.ModularFormsSpaceSL2Z)
```
