# Modular forms

## Creation
```@docs
eisenstein_series(::Int)
eisenstein_series(::Int, ::Int)
```

```@docs
ramanujan_delta()
```

## Interface
```@docs
ModularForms.sturm_bound(::ModularForms.ModularFormSL2Z{R}) where R
```

```@docs
fourier_expansion
```

## Numerical evaluation
```@docs
evaluate(::ModularForms.ModularFormSL2ZCongruenceType{I,R}, ::acb) where {I <: GroupElem, R <: RingElem}
evaluate(::ModularForms.ModularFormSL2Z{R}, ::acb, ::Int) where {R <: Union{fmpz, fmpq}}
```
