# Farey symbols
Farey symbols is an efficient way to characterize a subgroup $\Gamma \subseteq
\mathrm{SL}_2(\mathbb{Z})$.

To create such an entity, use
```@docs
ModularForms.FareySymbolSL2Z(::Function, ::Function)
```

## Basic functionality
To obtain Farey symbols of various groups, we write

```@docs
farey_symbol
```
