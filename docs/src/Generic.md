# Generic stuff

## Analytic type
I don't know what an analytic type is. But here it goes.

To create the cover weight of $\mathrm{SL}_2(\mathbb{R})$, we write

```@docs
SL2RCoverWeight
```
