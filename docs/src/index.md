# ModularForms.jl

ModularForms.jl is package in the Julia programming language maintained by
Martin Raum and Tobias Magnusson, with an intent of create a fast environment
for computing modular forms. The source code can be viewed
[here](https://gitlab.com/mraum/modularforms.jl).

The package builts on the [OSCAR project's](https://oscar.computeralgebra.de/),
including [Nemo](https://github.com/Nemocas/Nemo.jl) and
[AbstractAlgebra](https://github.com/Nemocas/AbstractAlgebra.jl).

## Installation
The Julia prerequisites for ModularForms.jl can be seen under `Project.toml`.
To install ModularForms.jl, run
```
$ git clone https://gitlab.com/mraum/modularforms.jl.git
$ julia
julia> Pkg.dev("modularforms.jl")
julia> Pkg.build("ModularForms")
```
and you should be ready to go.
