# Root of unity

Group                     | Element type  | Parent type        |
--------------------------|---------------|--------------------|
$\langle \zeta_N \rangle$ | `RootOfUnity` | `RootOfUnityGroup` |


```@docs
one(::RootOfUnityGroup)
```

```@docs
*(::RootOfUnity, ::RootOfUnity)
```

```@docs
Nemo.AcbField(::RootOfUnity)
```
