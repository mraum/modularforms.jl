# $\mathbb{Q}^{\mathrm{ab}}$

```@docs
cf_elem(::Int, ::Union{Int,Rational{Int},fmpz,fmpq,Vector{fmpz},Vector{fmpq}})
```
