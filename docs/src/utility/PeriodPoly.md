# Periodic polynomials
Bla bla look at [the article on periodic polynomials by Pasol and
Popa](https://arxiv.org/abs/1202.5802).

### Constructors
```@docs
PeriodPolySL2Z
```

### Interface
```@docs
ModularForms.degree(::PeriodPolySL2Z)
```

```@docs
ModularForms.period_polynomialspace(::Int)
```

### Evaluation
```@docs
act(::PeriodPolySL2Z, ::SL2ZElem)
```

```@docs
PeriodPolySL2Z(::SL2ZElem)
```
