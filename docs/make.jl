using Documenter
using ModularForms
import Nemo
import Nemo: fmpz, fmpq, acb
import AbstractAlgebra: Ring, GroupElem, RingElem

makedocs(
  format = Documenter.HTML(prettyurls = false),
  sitename = "ModularForms.jl",
  pages = [
    "index.md",
    "Generic.md",
    "Modular group" => [
                        "modular_group/SL2Z.md",
                        "modular_group/FareySymbol.md"
                        ],
    "Modular forms" => [
                        "modular_form/ModularFormsSpace.md",
                        "modular_form/ModularForm.md"
                       ],
    "Arithmetic types" => [
                           "arithmetic_types/SL2Z.md"
                           ],
    "Utilities" => [
                    "utility/ContinuedFraction.md",
                    "utility/RootOfUnity.md",
                    "utility/QQab.md",
                    "utility/PeriodPoly.md"
                   ]
   ]
)

deploydocs(
  repo   = "gitlab.com/mraum/modularforms.jl.git",
  target = "build"
)
