################################################################################
################################################################################
# trie with vector keys
################################################################################
################################################################################

mutable struct VectorTrie{K,A}
  val :: Union{Nothing,A}
  subtries :: Dict{K,VectorTrie{K,A}}

  function VectorTrie{K,Nothing}() where {K}
    throw(TypeError(:VectorTrie,
        "attempt to construct VectorTrie with values of type Nothing",
        Any, Nothing))
  end

  function VectorTrie{K,A}() where {K,A}
    trie = new{K,A}()
    trie.val = nothing
    trie.subtries = Dict{K,VectorTrie{K,A}}()
    return trie
  end

  function VectorTrie{K,A}(
      val :: Union{Nothing,A},
      subtries :: Dict{K,VectorTrie{K,A}}
     ) where {K,A}
    new{K,A}(val, subtries)
  end

  function VectorTrie{K,A}(
      subtries :: Dict{K,VectorTrie{K,A}}
     ) where {K,A}
    new{K,A}(nothing, subtries)
  end
end

mutable struct VectorTrieIteratorState{K,A}
  dicts  :: Vector{Dict{K,VectorTrie{K,A}}}
  states :: Vector{Int}
  k      :: Vector{K}
end

################################################################################
################################################################################
# submodules of vector spaces with an explicit map from generators
################################################################################
################################################################################

mutable struct SubmoduleAcc{A <: RingElem}
  mat :: MatElem{A}
  rkbd :: Int
  rkinc :: Int
end

# We need field elements, since otherwise the structure of generators is more
# complicated.
mutable struct SubmoduleAccWithGenMap{A <: FieldElem, G}
  # The first nrows(mat) entries of a row store the coordinates in the ambient
  # space.  The remaining entries store the coordinates in the generators.
  mat :: MatElem{A}
  gens :: Vector{G}
  gensacc :: Vector{G}
  gencoefflift :: Union{Nothing,Function}
end

mutable struct SubmoduleAccQQabWithGenMap{A, G}
  base_field :: Field
  dimension :: Int
  capacity :: Int
  pivots :: Vector{Int}
  # TODO: use gfp_fmpz_mat one in Nemo
  mats_finite :: Vector{Union{gfp_mat,AbstractAlgebra.Generic.MatSpaceElem{Nemo.gfp_fmpz_elem}}}
  mats_infty :: Vector{Union{arb_mat,acb_mat}}
  den_bound :: fmpz
  den_bound_arb :: arb
  primes :: PrimesSet{fmpz}
  prime_lower_bound :: fmpz
  gens :: Vector{G}
  gensacc :: Vector{G}
  genfcts :: Vector{Function}
  genfctsacc :: Vector{Function}
end

################################################################################
################################################################################
# trivial group
################################################################################
################################################################################

mutable struct TrivialGroup <: Group
end

mutable struct TrivialGroupElem <: GroupElem
end

parent_type(::Type{TrivialGroupElem}) = TrivialGroup
parent(::TrivialGroupElem) = TrivialGroup()
elem_type(::TrivialGroup) = TrivialGroupElem

################################################################################
################################################################################
# roots of unity
################################################################################
################################################################################

mutable struct RootOfUnity <: GroupElem
  exponent::fmpq
end

mutable struct RootOfUnityGroup <: Group
end

parent_type(::Type{RootOfUnity}) = RootOfUnityGroup
parent(::RootOfUnity) = RootOfUnityGroup()
elem_type(::RootOfUnityGroup) = RootOfUnity

################################################################################
################################################################################
# universal cyclotomic field
################################################################################
################################################################################

struct QQabField <: Field
end

mutable struct cf_elem <: FieldElem
  order::Int
  realization::nf_elem

  cf_elem(order::Int, a::nf_elem) = new(order, a)

  @doc Markdown.doc"""
      cf_elem(N::Int, a::Union{Int,Rational{Int},fmpz,fmpq,Vector{fmpz},Vector{fmpq}}))

  Return the cyclotomic field element $a_0 + a_1 \zeta_N + \cdots + a_n \zeta^n_N
  \in \mathbb{Q}(\zeta_N)$, where $\zeta_N$ is the $N$th primitive root of unity.
  """
  function cf_elem(order::Int, a::Union{Int,Rational{Int},fmpz,fmpq,Vector{fmpz},Vector{fmpq}})
    P, _ = PolynomialRing(QQ,qqab_subfield_var_name(order))
    return new(order, QQabSubfield(order)(P(a)))
  end
  
  function cf_elem(order::Int, poly::fmpq_poly)
    return new(order, QQabSubfield(order)(poly))
  end
end

parent_type(::Type{cf_elem}) = QQabField
parent(::cf_elem) = QQab
elem_type(::Type{QQabField}) = cf_elem

################################################################################
################################################################################
# cyclotomic towers and rings with MuInf action
################################################################################
################################################################################

abstract type MuInfTower end

################################################################################
# cyclotomic tower over CC
################################################################################

mutable struct CCMuInfTower <: MuInfTower
  acb_field :: AcbField
  muinf_embedding :: Dict{RootOfUnity, acb}

  function CCMuInfTower(f::AcbField)
    tower = new()
    tower.acb_field = f
    tower.muinf_embedding = Dict{Int,acb}()
    return tower
  end
end

################################################################################
# cyclotomic tower over QQ
################################################################################

mutable struct QQMuInfTower <: MuInfTower
  muinf_embeddings :: Dict{Int,Dict{RootOfUnity,nf_elem}}
  field_embeddings :: Dict{Tuple{Int,Int},fmpz_mat}
  field_retractions :: Dict{Tuple{Int,Int},Tuple{Vector{Int},fmpz_mat,fmpz,fmpz_mat}}
end

################################################################################
# cyclotomic tower over Galois fields
################################################################################

mutable struct GFpMuInfTower <: MuInfTower
  base_ring :: Union{GaloisField,GaloisFmpzField}
  # degree -> extension of base ring
  fields :: Union{Dict{Int,FqNmodFiniteField}, Dict{Int,FqFiniteField}}
  # order -> degree of extension ring
  muinf_degrees :: Dict{Int,Int}
  # degree -> maximal order of roots of unity
  muinf_orders :: Dict{Int,Int}
  # degree -> roots of unity
  muinf_embeddings :: Union{Dict{Int,Dict{RootOfUnity,fq_nmod}},Dict{Int,Dict{RootOfUnity,fq}}}
end

################################################################################
# modules over universal cyclotomic field
################################################################################

mutable struct QQabModule{T <: RingElem} <: Ring
  coeff_parent::Ring
end

mutable struct QQabModuleElem{T} <: RingElem
  parent::QQabModule{T}
  order::Int
  coeffs::Vector{T}
end

parent_type(::Type{QQabModuleElem{T}}) where T= QQabModule{T}
parent(a::QQabModuleElem{T}) where T = a.parent
elem_type(::QQabModule{T}) where T = QQabModuleElem{T}

################################################################################
################################################################################
# Dirichlet character
################################################################################
################################################################################

mutable struct DirichletGroup <: Group
  # we replicate the C structure to move memory management into Julia
  q::UInt                              # ulong q;                /* modulus */
  q_even::UInt                         # ulong q_even;           /* even part of modulus */
  # typedef struct
  # {
  #    mp_limb_t n;
  #    mp_limb_t ninv;
  #    flint_bitcnt_t norm;
  # } nmod_t;
  mod_n::Int                           # nmod_t mod;             /* modulus with precomputed inverse */
  mod_ninv::Int
  mod_norm::UInt
  rad_q::UInt                          # ulong rad_q;            /* radical = product of odd primes */
  phi_q::UInt                          # ulong phi_q;            /* phi(q) = group size */
  neven::Int                           # slong neven;            /* number of even components (in 0,1,2)*/
  num::Int                             # slong num;              /* number of prime components (even + odd) */
  expo::UInt                           # ulong expo;             /* exponent = largest order in G */
  dirichlet_prime_group_struct::Ptr{Nothing}  # dirichlet_prime_group_struct * P;
  generators::Ptr{UInt}                # ulong * generators;     /* generators lifted mod q */
  PHI::Ptr{UInt}                       # ulong * PHI;            /* PHI(k) = expo / phi(k)        */

  function DirichletGroup(q::UInt)
    G = new()
    ccall((:dirichlet_group_init, Nemo.libarb), Nothing, (Ref{DirichletGroup}, UInt), G, q)
    finalizer(G_f::DirichletGroup ->
              ccall((:dirichlet_group_clear, Nemo.libarb),
                    Nothing, (Ref{DirichletGroup}, ), G_f),
              G)
    return G
  end

  function DirichletGroup(dg::DirichletGroup, q::UInt)
    G = new()
    ccall((:dirichlet_subgroup_init, Nemo.libarb), Nothing, (Ref{DirichletGroup}, Ref{DirichletGroup}, UInt), G, dg, q)
    finalizer(G_f::DirichletGroup ->
              ccall((:dirichlet_group_clear, Nemo.libarb),
                    Nothing, (Ref{DirichletGroup}, ), G_f),
              G)
    return G
  end
end

mutable struct DirichletGroupPrimitive <: Set
  group :: DirichletGroup
  length :: Int
end

mutable struct DirichletCharacter <: GroupElem
  # we replicate the C structure to move memory management into Julia
  n::UInt         #  ulong n;           /* number */
  log::Ptr{UInt}  #  ulong * log;       /* s.t. prod generators[k]^log[k] = number */ 
  # end of C structure
  parent::DirichletGroup

  function DirichletCharacter(G::DirichletGroup)
    chi = new()
    ccall((:dirichlet_char_init, Nemo.libarb), Nothing,
          (Ref{DirichletCharacter}, Ref{DirichletGroup}),
          chi, G)
    finalizer(chi_f::DirichletCharacter ->
              ccall((:dirichlet_char_clear, Nemo.libarb), Nothing,
                    (Ref{DirichletCharacter}, ), chi_f),
             chi)
    chi.parent = G
    return chi
  end
end


parent_type(::Type{DirichletCharacter}) = DirichletGroup
parent(chi::DirichletCharacter) = chi.parent
elem_type(::DirichletGroup) = DirichletCharacter

################################################################################
################################################################################
# SL2Z modular group
################################################################################
################################################################################

mutable struct PSL2ZCusp
  isinfty :: Bool
  q :: fmpq
end

# Following the convenition in AbstractAlgebra, we use little z to refer to
# machine size integers.
mutable struct SL2zElem <: GroupElem
  a :: Int
  b :: Int
  c :: Int
  d :: Int
end

mutable struct SL2ZElem <: GroupElem
  a::fmpz
  b::fmpz
  c::fmpz
  d::fmpz

  @doc Markdown.doc"""
      SL2ZElem()

  Return uninitialized element $\gamma \in \mathrm{SL}_2(\mathbb{Z})$. Currently
  returns the identity element.
  """
  function SL2ZElem()
    g = new()
    g.a = one(ZZ)
    g.b = zero(ZZ)
    g.c = zero(ZZ)
    g.d = one(ZZ)
    return g
  end

  function SL2ZElem(a::fmpz, b::fmpz, c::fmpz, d::fmpz)
    new(a,b,c,d)
  end

  @doc Markdown.doc"""
      SL2ZElem(a::Union{Int,fmpz}, b::Union{Int,fmpz}, c::Union{Int,fmpz}, d::Union{Int,fmpz})

  Return $\begin{pmatrix} a & b \\ c & d \end{pmatrix} \in \mathrm{SL}_2(\mathbb{Z})$.

  Note: No checking is made to see that it actually belongs to $\mathrm{SL}_2(\mathbb{Z})$.
  """
  function SL2ZElem(a::Union{Int,fmpz}, b::Union{Int,fmpz}, c::Union{Int,fmpz}, d::Union{Int,fmpz})
    g = new()
    g.a = ZZ(a)
    g.b = ZZ(b)
    g.c = ZZ(c)
    g.d = ZZ(d)
    return g
  end
end


mutable struct FareySymbolSL2Z
  iselement :: Function
  iselement_plusminus :: Function

  pairings::Vector{Int}
  pairing_matrices::Vector{SL2ZElem}
  pairing_matrices_are_in_group::Vector{Bool}
  pairings_with::Vector{Int}
  nu2::Int
  nu3::Int

  as::Vector{fmpz}
  bs::Vector{fmpz}
  xs::Vector{fmpq}

  # a cusp class index assigned to each fraction in xs
  cusp_classes::Vector{Int}
  # the reverse dictionary: fraction indices are assigned to cusp class indices
  cusp_class_x_indices::Vector{Vector{Int}}
  # finite representatives of the cusp, we assume that infinity is always a cusp
  cusp_class_finite_reps::Vector{fmpq}
  # twice the width that is assigned to a fraction
  cusp_double_widths::Vector{Int}
  # width of a cusp class
  cusp_class_widths::Vector{Int}
  # reduction of a fraction to a representative of the cusp class
  cusp_reductions::Vector{SL2ZElem}

  generators::Vector{SL2ZElem}
  generators_from_pairing_matrices::Vector{Tuple{Int,Bool,Bool}}
  generator_odd_ix::Tuple{Int,Int}
  generator_dict::Dict{SL2ZElem,Tuple{Int,Bool,Bool}}
  even::Bool

  coset_reps::Vector{SL2ZElem}

  @doc Markdown.doc"""
      FareySymbolSL2Z(iselement::Function, iselement_plusminus::Function)

  Return Farey symbol associated to group determined by containment functions.
  """
  function FareySymbolSL2Z(iselement::Function, iselement_plusminus::Function)
    fs = new()

    if iselement(SL2ZElem(0,-1,1,0)) && iselement(SL2ZElem(1,1,0,1))
      FareySymbolSL2Z_MODULE.init_SL2Z!(fs)
    elseif iselement(SL2ZElem(0,1,-1,-1)) && iselement(SL2ZElem(-1,1,-1,0))
      FareySymbolSL2Z_MODULE.init_ix_2_4!(fs, iselement, iselement_plusminus)
    else
      FareySymbolSL2Z_MODULE.init_generic!(fs, iselement, iselement_plusminus)
    end

    return fs
  end
end 


mutable struct SL2ZFullGroup <: Group
  farey_symbol :: FareySymbolSL2Z
end

@attributes mutable struct SL2ZSubgroup <: Group
  farey_symbol :: FareySymbolSL2Z

  @doc Markdown.doc"""
      SL2ZSubgroup(farey_symbol::FareySymbolSL2Z)

  Return subgroup according to Farey symbol.
  """
  function SL2ZSubgroup(farey_symbol::FareySymbolSL2Z)
    group = new()
    group.farey_symbol = farey_symbol
    return group
  end
end

mutable struct SL2ZSubgroupElem <: GroupElem
  parent :: SL2ZSubgroup
  ga :: SL2ZElem

  @doc Markdown.doc"""
      SL2ZSubgroupElem(parent::SL2ZSubgroup, ga::SL2ZElem)

  Return element with given parent as parent.
  """
  SL2ZSubgroupElem(parent::SL2ZSubgroup, ga::SL2ZElem) = new(parent, ga)
end

const SL2ZGroup = Union{SL2ZFullGroup, SL2ZSubgroup}
const SL2ZElement = Union{SL2ZElem, SL2ZSubgroupElem}


mutable struct SL2ZSubgroupCover <: Group
  base_group :: SL2ZGroup

  # elementary divisors of the extension of the center of
  # SL(2,Z)
  centerext :: Vector{Int} 
  cocycle :: Any
end

mutable struct SL2ZSubgroupCoverElem <: GroupElem
  parent :: SL2ZSubgroupCover
  ga :: SL2ZElem
  om :: Vector{Int}
end

const SL2ZCover = Union{SL2ZGroup, SL2ZSubgroupCover}
const SL2ZCoverElement = Union{SL2ZElement, SL2ZSubgroupCoverElem}

parent_type(::Type{SL2ZSubgroupCoverElem}) = SL2ZSubgroupCover
parent(a::SL2ZSubgroupCoverElem) = a.parent
elem_type(group::Type{SL2ZSubgroupCover}) = SL2ZSubgroupCoverElem

################################################################################
# iteration over Γ_∞ \\ (SL₂(ℤ) - Γ_∞)
################################################################################

@doc Markdown.doc"""
    ParabolicDoubleCosetSL2ZIterator(c0::Int)

Return an iterator over the parabolic double coset
$\Gamma_\infty \backslash\backslash (\mathrm{SL}_2(\bbZ) - \Gamma_\infty)$.

The iterator itself returns a tuple `(dlen, alphavec)` which instructs how next
element in the sequence should be instructed, where `dlen` is the relative
length we have to go back from the previous element $\gamma$ in the sequence we
have to go back in order to construct the new element $\delta$. Then $\delta$ is
constructed via $\delta = S T^{\alpha_n} \cdots S T^{\alpha_1} \gamma$, where
$\alpha_j$ are the elements in `alphavec`.

!!! warning

    Will not include the identity element of
    $\Gamma_\infty \backslash\backslash \mathrm{SL}_2(\bbZ)$. 

The following example demonstrates a function which prints all elements in
the set from $c = 1$ to $c = c_0$.
# Examples
```julia
function printgamma(c0::Int)
  iter, n = DoubleCosetSL2ZIterator(c0)
  ga = Vector{SL2ZElem}(undef, n + 1)
  ix = 1
  ga[1] = SL2ZElem(0, -1, 1, 0)     # The generator S
  for (dlen, alpha) in iter
    ix -= dlen
    for al in alpha
      ix += 1
      ga[ix] = SL2ZElem(0, -1, 1, al) * ga[ix - 1]
    end
    println(ga[ix])
  end
end
```
"""
struct ParabolicDoubleCosetSL2ZIterator
  c0 :: Int
  
  function ParabolicDoubleCosetSL2ZIterator(c0::Int)
    c0 < 1 && DomainError(c0, "Iterator only iterates over |c| greater or equal to 1.")
    return new(c0), 1 + floor(Int, 2.08 * log(c0))
  end
end

IteratorSize(::Type{ParabolicDoubleCosetSL2ZIterator}) = Base.HasLength()

################################################################################
################################################################################
# (split) paramodular group
# 
# Since the paramodular groups are sometimes refered to as the compact twists
# of Sp(n,Z), we use SpnZTwist as a general naming pattern
################################################################################
################################################################################

@doc Markdown.doc"""
Covers of paramodular groups associated with diagonal level matrix.

For example, genus = 2, paramodular = [p] yields the paramodular group with
elements of the form
  ( Z   p Z | Z       Z )
  ( Z     Z | Z   1/p Z )
  ( Z   p Z | Z       Z )
  ( p Z p Z | p Z     Z )
"""
struct SpnZTwistCoverGroup <: Group
  genus :: Int

  # list of non-trivial paramodular levels
  paramodularlevel :: Vector{Int}

  centerext :: Vector{Int}
  cocycle :: Function
end

struct SpnZTwistCoverElem <: GroupElem
  parent :: SpnZTwistCoverGroup

  a :: fmpz_mat
  b :: fmpq_mat
  c :: fmpz_mat
  d :: fmpz_mat
  
  om :: Vector{Int}

  function SpnZTwistCoverElem(parent::SpnZTwistCoverGroup,
      a::fmpz_mat, b::fmpq_mat, c::fmpz_mat, d::fmpz_mat)
    new(parent, a,b,c,d, fill(0,length(parent.centerext)))
  end

  function SpnZTwistCoverElem(
      parent::SpnZTwistCoverGroup,
      a::Union{Matrix{Int},fmpz_mat},
      b::Union{Matrix{Int},Matrix{Rational{Int}},fmpz_mat,fmpq_mat},
      c::Union{Matrix{Int},fmpz_mat},
      d::Union{Matrix{Int},fmpz_mat}
     )
    g = new()
    g.parent = parent
    ms = isa(a,fmpz_mat) ? parent(a) : MatrixSpace(ZZ, size(a)...)
    g.a = ms(a)
    g.b = isa(b,fmpq_mat) ? b : MatrixSpace(QQ, nrows(ms), ncols(ms))(b)
    g.c = ms(c)
    g.d = ms(d)
    g.om = fill(0,length(parent.centerext))
    return g
  end
end

################################################################################
################################################################################
# Heisenberg group
################################################################################
################################################################################

## Heisenberg group for the standard symplectic form
## on Abelian groups of size rank * genux
## with image in rank * rank matrices
mutable struct HeisenbergSpnZTwistGroup <: Group
  genus :: Int
  rank :: Int

  paramodularlevel :: Vector{Int}
end

mutable struct HeisenbergSpnZTwistGroupElem <: GroupElem
  parent::HeisenbergSpnZTwistGroup

  ## the row index corresponds to the rank coordinates
  ## the col index corresponds to the genus coordinates
  la::fmpq_mat
  mu::fmpq_mat
  ka::fmpq_mat
end

parent_type(::Type{HeisenbergSpnZTwistGroupElem}) = HeisenbergSpnZTwistGroup
parent(h::HeisenbergSpnZTwistGroupElem) = h.parent
elem_type(::HeisenbergSpnZTwistGroup) = HeisenbergSpnZTwistGroupElem

################################################################################
################################################################################
# Jacobi groups
################################################################################
################################################################################

mutable struct JacobiGroup{G <: GroupElem, E <: GroupElem} <: Group
  base_group :: Group
  extension_group :: Group
end

mutable struct JacobiGroupElem{G <: GroupElem, E <: GroupElem} <: GroupElem
  parent::JacobiGroup{G,E}

  base_element::G
  extension_element::E
end

parent_type(::Type{JacobiGroupElem{G,E}}) where {G,E} = JacobiGroup{G,E}
parent(gJ::JacobiGroupElem{G, E}) where {G,E} = gJ.parent
function elem_type(GJ::JacobiGroup{G,E}) where {G,E} 
  JacobiGroupElem{elem_type(GJ.base_group), elem_type(GJ.extension_group)}
end

################################################################################
################################################################################
# images of arithmetic types
################################################################################
################################################################################

################################################################################
# matrix group
################################################################################

mutable struct MatrixGroup{T <: RingElem} <: Group
  matrix_parent::MatSpace
  coefftower::MuInfTower
end

mutable struct MatrixGroupElem{T <: RingElem} <: GroupElem
  parent :: MatrixGroup{T}
  matrix :: MatElem{T}
end

################################################################################
# twisted permutations
################################################################################

mutable struct TwistedPermGroup{T <: GroupElem} <: Group
  twist_parent::Group
  permsize::Int
end

mutable struct TwistedPerm{T <: GroupElem} <: GroupElem
  parent::TwistedPermGroup{T}
  perms::Vector{Int}
  twists::Vector{T}
  permsinv::Vector{Int}
  twistsinv::Vector{T}
end

################################################################################
# transformations in symmetric power space
################################################################################

mutable struct SymTransformationGroup{G <: GroupElem} <: Group
  coeffring :: Ring
  group :: Group
  degree :: Integer
  polyring :: PolyRing
  dualpolyring :: PolyRing
  isdual :: Bool

  @doc Markdown.doc"""
      SymTransformationGroup(cr::Ring, gp::Group, deg::Integer, symbol::Symbol, dualsymbol::Symbol)

  Return the group generated by $\mathrm{sym}^{\texttt{deg}}(X)(g) where $g$
  traverses the generators of $\texttt{gp}$. This groups acts on
  $\texttt{cr}[X]_{\texttt{deg}}$.
  """
  function SymTransformationGroup(cr::Ring, gp::Group, deg::Integer, symbol::Symbol, dualsymbol::Symbol)
    deg >= 0 || throw("negative degree not allowed")
    stg = new{elem_type(gp)}()
    stg.coeffring = cr
    stg.group = gp
    stg.degree = deg
    (stg.polyring,_) = PolynomialRing(cr,symbol)
    (stg.dualpolyring,_) = PolynomialRing(cr,dualsymbol)
    stg.isdual=false
    return stg
  end

  function SymTransformationGroup{G}(cr::Ring, gp::Group, deg::Integer,
                                     pr::PolyRing, dpr::PolyRing, id::Bool) where G
    stg = new{G}()
    stg.coeffring = cr
    stg.group = gp
    stg.degree = deg
    stg.polyring = pr
    stg.dualpolyring = dpr
    stg.isdual = id
    return stg
  end
end

mutable struct SymTransformation{G <: GroupElem} <: GroupElem
  parent :: SymTransformationGroup{G}
  group_elem :: G
end

################################################################################
################################################################################
# arithmetic types
################################################################################
################################################################################

abstract type ArithType{G <: GroupElem, I <: GroupElem} end

################################################################################
# trivial arithmetic type
################################################################################

@attributes mutable struct TrivialArithType{G,I} <: ArithType{G,I}
  group :: Group
  image_parent :: Group

  function TrivialArithType{G,I}(
      group::Group,
      image_parent::Group
     ) where {
      G <: GroupElem,
      I <: GroupElem
     }
    a = new{G,I}()
    a.group = group
    a.image_parent = image_parent
    return a
  end
end

################################################################################
# generic arithmetic types
################################################################################

@attributes mutable struct GenArithType{G <: GroupElem, I <: GroupElem} <: ArithType{G,I}
  group::Group
  image_parent::Group
  gen_images :: Vector{I}

  function GenArithType{G,I}(
      group::Group,
      image_parent::Group,
      gen_images::Vector{I}
     ) where {
      G <: GroupElem,
      I <: GroupElem
     }
    a = new{G,I}()
    a.group = group
    a.image_parent = image_parent
    a.gen_images = gen_images
    return a
  end
end

################################################################################
# Dirchlet character arithmetic type
################################################################################

@attributes mutable struct DirichletType{G <: GroupElem} <: ArithType{G,RootOfUnity}
  group :: Group
  chi   :: DirichletCharacter

  function DirichletType(
      grp :: Group,
      chi :: DirichletCharacter
     )
    rho = new{elem_type(grp)}()
    rho.group = grp
    rho.chi = chi
    return rho
  end
end

################################################################################
# restriction of arithmetic types
################################################################################

# FIXME: remove attributes once level is inferred
@attributes mutable struct ResArithType{G <: GroupElem, H <: GroupElem, I <: GroupElem} <: ArithType{G,I}
  group :: Group
  restricted_type :: ArithType{H,I}
  gen_images :: Union{Missing,Vector{I}}

  function ResArithType(
      grp             :: Group,
      restricted_type :: ArithType{G,I}
     ) where {
      G <: GroupElem,
      I <: GroupElem
     }
    new{elem_type(grp),G,I}(grp, restricted_type, missing)
  end
end

################################################################################
# pullback of arithmetic types
################################################################################

# FIXME: remove attributes once level is inferred

# NOTE: Not implement, only to reserve name.
@attributes mutable struct PullbackArithType{G <: GroupElem, H <: GroupElem, I <: GroupElem} <: ArithType{G,I}
  group     :: Group
  base_type :: ArithType{H,I}
  pullback  :: Function
end

# FIXME: remove attributes once level is inferred
# Special case of the pull back G -> G' given by g |-> h*g*inv(h).
@attributes mutable struct PullbackConjArithType{G <: GroupElem, H <: GroupElem, GH <: GroupElem, I <: GroupElem} <: ArithType{G,I}
  group             :: Union{Missing,Group}
  base_type         :: ArithType{H,I}
  pullback_elem     :: GH
  pullback_elem_inv :: GH

  function PullbackConjArithType{G,H,GH,I}(
      group             :: Union{Missing,Group},
      base_type         :: ArithType{H,I},
      pullback_elem     :: GH,
      pullback_elem_inv :: GH
     ) where {
      G <: GroupElem, H <: GroupElem, GH <: GroupElem, I <: GroupElem
     }
    rho = new{G,H,GH,I}()
    rho.group             = group
    rho.base_type         = base_type
    rho.pullback_elem     = pullback_elem
    rho.pullback_elem_inv = pullback_elem_inv
    return rho
  end
end

################################################################################
# induction of arithmetic types
################################################################################

@attributes mutable struct IndArithType{G <: GroupElem, H <: GroupElem, T <: GroupElem} <: ArithType{G,TwistedPerm{T}}
  group        :: Group
  image_parent :: Group
  base_type    :: ArithType{H,T}
  leftright    :: Symbol
  coset_reps   :: Vector{G}

  function IndArithType{G,H,T}(
      group        :: Group,
      image_parent :: Group,
      base_type    :: ArithType{H,T},
      leftright    :: Symbol,
      coset_reps   :: Vector{G}
     ) where {G, H, T}
    rho = new{G,H,T}()
    rho.group        = group
    rho.image_parent = image_parent
    rho.base_type    = base_type
    rho.leftright    = leftright
    rho.coset_reps   = coset_reps
    return rho
  end
end

################################################################################
# tensor arithmetic types
################################################################################

@attributes mutable struct TensorArithType{G <: GroupElem, I <: GroupElem} <: ArithType{G,I}
  group::Group
  image_parent::Group
  components :: Vector{ArithType}

  function TensorArithType{G,I}(
      group::Group,
      image_parent::Group,
      components :: Vector
     ) where {
      G <: GroupElem,
      I <: GroupElem
     }
    a = new{G,I}()
    a.group = group
    a.image_parent = image_parent
    a.components = components
    return a
  end
end

################################################################################
################################################################################
# transformations in Weil representations
################################################################################
################################################################################

# TODO: move this up to the other images once the dependence on GenArithType has
# been removed
mutable struct WeilImage{G <: GroupElem} <: Group
  preimage_parent  :: Group
  lattice          :: Hecke.ZLat
  matrix_type      :: GenArithType{G,MatrixGroupElem{cf_elem}}
  matrix_type_dual :: GenArithType{G,MatrixGroupElem{cf_elem}}
end

mutable struct WeilTransformation{G <: GroupElem} <: GroupElem
  parent   :: WeilImage{G}
  preimage :: G
end

################################################################################
################################################################################
# translation decomposition associated with arithmetic types
################################################################################
################################################################################

################################################################################
# translation orbit
################################################################################

# A structure to encode the action of a one-generated abelian group; Usually
# the translations T^ZZ in the modular group. To cover the cases of twisted
# permutation representations, we include an orbit description. Then the twist
# for an orbit of length N is the root of unity by which T^N acts. If there is
# no orbit, then we let N=1. For the time being, we only support the case that
# T^N acts as via diagonally on the initial block associated with the orbit. We
# store the component index inside this block.

# TODO: Later, we want to allow for a modified Jordan normal form, arising from
# the action of T on polynomials (T p)(X) = p(X T^-1) = p(X - 1).

# We also have transitions between the orbit elements. It must be given by
# group elements that act on the twist parent.

# The Fourier expansion shifts are the possible shifts in the Puiseux series
# that can occur for functions that are T-covariant with respect to this orbit.
# Think of it as a row vector. Then the Fourier coefficients twists are the
# constants needed to recover the ix-th component. The jx-th column covers the
# shifts needed for the coefficients of jx-th shift.

mutable struct TranslationOrbit
  orbit             :: Vector{Int}
  orbit_transitions :: Vector{GroupElem}
  component_dim     :: Int
  components        :: Vector{Int}
  twist             :: RootOfUnity

  fourier_expansion_shifts :: Vector{Rational{Int}}
  fourier_coefficient_twists :: Matrix{RootOfUnity}
end

################################################################################
# decomposition into translation orbits
################################################################################

# A structure reflecting the decomposition of an action of a twisted
# permutation or a general action. 

# The pivot elements for translation covariant expansions record the index in
# deflated vectors and the associated shift in the Fourier expansion. This is
# ordered by shifts ordered.

mutable struct TranslationDecomposition
  orbits :: Vector{TranslationOrbit}

  pivots_with_shift :: Vector{Tuple{Int,Rational{Int}}}
end


################################################################################
################################################################################
# homomorphisms of arithmetic types
################################################################################
################################################################################

abstract type ArithTypeHomSpace{D <: ArithType, C <: ArithType} <: Set end

abstract type ArithTypeHom{D <: ArithType, C <: ArithType} <: SetElem end

################################################################################
# generic arithmetic type hom spaces
################################################################################

mutable struct GenArithTypeHom{
    D <: ArithType,
    C <: ArithType
   } <: ArithTypeHom{D,C}
  parent :: ArithTypeHomSpace{D,C}
  orbits_init :: Vector{Int}
  orbits_gens :: Vector{Vector{G}} where {G <: GroupElem}
  mat :: Union{Nothing,MatElem}
end

mutable struct GenArithTypeHomSpace{
    D <: ArithType,
    C <: ArithType,
   } <: ArithTypeHomSpace{D,C}
  domain :: D
  codomain :: C
  basis :: Union{Missing,Vector}

  function GenArithTypeHomSpace(domain::D, codomain::C) where {D,C}
    homspace = new{D,C}()
    homspace.domain = domain
    homspace.codomain = codomain
    homspace.basis = missing
    return homspace
  end
end

################################################################################
################################################################################
# period polynomial space
################################################################################
################################################################################

# TODO: integrate into AbstractAlgebra hierarchy
mutable struct PeriodPolySL2ZSpace{C <: RingElem} <: Module{C}
  coeff_ring :: Ring
  degree     :: Int
  basis      :: fmpz_mat

  function PeriodPolySL2ZSpace(
      coeff_ring :: Ring,
      degree     :: Int,
      basis      :: fmpz_mat
     )
    new{elem_type(coeff_ring)}(coeff_ring, degree, basis)
  end
end

mutable struct PeriodPolySL2Z{C <: RingElem} <: ModuleElem{C}
  parent :: PeriodPolySL2ZSpace{C}
  coord  :: Vector{C}
end

# The value of a right cocycle associated with a given period polynomial
mutable struct PeriodPolySL2ZRCocycleValue{C <: RingElem} <: ModuleElem{C}
  period_poly :: PeriodPolySL2Z{C}
  poly_coefffs :: Vector{C}
end

################################################################################
################################################################################
# analytic types
################################################################################
################################################################################

mutable struct SL2RCoverWeightFamily <: Group
end

mutable struct SL2RCoverWeight <: GroupElem
  k::fmpq
end

const SL2RWeight = SL2RCoverWeight

parent_type(::Type{SL2RCoverWeight}) = SL2RCoverWeightFamily
parent(::SL2RCoverWeight) = SL2RCoverWeightFamily()
elem_type(::SL2RCoverWeightFamily) = SL2RCoverWeight


mutable struct SpnRCoverWeightFamily <: Group
end

mutable struct SpnRCoverWeight <: GroupElem
  genus :: Int
  k :: fmpq
  # A vector of lambdas, i.e. heighst weight vectors for sln, corresponding to
  # the Kronecker product of the underlying matrix represenations
  lambdas :: Vector{Vector{fmpz}}
end

parent_type(::Type{SpnRCoverWeight}) = SpnRCoverWeightFamily
parent(::SpnRCoverWeight) = SpnRCoverWeightFamily()
elem_type(::SpnRCoverWeightFamily) = SpnRCoverWeight


mutable struct SpnRJacobiCoverWeightFamily <: Group
end

mutable struct SpnRJacobiCoverWeight <: GroupElem
  genus :: Int
  sigma :: SpnRCoverWeight
end

parent_type(::Type{SpnRJacobiCoverWeight}) = SpnRJacobiCoverWeightFamily
parent(::SpnRJacobiCoverWeight) = SpnRJacobiCoverWeightFamily()
elem_type(::SpnRJacobiCoverWeightFamily) = SpnRJacobiCoverWeight

################################################################################
################################################################################
# modular forms
################################################################################
################################################################################

abstract type ModularFormsSpace <: Ring end

abstract type ModularForm <: RingElem end

################################################################################
# symbolic modular forms
################################################################################

abstract type ModularFormSymbolic end

abstract type ModularFormsSymbolicRing end

abstract type ModularFormsSymbolicRingElem end

################################################################################
# vector entries (Fourier expansions)
################################################################################

const ModularFormVectorEntryQQab = Union{
  cf_elem,
  PolyElem{cf_elem},
  NCPolyElem{cf_elem},
  MPolyElem{cf_elem},
  SeriesElem{cf_elem},
  PuiseuxSeriesElem{cf_elem},
  PuiseuxSeriesFieldElem{cf_elem}
  }

const ModularFormVectorEntryCC = Union{
  acb,
  PolyElem{acb},
  NCPolyElem{acb},
  MPolyElem{acb},
  SeriesElem{acb},
  PuiseuxSeriesElem{acb},
  PuiseuxSeriesFieldElem{acb}
  }

const ModularFormVectorEntry = Union{
  ModularFormVectorEntryQQab,
  ModularFormVectorEntryCC
  }

################################################################################
################################################################################
# elliptic modular forms
################################################################################
################################################################################

################################################################################
# symbolic
################################################################################

mutable struct ModularFormsSL2ZCongruenceSymbolicRing{
    RT <: MuInfTower,
    RB <: RingElement, # base ring type
    R <: RingElement   # tower ring type
   } <: Ring
  coefftower :: RT
end

mutable struct ModularFormsSL2ZCongruenceSymbolicRingElem{
    RT <: MuInfTower,
    RB <: RingElement,
    R <: RingElement
   } <: RingElem
  parent    :: ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}
  coeffring :: Ring
  terms     :: VectorTrie{ModularFormSymbolic,R}
end

################################################################################
# modular forms of congruence type
################################################################################

mutable struct ModularFormsSpaceSL2ZCongruenceType{
    I <: GroupElem,
    R <: RingElem
   } <: ModularFormsSpace
  weight        :: SL2RCoverWeight
  arithtype     :: ArithType{G,I} where {G <: SL2ZElement}
  scalar_valued :: Bool
  coeffring     :: Ring
  coefftower    :: MuInfTower
  basis         :: Union{Missing,Vector{ModularForm}}
end

mutable struct ModularFormSL2ZCongruenceType{
    I <: GroupElem,
    R <: RingElem
   } <: ModularForm
  parent            :: ModularFormsSpaceSL2ZCongruenceType{I,R}
  # NOTE: We cannot specify the type parameters of
  # ModularFormsSL2ZCongruenceSymbolicRingElem as they are not available from
  # the type of the modular form. The parameters are generated on runtime in
  # the corresponding property functions.
  symbolic          :: Union{Missing,
                             Vector{ModularFormsSL2ZCongruenceSymbolicRingElem}}
  fourier_expansion :: Union{Missing,Vector{PuiseuxSeriesElem}}
end

################################################################################
# Siegel modular forms (for paramodular groups)
################################################################################

mutable struct ModularFormsSiegelSpace <: ModularFormsSpace
  genus::Int
end

mutable struct ModularFormSiegel <: ModularForm
  parent :: ModularFormsSiegelSpace

  # Assigns to each co-genus a Fourier-Jacobi expansion. In particular,
  # co-genus n yields the Fourier expansion.
  fourier_jacobi_expansions :: Dict{Any}
end

################################################################################
# iterator types
################################################################################

struct OrbitChainIterator{G <: GroupElem}
  grp          :: Group
  grp_gens     :: Vector{G}
  grp_gap      :: Any
  permsizes    :: Vector{Int}
  permshifts   :: Vector{Int}
  rho_gap      :: Any
  provide_stab :: Bool
end

IteratorSize(::Type{OrbitChainIterator{G}}) where G = Base.SizeUnknown()

mutable struct OrbitChainIteratorState{G <: GroupElem}
  orbits_init   :: Vector{Vector{Int}}
  orbit_init    :: Vector{Int}
  stab          :: Vector{Any} # GAP
  orbit_cosets  :: Vector{Vector{G}}
  sx0           :: Int

  # orbits_init[sx]: An arbitrary choice of elements in each unprocessed orbits.
  # orbit_init[sx]: An arbitrary choice of elements in the current orbit.
  # stab[sx]: The stabilizer in the image of grp of orbit_init[sx].
  # orbit_cosets[sx]: The left coset representatives associated with the
  #   current orbit relative to orbit_init[sx].
  # sx0: The sigma component from which we have to recompute orbits.

  function OrbitChainIteratorState{G}(nsigmas::Int) where G
    state = new{G}()
    state.orbits_init   = Vector{Vector{Int}}(undef, nsigmas)
    state.orbit_init   = Vector{Int}(undef, nsigmas)
    state.stab          = Vector{Any}(undef, nsigmas)
    state.orbit_cosets  = Vector{Vector{G}}(undef, nsigmas)
    state.sx0 = 0
    return state
  end
end
