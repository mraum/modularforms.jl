################################################################################
################################################################################
# family functions
################################################################################
################################################################################

################################################################################
################################################################################
# family member functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

function ==(a::SpnRCoverWeight, b::SpnRCoverWeight)
  (genus(a) == genus(b) &&
   detexponent(a) == detexponent(b) &&
   heighest_weights_sln(a) == heighest_weights_sln(b)
  )
end

################################################################################
# properties
################################################################################

genus(sigma::SpnRCoverWeight) = sigma.genus

detexponent(sigma::SpnRCoverWeight) = sigma.k

heighest_weights_sln(sigma::SpnRCoverWeight) = sigma.lambdas

function irrepexponents(sigma::SpnRCoverWeight)
  n = genus(sigma)

  n == 1 && return fill([], nmb_tensor_factors(sigma))

  return [push!([la[jx] - la[jx+1] for jx in 1:n-2], la[n-1])
          for la in heighest_weights_sln(sigma)]
end

nmb_tensor_factors(sigma::SpnRCoverWeight) = length(heighest_weights_sln(sigma))

# this is (7.18) of Goodman, Wallach, Symmetry, Representations, and
# Invariants, which is written in such a way that it applies to GL(n,C)
# directly
function dim(sigma::SpnRCoverWeight)
  n = genus(sigma)

  if n == 1 || nmb_tensor_factors(sigma) == 0
    return 1
  elseif n == 2
    return prod(la[1] + 1 for la in heighest_weights_sln(sigma))
  else
    return prod(divexact(prod(la[i] - la[j] + j - i
                              for i in 1:n-2 for j in i+1:n-1) *
                         (la[n-1] + 1),
                         prod(factorial(i) for i in 1:n-1))
                for la in heighest_weights_sln(sigma))
  end

end

################################################################################
# show
################################################################################

function show(io::IO, sigma::SpnRCoverWeight)
  if isone(denominator(detexponent(sigma)))
    print(io, "det^$(numerator(detexponent(sigma)))")
  else
    print(io, "det^($(detexponent(sigma)))")
  end

  for la in heighest_weights_sln(sigma)
    print(io, " ⊗ SL($(genus(sigma)),CC)[$(join(la,','))]")
  end
end

################################################################################
# creation
################################################################################

SpnRCoverWeight(genus::Int, k::Union{Int,fmpz}) = SpnRCoverWeight(genus, fmpq(k), [])

################################################################################
# arithmetic
################################################################################

function tensor_product(a::SpnRCoverWeight, b::SpnRCoverWeight)
  genus(a) == genus(b) || error("incompatible genus")

  return SpnRCoverWeight(
      detexponent(a) + detexponent(b),
      vcat(heighest_weights_sln(a), heighest_weights_sln(b))
     )
end

################################################################################
# cocycle
################################################################################

function cocycle(sigma::SpnRCoverWeight, ga::SpnZElem, tau::acb_mat)
  isempty(irrepexponents(sigma)) || error("not implemented")
  return det(ga.c*tau + ga.d)^-detexponent(sigma)
end
