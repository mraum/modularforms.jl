################################################################################
################################################################################
# family functions
################################################################################
################################################################################

################################################################################
# display
################################################################################

function show(io::IO, a::SL2RCoverWeightFamily)
  print(io, "SL(2,ℤ)—cover—weights")
end

################################################################################
################################################################################
# family member functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

==(a::SL2RCoverWeight, b::SL2RCoverWeight) = a.k == b.k

################################################################################
# properties
################################################################################

dim(::SL2RCoverWeight) = 1

isintegral(a::SL2RCoverWeight) = isone(denominator(a.k))

################################################################################
# display
################################################################################

function show(io::IO, a::SL2RCoverWeight)
  if isone(denominator(a.k))
    print(io, numerator(a.k))
  else
    print(io, a.k)
  end
end

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    SL2RCoverWeight(k::Union{Int, fmpz, Rational{Int}, fmpq})

Return the cover weight of $\mathrm{SL}_2(\mathbb{R})$.
"""
function SL2RCoverWeight end

function SL2RCoverWeight(k::Union{Int, fmpz, Rational{Int}})
  SL2RCoverWeight(fmpq(k))
end

################################################################################
# conversion
################################################################################

function (::Type{Int})(a::SL2RCoverWeight)
  isone(denominator(a.k)) || error("weight not integral")
  return Int(numerator(a.k))
end

function (::Type{Rational{Int}})(a::SL2RCoverWeight)
  return Int(numerator(a.k))//Int(denominator(a.k))
end

################################################################################
# arithmetic
################################################################################

tensor_product(a::SL2RCoverWeight, b::SL2RCoverWeight) = SL2RCoverWeight(a.k + b.k)

dual(a::SL2RCoverWeight) = SL2RCoverWeight(-a.k)

################################################################################
# cocycle
################################################################################

function cocycle(wt::SL2RCoverWeight, g::SL2ZElem, tau::acb)
  @assert isone(denominator(wt.k))
  (g.c*tau + g.d)^-Int(numerator(wt.k))
end

cocycle(wt::SL2RCoverWeight, g::SL2ZSubgroupElem, tau::acb) = cocycle(wt, g.ga, tau)

function cocycle(wt::SL2RCoverWeight, g::SL2ZSubgroupCoverElem, tau::acb)
  cocycle(parent(g)) == mp1z_cocycle || error("not implemented")

  @assert isone(denominator(2*wt.k))
  k2 = Int(numerator(2*wt.k))

  CC = parent(tau)
  RR = ArbField(precision(CC))

  ga = base_group_element(g)
  om = cocycle_value(g)[1]

  if iszero(ga.c)
    if ga.d > 0
      j = om == 0 ? sqrt(CC(ga.d)) : -sqrt(CC(ga.d))
    else
      j = om == 0 ? onei(CC)*sqrt(CC(-ga.d)) : -onei(CC)*sqrt(CC(-ga.d))
    end
  else
    j = sqrt(ga.c*tau + ga.d)
    absanglej = abs(angle(j)) - const_pi(RR)//2
    if contains_zero(absanglej)
      error("insufficient precision to determine cocycle")
    elseif om == 0 && contains_positive(absanglej)
      j = -j
    elseif om == 1 && contains_negative(absanglej)
      j = -j
    end
  end

  return j^-k2
end
