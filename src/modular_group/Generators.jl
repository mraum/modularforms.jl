function gen_word(parent::G, word::Vector{Tuple{Int,Int}}) where {G <: Group}
  isempty(word) && return one(parent)
  gs = gens(parent)
  prod(gs[wx]^we for (wx,we) in word)
end
