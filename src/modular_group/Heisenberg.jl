################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

genus(gp::HeisenbergSpnZTwistGroup) = gp.genus

rank(gp::HeisenbergSpnZTwistGroup) = gp.rank

################################################################################
# comparison
################################################################################

function ==(a::HeisenbergSpnZTwistGroup, b::HeisenbergSpnZTwistGroup)
  genus(a) == genus(b) && rank(a) == rank(b) &&
      paramodularlevel(a) == paramodularlevel(b)
end

################################################################################
# creation
################################################################################

function siegel_modular_heisenberg_group(genus::Int, rank::Int)
  HeisenbergSpnZTwistGroup(genus, rank, [])
end

################################################################################
# generators
################################################################################

function ngens(gp::HeisenbergSpnZTwistGroup)
  2*genus(gp)*rank(gp) + div(rank(gp)*(rank(gp)+1), 2)
end

function gens(gp::HeisenbergSpnZTwistGroup)
  rank(gp) == 0 && return []

  # TODO: for the paramodular case, simply specify the entries as 1/N for
  # suitable N; implement variant of elementary_matrix
  paramodularlevel(gp) == [] || error("not implemented")

  return vcat(
    [gp(elementary_matrix(QQ, rank(gp), genus(gp), ix, jx),
         zero_matrix(QQ, rank(gp), genus(gp)))
     for ix in 1:rank(gp) for jx in 1:genus(gp)],
    [gp(zero_matrix(QQ, rank(gp), genus(gp)),
         elementary_matrix(QQ, rank(gp), genus(gp), ix, jx))
     for ix in 1:rank(gp) for jx in 1:genus(gp)],
    [gp(elementary_symmetric_matrix(QQ, rank(gp), rank(gp), ix, jx))
     for ix in 1:rank(gp) for jx in ix:rank(gp)]
    )
end

function word_in_gens(h::HeisenbergSpnZTwistGroupElem)
  gp = parent(h)
  # TODO: for the paramodular case multiply the generator accordingly
  paramodularlevel(gp) == [] || error("not implemented")

  g = genus(gp)
  rk = rank(gp)

  la = h.la
  mu = h.mu
  kalamu = h.ka - la*transpose(mu)

  return collect(enumerate(vcat(
    [Int(numerator(la[ix,jx]))
     for ix in 1:rank(gp) for jx in 1:genus(gp)],
    [Int(numerator(mu[ix,jx]))
     for ix in 1:rank(gp) for jx in 1:genus(gp)],
    [Int(numerator(kalamu[ix,jx]))
     for ix in 1:rank(gp) for jx in ix:rank(gp)]
   )))
end

function gen_word(parent::HeisenbergSpnZTwistGroup, word::Vector{Tuple{Int,Int}})
  isempty(word) && return zero(parent)
  gs = gens(parent)
  prod(gs[wx]^we for (wx,we) in word)
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# creation
################################################################################

function (gp::HeisenbergSpnZTwistGroup)()
  one(gp)
end

function (gp::HeisenbergSpnZTwistGroup)(la::Matrix{fmpq}, mu::Matrix{fmpq}, ka::Matrix{fmpq})
  size(la) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of lambda"))
  size(mu) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of mu"))
  size(ka) == (rank(gp), rank(gp)) ||
      throw(ArgumentError("incorrect size of kappa"))

  return HeisenbergSpnZTwistGroupElem(gp,
    matrix(QQ,la), matrix(QQ,mu), matrix(QQ,ka))
end

function (gp::HeisenbergSpnZTwistGroup)(la::MatElem{fmpq}, mu::MatElem{fmpq}, ka::MatElem{fmpq})
  size(la) == (rank(gp), genus(gp)) ||
    throw(ArgumentError("incorrect size of lambda"))
  size(mu) == (rank(gp), genus(gp)) ||
    throw(ArgumentError("incorrect size of mu"))
  size(ka) == (rank(gp), rank(gp)) ||
    throw(ArgumentError("incorrect size of kappa"))

  return HeisenbergSpnZTwistGroupElem(gp, la, mu, ka)
end

function (gp::HeisenbergSpnZTwistGroup)(la::Matrix{fmpq}, mu::Matrix{fmpq})
  size(la) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of lambda"))
  size(mu) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of mu"))

  return HeisenbergSpnZTwistGroupElem(gp,
      matrix(QQ, la), matrix(QQ, mu),
      zero_matrix(QQ, rank(gp), rank(gp))
      )
end

function (gp::HeisenbergSpnZTwistGroup)(la::MatElem{fmpq}, mu::MatElem{fmpq})
  size(la) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of lambda"))
  size(mu) == (rank(gp), genus(gp)) ||
      throw(ArgumentError("incorrect size of mu"))

  return HeisenbergSpnZTwistGroupElem(gp,
      la, mu, zero_matrix(QQ, rank(gp), rank(gp))
      )
end

function (gp::HeisenbergSpnZTwistGroup)(ka::Matrix{fmpq})
  size(ka) == (rank(gp), rank(gp)) ||
      throw(ArgumentError("incorrect size of kappa"))

  return HeisenbergSpnZTwistGroupElem(gp,
      zero_matrix(QQ, rank(gp), genus(gp)),
      zero_matrix(QQ, rank(gp), genus(gp)),
      matrix(QQ, ka)
      )
end

function (gp::HeisenbergSpnZTwistGroup)(ka::MatElem{fmpq})
  size(ka) == (rank(gp), rank(gp)) ||
      throw(ArgumentError("incorrect size of kappa"))

  return HeisenbergSpnZTwistGroupElem(gp,
      zero_matrix(QQ, rank(gp), genus(gp)),
      zero_matrix(QQ, rank(gp), genus(gp)),
      ka)
end

function one(gp::HeisenbergSpnZTwistGroup)
  return HeisenbergSpnZTwistGroupElem(gp,
      zero_matrix(QQ, rank(gp), genus(gp)),
      zero_matrix(QQ, rank(gp), genus(gp)),
      zero_matrix(QQ, rank(gp), rank(gp))
      )
end

function copy(a::HeisenbergSpnZTwistGroupElem)
  parent(a)(copy(a.la, a.mu, a.ka))
end

function deepcopy_internal(a::HeisenbergSpnZTwistGroupElem, stackdict::IdDict) where T
  if haskey(stackdict, a)
    return stackdict[a]
  end

  b = parent(a)(
          deepcopy_internal(a.la, stackdict),
          deepcopy_internal(a.mu, stackdict),
          deepcopy_internal(a.ka, stackdict))
  stackdict[a] = b
  return b
end

################################################################################
# comparison
################################################################################

function ==(h1::HeisenbergSpnZTwistGroupElem, h2::HeisenbergSpnZTwistGroupElem)
  parent(h1) == parent(h2) &&
    h1.la == h2.la && h1.mu == h2.mu && h1.ka == h2.ka
end

function isone(h::HeisenbergSpnZTwistGroupElem)
  iszero(h.la) && iszero(h.mu) && iszero(h.ka)
end

################################################################################
# aritmetic
################################################################################

function *(h1::HeisenbergSpnZTwistGroupElem, h2::HeisenbergSpnZTwistGroupElem)
  parent(h1) == parent(h2) || error("unequal parents")

  return HeisenbergSpnZTwistGroupElem(parent(h1), h1.la+h2.la, h1.mu+h2.mu,
    h1.ka + h2.ka + h1.la*transpose(h2.mu) - h1.mu*transpose(h2.la))
end

function mul!(
    hr::HeisenbergSpnZTwistGroupElem,
    h1::HeisenbergSpnZTwistGroupElem,
    h2::HeisenbergSpnZTwistGroupElem
   )
  if parent(hr) != parent(h1) || parent(hr) != parent(h2)
    throw(DomainError((hr,h1,h2), "incompatible parents"))
  end

  local t = parent(hr.ka)()
  hr.ka = add!(hr.ka, h1.ka, h2.ka)
  t = mul!(t, h1.la, transpose(h2.mu))
  hr.ka = add!(hr.ka, hr.ka, t)
  t = mul!(t, h2.mu, transpose(h2.la))
  hr.ka = add!(hr.ka, hr.ka, t)

  hr.la = add!(hr.la, h1.la, h2.la)
  hr.mu = add!(hr.mu, h1.mu, h2.mu)

  return hr
end

function Base.inv(h::HeisenbergSpnZTwistGroupElem)
  parent(h)(-h.la, -h.mu, - h.ka + h.la*transpose(h.mu) - h.mu*transpose(h.la))
end

function ^(h::HeisenbergSpnZTwistGroupElem, n::Int)
  if n >= 0
    return power_by_squaring(h, n)
  else
    return power_by_squaring(inv(h), -n)
  end
end

################################################################################
# actions: SL2Z and its covers
################################################################################

function *(h::HeisenbergSpnZTwistGroupElem, g::G) where {G <: Union{SL2ZElem, SpnZTwistCoverElem}}
  genus(parent(h)) == genus(parent(g)) || error("incorrect genus")

  return parent(h)(h.la*g.a + h.mu*g.c, h.la*g.b + h.mu*g.d, h.ka)
end

function *(h::HeisenbergSpnZTwistGroupElem, ga::G) where {G <: Union{SL2ZSubgroupElem,SL2ZSubgroupCoverElem}}
  return h * ga.ga
end

################################################################################
# random
################################################################################

rand(gp::HeisenbergSpnZTwistGroup, complexity) = rand(GLOBAL_RNG, gp, complexity)

function rand(r::AbstractRNG, gp::HeisenbergSpnZTwistGroup, complexity)
  # TODO: for the paramodular case, simply specify the entries as 1/N for
  # suitable N; implement variant of elementary_matrix
  paramodularlevel(gp) == [] || error("not implemented")

  la = matrix(QQ, rand(r, MatrixSpace(ZZ, rank(gp), genus(gp)), complexity))
  mu = matrix(QQ, rand(r, MatrixSpace(ZZ, rank(gp), genus(gp)), complexity))
  ka = matrix(QQ, rand(r, MatrixSpace(ZZ, rank(gp), rank(gp)), complexity))
  ka = ka + transpose(ka) +
       diag_matrix(QQ, [rand(r, ZZ, complexity) for ix in 1:rank(gp)])

  return gp(la, mu, ka)
end
