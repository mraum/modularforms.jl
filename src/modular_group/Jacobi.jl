################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

base_group(gp::JacobiGroup{G,E}) where {G,E} = gp.base_group

extension_group(gp::JacobiGroup{G,E}) where {G,E} = gp.extension_group

################################################################################
# creation
################################################################################

function Jacobi(group::G, extension::E) where {G <: Group, E <: Group}
  JacobiGroup{elem_type(group), elem_type(extension)}(group, extension)
end

################################################################################
# generators
################################################################################

function ngens(group::JacobiGroup{G,E}) where {G,E}
  ngens(group.base_group) + ngens(group.extension_group)
end

function gens(group::JacobiGroup{G,E}) where {G,E}
  map(group, vcat(gens(group.base_group), gens(group.extension_group)))
end

function word_in_gens(g::JacobiGroupElem{G,E}) where {G,E}
  vcat(word_in_gens(g.base_group_element),
       [(gx+ngens(base_group(parent(g))), ge)
        for (gx,ge) in word_in_gens(g.extension_element)]
      )
end

################################################################################
# default instances
################################################################################

JacobiSL2Z = Jacobi(SL2Z, HeisenbergSpnZTwistGroup(1, 1, []))
JacobiMp1Z = Jacobi(Mp1Z, HeisenbergSpnZTwistGroup(1, 1, []))

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# creation
################################################################################

(gp::JacobiGroup{G,E})(ga::G, h::E) where {G,E} =
  JacobiGroupElem{G,E}(gp, gp.base_group(ga), gp.extension_group(h))

(gp::JacobiGroup{G,E})(ga::G) where {G,E} =
  JacobiGroupElem{G,E}(gp, gp.base_group(ga), zero(gp.extension_group))

(gp::JacobiGroup{G,E})(h::E) where {G,E} =
  JacobiGroupElem{G,E}(gp, one(gp.base_group), gp.extension_group(h))

function one(gp::JacobiGroup{G,HeisenbergSpnZTwistGroupElem}) where G
  gp( one(base_group(gp)), zero(extension_group(gp)))
end
