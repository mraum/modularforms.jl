################################################################################
################################################################################
# iteration over cosets in SL2Z with respect to the group of upper triangular
# matrices
################################################################################
################################################################################

# Left fold f over the double coset representatives
#   [a b; c d] ∈ Γ_∞ \ SL2Z Γ_∞ with 0 < c ≤ C.
# The function f takes two arguments, a value of type V and an SLzElem from the
# folding.
function foldl_sl2z_parabolic_double_coset(
    C        :: Int,
    apply!   :: Function,
    init     :: V
   ) where V
  C >= 0 || error("C must be nonnegative")

  # Apart from the special case [0 -1; 1 0], we need to iterate through
  # matrices with bottom row c d, 0 < c ≤ C and 0 < d < c. This can be done
  # recursively on the Schreier tree generated by multiplication from the right
  # with matrices - T^-n S = [n 1; -1 0]. Given c and d, the new element has
  # bottom row -(d+nc) c. We want
  #   0 < c < -(d-nc) ≤ C.
  # Since d < c this gives n ≥ 2 > d/c + 1 and (C+d)/c ≥ n.

  # multiplication from the right by - T^-n S = [n 1; -1 0]
  function mul_stn(n::Int, ga::SL2zElem)
    SL2zElem(n*ga.a - ga.b, ga.a,
             n*ga.c - ga.d, ga.c )
  end

  function subiteration!(
      ga :: SL2zElem,
      C  :: Int,
      v
     )
    for n in 2:(C + ga.d) ÷ ga.c
      delta = mul_stn(n, ga)
      v = apply!(v, delta)
      v = subiteration!(delta, C, v)
    end

    return v
  end

  v = init
  ga = SL2zElem(0, -1, 1, 0)
  if C > 0
    v = apply!(v, ga)
  end

  return subiteration!(ga, C, v)
end


################################################################################
################################################################################
# iterator over Γ_∞ \\ (SL₂(ℤ) - Γ_∞)
################################################################################
################################################################################

function show(io::IO, iter::ParabolicDoubleCosetSL2ZIterator)
  print("Iterator over the set Γ_∞ \\\\ (SL₂(ℤ) - Γ_∞) from c = 1 to $(iter.c0).")
end

function iterate(iter::ParabolicDoubleCosetSL2ZIterator)
  n = (1 + floor(Int, 2.08 * log(iter.c0))) << 2
  
  leninfo = Vector{Int}(undef, n)

  leninfo[1] = 1        # α
  leninfo[2] = iter.c0  # α_{max}
  leninfo[3] = -1       # a
  leninfo[4] = 1        # c

  len = 0

  return (0, [1]), (len, leninfo)
end

function iterate(
    iter            :: ParabolicDoubleCosetSL2ZIterator,
    (len, leninfo)  :: Tuple{Int, Vector{Int}}
  ) :: Union{Nothing, Tuple{Tuple{Int, Vector{Int}}, Tuple{Int, Vector{Int}}}}
  # TODO: If we know that the previous α could not proceed to the next level, we
  # know that the current α cannot go to the next level. I think that at most
  # half of the α on the same length can reach another level, so caring for this
  # should increase the speed of the iterator.

  # TODO: Iterate in reverse order instead. Doing this, we do not have to check
  # neither c0 nor alphamax, only check with 1 and 0.

  ##############################################################################
  # All elements γ in the set is uniquely described via
  #
  #   γ = S T^{α_l} S ⋯ S T^{α_0} S
  #
  # where |α_i| ≥ 1, |α_l| ≠ 1 and sgn(α_i) = -sgn(α_{i + 1}).
  ##############################################################################

  ix = len << 2 + 1
  c0 = iter.c0
  alpha = leninfo[ix]
  alphamax = leninfo[ix + 1]
  a = leninfo[ix + 2]
  c = leninfo[ix + 3]

  if alpha < c0
    ############################################################################
    # Calculate α_{max} for the next level.
    ############################################################################
    newalphamax = (c0 - (a > 0 ? a : -a)) ÷ (c > 0 ? c : -c)

    if newalphamax < 2
      ##########################################################################
      # If α_{max} < 2, there is no use of increasing the level.
      ##########################################################################

      if -alphamax < alpha && alpha < alphamax
        ########################################################################
        # We can stay on the same level.
        #
        #   α ↦ α + sgn(α)
        #   c ↦ c - a sgn(α)
        ########################################################################
        if alpha > 0
          leninfo[ix + 3] -= a
          leninfo[ix] += 1
        else
          leninfo[ix + 3] += a
          leninfo[ix] -= 1
        end
        return (1, [leninfo[ix]]), (len, leninfo)
      else
        ########################################################################
        # We can neither increment α nor increase the level. Go back to the
        # nearest previous level where we can increment its α.
        ########################################################################
        newlen = len - 1
        ix -= 4
        while leninfo[ix] == leninfo[ix + 1] || leninfo[ix] == -leninfo[ix + 1]
          newlen -= 1
          ix -= 4
        end

        dlen = len - newlen
        a = leninfo[ix + 2]
        if leninfo[ix] > 0
          leninfo[ix + 3] -= a
          leninfo[ix] += 1
        else
          leninfo[ix + 3] += a
          leninfo[ix] -= 1
        end
        return (dlen + 1, [leninfo[ix]]), (newlen, leninfo)
      end
    else
      ##########################################################################
      # Else keep incrementing the length, where we set α = ±1 for each length,
      # until we find some length where α_{max} < 2. When that happens, we go
      # back to the previous length and increment α.
      ##########################################################################
      if alpha > 0
        alpha = 1
      else
        alpha = -1
      end
      alphavec = Int[]
      tmp = 0
      while (newalphamax >= 2)
        alpha = -alpha
        len += 1
        ix += 4
        tmp = a
        a = -c
        if (alpha < 0)
          c = -c
        end
        c += tmp
        push!(alphavec, alpha)
        leninfo[ix]     = alpha
        leninfo[ix + 1] = newalphamax
        leninfo[ix + 2] = a
        leninfo[ix + 3] = c

        newalphamax = (c0 - (a > 0 ? a : -a)) ÷ (c > 0 ? c : -c)
      end
      alphavec[end] += alpha
      leninfo[ix] += alpha
      if alpha > 0
        leninfo[ix + 3] -= a
      else
        leninfo[ix + 3] += a
      end
      return (0, alphavec), (len, leninfo)
    end
  else
    return nothing
  end
end
