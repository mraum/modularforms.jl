function hecke_transformation(m::fmpz_mat, g::SL2ZElement)
  gp = parent(g)
  @assert _issl2z(gp)

  (word, mr) = hecke_transformation_word(parent(g), m*g)
  return (gen_word(gp,word), mr)
end

function hecke_transformation(m::fmpz_mat, g::SL2ZCoverElement)
  # In the base group: Given m and g, we obtain gg*mm = m*g, where m is in the
  # prescribed set of Hecke matrices.
  #
  # For cover groups, we fix the embedding from m into the group, where the
  # cocycle-value is trivial. We have gg = m*g*inv(mm), and this allows us to
  # determine the cocycle, which equals om(gg) = 0 + om(g) - sigma(mm,inv(mm))
  # + sigma(m,g) + sigma(m*g, inv(mm)).
  gp = parent(g)

  gb = base_group_element(g)
  mg = m*gb
  (ggb,mm) = hecke_transformation(m,gb)

  # Since the cocycle is invariant under scaling by positive real numbers, we
  # can use the adjugate instead of the inverse.
  amm = matrix(ZZ, [mm[2,2] -m[1,2]; -m[2,1] m[1,1]])
  gg = gp(ggb,
          cocycle_value(g) .- cocycle(gp)(mm,amm)
          .+ cocycle(gp)(m,gb) .+ cocycle(gp)(mg,amm)
        )

  return (gg,mm)
end

function hecke_transformation_word(gp::SL2ZGroup, m::fmpz_mat)
  _issl2z(gp) || error("not implemented")
  (a,b,c,d) = (m[1,1], m[1,2], m[2,1], m[2,2])

  sgen = 1; tgen = 2
  word = Tuple{Int,Int}[]
  while !iszero(c)
    if abs(a) < abs(c)
      push!(word,(sgen,1))
      (a,b,c,d) = (c,d,-a,-b)
    else
      t = fdiv(abs(a),abs(c))
      if sign(a) == sign(c)
        push!(word,(tgen,t))
        (a,b) = (a-t*c,b-t*d)
      else
        push!(word,(tgen,-t))
        (a,b) = (a+t*c,b+t*d)
      end
    end
  end

  if a < 0
    push!(word,(sgen,2))
    (a,b,d) = (-a,-b,-d)
  end

  if d != 0
    if abs(b) >= abs(d)
      t = fdiv(abs(b), abs(d))
      if sign(b) == sign(d)
        push!(word,(tgen,t))
        b = b-t*d
      else
        push!(word,(tgen,-t))
        b = b+t*d
      end
    end
    if b < 0
      push!(word,(tgen,-sign(d)))
      b = b+abs(d)
    end
  end

  return (word, parent(m)([a b; c d]))
end

hecke_matrix(a::Union{Int,fmpz}, b::Union{Int,fmpz}, d::Union{Int,fmpz}) = hecke_matrix(ZZ(a), ZZ(b), ZZ(d))

hecke_matrix(a::fmpz, b::fmpz, d::fmpz) = MatrixSpace(ZZ,2,2)([a b; 0 d])

function hecke_matrices(n::Union{Int,fmpz}, gp::SL2ZCover)
  if !(gp isa SL2ZSubgroupCover && _issl2z(base_group(gp)) || _issl2z(gp))
    throw(DomainError(gp, "not implemented"))
  end

  ms = MatrixSpace(ZZ,2,2)
  return [ms([divexact(n,d) b; 0 d])
          for d in sort!(divisors(n))
          for b in 0:Int(d)-1]
end

function hecke_new_matrices(n::Union{Int,fmpz}, gp::SL2ZCover)
  [m for m in hecke_matrices(n,gp)
   if isone(gcd([m[1,1], m[1,2], m[2,1], m[2,2]]))]
end
