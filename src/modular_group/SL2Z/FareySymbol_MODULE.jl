################################################################################
################################################################################
# This is a port of Hartmut Monien's C++ library integrated into Sage.
# Data structures are modified troughout to achieve n log(n) runtime (as
# opposed to n^2).
################################################################################
################################################################################

module FareySymbolSL2Z_MODULE

using AbstractAlgebra:
  addmul!,
  inv!,
  mul!
  
using Nemo:
  QQ,
  ZZ,
  fmpq,
  fmpz

using ModularForms:
  FareySymbolSL2Z,
  PSL2ZCusp,
  SL2ZElem,
  moebius,
  neg!
import ModularForms
import ModularForms:
  inv,
  isinfty

import Base:
  append!,
  delete!,
  first,
  in,
  iterate,
  last

################################################################################
################################################################################
# auxiliar functionality
################################################################################
################################################################################

################################################################################
# special values for pairing
################################################################################

FareySymbolSL2ZPairingInvalid = -10
FareySymbolSL2ZPairingOdd     = -3
FareySymbolSL2ZPairingEven    = -2
FareySymbolSL2ZPairingNo      = 0

################################################################################
################################################################################
# initialization
################################################################################
################################################################################

################################################################################
# initialization of SL2Z
################################################################################

function init_SL2Z!(fs::FareySymbolSL2Z)
  fs.iselement = ga::SL2ZElem -> true
  fs.iselement_plusminus = fs.iselement

  fs.pairings = Int[FareySymbolSL2ZPairingEven, FareySymbolSL2ZPairingOdd]
  fs.nu2 = 1
  fs.nu3 = 1

  fs.as = [zero(ZZ),one(ZZ)]
  fs.bs = [one(ZZ),zero(ZZ)]
  fs.xs = [zero(QQ)]

  fs.pairing_matrices = [pairing_matrix_even(fs,1), pairing_matrix_odd(fs,2)]
  fs.pairing_matrices_are_in_group = [true, true] 
  fs.pairings_with = [1,2]

  # the cusp structure in the case of SL(2,Z) diverges from what
  # would be computed from the pairing. We take infinity as a cusp,
  # while the Farey symbol would suggest to use 0.
  fs.cusp_classes = [1]
  fs.cusp_class_x_indices = [[1]]
  fs.cusp_class_finite_reps = []
  fs.cusp_double_widths = [2]
  fs.cusp_class_widths = [1]
  fs.cusp_reductions = [SL2ZElem(1,0,0,1)]

  fs.even = true
  fs.generators = fs.pairing_matrices
  fs.generators_from_pairing_matrices = [(1,false,false),(2,false,false)]
  fs.generator_odd_ix = (1,2)
  fs.generator_dict = compute_generator_dict(fs)

  fs.coset_reps = [SL2ZElem(1,0,0,1)]
end

################################################################################
# initialization of SL2Z subgroups of index 2 and 4
################################################################################

function init_ix_2_4!(fs::FareySymbolSL2Z, iselement::Function, iselement_plusminus::Function)
  fs.iselement = iselement
  fs.iselement_plusminus = iselement_plusminus

  fs.pairings = Int[FareySymbolSL2ZPairingOdd, FareySymbolSL2ZPairingOdd]
  fs.nu2 = 0
  fs.nu3 = 2

  fs.as = [zero(ZZ),one(ZZ)]
  fs.bs = [one(ZZ),zero(ZZ)]
  fs.xs = [zero(QQ)]

  fs.pairing_matrices = [pairing_matrix_odd(fs,1), pairing_matrix_odd(fs,2)]
  fs.pairing_matrices_are_in_group = map(iselement, fs.pairing_matrices)
  fs.pairings_with = [1,2]

  fs.cusp_classes = [1]
  fs.cusp_class_x_indices = [[1]]
  fs.cusp_class_finite_reps = []
  fs.cusp_double_widths = [2]
  fs.cusp_class_widths = [1]
  fs.cusp_reductions = [SL2ZElem(1,0,0,1)]

  ## special case: even index 2 subgroup
  if iselement(SL2ZElem(0,-1,1,1))
    fs.even = true
    fs.generators = [SL2ZElem(0,1,-1,-1), SL2ZElem(-1,1,-1,0)]
    # TODO: insert 
    # fs.generators_from_pairing_matrices = [(1,false,false),(2,false,false)]
    fs.generator_odd_ix = (1,3)
    fs.generator_dict = compute_generator_dict(fs)
    fs.coset_reps = [SL2ZElem(1,0,0,1), SL2ZElem(0,1,-1,0)]
  ## special case: odd index 4 subgroup
  else
    fs.even = false
    fs.generators = [SL2ZElem(0,1,-1,-1), SL2ZElem(-1,1,-1,0)]
    # TODO: insert 
    # fs.generators_from_pairing_matrices = [(1,false,false),(2,false,false)]
    fs.generator_odd_ix = (1,3)
    fs.generator_dict = compute_generator_dict(fs)
    fs.coset_reps = [SL2ZElem(1,0,0,1), SL2ZElem(0,-1,1,0), SL2ZElem(1,-1,1,0), SL2ZElem(1,0,-1,1)]
  end
end

################################################################################
# initialization of generic groups
################################################################################

function init_generic!(fs::FareySymbolSL2Z, iselement::Function, iselement_plusminus::Function)
  fs.iselement = iselement
  fs.iselement_plusminus = iselement_plusminus

  (fs.pairings, fs.pairing_matrices, fs.as, fs.bs) =
    compute_pairings(iselement, iselement_plusminus)
  fs.pairing_matrices_are_in_group = map(iselement, fs.pairing_matrices)
  fs.xs = [n//d for (n,d) in zip(fs.as[1:end-1],fs.bs[1:end-1])]

  fs.nu2 = 0
  fs.nu3 = 0
  pairing_duplets = fill(0, length(fs.pairings))
  fs.pairings_with = fill(0, length(fs.pairings))
  for (px,p) in enumerate(fs.pairings)
    if p == FareySymbolSL2ZPairingEven
      fs.pairings_with[px] = px
      fs.nu2 += 1
    elseif p == FareySymbolSL2ZPairingOdd
      fs.pairings_with[px] = px
      fs.nu3 += 1
    else
      if pairing_duplets[p] != 0
        px2 = pairing_duplets[p]
        fs.pairings_with[px] = px2
        fs.pairings_with[px2] = px
      else
        pairing_duplets[p] = px
      end
    end
  end

  (fs.cusp_classes, fs.cusp_class_x_indices) = compute_cusp_classes(fs)
  fs.cusp_class_finite_reps = fmpq[]
  sizehint!(fs.cusp_class_finite_reps, length(fs.cusp_classes))
  for fractions in fs.cusp_class_x_indices
    (xx,lx) = extrema(fractions)
    if lx != length(fs.as) # cusp class contains infinity
      push!(fs.cusp_class_finite_reps, fs.xs[xx])
    end
  end
  (fs.cusp_double_widths, fs.cusp_class_widths) = compute_cusp_widths(fs)
  fs.cusp_reductions = compute_cusp_reductions(fs)

  fs.even = iselement(SL2ZElem(-1,0,0,-1))

  (fs.generators, fs.generators_from_pairing_matrices, fs.generator_odd_ix) = compute_generators(fs)
  fs.generator_dict = compute_generator_dict(fs)
  fs.coset_reps = compute_coset_reps(fs)
end

################################################################################
################################################################################
# list of Farey fraction
################################################################################
################################################################################

# Farey fractions are accumulated in a single linked list in reverse order

mutable struct FareyFraction
  num :: fmpz
  den :: fmpz
  approx :: Float64
  pairing :: Int
  pairing_matrix :: Union{Missing,SL2ZElem}
  prev :: FareyFraction

  function FareyFraction(num::Union{Int,fmpz}, den::Union{Int,fmpz})
    ff = new()

    ff.num = ZZ(num)
    ff.den = ZZ(den)
    if den != 0
      ff.approx = Float64(num)/Float64(den)
    else
      ff.approx = sign(num)*maxintfloat(Float64)
    end

    ff.pairing = FareySymbolSL2ZPairingNo
    ff.pairing_matrix = missing

    ff.prev = ff

    return ff
  end
end

mutable struct FareyFractionList
  last :: FareyFraction
end

################################################################################
# append, insert, isinfty for Farey fractions
################################################################################

function append!(ffl::FareyFractionList, ff::FareyFraction)
  ff.prev = ffl.last
  ffl.last = ff
end

function insert!(ffl::FareyFractionList, ix::FareyFraction, ff::FareyFraction)
  ff.prev = ix.prev
  ix.prev = ff
end

function isinfty(ff::FareyFraction)
  ff.den == 0
end

################################################################################
# iteration of Farey fractions
################################################################################

function iterate(ffl::FareyFractionList)
  return (ffl.last, ffl.last)
end

function iterate(ffl::FareyFractionList, state::FareyFraction)
  state.prev == state && return nothing
  return (state.prev, state.prev)
end

################################################################################
################################################################################
# list of unpaired Farey fractions
################################################################################
################################################################################

# a doubly linked list keeps track of the non-paired fractions in forward order

mutable struct Unpaired
  ff :: FareyFraction
  prev :: Unpaired
  next :: Unpaired

  Unpaired(ff::FareyFraction, prev::Unpaired, next::Unpaired) = new(ff,prev,next)

  function Unpaired()
    up = new()
    up.prev = up
    up.next = up
    return up
  end
end

mutable struct UnpairedList
  next_pairing_ix :: Int
  first :: Unpaired
  last :: Unpaired
end

function UnpairedList()
  first = Unpaired()
  last = Unpaired()
  first.next = last
  last.prev = first

  UnpairedList(1,first,last)
end

first(upl::UnpairedList) = upl.first.next

last(upl::UnpairedList) = upl.last.prev

################################################################################
# append, insert, delete of unpaired Farey fractions
################################################################################

function append!(upl::UnpairedList, ff::FareyFraction)
  up = Unpaired(ff, upl.last.prev, upl.last)
  upl.last.prev.next = up
  upl.last.prev = up
end

function insert!(upl::UnpairedList, ix::Unpaired, ff::FareyFraction)
  up = Unpaired(ff, ix.prev, ix)
  ix.prev.next = up
  ix.prev = up
end

function delete!(upl::UnpairedList, ix::Unpaired)
  if ix.prev != ix && ix.next != ix 
    ix.prev.next = ix.next
    ix.next.prev = ix.prev
  end
end

################################################################################
# iteration of unpaired Farey fractions
################################################################################

function iterate(upl::UnpairedList)
  upl.first.next == upl.last && return nothing
  return (upl.first.next,upl.first.next)
end

function iterate(upl::UnpairedList, state::Unpaired)
  state.next.next == state.next && return nothing
  return (state.next,state.next)
end

################################################################################
################################################################################
# computate all Farey fractions with pairings
#
# We start with two fractions and two values of infinity, which we may, since
# the special cases of index <= 4 have been taken care of before. Then as long
# as there are unpaired Farey fractions, we insert new ones. Insertion happens
# at the place of greatest distance between Farey fractions.
#
################################################################################
################################################################################

function compute_pairings(iselement::Function, iselement_plusminus::Function)
  ups = UnpairedList()

  ## the first entry is auxilar and will later be removed
  ## its only purpose is to reduce the number of branches in the code
  ffs = FareyFractionList(FareyFraction(-1,0))
  ffs.last.pairing = FareySymbolSL2ZPairingInvalid

  if iselement(SL2ZElem(-1,1,-1,0))
    append!(ffs, FareyFraction(-1,1))
    append!(ups, ffs.last)
    append!(ffs, FareyFraction( 0,1))
    append!(ups, ffs.last)
  else
    append!(ffs, FareyFraction(0,1))
    append!(ups, ffs.last)
    append!(ffs, FareyFraction(1,1))
    append!(ups, ffs.last)
  end
  append!(ffs, FareyFraction(1,0))
  append!(ups, ffs.last)

  for up in ups
    update_pairing!(ups, up, iselement_plusminus)
  end

  ## we next update Farey fractions towards - and + infty
  ## their distances exceed the other ones
  while !isempty(ups) && isinfty(last(ups).ff)
    add_farey_fraction!(ffs, ups, last(ups), iselement_plusminus)
  end
  while !isempty(ups) && isinfty(first(ups).ff)
    add_farey_fraction!(ffs, ups, first(ups), iselement_plusminus)
  end

  while !isempty(ups)
    up = find_missing_pair(ups)
    add_farey_fraction!(ffs, ups, up, iselement_plusminus)
  end

  pairings = Int[]
  pairing_matrices = SL2ZElem[]
  as = fmpz[]
  bs = fmpz[]
  for ff in ffs
    ff.pairing != FareySymbolSL2ZPairingInvalid || continue
    push!(pairings, ff.pairing)
    push!(pairing_matrices, ff.pairing_matrix)
    push!(as, ff.num)
    push!(bs, ff.den)
  end
  reverse!(pairings)
  reverse!(pairing_matrices)
  reverse!(as)
  reverse!(bs)

  # was before: as, bs shortened by auxiliar elements
  # return (pairings, pairing_matrices, as[1:end-1], bs[1:end-1])
  return (pairings, pairing_matrices, as, bs)
end

################################################################################
# find unpaired Farey fraction with largest distance to its predecesor
################################################################################

function find_missing_pair(ups::UnpairedList)
  ## we assume that ups is not empty
  distance = Float64(-1)
  upfound = ups.first
  for up in ups
    d = up.ff.approx - up.ff.prev.approx
    if d > distance
      distance = d
      upfound = up
    end
  end
  return upfound
end

################################################################################
# insert Farey fraction in front of an unpaired one; try to pair both the new
# and the old one
################################################################################

function add_farey_fraction!(ffs, ups, up, iselement_plusminus)
  as = up.ff.num + up.ff.prev.num
  bs = up.ff.den + up.ff.prev.den

  ff = FareyFraction(as, bs)
  insert!(ffs, up.ff, ff)
  insert!(ups, up, ff)

  update_pairing!(ups, up.prev, iselement_plusminus)
  update_pairing!(ups, up, iselement_plusminus)
end

################################################################################
# try to find a pairing for a Farey fraction in the group
################################################################################

function update_pairing!(ups, up, iselement_plusminus)
  m = SL2ZElem()
  pairing_matrix_even!(m, up)
  if iselement_plusminus(m)
    up.ff.pairing = FareySymbolSL2ZPairingEven
    up.ff.pairing_matrix = m
    delete!(ups, up)
    return
  end

  pairing_matrix_odd!(m, up)
  if iselement_plusminus(m)
    up.ff.pairing = FareySymbolSL2ZPairingOdd
    up.ff.pairing_matrix = m
    delete!(ups, up)
    return
  end

  for upother in ups
    up == upother && continue
    pairing_matrix!(m, up, upother)
    if iselement_plusminus(m)
      up.ff.pairing = ups.next_pairing_ix
      up.ff.pairing_matrix = m
      upother.ff.pairing = ups.next_pairing_ix
      upother.ff.pairing_matrix = inv(-m) # == pairing_matrix(upother, up)
      ups.next_pairing_ix += 1
      delete!(ups, up)
      delete!(ups, upother)
      return
    end
  end
end

################################################################################
################################################################################
# pairing matrices
################################################################################
################################################################################

################################################################################
# formulas for pairing matrices
################################################################################

function pairing_matrix_even(n::fmpz,d::fmpz, np::fmpz,dp::fmpz)
  m = SL2ZElem()
  pairing_matrix_even!(m, n,d, np,dp)
  return m
end

function pairing_matrix_odd(n::fmpz,d::fmpz, np::fmpz,dp::fmpz)
  m = SL2ZElem()
  pairing_matrix_odd!(m, n,d, np,dp)
  return m
end

function pairing_matrix(n1::fmpz,d1::fmpz, np1::fmpz,dp1::fmpz,  n2::fmpz,d2::fmpz, np2::fmpz,dp2::fmpz)
  m = SL2ZElem()
  pairing_matrix!(m, n1,d1, np1,dp1,  n2,d2, np2,dp2)
  return m
end

function pairing_matrix_even!(m::SL2ZElem, n::fmpz,d::fmpz, np::fmpz,dp::fmpz)
  # numerator (n) and denominator (d) of a Farey fraction and its predecesor (p)

  # note: by simplifying the algebraic expressions, this can still be sped up
  # original matrix
  # SL2ZElem(n*d+np*dp, -np*np-n*n,
  #          dp*dp+d*d, -n*d-np*dp)
  mul!(m.a, n,d)
  addmul!(m.a, np,dp)
  mul!(m.b, np,np)
  addmul!(m.b, n,n)
  neg!(m.b,m.b)
  mul!(m.c, dp,dp)
  addmul!(m.c, d,d)
  mul!(m.d, n,d)
  addmul!(m.d, np,dp)
  neg!(m.d,m.d)
end

function pairing_matrix_odd!(m::SL2ZElem, n::fmpz,d::fmpz, np::fmpz,dp::fmpz)
  # numerator (n) and denominator (d) of a Farey fraction and its predecesor (p)

  # note: by simplifying the algebraic expressions, this can still be sped up
  # original matrix
  # SL2ZElem(n*d+np*d+np*dp, -np*np-np*n-n*n,
  #          dp*dp+dp*d+d*d, -n*d-n*dp-np*dp)
  mul!(m.a, n,d)
  mul!(m.b, np,np)
  mul!(m.c, dp,dp)
  mul!(m.d, n,d)
  addmul!(m.a, np,d)
  addmul!(m.b, np,n)
  addmul!(m.c, dp,d)
  addmul!(m.d, n,dp)
  addmul!(m.a, np,dp)
  addmul!(m.b, n,n)
  addmul!(m.c, d,d)
  addmul!(m.d, np,dp)
  neg!(m.b,m.b)
  neg!(m.d,m.d)
end

function pairing_matrix!(m::SL2ZElem, n1::fmpz,d1::fmpz, np1::fmpz,dp1::fmpz,  n2::fmpz,d2::fmpz, np2::fmpz,dp2::fmpz)
  # numerator (n) and denominator (d) of a Farey fraction and its predecesor (p)
  # for the pairing of two fractions, numbered 1 and 2

  # original matrix
  # SL2ZElem(n2*d1+np2*dp1, -np2*np1-n2*n1,
  #          dp2*dp1+d2*d1, -n1*d2-np1*dp2)
  mul!(m.a, n2, d1)
  mul!(m.b, np2,np1)
  mul!(m.c, dp2,dp1)
  mul!(m.d, n1, d2)
  addmul!(m.a, np2,dp1)
  addmul!(m.b, n2, n1)
  addmul!(m.c, d2, d1)
  addmul!(m.d, np1,dp2)
  neg!(m.b,m.b)
  neg!(m.d,m.d)
end

################################################################################
# pairing matrices computed from a Farey symbol with given list of pairings
################################################################################

function fraction_nd(fs::FareySymbolSL2Z, ix::Int)
  if ix == 0
    return (ZZ(-1),zero(ZZ))
  elseif ix == length(fs.as)+1
    return (one(ZZ),zero(ZZ))
  else
    return (fs.as[ix], fs.bs[ix])
  end
end

function pairing_matrix_even(fs::FareySymbolSL2Z, ix::Int)
  (n,d) = fraction_nd(fs, ix)
  (np,dp) = fraction_nd(fs, ix-1)
  pairing_matrix_even(n,d, np,dp)
end

function pairing_matrix_odd(fs::FareySymbolSL2Z, ix::Int)
  (n,d) = fraction_nd(fs, ix)
  (np,dp) = fraction_nd(fs, ix-1)
  pairing_matrix_odd(n,d, np,dp)
end

function pairing_matrix(fs::FareySymbolSL2Z, ix::Int, jx::Int)
  (n1,d1) = fraction_nd(fs, ix)
  (np1,dp1) = fraction_nd(fs, ix-1)
  (n2,d2) = fraction_nd(fs, jx)
  (np2,dp2) = fraction_nd(fs, jx-1)
  pairing_matrix(n1,d1, np1,dp1,  n2,d2, np2,dp2)
end

################################################################################
# pairing matrices computed from an unpaired Farey fraction
################################################################################

function fraction_nd_ndp(up::Unpaired)
  (up.ff.num, up.ff.den,
   up.ff.prev.num, up.ff.prev.den)
end

function pairing_matrix_even(up::Unpaired)
  m = SL2ZElem()
  pairing_matrix_even!(m, up)
  return m
end

function pairing_matrix_odd(up::Unpaired)
  m = SL2ZElem()
  pairing_matrix_odd!(m, up)
  return m
end

function pairing_matrix(up1::Unpaired, up2::Unpaired)
  m = SL2ZElem()
  pairing_matrix!(m, up1, up2)
  return m
end

function pairing_matrix_even!(m::SL2ZElem, up::Unpaired)
  (n,d, np,dp) = fraction_nd_ndp(up)
  pairing_matrix_even!(m, n,d, np,dp)
end

function pairing_matrix_odd!(m::SL2ZElem, up::Unpaired)
  (n,d, np,dp) = fraction_nd_ndp(up)
  pairing_matrix_odd!(m, n,d, np,dp)
end

function pairing_matrix!(m::SL2ZElem, up1::Unpaired, up2::Unpaired)
  (n1,d1, np1,dp1) = fraction_nd_ndp(up1)
  (n2,d2, np2,dp2) = fraction_nd_ndp(up2)
  pairing_matrix!(m, n1,d1, np1,dp1,  n2,d2, np2,dp2)
end

################################################################################
################################################################################
# init Farey symbol from the pairing data
#
# All functions assume that the pairing data of the
# Farey symbol has been initialized already.
################################################################################
################################################################################

################################################################################
# cusp classes, width, representatives, reductions
################################################################################

function compute_cusp_classes(fs::FareySymbolSL2Z)
  nps = length(fs.pairings)

  cusp_class_x_indices = Vector{Int}[]
  cusp_classes = Int[0 for _ in 1:nps]

  cur_cusp_class = 0
  for px in 1:nps
    cusp_classes[px] == 0 || continue
    cur_cusp_class += 1
    push!(cusp_class_x_indices, Int[])

    ix = px
    while cusp_classes[ix] == 0
      cusp_classes[ix] = cur_cusp_class
      push!(cusp_class_x_indices[cur_cusp_class], ix)
      ix = fs.pairings_with[ix != nps ? ix+1 : 1]
    end
  end

  return (cusp_classes, cusp_class_x_indices)
end

function compute_cusp_widths(fs::FareySymbolSL2Z)
  function double_width(pxprev,px,pxnext)
    dw = abs(fs.as[pxprev]*fs.bs[pxnext] - fs.as[pxnext]*fs.bs[pxprev]) << 1
    fs.pairings[px] == FareySymbolSL2ZPairingOdd && (dw += 1)
    fs.pairings[pxnext] == FareySymbolSL2ZPairingOdd && (dw += 1)
    return dw
  end

  cusp_double_widths = fmpz[]

  nps = length(fs.pairings)
  sizehint!(cusp_double_widths, nps)
  push!(cusp_double_widths, double_width(nps,1,2))
  for px in 2:nps-1
    push!(cusp_double_widths, double_width(px-1,px,px+1))
  end
  push!(cusp_double_widths, double_width(nps-1,nps,1))

  cusp_class_widths = fmpz[sum(cusp_double_widths[xx] for xx in xxs) >> 1
                           for xxs in fs.cusp_class_x_indices]

  return (cusp_double_widths, cusp_class_widths)
end

function compute_cusp_reductions(fs::FareySymbolSL2Z)
  reductions = SL2ZElem[]
  sizehint!(reductions, length(fs.pairings))
  nmb_cusp_class_finite_reps = length(fs.cusp_class_finite_reps)

  for (xx,x) in enumerate(fs.xs)
    cx = searchsortedfirst(fs.cusp_class_finite_reps, x)
    if cx != length(fs.cusp_class_finite_reps) + 1 && x == fs.cusp_class_finite_reps[cx]
      push!(reductions, SL2ZElem(1,0,0,1))
      continue
    end

    m = fs.pairing_matrices[xx]
    x = moebius(m,x)
    if isinfty(x)
      push!(reductions, m)
      continue
    end
    x = get(x)
    cx = searchsortedfirst(fs.cusp_class_finite_reps, x)

    while cx == length(fs.cusp_class_finite_reps) + 1 || x != fs.cusp_class_finite_reps[cx]
      pm = fs.pairing_matrices[searchsortedfirst(fs.xs, x)]
      m = pm * m
      x = moebius(pm,x)
      isinfty(x) && break
      x = get(x)
      cx = searchsortedfirst(fs.cusp_class_finite_reps, x)
    end
    push!(reductions, m)
  end

  return reductions
end

################################################################################
# generators
################################################################################

function compute_generators(fs::FareySymbolSL2Z)
  generators = SL2ZElem[]
  generators_from_pairing_matrices = Tuple{Int,Bool,Bool}[]
  generator_odd_ix = (0,0)

  sizehint!(generators, (length(fs.pairings) + fs.nu2 + fs.nu3) >> 1)
  sizehint!(generators_from_pairing_matrices, (length(fs.pairings) + fs.nu2 + fs.nu3) >> 1)

  gx = 1
  mneg = false
  for (px,p) in enumerate(fs.pairings)
    if px > fs.pairings_with[px]
      (gxx,minv,mneg) = generators_from_pairing_matrices[fs.pairings_with[px]]
      mneg = xor(!mneg, fs.pairing_matrices_are_in_group[px],
                 fs.pairing_matrices_are_in_group[fs.pairings_with[px]])
      push!(generators_from_pairing_matrices, (gxx,!minv,mneg))
      continue
    end
     
    if generator_odd_ix == (0,0)
      p == FareySymbolSL2ZPairingOdd && (generator_odd_ix = (gx,3))
      p == FareySymbolSL2ZPairingEven && (generator_odd_ix = (gx,2))
    end

    m = fs.pairing_matrices[px]
    if !fs.pairing_matrices_are_in_group[px]
      mneg = false
      m = -m
    elseif fs.even && p == FareySymbolSL2ZPairingOdd
      mneg = true
      m = -m
    else
      mneg = false
    end

    push!(generators, m)
    push!(generators_from_pairing_matrices, (gx,false,mneg))
    gx += 1
  end

  if fs.even && fs.nu2 == 0 && fs.nu3 == 0
    push!(generators, SL2ZElem(-1,0,0,-1))
    generator_odd_ix = (gx,1)
  end

  return (generators, generators_from_pairing_matrices, generator_odd_ix)
end

function compute_generator_dict(fs::FareySymbolSL2Z)
  generator_dict = Dict{SL2ZElem,Tuple{Int,Bool,Bool}}()
  for (gx,g) in enumerate(fs.generators)
    generator_dict[-inv(g)] = (gx,true,true)
    generator_dict[inv(g)] = (gx,true,false)
    generator_dict[-g] = (gx,false,true)
    generator_dict[g] = (gx,false,false)
  end
  generator_dict[SL2ZElem(1,0,0,1)] = (0,false,false)
  generator_dict[SL2ZElem(-1,0,0,-1)] = (0,false,true)

  return generator_dict
end

################################################################################
# coset representatives
#
# We assume that the cusp widths have already been determined
################################################################################

function compute_coset_reps(fs::FareySymbolSL2Z)
  function gen(c,d, pxprev, px, pxnext)
    a = fs.bs[px]
    if a != 0
      b = -fs.as[px] 
    else
      b = -1
    end

    gen_width = fs.cusp_double_widths[pxprev]
    fs.pairings[pxprev] == FareySymbolSL2ZPairingOdd && (gen_width += 1)
    fs.pairings[px] == FareySymbolSL2ZPairingOdd && (gen_width -= 1)
    for n in 0:(gen_width >> 1 - 1)
      push!(representatives, SL2ZElem(a-n*c, b-n*d, c, d))
    end
  end

  representatives = SL2ZElem[]
  sizehint!(representatives, sum(fs.cusp_double_widths) >> 1)

  gen(0,1, length(fs.pairings),1,2)
  for px in 2:length(fs.xs)
    gen(fs.bs[px-1],-fs.as[px-1], px-1,px,px+1)
  end
  gen(fs.bs[length(fs.xs)],-fs.as[length(fs.xs)], length(fs.xs),length(fs.pairings),1)

  return representatives
end

################################################################################
################################################################################
# Farey symbol functions
################################################################################
################################################################################

################################################################################
# element testing
################################################################################

iselement(fs::FareySymbolSL2Z, ga::SL2ZElem) = fs.iselement(ga)

################################################################################
# pairing matrices
################################################################################

function pairing_matrix_in_group(fs::FareySymbolSL2Z, px::Int)
  if fs.pairing_matrices_are_in_group[px]
    fs.pairing_matrices[px]
  else
    -fs.pairing_matrices[px]
  end
end

################################################################################
# coset reduction
################################################################################

function left_coset_transformation_with_index(fs::FareySymbolSL2Z, m::SL2ZElem)
  # decompose m as +- m' * ga for one of the coset representatives m'
  tmp = SL2ZElem()
  ga = SL2ZElem()
  for (mx,mp) in enumerate(fs.coset_reps)
    ga = mul!(ga, inv!(tmp, mp), m)
    iselement(fs,ga) && return (mp, mx, ga)
    if !fs.even
      ga = neg!(ga, ga)
      iselement(fs,ga) && return (neg!(tmp, mp), -mx, ga)
    end
  end
end

function left_coset_transformation(fs::FareySymbolSL2Z, m::SL2ZElem)
  (mp,_,ga) = left_coset_transformation_with_index(fs, m)
  (mp,ga)
end

################################################################################
# cusp reduction
################################################################################

function cusp_transformation(fs::FareySymbolSL2Z, c::PSL2ZCusp)
  isinfty(c) && return (SL2ZElem(1,0,0,1),c)
  cusp_transformation(fs, get(c))
end

function cusp_transformation(fs::FareySymbolSL2Z, c::fmpq)
  # decompose a cusp c as m c' for a cusp representative c'
  m = ModularForms.cusp_inv_transformation(c)
  (mp,ga) = left_coset_transformation(fs,m)

  x = inv(mp)*PSL2ZCusp()

  isinfty(x) && return (inv(ga),x)
  gap = fs.cusp_reductions[searchsortedfirst(fs.xs,get(x))]
  return (inv(gap*ga), gap*x)
end

################################################################################
# word problem
################################################################################

function word_in_gens(fs::FareySymbolSL2Z, g::SL2ZElem) :: Vector{Tuple{Int,Int}}
  # Decompose g as a product of generator powers. A list of pairs (gx,ge)
  # corresponds to the pe-th power of the gx-th generator
  (word_pmat,g) = word_in_pairing_matrices(fs, g)
  word_gen = []

  (wx,we,wneg) = (0,0,false)
  for vx in word_pmat
    (gx,ginv,gneg) = fs.generators_from_pairing_matrices[abs(vx)]
    ginv = xor(ginv, vx < 0)

    if wx == gx
      we += ginv ? -1 : 1
    else
      wx != 0 && push!(word_gen, (wx,we))
      (wx,we) = (gx, ginv ? -1 : 1)
    end
    wneg = xor(wneg,gneg)
  end

  (gx,ginv,gneg) = fs.generator_dict[g]
  if gx != 0
    if wx == gx
      we += ginv ? -1 : 1
    else
      wx != 0 && push!(word_gen, (wx,we))
      (wx,we) = (gx,ginv ? -1 : 1)
    end
  end
  wx != 0 && push!(word_gen, (wx,we))
  xor(wneg,gneg) && push!(word_gen, fs.generator_odd_ix)

  return word_gen
end

function in(g::SL2ZElem, fs::FareySymbolSL2Z)
  haskey(fs.generator_dict, word_in_pairing_matrices(fs,g)[2])
end

function word_in_pairing_matrices(fs::FareySymbolSL2Z, g::SL2ZElem) :: Tuple{Vector{Int},SL2ZElem}
  # decompose an element g as word * g', where \pm g' or \pm g'^{-1} is among
  # the generators, and word is encoded as a list of signed indices of pairing
  # matrices such that positive index corrsponds to the matrix and negative
  # index corrsponds to its inverse.

  # Based on: Kurth/Long - Computations with finite index subgroups of
  # PSL_2(ZZ) using Farey Symbols, p.13f
  word = Int[]
  while true
  local ac::fmpq, bd::fmpq
    a = g.a; b = g.b; c = g.c; d = g.d

    px = 0
    if d == 0
      ac = a//c
      if ac < fs.xs[1]
        px = 1
      elseif fs.xs[end] < ac
        px = length(fs.pairing_matrices)
      end
    elseif c == 0
      bd = b//d
      if fs.xs[end] < bd
        px = length(fs.pairing_matrices)
      elseif bd < fs.xs[1]
        px = 1
      end
    else
      ac = a//c; bd = b//d
      if ac <= fs.xs[1] && bd <= fs.xs[1]
        px = 1
      elseif fs.xs[end] <= bd && fs.xs[end] <= ac
        px = length(fs.pairing_matrices)
      elseif bd < ac
        for (ixnext,x,xnext) in zip(2:length(fs.xs), fs.xs[1:end-1], fs.xs[2:end])
          if x < bd && ac <= xnext || x <= bd && ac < xnext
            px = ixnext
            break
          end
        end
      elseif ac < bd
        for (ixnext,x,xnext) in zip(2:length(fs.xs), fs.xs[1:end-1], fs.xs[2:end])
          if x < ac && bd <= xnext || x <= ac && bd < xnext
            px = ixnext
            break
          end
        end
      end
    end

    if px == 0
      return (word, g)
    end

    # If the pairing, we found is ODD, we need to split the interval.
    # If the arc is in the right part, we choose the pairing matrix
    # If it is in the left part, we choose its inverse.
    # Note, that this choice differs from the article.
    if fs.pairings[px] != FareySymbolSL2ZPairingOdd
      g = pairing_matrix_in_group(fs,px) * g
      push!(word, -px)
    else
      if px == 1
        m = (fs.as[1]-1) // fs.bs[1]
      elseif px == length(fs.xs)
        m = (fs.as[1]+1) // fs.bs[1]
      else
        m = (fs.as[px-1]+fs.as[px]) // (fs.bs[px-1]+fs.bs[px])
      end

      if iszero(c)
        if bd <= m
          g = inv(pairing_matrix_in_group(fs,px)) * g
          push!(word, px)
        else
          g = pairing_matrix_in_group(fs,px) * g
          push!(word, -px)
        end
      elseif iszero(d)
        if ac <= m
          g = inv(pairing_matrix_in_group(fs,px)) * g
          push!(word, px)
        else
          g = pairing_matrix_in_group(fs,px) * g
          push!(word, -px)
        end
      elseif ac <= m && bd <= m
        g = inv(pairing_matrix_in_group(fs,px)) * g
        push!(word, px)
      elseif ac >= m && bd >= m
        g = pairing_matrix_in_group(fs,px) * g
        push!(word, -px)
      else
        # Based on Lemma 4 of the article by Kurth/Long, 
        # this case can not occure.
        error("runtime error")
      end
    end
  end
end

end

using .FareySymbolSL2Z_MODULE

################################################################################
# creation
################################################################################

function FareySymbolSL2Z(iselement::Function)
  FareySymbolSL2Z(iselement, m -> iselement(m) || iselement(-m))
end
