module PermStabGL2Z_MODULE

using AbstractAlgebra:
  cycles,
  perm

import Nemo
using Nemo:
  fmpq,
  fmpz

import ModularForms
using ModularForms:
  SL2ZElem,
  libmodularforms_jll


mutable struct PermStabGL2ZIsElement
  elts::Ptr{Int}
  nelts::Int
  epsperm::Ptr{Int}
  negperm::Ptr{Int}
  sperm::Ptr{Int}
  permsize::Int
  tcycls::Ptr{Int}
  tcycllcm::Ptr{Int}
  cfrac::Ptr{fmpz}
  cfrac_len::Int

  function PermStabGL2ZIsElement(
      elts::Vector{Int},
      epsperm::Vector{Int},
      negperm::Vector{Int},
      sperm::Vector{Int},
      tcycls::Vector{Tuple{Int,Int,Int}},
      tcycllcm::fmpz
      )
    # We shift all indices to match 0-based indexing in C.
    elts = [e-1 for e in elts]
    epsperm = [a-1 for a in epsperm]
    negperm = [a-1 for a in negperm]
    sperm = [a-1 for a in sperm]
    # T-cycles are translated to a continguous array.
    tcycls = vcat([[ox-1,init,len] for (ox,init,len) in tcycls]...)

    stab = new()
    ccall((:perm_stab_gl2z_init, libmodularforms_jll), Nothing,
        (Ref{PermStabGL2ZIsElement},
         Ref{Int}, Int,
         Ref{Int}, Ref{Int}, Ref{Int}, Int,
         Ref{Int}, Ref{fmpz}),
        stab,
        elts, length(elts),
        epsperm, negperm, sperm, length(epsperm),
        tcycls, tcycllcm
       )
    finalizer(_perm_stab_gl2z_clear, stab)
    return stab
  end
end

function _perm_stab_gl2z_clear(stab::PermStabGL2ZIsElement)
  ccall((:perm_stab_gl2z_clear, libmodularforms_jll), Nothing,
        (Ref{PermStabGL2ZIsElement},),
        stab)
end

function permutation_stabilizer_sl2z_iselement(
    perms::Vector{Tuple{Vector{Int},Vector{Int}}},
    elts::Vector{Int}
   )
  # We encode the SL2Z permutation as the induction to GL2Z. The swap between
  # cosets is given by \epsilon = [1 0; 0 -1], where the first half of each
  # permutation in PermStabGL2ZIsElement.perms is the trivial coset and the
  # second one is the \epsilon-coset. We have
  #   S^\epsilon = S^{-1} and T^\epsilon = T^{-1}. 
  # In particular, the \epsilon-cosets arise from the inverse of the identity
  # cosets.
  elts = copy(elts)
  epsperm_total = Int[]
  negperm_total = Int[]
  sperm_total = Int[]
  tperm_total = Int[]
  sh = 0
  for (px,(sperm,tperm)) in enumerate(perms)
    # The initial elements
    elts[px] = elts[px]+sh

    # The action of epsilon
    append!(epsperm_total, collect(length(sperm)+1+sh:2*length(sperm)+sh))
    append!(epsperm_total, collect(1+sh:length(sperm)+sh))

    # The action of S
    sperm_sl2z = copy(sperm)
    append!(sperm_sl2z, [a+length(sperm) for a in invperm(sperm)])

    # The action of -I = S^2
    append!(negperm_total, [a+sh for a in permute!(sperm_sl2z,sperm_sl2z)])

    # The action of S' 
    sperm_gl2z = copy(sperm)
    prepend!(sperm_gl2z, [a+length(sperm) for a in invperm(sperm)])
    append!(sperm_total, [a+sh for a in sperm_gl2z])

    # The action of T.
    tperm_gl2z = copy(tperm)
    append!(tperm_gl2z, [a+length(tperm) for a in invperm(tperm)])
    append!(tperm_total,[a+sh for a in tperm_gl2z])

    sh += 2*length(sperm)
  end

  # In order to sort the cycles, we compute the orbits and then conjucate by
  # their concatenation viewed as a permutation. For permutations p1 and p2, we
  # have
  #   p1 \circ p2 = permute!(p2,p1).
  # We want to compute orbitperm \circ perm \circ orbitperm^-1.
  tcycls = Tuple{Int,Int,Int}[]
  tcycllcm = fmpz(1)
  let
    tcycles = collect(cycles(perm(tperm_total)))
    orbitperm = vcat(tcycles...)
    orbitinvperm = invperm(orbitperm)

    # apply permutation to elts
    elts = orbitinvperm[elts]
    # apply permutation to \epsilon
    epsperm_total .= orbitinvperm[permute!(epsperm_total,orbitperm)]
    # apply permutation to -I
    negperm_total .= orbitinvperm[permute!(negperm_total,orbitperm)]
    # apply permutation to S'
    sperm_total .= orbitinvperm[permute!(sperm_total,orbitperm)]

    # TODO: debug
    tperm_total .= orbitinvperm[permute!(tperm_total,orbitperm)]

    tcycl = Vector{Tuple{Int,Int,Int}}(undef,length(tperm_total))
    let ix = 1
      for c in tcycles
        tcycllcm = lcm(tcycllcm,fmpz(length(c)))
        for ox in 1:length(c)
          push!(tcycls, (ox,ix-ox,length(c)))
          ix += 1
        end
      end
    end
  end

  return PermStabGL2ZIsElement(
      elts,
      epsperm_total, negperm_total, sperm_total,
      tcycls, tcycllcm)
end

permsize(stab::PermStabGL2ZIsElement) = stab.permsize

function epsperm(stab::PermStabGL2ZIsElement)
  epsperm = Vector{Int}(undef, stab.permsize)
  for ix in 1:stab.permsize
    epsperm[ix] = unsafe_load(stab.epsperm,ix)+1
  end
  return epsperm
end

function negperm(stab::PermStabGL2ZIsElement)
  negperm = Vector{Int}(undef, stab.permsize)
  for ix in 1:stab.permsize
    negperm[ix] = unsafe_load(stab.negperm,ix)+1
  end
  return negperm
end

function sperm(stab::PermStabGL2ZIsElement)
  sperm = Vector{Int}(undef, stab.permsize)
  for ix in 1:stab.permsize
    sperm[ix] = unsafe_load(stab.sperm,ix)+1
  end
  return sperm
end

function tperm(stab::PermStabGL2ZIsElement)
  tperm = Vector{Int}(undef, stab.permsize)
  for ix in 1:stab.permsize
    ox   = unsafe_load(stab.tcycls,3*(ix-1)+1)
    init = unsafe_load(stab.tcycls,3*(ix-1)+2)
    len  = unsafe_load(stab.tcycls,3*(ix-1)+3)
    if ox != len-1
      tperm[ix] = init+1+ox+1
    else
      tperm[ix] = init+1
    end
  end
  return tperm
end

function (stab::PermStabGL2ZIsElement)(g::SL2ZElem) :: Tuple{Bool,Bool}
  iselem = Ref(0)
  isnegelem = Ref(0)
  ccall((:perm_stab_gl2z_is_element, libmodularforms_jll), Nothing,
      (Ref{Int}, Ref{Int}, Ref{PermStabGL2ZIsElement},
       Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}),
      iselem, isnegelem, stab,
      g.a, g.b, g.c, g.d)
  return (iselem[] != 0, isnegelem[] != 0)
end

end

using .PermStabGL2Z_MODULE
