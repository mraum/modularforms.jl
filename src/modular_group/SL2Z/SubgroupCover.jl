################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# auxiliar
################################################################################

function check_cocycletag(a::SL2ZSubgroupCover, b::SL2ZSubgroupCover)
  if ismissing(cocycletag(a)) || cocycletag(a) != cocycletag(b)
    error("cocycles must coincide")
  end
end

################################################################################
# properties
################################################################################

_issl2z(::SL2ZSubgroupCover) = false

_ismp1z(group::SL2ZSubgroupCover) = cocycletag(group) == cocycletag(Mp1Z) && _issl2z(base_group(group))

level(group::SL2ZSubgroupCover) = level(base_group(group))

farey_symbol(group::SL2ZSubgroupCover) = farey_symbol(base_group(group))

iscover(group::SL2ZSubgroupCover) = !isempty(centerext(group))

centerext(group::SL2ZSubgroup) = []
centerext(group::SL2ZSubgroupCover) = group.centerext

centerorder(gp::SL2ZSubgroupCover) = prod(centerext(gp)) * centerorder(base_group(gp))

cocycle(group::SL2ZSubgroup) = nothing
cocycle(group::SL2ZSubgroupCover) = group.cocycle

cocycletag(group::SL2ZSubgroup) = nothing
cocycletag(group::SL2ZSubgroupCover) = group.cocycle

################################################################################
# show
################################################################################

function show(io::IO, group::SL2ZSubgroupCover)
  if _ismp1z(group)
    print(io, "Mp(1,ℤ)")
  else
    show(io, base_group(group))
    print(io, "—cover")
  end
end

################################################################################
# conversion
################################################################################

base_group(group::SL2ZSubgroupCover) = group.base_group

function change_base_group(bgp::SL2ZGroup, gp::SL2ZSubgroupCover)
    SL2ZSubgroupCover(bgp, centerext(gp), cocycle(gp))
end

################################################################################
# creation
################################################################################

function Gamma0Mp1Z(N::Int)
  SL2ZSubgroupCover(Gamma0SL2Z(N), [2], mp1z_cocycle)
end

function Gamma1Mp1Z(N::Int)
  SL2ZSubgroupCover(Gamma1SL2Z(N), [2], mp1z_cocycle)
end

function GammaMp1Z(N::Int)
  SL2ZSubgroupCover(GammaSL2Z(N), [2], mp1z_cocycle)
end

function GammaNSMp1Z(N::Int)
  SL2ZSubgroupCover(GammaNSSL2Z(N), [2], mp1z_cocycle)
end

function GammaNSPlusMp1Z(N::Int)
  SL2ZSubgroupCover(GammaNSPlusSL2Z(N), [2], mp1z_cocycle)
end

Gamma0(::SL2ZSubgroupCover, N::Int) = Gamma0Mp1Z(N)
Gamma1(::SL2ZSubgroupCover, N::Int) = Gamma1Mp1Z(N)
Gamma(::SL2ZSubgroupCover, N::Int) = GammaMp1Z(N)
GammaNS(::SL2ZSubgroupCover, N::Int) = GammaNSMp1Z(N)
GammaNSPlus(::SL2ZSubgroupCover, N::Int) = GammaNSPlusMp1Z(N)

function intersect(gp::SL2ZSubgroupCover, hp::SL2ZSubgroup)
  return change_base_group(intersect(base_group(gp), hp), gp)
end

function intersect(gp::SL2ZSubgroupCover, hp::SL2ZSubgroupCover)
  check_cocycletag(gp,hp)
  return intersect(gp, base_group(hp))
end

################################################################################
# comparison
################################################################################

function ==(g::SL2ZSubgroupCover, h::SL2ZSubgroupCover)
  base_group(g) == base_group(h) && !ismissing(cocycletag(g)) && cocycletag(g) == cocycletag(h)
end

################################################################################
# generators
################################################################################

function ngens(group::SL2ZSubgroupCover)
  _ismp1z(group) && return 2
  return ngens(base_group(group)) + length(centerext(group))
end

function gens(group::SL2ZSubgroupCover)
  if _ismp1z(group)
    return [group(SL2ZElem(0,-1,1,0), [0]),
            group(SL2ZElem(1,1,0,1), [0])]
  else
    return vcat([group(SL2Z(g), fill(0, length(centerext(group))))
                 for g in gens(base_group(group))],
                [group(SL2Z(1,0,0,1), unitvec(Int, length(centerext(group)), ix))
                 for ix in 1:length(centerext(group))])
  end
end

function gen_relation_words(group::SL2ZSubgroupCover)
  _ismp1z(group) || error("not implemented")
  return [[(1,8)],
          [(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),
           (1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1)]]
end

################################################################################
# cosets
################################################################################

function index(supgroup::SL2ZSubgroupCover, subgroup::SL2ZSubgroupCover)
  check_cocycletag(supgroup, subgroup)
  return index(base_group(supgroup), base_group(subgroup))
end


function left_coset_representatives(supgroup::SL2ZSubgroupCover, subgroup::SL2ZSubgroupCover)
  check_cocycletag(supgroup, subgroup)
  return map(supgroup, left_coset_representatives(base_group(supgroup), base_group(subgroup)))
end

function left_coset_transformation_with_index(group::SL2ZSubgroupCover, m::SL2ZSubgroupCoverElem)
  check_cocycletag(group, parent(m))

  (mr,mx,gr) = left_coset_transformation_with_index(base_group(group), base_group_element(m))
  mr = parent(m)(mr)
  gr = group(gr)
  gr.om .= (cocycle_value(gr) .+ cocycle_value(m) .- cocycle_value(mr*gr) ) .% centerext(group)

  return (mr,mx,gr)
end

function left_coset_transformation(group::SL2ZSubgroupCover, m::SL2ZSubgroupCoverElem)
  check_cocycletag(group, parent(m))

  (mr,gr) = left_coset_transformation(base_group(group), base_group_element(m))
  mr = parent(m)(mr)
  gr = group(gr)
  gr.om .= (cocycle_value(gr) .+ cocycle_value(m) .- cocycle_value(mr*gr) ) .% centerext(group)

  return (mr,gr)
end


function right_coset_representatives(supgroup::SL2ZSubgroupCover, subgroup::SL2ZSubgroupCover)
  check_cocycletag(supgroup, subgroup)
  return map(supgroup, right_coset_representatives(base_group(supgroup), base_group(subgroup)))
end

function right_coset_transformation_with_index(group::SL2ZSubgroupCover, m::SL2ZSubgroupCoverElem)
  check_cocycletag(group, parent(m))

  (gr,mr,mx) = right_coset_transformation_with_index(base_group(group), base_group_element(m))
  gr = group(gr)
  mr = parent(m)(mr)
  gr.om .= (cocycle_value(gr) .+ cocycle_value(m) .- cocycle_value(gr*mr) ) .% centerext(group)

  return (gr,mr,mx)
end

function right_coset_transformation(group::SL2ZSubgroupCover, m::SL2ZSubgroupCoverElem)
  check_cocycletag(group, parent(m))

  (gr,mr) = right_coset_transformation(base_group(group), base_group_element(m))
  gr = group(gr)
  mr = parent(m)(mr)
  gr.om .= (cocycle_value(gr) .+ cocycle_value(m) .- cocycle_value(gr*mr) ) .% centerext(group)

  return (gr,mr)
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

farey_symbol(g::SL2ZSubgroupCoverElem) = farey_symbol(parent(g))

base_group_element(g::SL2ZSubgroupCoverElem) = base_group(parent(g))(SL2ZElem(g))

cocycle_value(g::SL2ZSubgroupCoverElem) = g.om

function in(g::SL2ZSubgroupCoverElem, gp::SL2ZSubgroupCover)
  !ismissing(cocycletag(parent(g))) && cocycletag(parent(g)) == cocycletag(gp) && base_group_element(g) in base_group(gp)
end

################################################################################
# show
################################################################################

function show(io::IO, g::SL2ZSubgroupCoverElem)
  print(io, "(")
  show(io, base_group_element(g))
  print(io, ",[")
  cs = cocycle_value(g)
  if !isempty(cs)
    print(io,first(cs))
  end
  for c in cs[2:end]
    print(io,",")
    print(io, c)
  end
  print(io,"]")
  print(io, ")")
end

################################################################################
# conversion
################################################################################

SL2ZElem(g::SL2ZSubgroupCoverElem) = g.ga

function (group::SL2ZSubgroup)(g::SL2ZSubgroupCoverElem)
  group == parent(g) || throw("incompatible parent")
  return SL2ZSubgroupElem(group, SL2ZElem(g))
end

function (group::SL2ZSubgroupCover)(g::SL2ZSubgroupCoverElem)
  check_cocycletag(group, parent(g))

  return SL2ZSubgroupCoverElem(group, SL2Z(base_group(group)(SL2ZElem(g))),
                               cocycle_value(g))
end

################################################################################
# comparison
################################################################################

isone(g::SL2ZSubgroupCoverElem) = isone(SL2ZElem(g)) && all(e==0 for e in g.om)

function ==(g::SL2ZSubgroupCoverElem, h::SL2ZSubgroupCoverElem)
  parent(g) == parent(h) &&
    SL2ZElem(g) == SL2ZElem(h) &&
    cocycle_value(g) == cocycle_value(h)
end

################################################################################
# creation
################################################################################

function (grp::SL2ZSubgroupCover)()
  one(grp)
end

function one(group::SL2ZSubgroupCover)
  return SL2ZSubgroupCoverElem(group, SL2ZElem(1,0,0,1), fill(0, length(centerext(group))))
end

function (group::SL2ZSubgroupCover)(ga::G) where G <: SL2ZElement
  return SL2ZSubgroupCoverElem(group, SL2Z(base_group(group)(ga)), fill(0,length(centerext(group))))
end

function (group::SL2ZSubgroupCover)(ga::G, om::Vector{Int}) where G <: SL2ZElement
  length(om) == length(centerext(group)) || error("incompatible cocycle value")
  return SL2ZSubgroupCoverElem(group, SL2Z(base_group(group)(ga)), om)
end

copy(a::SL2ZSubgroupCoverElem) = parent(a)(copy(a.ga),copy(a.om))

function deepcopy_internal(a::SL2ZSubgroupCoverElem, stackdict::IdDict)
  return parent(a)(
          deepcopy_internal(a.ga, stackdict),
          deepcopy_internal(a.om, stackdict))
end

################################################################################
# action on cusps; SL2ZElem action defined already
################################################################################

moebius(g::SL2ZSubgroupCoverElem, q::fmpq) = moebius(SL2ZElem(g),q)

moebius(g::SL2ZSubgroupCoverElem, q::PSL2ZCusp) = moebius(SL2ZElem(g),q)

*(g::SL2ZSubgroupCoverElem, q::PSL2ZCusp) = *(SL2ZElem(g),q)

################################################################################
# action on CC; SL2ZElem action defined already
################################################################################

moebius(g::SL2ZSubgroupCoverElem, tau::acb) = moebius(SL2ZElem(g), tau)

################################################################################
# arithmetic
################################################################################

function *(g::SL2ZSubgroupCoverElem, h::SL2ZSubgroupCoverElem)
  check_cocycletag(parent(g), parent(h))

  if base_group(parent(g)) == base_group(parent(h))
    group = parent(g)
  elseif _issl2z(base_group(parent(g)))
    group = parent(g)
  elseif _issl2z(base_group(parent(h)))
    group = parent(h)
  else
    error("incompatible base groups")
  end

  if !iscover(group)
    return SL2ZSubgroupCoverElem(group, SL2ZElem(g)*SL2ZElem(h), [])
  else
    bg = SL2ZElem(g)
    bh = SL2ZElem(h)
    return SL2ZSubgroupCoverElem(group, bg*bh,
        (cocycle_value(g) .+ cocycle_value(h) .+ cocycle(group)(bg,bh)) .% centerext(group))
  end
end

function mul!(
    f::SL2ZSubgroupCoverElem,
    g::SL2ZSubgroupCoverElem,
    h::SL2ZSubgroupCoverElem
   )
  if parent(f) != parent(g) || parent(f) != parent(h)
    throw(DomainError((f,g,h), "incompatible parents"))
  end
  group = parent(g)
  if iscover(group)
    f.om .= (g.om .+ h.om .+ cocycle(group)(g.ga,h.ga)) .% centerext(group)
  end
  f.ga = mul!(f.ga,g.ga,h.ga)
  return f
end

function Base.inv(g::SL2ZSubgroupCoverElem)
  group = parent(g)
  bg = SL2ZElem(g)
  bginv = inv(bg)
  # FIXME: this assumes positivity, use mod instead
  return group(bginv, (.-cocycle_value(g) .- cocycle(group)(bg,bginv)) .% centerext(group))
end

function ^(g::SL2ZSubgroupCoverElem, n::Int)
  if n >= 0
    power_by_squaring(g,n)
  else
    power_by_squaring(inv(g), -n)
  end
end

################################################################################
# generator words
################################################################################

function word_in_gens(g::SL2ZSubgroupCoverElem)
  wordsl2z = word_in_gens(base_group_element(g))
  h = gen_word(parent(g), wordsl2z)
  if _ismp1z(parent(g))
    if isodd(cocycle_value(g)[1] + cocycle_value(h)[1])
      push!(wordsl2z, (1,4))
    end
    return wordsl2z
  else
    ngenssl2z = ngens(base_group(parent(g)))
    wordcenter = [(cx+ngenssl2z,ce) for (cx,ce) in enumerate(g.om-h.om)]
    return vcat(wordsl2z, wordcenter)
  end
end

################################################################################
# random
################################################################################

rand(gp::SL2ZSubgroupCover, complexity) = rand(GLOBAL_RNG, gp, complexity)
rand(r::AbstractRNG, gp::SL2ZSubgroupCover, complexity::Int) = rand(GLOBAL_RNG, gp, -complexity:complexity)

function rand(r::AbstractRNG, gp::SL2ZSubgroupCover, complexity::UnitRange{Int})
  bg = rand(base_group(gp), complexity)
  om = [rand(0:e-1) for e in centerext(gp)]
  return gp(bg,om)
end

################################################################################
################################################################################
# default instances
################################################################################
################################################################################

mp1z_cocycle(g::SL2ZSubgroupElem, h::SL2ZSubgroupElem) = mp1z_cocycle(g.ga, h.ga)

function mp1z_cocycle(g::fmpz_mat, h::fmpz_mat)
  @assert nrows(g) == 2 && ncols(g) == 2
  @assert nrows(h) == 2 && ncols(h) == 2
  @assert !iszero(det(g))
  @assert !iszero(det(h))
  _mp1z_cocycle(g[1,1], g[1,2], g[2,1], g[2,2],
                h[1,1], h[1,2], h[2,1], h[2,2])
end

function mp1z_cocycle(g::SL2ZElem, h::fmpz_mat)
  @assert nrows(h) == 2 && ncols(h) == 2
  @assert !iszero(det(h))
  _mp1z_cocycle(g.a, g.b, g.c, g.d,
                h[1,1], h[1,2], h[2,1], h[2,2])
end

function mp1z_cocycle(g::fmpz_mat, h::SL2ZElem)
  @assert nrows(g) == 2 && ncols(g) == 2
  @assert !iszero(det(g))
  _mp1z_cocycle(g[1,1], g[1,2], g[2,1], g[2,2],
                h.a, h.b, h.c, h.d)
end
function mp1z_cocycle(g::SL2ZElem, h::SL2ZElem)
  _mp1z_cocycle(g.a, g.b, g.c, g.d,
                h.a, h.b, h.c, h.d)
end

function _mp1z_cocycle(ga::I, gb::I, gc::I, gd::I, ha::I, hb::I, hc::I, hd::I) where {I <: Union{Int,fmpz}}
  # We use the cocycle \sqrt(c h \tau + d) \sqrt(c' \tau + d') / \sqrt(c(gh)
  # \tau + d(gh)).  The cocycle is computed explicitly in Strömberg, Weil
  # representations associated to finite quadratic modules, p. 11. His branch
  # of the square root takes values with angles between -\pi/2 and +\pi/2.
  sigg = !iszero(gc) ? gc : gd
  sigh = !iszero(hc) ? hc : hd

  symbinf(e,f) = e < 0 && f < 0 ? 1 : 0 #Hilbert symbol at infinity 
  if !iszero(gc) && iszero(hc)
    return symbinf(-sigg, sigh)
  elseif iszero(gc) && !iszero(hc)
    return symbinf(-sigh, sigg)
  end

  ghc = gc*ha + gd*hc
  siggh = !iszero(ghc) ? ghc : gc*hb + gd*hd
 
  if !iszero(gc) && !iszero(hc) && !iszero(ghc)
    return symbinf(siggh*sigg, siggh*sigh)
  elseif (!iszero(gc) && !iszero(hc) && iszero(ghc)) || (iszero(gc) && iszero(hc))
    return symbinf(sigg, sigh)
  end
end

################################################################################
################################################################################
# default instances
################################################################################
################################################################################

Mp1Z = SL2ZSubgroupCover(SL2Z, [2], mp1z_cocycle)
