################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

@doc Markdown.doc"""
    parent_type(::Type{SL2ZElem})

Return the type `SL2ZFullGroup`.
"""
parent_type(::Type{SL2ZElem}) = SL2ZFullGroup

@doc Markdown.doc"""
    parent(ga::SL2ZElem)

Return the modular group $\mathrm{SL}_2(\mathbb{Z})$.
"""
parent(ga::SL2ZElem) = SL2Z

@doc Markdown.doc"""
    elem_type(::Type{SL2ZFullGroup})

Return the type `SL2ZElem`.
"""
elem_type(::Type{SL2ZFullGroup}) = SL2ZElem

@doc Markdown.doc"""
    parent_type(::Type{SL2ZSubgroupElem})

Return the type `SL2ZSubgroup`.
"""
parent_type(::Type{SL2ZSubgroupElem}) = SL2ZSubgroup

@doc Markdown.doc"""
    parent(ga::SL2ZSubgroupElem)

Return the parent of the element `ga`.
"""
parent(ga::SL2ZSubgroupElem) = ga.parent

@doc Markdown.doc"""
    elem_type(::Type{SL2ZSubgroup})

Return the type `SL2ZSubgroupElem`.
"""
elem_type(::Type{SL2ZSubgroup}) = SL2ZSubgroupElem

################################################################################
# properties
################################################################################

# NOTE: With [g,s]et_special, no special type for SL2Z is needed. We simply use
# the field congruence for "named subgroups".

isstandard_congruence(grp::SL2ZFullGroup) = true

function isstandard_congruence(grp::SL2ZSubgroup)
  !isnothing(get_attribute(grp, :congruence))
end

_issl2z(::SL2ZFullGroup) = true

function _issl2z(group::SL2ZSubgroup)
  !isnothing(get_attribute(group, :level)) &&
      get_attribute(group, :level) == 1
end

_isgamma0(group::SL2ZFullGroup) = true

function _isgamma0(group::SL2ZSubgroup)
  isstandard_congruence(group) &&
      get_attribute(group, :congruence)==:Γ_0
end

_isgamma1(group::SL2ZFullGroup) = true

function _isgamma1(group::SL2ZSubgroup)
  isstandard_congruence(group) &&
      get_attribute(group, :congruence)==:Γ_1
end

_isgamma(group::SL2ZFullGroup) = true

function _isgamma(group::SL2ZSubgroup)
  isstandard_congruence(group) &&
      get_attribute(group, :congruence)==:Γ
end

_ismp1z(group::SL2ZGroup) = false

@doc Markdown.doc"""
    level(group::SL2ZGroup)

Return the level $N$ of the group.
"""
function level(group::SL2ZGroup) end

level(group::SL2ZFullGroup) = 1

function level(
    group    :: SL2ZSubgroup;
    fallback :: Bool=false,
    Nmax     :: Int=1000000
   ) :: Union{Missing,Int}
  if isnothing(get_attribute(group, :level))
    fallback || return missing
    lvl = 1
    while lvl <= Nmax
      if all([γ in group for γ in gens(Gamma(lvl))])
        set_attribute!(group, :level => lvl)
        return lvl
      end
      lvl += 1
    end
    return missing
  else
    return get_attribute(group, :level)
  end
end

@doc Markdown.doc"""
    farey_symbol(group::SL2ZGroup)

Return Farey symbol of group.
"""
function farey_symbol(group::SL2ZGroup) end

farey_symbol(group::SL2ZFullGroup) = group.farey_symbol

farey_symbol(group::SL2ZSubgroup) = group.farey_symbol

iseven(group::SL2ZFullGroup) = true

iseven(group::SL2ZSubgroup) = farey_symbol(group).even

function issubgroup(subgroup::SL2ZGroup, supgroup::SL2ZGroup)
  all(g in supgroup for g in gens(subgroup))
end

centerorder(gp::SL2ZGroup) = iseven(gp) ? 2 : 1

################################################################################
# show
################################################################################

function show(io::IO, group::SL2ZFullGroup)
  print(io, "SL(2,ℤ)")
end

function show(io::IO, group::SL2ZSubgroup)
  if _issl2z(group)
    print(io, "SL(2,ℤ)")
  elseif isstandard_congruence(group)
    print(io, "$(string(get_attribute(group, :congruence)))($(level(group)))")
  else
    print(io, "SL(2,ℤ)—subgroup")
  end
end

################################################################################
# creation: auxiliar functions
################################################################################

function in_sl2z()
  function contains(g::SL2ZElem)
    return true
  end
  (contains, contains)
end

function in_sl2z_gamma0(N::Union{Int,fmpz})
  N = ZZ(N)
  function contains(g::SL2ZElem)
    divisible(g.c,N)
  end
  (contains, contains)
end

function in_sl2z_gamma1(N::Union{Int,fmpz})
  N = ZZ(N)
  one = ZZ(1)
  negone = ZZ(-1)
  tmp = fmpz()
  function contains(g::SL2ZElem)
    divisible(g.c,N) && divisible(add!(tmp,g.a,negone),N)
  end
  function contains_plusminus(g::SL2ZElem)
    divisible(g.c,N) &&
        (divisible(add!(tmp,g.a,negone),N) || divisible(add!(tmp,g.a,one),N))
  end
  (contains, contains_plusminus)
end

function in_sl2z_gamma(N::Union{Int,fmpz})
  N = ZZ(N)
  one = ZZ(1)
  negone = ZZ(-1)
  tmp = fmpz()
  function contains(g::SL2ZElem)
    divisible(g.c,N) && divisible(g.b,N) && divisible(add!(tmp,g.a,negone),N)
  end
  function contains_plusminus(g::SL2ZElem)
    divisible(g.c,N) && divisible(g.b,N) &&
        (divisible(add!(tmp,g.a,negone),N) || divisible(add!(tmp,g.a,one),N))
  end
  (contains, contains_plusminus)
end

# Containment function for non-split Cartan subgroup.
function in_sl2z_gamma_ns(N::Union{Int,fmpz})
  N = ZZ(N)
  if iseven(N) || !issquarefree(N)
    throw(DomainError(N, "level must be odd and squarefree"))
  end

  # EXPLANATION:
  # Let disc be the smallest positive integer congruent to 1 modulo 4, such that
  # disc is a _quadratic non-residue_ modulo p for every prime p | N.
  # Since disc(QQ(√disc))=disc, this means that every prime p | N, is inert in
  # QQ(√disc).
  disc = ZZ(1)
  for e in Iterators.countfrom(1,4)
    if all([jacobi_symbol(e,p) == -1 for (p,_) in factor(N)])
      disc = ZZ(e)
      break
    end
  end

  function contains(g::SL2ZElem)
    divisible(g.a-g.d,N) && divisible(disc*g.b-g.c,N)
  end
  return (contains, contains)
end

function in_sl2z_normalizer(gp::SL2ZSubgroup)
  function contains(g::SL2ZElem)
    all([g*s*g^(-1) in gp for s in gens(gp)])
  end
  return (contains, contains)
end

################################################################################
# creation
################################################################################

function normalizer(gp::SL2ZSubgroup)
  ngp = SL2ZSubgroup(FareySymbolSL2Z(in_sl2z_normalizer(gp)...))
  return ngp
end

function Gamma0SL2Z(N::Int)
  gp = SL2ZSubgroup(FareySymbolSL2Z(in_sl2z_gamma0(N)...))
  set_attribute!(gp, :level => N)
  set_attribute!(gp, :congruence => :Γ_0)
  return gp
end

function Gamma1SL2Z(N::Int)
  gp = SL2ZSubgroup(FareySymbolSL2Z(in_sl2z_gamma1(N)...))
  set_attribute!(gp, :level => N)
  set_attribute!(gp, :congruence => :Γ_1)
  return gp
end

function GammaSL2Z(N::Int)
  gp = SL2ZSubgroup(FareySymbolSL2Z(in_sl2z_gamma(N)...))
  set_attribute!(gp, :level => N)
  set_attribute!(gp, :congruence => :Γ)
  return gp
end

function GammaNSSL2Z(N::Int)
  gp = SL2ZSubgroup(FareySymbolSL2Z(in_sl2z_gamma_ns(N)...))
  set_attribute!(gp, :level => N)
  set_attribute!(gp, :congruence => :Γ_ns)
  return gp
end

function GammaNSPlusSL2Z(N::Int)
  gp = normalizer(GammaNS(N))
  set_attribute!(gp, :level => N)
  set_attribute!(gp, :congruence => :(Γ_ns^+))
  return gp
end

@doc Markdown.doc"""
    Gamma0(N::Int)

Return the congruence subgroup $\Gamma_0(N) \subseteq \mathrm{SL}_2(\mathbb{Z})$,
whose elements $\gamma$ have the relation $c(\gamma) \equiv 0 \pmod N$.
"""
Gamma0(N::Int) = Gamma0SL2Z(N)

@doc Markdown.doc"""
    Gamma1(N::Int)

Return the congruence subgroup $\Gamma_1(N) \subseteq \mathrm{SL}_2(\mathbb{Z})$,
whose elements $\gamma$ have the relations $a(\gamma) \equiv 1 \pmod N$,
$c(\gamma) \equiv 0 \pmod N$ and $d(\gamma) \equiv 1 \pmod N$.
"""
Gamma1(N::Int) = Gamma1SL2Z(N)

@doc Markdown.doc"""
    Gamma(N::Int)

Return the congruence subgroup $\Gamma(N) \subseteq \mathrm{SL}_2(\mathbb{Z})$,
whose elements $\gamma$ are congruent to the identity modulo $N$.
"""
Gamma(N::Int) = GammaSL2Z(N)

@doc Markdown.doc"""
    GammaNS(N::Int)

Return the non-split Cartan subgroup $\Gamma_{\mathrm{ns}}(N) \subseteq
\mathrm{SL}_2(\mathbb{Z})$.
"""
GammaNS(N::Int) = GammaNSSL2Z(N)

@doc Markdown.doc"""
    GammaNSPlus(N::Int)

Return $\Gamma_{\mathrm{ns}}^+(N) \subseteq \mathrm{SL}_2(\mathbb{Z})$, the
normalizer of a non-split Cartan subgroup.
"""
GammaNSPlus(N::Int) = GammaNSPlusSL2Z(N)

Gamma0(::SL2ZFullGroup, N::Int) = Gamma0SL2Z(N)
Gamma1(::SL2ZFullGroup, N::Int) = Gamma1SL2Z(N)
Gamma(::SL2ZFullGroup, N::Int) = GammaSL2Z(N)
GammaNS(::SL2ZFullGroup, N::Int) = GammaNSSL2Z(N)
GammaNSPlus(::SL2ZFullGroup, N::Int) = GammaNSPlusSL2Z(N)

function subgroup(gp::SL2ZFullGroup, f)
  SL2ZSubgroup(FareySymbolSL2Z_MODULE.FareySymbolSL2Z(f))
end

function subgroup(gp::SL2ZSubgroup, f)
  SL2ZSubgroup(FareySymbolSL2Z_MODULE.FareySymbolSL2Z(g -> g in gp && f(g)))
end

function intersect(gp::SL2ZGroup, hp::SL2ZGroup)
  SL2ZSubgroup(FareySymbolSL2Z_MODULE.FareySymbolSL2Z(g -> g in gp && g in hp))
end

################################################################################
# comparison
################################################################################

@doc Markdown.doc"""
    ==(ggp::SL2ZGroup, hgp::SL2ZGroup)

Return true if the groups are the same.
"""
function ==(ggp::SL2ZGroup, hgp::SL2ZGroup)
  ggp === hgp ||
    all(g in hgp for g in gens(ggp)) &&
    all(h in ggp for h in gens(hgp))
end

################################################################################
# generators
################################################################################

@doc Markdown.doc"""
    ngens(::SL2ZGroup)

Return the number of generators of the group.
"""
function ngens(::SL2ZGroup) end

ngens(::SL2ZFullGroup) = 2

function ngens(group::SL2ZSubgroup)
  if _issl2z(group)
    return 2
  else
    return length(group.farey_symbol.generators)
  end
end

@doc Markdown.doc"""
    gens(group::SL2ZGroup)

Return the generators of the group.
"""
function gens(group::SL2ZGroup) end

gens(::SL2ZFullGroup) = [SL2ZElem(0,-1,1,0), SL2ZElem(1,1,0,1)]

function gens(group::SL2ZSubgroup)
  [group(g; check = false) for g in group.farey_symbol.generators]
end

function gen_relation_words(::SL2ZFullGroup)
  return [[(1,4)],
          [(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1),(1,1),(2,1)]]
end

function gen_relation_words(group::SL2ZSubgroup)
  error("not implemented")
end

################################################################################
# cosets
################################################################################

@doc Markdown.doc"""
    index(::SL2ZGroup)

Return the index of the group, whose supergroup is $\mathrm{SL}_2(\mathbb{Z})$.
"""
function index(::SL2ZGroup) end

index(::SL2ZFullGroup) = 1

function index(group::SL2ZSubgroup)
  if iseven(group)
    return length(farey_symbol(group).coset_reps)
  else
    return 2*length(farey_symbol(group).coset_reps)
  end
end

@doc Markdown.doc"""
    index(supgroup::SL2ZGroup, group::SL2ZSubgroup)

Return the index of subgroup with respect to supergroup. Return error if group
is not contained in supergroup.
"""
function index(::SL2ZGroup, group::SL2ZSubgroup) end

index(::SL2ZFullGroup, group::SL2ZSubgroup) = index(group)

function index(supgroup::SL2ZSubgroup, subgroup::SL2ZSubgroup)
  _issl2z(supgroup) || issubgroup(subgroup, supgroup) || error("not included")
  divexact(index(subgroup), index(supgroup))
end

# left cosets

left_coset_representatives(::SL2ZFullGroup) = [one(SL2Z)]

left_coset_representatives(::SL2ZFullGroup, ::SL2ZFullGroup) = left_coset_representatives(SL2Z)

function left_coset_representatives(group::SL2ZSubgroup)
  if iseven(group)
    return map(SL2Z, farey_symbol(group).coset_reps)
  else
    return vcat(map(SL2Z, farey_symbol(group).coset_reps),
                map(m -> SL2Z(-m), farey_symbol(group).coset_reps)
               )
  end
end

function left_coset_representatives(::SL2ZFullGroup, grp::SL2ZSubgroup)
  left_coset_representatives(grp)
end

function left_coset_representatives(supgroup::SL2ZSubgroup, subgroup::SL2ZSubgroup)
  _issl2z(supgroup) || error("general super groups not implemented")
  map(supgroup, left_coset_representatives(subgroup))
end


function left_coset_transformation_with_index(grp::SL2ZFullGroup, m::SL2ZElem)
  # This must be the trivial coset, since parent(m) == SL2Z == grp
  return (one(grp), 1, m)
end

function left_coset_transformation_with_index(group::SL2ZSubgroup, m::SL2ZElem)
  (mp,mx,ga) = FareySymbolSL2Z_MODULE.left_coset_transformation_with_index(
                   farey_symbol(group),m)
  if mx > 0
    return (mp,mx,group(ga))
  else
    return (mp,length(farey_symbol(group).coset_reps)-mx,group(ga))
  end
end

function left_coset_transformation_with_index(subgroup::SL2ZSubgroup, m::SL2ZSubgroup)
  supgroup = parent(m)
  _issl2z(supgroup) || error("general super groups not implemented")
  (mp,mx,ga) = left_coset_transformation_with_index(subgroup, m.ga)
  return (supgroup(mp),mx,ga)
end


function left_coset_transformation(grp::SL2ZFullGroup, m::SL2ZElem)
  # This must be the trivial coset, since parent(m) == SL2Z == grp
  (one(grp), m)
end

function left_coset_transformation(subgrp::SL2ZSubgroup, m::SL2ZElem)
  (mp,ga) = FareySymbolSL2Z_MODULE.left_coset_transformation(farey_symbol(subgrp),m)
  return (mp,subgrp(ga))
end

function left_coset_transformation(subgrp::SL2ZSubgroup, m::SL2ZSubgroupElem)
  supgrp = parent(m)
  _issl2z(supgrp) || error("general super groups not implemented")
  (mp,ga) = left_coset_transformation(subgrp, m)
  return (supgrp(mp), subgrp(ga))
end

# inverse of left cosets, which are a kind of right cosets

function leftinv_coset_representatives(group::SL2ZGroup)
  map(inv, left_coset_representatives(group))
end

function leftinv_coset_representatives(supgroup::SL2ZGroup, subgroup::SL2ZGroup)
  map(inv, left_coset_representatives(supgroup,subgroup))
end

function leftinv_coset_transformation_with_index(subgrp::SL2ZGroup, m::SL2ZElement)
  (mp,mx,ga) = left_coset_transformation_with_index(subgrp, inv(m))
  return (inv(ga),inv(mp),mx)
end

function leftinv_coset_transformation(group::SL2ZGroup, m::SL2ZElement)
  (mp,ga) = left_coset_transformation(group, inv(m))
  return (inv(ga),inv(mp))
end

# right cosets

function right_coset_representatives(group::SL2ZGroup)
  leftinv_coset_representatives(group)
end

function right_coset_representatives(supgroup::SL2ZGroup, subgroup::SL2ZGroup)
  leftinv_coset_representatives(supgroup,subgroup)
end

function right_coset_transformation_with_index(subgrp::SL2ZGroup, m::SL2ZElement)
  leftinv_coset_transformation_with_index(subgrp, m)
end

function right_coset_transformation(grp::SL2ZGroup, m::SL2ZElement)
  leftinv_coset_transformation(grp, m)
end

# inverse of right cosets (a set of left cosets)
# NOTE: we use that right cosets are leftinv cosets.

function rightinv_coset_representatives(group::SL2ZGroup)
  left_coset_representatives(group)
end

function rightinv_coset_representatives(supgroup::SL2ZGroup, subgroup::SL2ZGroup)
  left_coset_representatives(supgroup, subgroup)
end

function rightinv_coset_transformation_with_index(subgrp::SL2ZGroup, m::SL2ZElement)
  left_coset_transformation_with_index(subgrp, m)
end

function rightinv_coset_transformation(grp::SL2ZGroup, m::SL2ZElement)
  left_coset_transformation(grp, m)
end

################################################################################
# conjucation
################################################################################

function left_conjugate(g::SL2ZElement, grp::SL2ZGroup)
  left_conjugate(g, inv(g), grp)
end

function left_conjugate(g::SL2ZElement, ginv::SL2ZElement, grp::SL2ZGroup)
  g = SL2Z(g)
  ginv = SL2Z(ginv)
  return subgroup(SL2Z, h -> ginv*h*g in grp)
end

function right_conjugate(g::SL2ZElement, grp::SL2ZGroup)
  left_conjugate(inv(g), g, grp)
end

function right_conjugate(g::SL2ZElement, ginv::SL2ZElement, grp::SL2ZGroup)
  left_conjugate(ginv, g, grp)
end

################################################################################
# random
################################################################################

function rand(r::AbstractRNG, ::Type{SL2ZSubgroup}, complexity::UnitRange{Int})
  gptype = rand(r, 1:3)
  N = rand(r, complexity)
  if gptype == 1
    Gamma0SL2Z(N)
  elseif gptype == 2
    Gamma1SL2Z(N)
  else
    GammaSL2Z(N)
  end
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

in(g::SL2ZElem, G::SL2ZGroup) = farey_symbol(G).iselement(g)

in(g::SL2ZSubgroupElem, G::SL2ZGroup) = SL2Z(g) in G

################################################################################
# display
################################################################################

function show(io::IO, g::SL2ZElem)
  print(io,"[$(g.a) $(g.b); $(g.c) $(g.d)]")
end

function show(io::IO, g::SL2ZSubgroupElem)
  show(io,SL2Z(g))
end

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    one(g::SL2ZElement)

Return the identity element of $g$'s parent.
"""
one(::Type{SL2ZElem}) = SL2ZElem(1,0,0,1)
one(g::SL2ZSubgroupElem) = SL2ZSubgroupElem(parent(g), SL2ZElem(1,0,0,1))

@doc Markdown.doc"""
    one(group::SL2ZGroup)

Return the identity matrix of group.
"""
one(group::SL2ZGroup) = group(1, 0, 0, 1)


copy(g::SL2ZElem) = SL2ZElem(g.a,g.b,g.c,g.d)
copy(m::SL2ZSubgroupElem) = SL2ZSubgroupElem(parent(m), m.ga)


function deepcopy_internal(g::SL2ZElem, stackdict::IdDict) where T
  return SL2ZElem(
          deepcopy_internal(g.a, stackdict),
          deepcopy_internal(g.b, stackdict),
          deepcopy_internal(g.c, stackdict),
          deepcopy_internal(g.d, stackdict))
end

function deepcopy_internal(g::SL2ZSubgroupElem, stackdict::IdDict) where T
  return parent(g)(
          deepcopy_internal(g.ga.a, stackdict),
          deepcopy_internal(g.ga.b, stackdict),
          deepcopy_internal(g.ga.c, stackdict),
          deepcopy_internal(g.ga.d, stackdict))
end


function (::SL2ZFullGroup)()
  SL2ZElem()
end

function (::SL2ZFullGroup)(
    a::Union{Int,fmpz},
    b::Union{Int,fmpz},
    c::Union{Int,fmpz},
    d::Union{Int,fmpz}
   )
  SL2ZElem(a,b,c,d)
end

function (group::SL2ZSubgroup)()
  group(SL2ZElem())
end

function (group::SL2ZSubgroup)(
    a::Union{Int,fmpz},
    b::Union{Int,fmpz},
    c::Union{Int,fmpz},
    d::Union{Int,fmpz}
   )
  group(SL2ZElem(a,b,c,d))
end

function SL2ZElem_from_bottom_row(
    c :: Union{Int,fmpz},
    d :: Union{Int,fmpz};
    return_nothing :: Bool = false
   )
  h,bneg,a = gcdx(fmpz(c),fmpz(d))
  if !isone(h)
    if return_nothing
      return nothing
    else
      throw(DomainError(h, "gcd of c(=$c) and d(=$d) must be one"))
    end
  end
  return SL2ZElem(a,-bneg,c,d)
end

function SL2zElem_from_bottom_row(
    c :: Int,
    d :: Int;
    return_nothing :: Bool = false
   )
  h,bneg,a = gcdx(c,d)
  if !isone(h)
    if return_nothing
      return nothing
    else
      throw(DomainError(h, "gcd of c(=$c) and d(=$d) must be one"))
    end
  end
  return SL2zElem(a,-bneg,c,d)
end

################################################################################
# conversion
################################################################################

@doc Markdown.doc"""
    (group::SL2ZGroup)(m::SL2ZElement)

Return element in group if the element is contained in it, else return error.
"""
function (group::SL2ZGroup)(m::SL2ZElement) end

(::SL2ZFullGroup)(m::SL2ZElem; check :: Bool = true) = m

(::SL2ZFullGroup)(m::SL2ZSubgroupElem; check :: Bool = true) = m.ga

function (group::SL2ZSubgroup)(m::SL2ZElem; check :: Bool = true)
  if check && !(m in group)
    throw(DomainError(m, "element not contained in group $group"))
  end
  return SL2ZSubgroupElem(group, m)
end

function (group::SL2ZSubgroup)(m::SL2ZSubgroupElem; check :: Bool = true)
  if check && !(m in group)
    throw(DomainError(m, "element not contained in group $group"))
  end
  return SL2ZSubgroupElem(group, SL2Z(m))
end

function (matspace::FmpzMatSpace)(g::SL2ZElem)
  m = matspace()
  m[1, 1] = set!(m[1, 1], g.a)
  m[1, 2] = set!(m[1, 2], g.b)
  m[2, 1] = set!(m[2, 1], g.c)
  m[2, 2] = set!(m[2, 2], g.d)
  return m
end
(ms::FmpzMatSpace)(g::SL2ZSubgroupElem) = ms(g.ga)

matrix(g::SL2ZElem) = MatrixSpace(ZZ, 2, 2)(g)
matrix(g::SL2ZSubgroupElem) = matrix(g.ga)

################################################################################
# comparison
################################################################################

isone(m::SL2ZElem) = iszero(m.b) && iszero(m.c) && isone(m.a)

isone(m::SL2ZSubgroupElem) = isone(m.ga)

==(g::SL2zElem, h::SL2zElem) = g.a == h.a && g.b == h.b && g.c == h.c && g.d == h.d

==(g::SL2ZElem, h::SL2ZElem) = g.a == h.a && g.b == h.b && g.c == h.c && g.d == h.d

==(g::SL2ZSubgroupElem, h::SL2ZSubgroupElem) = parent(g) == parent(h) && g.ga == h.ga

==(g::SL2ZElem, h::SL2ZSubgroupElem) = parent(h) == SL2Z && g == SL2Z(h)
==(g::SL2ZSubgroupElem, h::SL2ZElem) = parent(g) == SL2Z && SL2Z(g) == h

hash(g::SL2ZElem) = xor(hash(g.a), hash(g.b), hash(g.c))
hash(g::SL2ZSubgroupElem) = hash(SL2Z(g))

################################################################################
# cusps
################################################################################

function cusp_representatives(grp::SL2ZGroup)
  push!(map(PSL2ZCusp,farey_symbol(grp).cusp_class_finite_reps), PSL2ZCusp())
end

function cusp_representative(grp::SL2ZSubgroup, c::PSL2ZCusp)
  cusp_transformation(grp,c)[2]
end

function cusp_transformation(grp::SL2ZSubgroup, c::PSL2ZCusp)
  FareySymbolSL2Z_MODULE.cusp_transformation(farey_symbol(grp),c)
end

# action on cusps; SL2ZElem action defined already

@doc Markdown.doc"""
    moebius(g::SL2ZElement, tau::Union{fmpq, PSL2ZCusp, acb})

Return the Möbius transformation of $\gamma$ on $\tau$.
"""
function moebius(g::SL2ZElement, q::Union{fmpq, PSL2ZCusp, acb}) end

@doc Markdown.doc"""
    act(g::SL2ZElement, tau::Union{fmpq, PSL2ZCusp, acb})

Alias for `moebius`.
"""
function act(g::SL2ZElement, q::Union{fmpq, PSL2ZCusp, acb}) end

moebius(g::SL2ZSubgroupElem, q::fmpq) = moebius(g.ga,q)
act(g::SL2ZSubgroupElem, q::fmpq) = moebius(g, q)

moebius(g::SL2ZSubgroupElem, q::PSL2ZCusp) = moebius(g.ga,q)
act(g::SL2ZSubgroupElem, q::PSL2ZCusp) = moebius(g, q)

*(g::SL2ZSubgroupElem, q::PSL2ZCusp) = *(g.ga,q)

################################################################################
# action on CC
################################################################################

moebius(g::SL2ZElem, tau::acb) = (g.a*tau + g.b)//(g.c*tau + g.d)
act(g::SL2ZElem, tau::acb) = moebius(g, tau)

moebius(g::SL2ZSubgroupElem, tau::acb) = moebius(g.ga, tau)
act(g::SL2ZSubgroupElem, tau::acb) = moebius(g, tau)

################################################################################
# arithmetic
################################################################################

-(g::G) where {G <: Union{SL2zElem, SL2ZElem}} = G(-g.a, -g.b, -g.c, -g.d)

function *(g::SL2zElem, h::SL2zElem)
  SL2zElem(g.a * h.a + g.b * h.c,
           g.a * h.b + g.b * h.d,
           g.c * h.a + g.d * h.c,
           g.c * h.b + g.d * h.d)
end

function *(g::SL2ZElem, h::SL2ZElem)
  # original matrix
  # SL2ZElem(g.a*h.a+g.b*h.c, g.a*h.b+g.b*h.d,
  #          g.c*h.a+g.d*h.c, g.c*h.b+g.d*h.d)

  a = ZZ()
  a = fmma!(a, g.a, h.a, g.b, h.c)
  b = ZZ()
  b = fmma!(b, g.a, h.b, g.b, h.d)
  c = ZZ()
  c = fmma!(c, g.c, h.a, g.d, h.c)
  d = ZZ()
  d = fmma!(d, g.c, h.b, g.d, h.d)

  SL2ZElem(a, b, c, d)
end

function mul!(f::SL2ZElem, g::SL2ZElem, h::SL2ZElem)
  # original matrix
  # SL2ZElem(g.a*h.a+g.b*h.c, g.a*h.b+g.b*h.d,
  #          g.c*h.a+g.d*h.c, g.c*h.b+g.d*h.d)

  f.a = fmma!(f.a, g.a, h.a, g.b, h.c)
  f.b = fmma!(f.b, g.a, h.b, g.b, h.d)
  f.c = fmma!(f.c, g.c, h.a, g.d, h.c)
  f.d = fmma!(f.d, g.c, h.b, g.d, h.d)

  return f
end

function inv(g::SL2zElem)
  SL2zElem(g.d, -g.b, -g.c, g.a)
end

function inv!(h::SL2ZElem, g::SL2ZElem)
  # We dannot use usual assignment, since this swaps pointers, and hence leaves
  # g in an incorrect state.
  # (h.a, h.d) = (g.d, g.a)
  h.a = set!(h.a, g.d)
  h.d = set!(h.d, g.a)
  h.b = neg!(h.b, g.b)
  h.c = neg!(h.c, g.c)
  return h
end

function inv(g::SL2ZElem)
  h = SL2ZElem()
  inv!(h, g)
  return h
end

function ^(g::Union{SL2zElem,SL2ZElem}, n::Int)
  if n >= 0
    power_by_squaring(g,n)
  else
    power_by_squaring(inv(g), -n)
  end
end

function neg!(g::SL2ZElem, h::SL2ZElem)
  g.a = neg!(g.a, h.a)
  g.b = neg!(g.b, h.b)
  g.c = neg!(g.c, h.c)
  g.d = neg!(g.d, h.d)
  return g
end

-(g::SL2ZSubgroupElem) = SL2ZSubgroupElem(parent(g), -g.ga)

# TODO: We are violating the rule that parents must coincide for arithmetic
# operations. This is to encompass actions on cosets. Remove this once we have
# introduced functions act, act_left, act_right.
function *(g::SL2ZSubgroupElem, h::SL2ZSubgroupElem)
  if parent(g) == parent(h) || h in parent(g)
    return SL2ZSubgroupElem(parent(g), g.ga*h.ga)
  elseif g in parent(h)
    return SL2ZSubgroupElem(parent(h), g.ga*h.ga)
  else
    throw(DomainError((g,h), "incompatible parents"))
  end
end

*(g::SL2ZElem, h::SL2ZSubgroupElem) = g * h.ga
*(g::SL2ZSubgroupElem, h::SL2ZElem) = g.ga * h

function mul!(f::SL2ZSubgroupElem, g::SL2ZSubgroupElem, h::SL2ZSubgroupElem)
  if parent(f) != parent(g) || parent(f) != parent(h)
    throw(DomainError((f,g,h), "incompatible parents"))
  end
  f.ga = mul!(f.ga, g.ga,h.ga)
  return f
end

inv(g::SL2ZSubgroupElem) = SL2ZSubgroupElem(parent(g), inv(g.ga))

^(g::SL2ZSubgroupElem, n::Int) = parent(g)(SL2Z(g)^n)

################################################################################
# action on matrices
################################################################################

*(g::SL2ZElement, h::MatElem) = parent(h)(g) * h
*(h::MatElem, g::SL2ZElement) = h * parent(h)(g)

################################################################################
# operations
################################################################################

tr(g::SL2ZElem) = g.a + g.d
tr(g::SL2ZSubgroupElem) = tr(g.ga)

################################################################################
# generator words
################################################################################

function word_in_gens(g::SL2ZElem)
  word = Vector{Tuple{Int,Int}}()

  a = fmpz()
  b = fmpz()
  c = fmpz()
  d = fmpz()
  q = fmpz()
  r = fmpz()
  t = fmpz()

  a = set!(a,g.a)
  b = set!(b,g.b)
  c = set!(c,g.c)
  d = set!(d,g.d)

  while !iszero(c)
#    if abs(c) > abs(a)
#      push!(word, (1,1))
#      # (a,b,c,d) = (c,d,-a,-b)
#      (a,b,c,d) = (c,d,neg!(a,a),neg!(b,b))
#    end
    if c > 0
      if a > 0
        if c > a
          push!(word, (1,1))
          # (a,b,c,d) = (c,d,-a,-b)
          (a,b,c,d) = (c,d,neg!(a,a),neg!(b,b))
        end
      else
        if c > neg!(t,a)
          push!(word, (1,1))
          # (a,b,c,d) = (c,d,-a,-b)
          (a,b,c,d) = (c,d,neg!(a,a),neg!(b,b))
        end
      end
    else
      if a > 0
        if c < neg!(t,a)
          push!(word, (1,1))
          # (a,b,c,d) = (c,d,-a,-b)
          (a,b,c,d) = (c,d,neg!(a,a),neg!(b,b))
        end
      else
        if c < a
          push!(word, (1,1))
          # (a,b,c,d) = (c,d,-a,-b)
          (a,b,c,d) = (c,d,neg!(a,a),neg!(b,b))
        end
      end
    end

    if c > 0
      if a > 0
        # (q,r) = divrem(a,c)
        (q,r) = divrem!(q,r,a,c)
        push!(word, (2,Int(q)))
        # (a,b,c,d) = (r,b-q*d,c,d)
        q = neg!(q,q)
        t = mul!(t,q,d)
        b = addeq!(b,t)
        (a,r) = (r,a)
      else
        # (q,r) = divrem(-a,c)
        a = neg!(a,a)
        (q,r) = divrem!(q,r,a,c)
        push!(word, (2,-Int(q)))
        # (a,b,c,d) = (-r,b+q*d,c,d)
        r = neg!(r,r)
        t = mul!(t,q,d)
        b = addeq!(b,t)
        (a,r) = (r,a)
      end
    elseif c < 0
      if a > 0
        # (q,r) = divrem(a,-c)
        t = neg!(t,c)
        (q,r) = divrem!(q,r,a,t)
        push!(word, (2,-Int(q)))
        # (a,b,c,d) = (r,b+q*d,c,d)
        t = mul!(t,q,d)
        b = addeq!(b,t)
        (a,r) = (r,a)
      else
        # (q,r) = divrem(-a,-c)
        a = neg!(a,a)
        t = neg!(t,c)
        (q,r) = divrem!(q,r,a,t)
        push!(word, (2,Int(q)))
        # (a,b,c,d) = (-r,b-q*d,c,d)
        q = neg!(q,q)
        t = mul!(t,q,d)
        b = addeq!(b,t)
        (a,r) = (neg!(r,r), a)
      end
    end
  end

  if a == -1
    if !iszero(b)
      push!(word, (2,-Int(b)))
    end
    push!(word, (1,2))
  elseif !iszero(b)
    push!(word, (2,Int(b)))
  end

  return word
end

function word_in_gens(g::SL2ZSubgroupElem) :: Vector{Tuple{Int,Int}}
  FareySymbolSL2Z_MODULE.word_in_gens(farey_symbol(parent(g)), g.ga)
end

# Return (l,[n1,n2,...]) where ga = ± T^l S T^n1 S T^n2 ...
# Essential a minus continued fraction expansion
function _word_in_stn(ga :: G) where {G <: Union{SL2ZElem,SL2zElem}}
  if ga.c >= 0
    a,b,c,d = ga.a,ga.b,ga.c,ga.d
    s = 1
  else
    a,b,c,d = -ga.a,-ga.b,-ga.c,-ga.d
    s = -1
  end

  # We subsume r into the list of ns
  ns = G === SL2zElem ? Int[] : fmpz[]

  while c != 0
    # reduce the d entry
    n,d = fldmod(d,c)
    b -= n*a
    insert!(ns,1,n)
    # swap the entrie, making sure that c is positive
    a,b,c,d = b,-a,d,-c
    s = -1
  end

  if d==1
    l = b
  else
    l = -b
    s = -s
  end

  return (s,l,ns)
end

# Return ([n1,n2,...],r) where ga = ± T^n1 S T^n2 ... T^r
function _word_in_tns(ga :: G) where {G <: Union{SL2ZElem,SL2zElem}}
  s, l,ns = _word_in_stn(ga)
  if isempty(ns)
    return (s,ns,l)
  end
  r = pop!(ns)
  insert!(ns,1,l)
  return (s,ns,r)
end

################################################################################
# random
################################################################################

rand(::Type{SL2zElem}) = rand(GLOBAL_RNG, SL2zElem)
function rand(r::AbstractRNG, ::Type{SL2zElem})
  a = rand(Int)
  b = rand(Int)
  (h,d,c) = gcdx(a,-b)
  return SL2zElem(a÷h, b÷h, c, d)
end

rand(gp::SL2ZGroup, complexity) = rand(GLOBAL_RNG, gp, complexity)
rand(r::AbstractRNG, gp::SL2ZGroup, complexity::Int) = rand(GLOBAL_RNG, gp, -complexity:complexity)

function rand(r::AbstractRNG, gp::SL2ZGroup, complexity::UnitRange{Int})
  if _isgamma0(gp)
    c = rand(r,ZZ,complexity)
    d = rand(r,ZZ,complexity)
    while iszero(c) && iszero(d) || !isone(level(gp)) && iszero(d)
      c = rand(r,ZZ,complexity)
      d = rand(r,ZZ,complexity)
    end
    c = lcm(c,ZZ(level(gp)))

    (h,e,f) = gcdx(c,d)
    while !isone(h)
      c = divexact(c,h); d = divexact(d,h)
      c = lcm(c,ZZ(level(gp)))
      (h,e,f) = gcdx(c,d)
    end
    return gp(f,-e,c,d)
  else
    m = one(SL2ZElem)
    for _ in 1:abs(rand(complexity))
      m = m * farey_symbol(gp).generators[rand(1:ngens(gp))]^rand(complexity)
    end
    return gp(m)
  end
end

################################################################################
# evaluation of Dirichlet characters
################################################################################

function (chi::DirichletCharacter)(g::SL2ZElem)
  if !divisible(g.c, modulus(chi))
    throw(DomainError(g, "element must be contained in Gamma0($(modulus(chi)))"))
  end
  return chi(g.d)
end

(chi::DirichletCharacter)(g::SL2ZSubgroupElem) = chi(g.ga)

################################################################################
################################################################################
# default instances
################################################################################
################################################################################

@doc Markdown.doc"""
    SL2Z

Return the modular group $\mathrm{SL}_2(\mathbb{Z})$.
"""
SL2Z = SL2ZFullGroup(FareySymbolSL2Z(in_sl2z()...))
