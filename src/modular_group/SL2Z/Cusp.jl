################################################################################
################################################################################
# cusps for PSL2ZZ, so that Moebius transformations can act
################################################################################
################################################################################

################################################################################
# creation
################################################################################

PSL2ZCusp() = PSL2ZCusp(true, zero(QQ))

PSL2ZCusp(q::fmpq) = PSL2ZCusp(false, q)

PSL2ZCusp(q::Rational{Int}) = PSL2ZCusp(QQ(q))

################################################################################
# attributes
################################################################################

get(c::PSL2ZCusp) = c.isinfty ? throw("cusp at infinity") : c.q

################################################################################
# display
################################################################################

function show(io::IO, c::PSL2ZCusp)
  if c.isinfty
    print(io, "∞")
  else
    show(io,get(c))
  end
end

################################################################################
# comparison
################################################################################

isinfty(c::PSL2ZCusp) = c.isinfty

function ==(c::PSL2ZCusp, d::PSL2ZCusp)
  if c.isinfty
    return d.isinfty
  elseif d.isinfty
    return false
  else
    return c.q == d.q
  end
end

hash(c::PSL2ZCusp) = xor(hash(c.isinfty), hash(c.q))

################################################################################
# action of SL2ZElem
################################################################################

function moebius(g::SL2ZElem, q::fmpq)
  n = numerator(q)
  d = denominator(q)
  h = g.c*n+g.d*d
  if h == 0
    return PSL2ZCusp()
  else
    return PSL2ZCusp((g.a*n+g.b*d)//h)
  end
end
act(g::SL2ZElem, q::fmpq) = moebius(g, q)

function moebius(g::SL2ZElem, q::PSL2ZCusp)
  if isinfty(q)
    if iszero(g.c)
      return PSL2ZCusp()
    else
      return PSL2ZCusp(g.a//g.c)
    end
  else
    return moebius(g, get(q))
  end
end
act(g::SL2ZElem, q::PSL2ZCusp) = moebius(g, q)

*(g::SL2ZElem, q::PSL2ZCusp) = moebius(g,q)

################################################################################
# action of SL2ZElem
################################################################################

function cusp_inv_transformation(c::fmpq)
  (_, a, b) = gcdx(numerator(c),denominator(c))
  m = SL2ZElem(a,b,-denominator(c),numerator(c))
end

function cusp_inv_transformation(c::PSL2ZCusp)
  isinfty(c) && return SL2ZElem(1,0,0,1)
  cusp_inv_transformation(get(c))
end

################################################################################
# random
################################################################################

rand(::Type{PSL2ZCusp}, complexity) = rand(GLOBAL_RNG, PSL2ZCusp, complexity)

function rand(r::AbstractRNG, ::Type{PSL2ZCusp}, complexity)
  ## strongly biased towards the cusp at infinity
  if rand(r, Bool)
    return PSL2ZCusp()
  else
    return PSL2ZCusp(rand(r,QQ,complexity))
  end
end
