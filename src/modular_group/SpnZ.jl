################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

genus(gp::SpnZTwistCoverGroup) = gp.genus

function paramodularlevel(gp::G; padded :: Bool = false) where {G <: Union{SpnZTwistCoverGroup, HeisenbergSpnZTwistGroup}}
  if !padded || genus(gp) == length(gp.paramodularlevel)
    return gp.paramodularlevel
  end
  return vcat(fill(1,genus(gp)-length(gp.paramodularlevel)), gp.paramodularlevel)
end

centerext(gp::SpnZTwistCoverGroup) = gp.centerext

isspnz(gp::G) where G = false
isspnz(gp::SpnZTwistCoverGroup) = isempty(paramodularlevel) && isempty(centerext(gp))

isspnzcover(gp::G) where G = false
isspnzcover(gp::SpnZTwistCoverGroup) = isempty(paramodularlevel)

ismpnz(gp::G) where G = false
ismpnz(gp::SpnZTwistCoverGroup) = isempty(paramodularlevel) && cocycletag(gp) == mpnz_cocycletag(genus(gp))

cocycletag(gp::SpnZTwistCoverGroup) = gp.cocycle

function ngens(gp::SpnZTwistCoverGroup)
  isspnz(gp) || error("not implemented")
  return 1 + div(genus(gp)*(genus(gp)+1), 2)
end

function gens(gp::SpnZTwistCoverGroup)
  isspnz(gp) || error("not implemented")
  return [symplectic_involution(gp), translations(gp)]
end

function symplectic_involution(gp::SpnZTwistCoverGroup)
  n = genus(gp)
  lvl = paramodularlevel(gp; padded = false)
  return SpnZTwistCoverElem(gp,
      zero_matrix(ZZ,n,n),
      diag_matrix(QQ, vcat(fill(-one(QQ),n-length(lvl)), [-one(QQ)//l for l in lvl])),
      diag_matrix(ZZ, vcat(fill(one(ZZ),n-length(lvl)), [ZZ(l) for l in lvl])),
      zero_matrix(ZZ,n,n)
      )
end

function translations(gp::SpnZTwistCoverGroup)
  n = genus(gp)
  lvl = paramodularlevel(gp)
  trans = SpnZTwistCoverElem[]
  for ix in 1:n
    b = zero_matrix(QQ,n,n)
    b[ix,ix] = one(QQ)//lvl[ix]
    push!(trans,
          SpnZTwistCoverElem(gp,
              identity_matrix(ZZ,n,n),
              b,
              zero_matrix(ZZ,n,n),
              identity_matrix(ZZ,n,n)
              )
        )
    for jx in ix+1:n
      b = zero_matrix(QQ,n,n)
      # here we assume that the level
      b[ix,jx] = one(QQ)//lvl[ix]
      b[jx,ix] = one(QQ)//lvl[ix]
      push!(trans,
            SpnZTwistCoverElem(gp,
                identity_matrix(ZZ,n,n),
                b,
                zero_matrix(ZZ,n,n),
                identity_matrix(ZZ,n,n)
                )
          )
    end
  end
  return trans
end
  
################################################################################
# creation
################################################################################

siegel_modular_group(genus::Int) = SpnZTwistCoverGroup(genus, [], [], spnz_cocycle)

siegel_modular_metaplectic_group(genus::Int) = SpnZTwistCoverGroup(genus, [], [2], mpnz_cocycle)

################################################################################
# auxiliar
################################################################################

function check_parent(a::SpnZTwistCoverGroup, b::SpnZTwistCoverGroup)
   (genus(a) == genus(b) && paramodularlevel(a) == paramodularlevel(b)) ||
      error("Incompatible paramodular groups")
end

################################################################################
# cocycles
################################################################################

function spnz_cocycle(a::SpnZTwistCoverElem, b::SpnZTwistCoverElem)
  error("should never be called")
end

spnz_cocycletag(::Int) = spnz_cocycle

function mpnz_cocycle(g::SpnZTwistCoverElem, h::SpnZTwistCoverElem, gh::SpnZTwistCoverElem)
  # The cocyclye omege(g,i) attached to an element g is sqrt(det(c*i+d)) with
  # argument in (-pi/2, pi/2]. The cocycle for the of g and h is
  #   sqrt(det(cg*moebius(h,i)+dg)) * sqrt(det(ch*i+dh)) / sqrt(det(cgh*i+dgh))
  # and this can be evaluated numerically.

  prc = 10
  i = RootOfUnity(4) * identity_matrix(QQab, genus(parent(g)))
  gomsq = det(g.c*moebius(h,i)+g.d)
  homsq = det(h.c*i+h.d)
  ghomsq = det(gh.c*i+gh.d)

  function omega_angle(a::cf_elem, RR::ArbField)
    if a != -1
      ang = angle(CC(a))/2
      if !contains_negative(ang - const_pi(RR)/2)
        return ang - const_pi(RR)
      else
        return ang
      end
    else
      return const_pi(RR)/2
    end
  end

  while true
    RR = ArbField(prc)
    gomhom = omega_angle(gomsq, RR) + omega_angle(homsq, RR)
    ghom = omega_angle(ghomsq, RR)

    if overlaps(gomhom, ghom)
      if !overlaps(gomhom, ghom + const_pi(RR)) && overlaps(gomhom, ghom - const_pi(RR))
        return +1
      end
    elseif overlaps(gomhom, ghom + const_pi(RR)) || overlaps(gomhom, ghom - const_pi(RR))
      return -1
    end

    prc += 10
  end
end

mpnz_cocycletag(::Int) = mpnz_cocycle

################################################################################
# subgroups
################################################################################

function klingensubgroup(gp::SpnZTwistCoverGroup, cogenus::Int)
  cogenus == 0 && return gp
  cogenus < 0 && error("cogenus must be positive (is $cogenus)")
  cogenus >= genus(gp) &&
      error("cogenus must less than genus of group (is $cogenus >= $(genus(gp)))")

  length(paramodularlevel(gp)) <= cogenus ||
      error("reductive part of Klingen subgroup is not a SL(2,Z) cover")

  if cogenus+1 == genus(gp)
    if cocycletag(gp) == spnz_cocycletag(genus(gp))
      sgp = SL2Z
    elseif cocycletag(gp) == mpnz_cocycletag(genus(gp))
      # NOTE: We assume that the implementations of Mp1Z and MpnZ cocycles
      # are compatibe to each other.
      sgp = Mp1Z
    else
      error("not implemented")
    end
  else
    # NOTE: We rely on a uniform implementation of cocycles for all genera
    # greater than 1.
    sgp = SpnZTwistCoverGroup(genus(gp)-cogenus,
              paramodularlevel(gp), centerext, cocycle(gp))
  end

  return JacobiGroup(sgp, SiegelHeisenbergGroup(ZZ, genus(gp)-cogenus, cogenus))
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# action on CC
################################################################################

function moebius(g::SpnZTwistCoverElem, tau::acb_mat)
  msCC = parent(tau)
  return (msCC(g.a)*tau + msCC(g.b)) * inv(msCC(g.c)*tau + msCC(g.d))
end

################################################################################
# arithmetic
################################################################################

-(g::SpnZTwistCoverElem) = SpnZTwistCoverElem(parent(g), -g.a, -g.b, -g.c, -g.d)

function *(g::SpnZTwistCoverElem, h::SpnZTwistCoverElem)
  check_parent(g,h)
  msZZ = parent(g.a)
  msQQ = parent(g.b)
  SpnZTwistCoverElem(
      g.a*h.a+msZZ(g.b*msQQ(h.c)), msQQ(g.a)*h.b+g.b*msQQ(h.d),
      g.c*h.a+g.d*h.c,             msZZ(msQQ(g.c)*h.b)+g.d*h.d
     )
end

function Base.inv(g::SpnZTwistCoverElem)
  SpnZTwistCoverElem(parent(g),
      transpose(g.d), -transpose(g.b),
      -transpose(g.c), transpose(g.a))
end

function ^(g::SpnZTwistCoverElem, n::Int)
  if n >= 0
    power_by_squaring(g,n)
  else
    power_by_squaring(inv(g), -n)
  end
end

function neg!(g::SpnZTwistCoverElem)
  neg!(g.a)
  neg!(g.b)
  neg!(g.c)
  neg!(g.d)
end
