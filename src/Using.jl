using Base:
  Iterators

using IterTools:
  imap

import Markdown

using Nemo
import AbstractAlgebra
import Nemo
import Hecke
import GAP

using AbstractAlgebra:
  @enable_all_show_via_expressify,
  @attributes,
  Field,
  FieldElem,
  Group,
  GroupElem,
  Module,
  Ring,
  RingElem,
  RingElement,
  Set,
  SetElem,
  set_attribute!,
  get_attribute

using AbstractAlgebra.Generic:
  FreeModuleElem,
  PuiseuxSeriesElem,
  RelSeries

using Random:
  AbstractRNG,
  GLOBAL_RNG,
  randperm

using Nemo:
  CyclotomicField,
  FmpqRelSeriesRing,
  FmpzMatSpace,
  FqFiniteField,
  FqNmodFiniteField,
  GaloisField,
  GaloisFmpzField,
  NmodRing,
  QQ,
  ZZ,
  fmpq,
  fmpq_mpoly,
  fmpz,
  libarb,
  libflint,
  nmod,
  nmod_mat

using Hecke:
  PrimesSet,
  Zlattice,
  abelian_group,
  discriminant_group,
  gram_matrix,
  iscyclotomic_type,
  rational_reconstruction,
  rescale

using ModularForms_jll:
  libmodularforms_jll
