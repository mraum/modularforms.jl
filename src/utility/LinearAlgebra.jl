################################################################################
# construction
################################################################################

function diag_matrix(base_ring::R, entries::Vector{A}) where {R <: Ring, A <: RingElement}
  m = zero_matrix(base_ring, length(entries), length(entries))
  for (ix,a) in enumerate(entries)
    m[ix,ix] = a
  end
  return m
end

################################################################################
# properties
################################################################################

function pivots(a::MatElem, rk::Union{Nothing,Int}=nothing)
  if !isexact_type(elem_type(base_ring(a)))
    throw(DomainError(a, "base ring must be exact"))
  end

  if isnothing(rk)
    r,c = size(a)
    if r == 0 || c == 0
      return Int[]
    end
    pivots = [] 
    ix = 1
    for jx in 1:c
      if !iszero(a[ix,jx])
        push!(pivots, jx)
        if ix == r
          return pivots
        else
          ix += 1
        end
      end
    end
    return pivots
  else
    pivots = Vector{Int}(undef,rk)
    jx = 1
    for ix in 1:rk
      while iszero(a[ix,jx])
        jx += 1
      end
      pivots[ix] = jx
    end
    return pivots
  end
end

function pivots_upper_bound(a::M) where {M <: Union{arb_mat,acb_mat}}
  r,c = size(a)
  if r == 0 || c == 0
    return Int[]
  end
  pivots = Int[]
  ix = 1
  for jx in 1:c
    if !contains_zero(a[ix,jx])
      push!(pivots, jx)
      if ix == r
        return pivots
      else
        ix += 1
      end
    end
  end
  return pivots
end

################################################################################
# change of basis
################################################################################

function change_of_basis(a::M, b::M) where {T <: FieldElem, M<:MatElem{T}}
  parent(a) == parent(a) || throw("incompatible parents")

  (nu,m) = nullspace(hcat(a,b))
  nu == ncols(a) || throw("columns do not span the same spaces")

  mspace = MatrixSpace(base_ring(parent(a)), nu, nu)
  return - view(m,1:nu,:) * inv(view(m,(nu+1):nrows(m),:))
end

################################################################################
# smith normal form
################################################################################

# TODO: replace by Nemo functions

function smith_normal_form(M::MatElem{T}) where T
  (r,c) = size(M)
  p = parent(M)
  S = deepcopy(M)
  U = one(MatrixSpace(base_ring(p),r,r))
  V = one(MatrixSpace(base_ring(p),c,c))
  smith_normal_form!(S, U, V)
  return (S, U, V)
end

## todo: if this has to be spead up, obvious improvements are to 
## 1 not transpose explicitely, but change the index order
## 2 do not transform the whole matrix, but only from j on
function smith_normal_form!(M::MatElem{T}, U::MatElem{T}, V::MatElem{T}) where T
  reduce_col = false
  col_reduction_performed = false
  (imax,jmax) = size(M)


  @inline function swap_rows(i1,i2)
    for j in d:jmax
      M[i1,j],M[i2,j] = M[i2,j],M[i1,j]
    end
    for k in 1:nrows(M)
      U[k,i1],U[k,i2] = U[k,i2],U[k,i1]
    end
  end

  @inline function swap_cols(j1,j2)
    for i in d:imax
      M[i,j1],M[i,j2] = M[i,j2],M[i,j1]
    end
    for k in 1:ncols(M)
      V[j1,k],V[j2,k] = V[j2,k],V[j1,k]
    end
  end

  d = 1
  while d <= imax && d <= jmax
    a = M[d,d]

    if !reduce_col
      ## if the diagonal entry is zero try to swap the row
      if iszero(a)
        for i = d+1:imax
          if !iszero(M[i,d])
            swap_rows(d,i)
            a = M[d,d]
            break
          end
        end
        if iszero(a)
          # the whole column is zero; move it to the end
          swap_cols(d,jmax)
          jmax -= 1
          continue
        end
      end
  
      # reduce the row and remember the index of one nonzero entry
      j_nonzero = d
      for j = d+1:jmax
        (q,r) = divrem(M[d,j], a)
        if !iszero(q)
          for i in d:imax
            M[i,j] -= q * M[i,d]
          end
          for k in 1:ncols(M)
            V[d,k] += q * V[j,k]
          end
        end
        if j_nonzero == d && !iszero(r)
          j_nonzero = j
        end
      end
  
      # if all entries vanish, the row is reduced: reduce the column
      if j_nonzero == d
        col_reduction_performed = false
        reduce_col = true
      # otherwise, swap the columns and continue reducing the row
      else
        swap_cols(d,j_nonzero)
      end

    else # reduce_col
      # when reducing the column, the diagonal is guaranteed to be nonzero
      a = M[d,d]

      # reduce the column and remember the index of one nonzero entry
      i_nonzero = d
      for i = d+1:imax
        (q,r) = divrem(M[i,d], a)
        if !iszero(q)
          col_reduction_performed = true
          for j in d:jmax
            M[i,j] -= q * M[d,j]
          end
          for k in 1:nrows(M)
            U[k,d] += q * U[k,i]
          end
        end
        if i_nonzero == d && !iszero(r)
          i_nonzero = i
        end
      end

      # if all entries vanished, the column is reduced
      if i_nonzero == d
        # if all entries vanished prior to reduction, the hook is reducted
        if !col_reduction_performed
          d += 1
        end
        reduce_col = false
      # otherwise, swap the rows and continue reducing the column
      else
        swap_rows(d,i_nonzero)
      end
    end
  end
end
