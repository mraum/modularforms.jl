################################################################################
# orbit algorithm with reduction of elements
################################################################################

left_orbit(gs::Vector{G}, c::C) where {G,C} = left_orbit(gs, c, identity)

function left_orbit(gs::Vector{G}, c::C, reduction::Function) :: Vector{C} where {G,C}
  remaining = C[c]
  orbit = C[]

  while !isempty(remaining)
    c = pop!(remaining)
    push!(orbit,c)
    for g in gs
      gc = reduction(g*c)
      if !(gc in orbit || gc in remaining)
        push!(remaining, gc)
      end
    end
  end

  return orbit
end

################################################################################
# orbits for a single permutation
################################################################################

function orbits(perm::Vector{Int})
  sz = length(perm)

  # The result variable, whose elements are tuples with the following components:
  # First: An orbit
  # Second: A distinguished element of the orbit.
  res = Vector{Int}[]

  unprocessed = Base.Set{Int}(1:sz)
  orbit_unprocessed = Base.Set{Int}()
  while !isempty(unprocessed)
    ix = pop!(unprocessed)

    orbit_unprocessed = Base.Set{Int}([ix])
    orbit = Int[ix]

    while !isempty(orbit_unprocessed)
      ix = pop!(orbit_unprocessed)
      jx = perm[ix]
      ojx = searchsorted(orbit, jx)
      if isempty(ojx)
        ojx = first(ojx)
        insert!(orbit, ojx, jx)
        push!(orbit_unprocessed, jx)
        delete!(unprocessed, jx)
      end
    end

    push!(res, orbit)
  end

  return res
end

