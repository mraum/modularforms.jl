function rotatedims_with_perm(b::AbstractArray{A}, ix::Int) where A
  sz = size(b)
  ndim = length(sz)
  ix <= 0 && throw(DomainError(ix, "dimension index must be positive"))
  if ix > ndim
    throw(DomainError(ix, "dimension index must be less equal number " *
                          "of dimensions"))
  end

  perm = collect(1:ndim)
  perm[1] = ix
  perm[2:ix] .= collect(1:ix-1)
  return (permutedims(b, perm), perm)
end


function merge_sorted!(as::Vector{A}, bs::Vector{A}; lt = isless, by = identity, rev::Union{Nothing,Bool} = nothing, unique:: Bool = false) where A
  # TODO: use to better effect that bs is sorted
  for b in bs
    ix = searchsortedfirst(as,b, by=by, lt=lt, rev=rev)
    if !unique || ix > length(as) || as[ix] != b
      insert!(as,ix,b)
    end
  end
  return as
end


unitvec(ambient::Union{Type,Ring}, size::Int, i::Int) =
  [i==j ? one(ambient) : zero(ambient) for j = 1:size]

unitmat(ambient::Union{Type,Ring}, size::Int) =
  [i==j ? one(ambient) : zero(ambient) for i = 1:size, j = 1:size]


# TODO: consider removing
integralpart(a::Rational{I}) where {I <: Integer} = fld(numerator(a), denominator(a))

fractionalpart(a::Rational{I}) where {I <: Integer} = mod(numerator(a), denominator(a)) // denominator(a)

rand(::Type{I}, complexity) where {I <: Integer} = rand(GLOBAL_RNG, I, complexity)

function rand(r::AbstractRNG, ::Type{I}, complexity) where {I <: Integer}
  convert(I, rand(r, complexity))
end

rand(::Type{Rational{I}}, complexity) where {I <: Integer} = rand(GLOBAL_RNG, Rational{I}, complexity)

function rand(r::AbstractRNG, ::Type{Rational{I}}, complexity) where {I <: Integer}
  d = rand(r, I, complexity)
  while d == 0
    d = rand(r, I, complexity)
  end
  n = rand(r, I, complexity)
  return n // d
end

# TODO: Can these be removed?

function *(a::fq_nmod, b::fmpq)
  K = parent(a)
  return a*K(b.num)*K(b.den)^(-1)
end

function //(a::fq_nmod, b::fmpq) 
  return a*b^(-1)
end

function *(a::fmpq, b::fq_nmod)
  return b*a
end

function //(a::fmpq, b::fq_nmod)
  return a*b^(-1)
end

# In weight 2, Rational{Int} is used.
(K::FqNmodFiniteField)(a::Union{Rational{Int},fmpq}) = K(a.num)*K(a.den)^(-1)
