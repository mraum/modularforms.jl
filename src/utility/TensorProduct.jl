function kronecker_product(as::Vector{A}, bs::Vector{B}) where {A,B}
  [a*b for a in as for b in bs]
end

⊗(a,b) = tensor_product(a,b)
