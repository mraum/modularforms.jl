@doc Markdown.doc"""
    minus_continued_fraction(a::arb, l::Union{Missing,Int} = missing)::Vector{fmpz}

Return the shortest minus continued fraction contained in $a$. If $l$ is
specified and more than $l$ iterations has been made, an error will be
generated.
"""
function minus_continued_fraction(a::arb, l::Union{Missing,Int} = missing)::Vector{fmpz}
  a == 0 && return fmpz[]

  expansion = fmpz[]
  while ismissing(l) ? true : length(expansion) < l
    (flag,w) = unique_integer(ceil(a))
    flag || (ismissing(l) ? break : error("insufficient precision"))
    push!(expansion, w)
    contains(a, w) && break
    a = inv(w - a)
  end

  return expansion
end
