################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# subfields, cached
################################################################################

const QQabSubfieldCache = Dict{Int, Field}()

qqab_subfield_gen_name(order::Int) = string("ζ_",order)
qqab_subfield_var_name(order::Int) = string("_zeta",order)

@doc Markdown.doc"""
    ModularForms.QQabSubfield(order::Int)

Return a subfield $F \subseteq \mathbb{Q}(\zeta_n)$.
"""
function QQabSubfield(order::Int)
  if !iseven(order)
    throw(DomainError(order, "must be even"))
  end
  if haskey(QQabSubfieldCache, order)
    return QQabSubfieldCache[order]
  else
    field = CyclotomicField( order,
                             qqab_subfield_gen_name(order),
                             qqab_subfield_var_name(order) )[1]
    QQabSubfieldCache[order] = field
    return field
  end
end

@doc Markdown.doc"""
    ModularForms.QQabSubfield(order::fmpz)

Return a subfield $F \subseteq \mathbb{Q}(\zeta_n)$.
"""
QQabSubfield(order::fmpz) = QQabSubfield(Int(order))

################################################################################
# properties
################################################################################

@doc Markdown.doc"""
    base_ring(::QQabField)

Return the base ring of given $\mathbb{Q}(\zeta_n)$. In other words it returns
$\mathbb{Q}$.
"""
base_ring(::QQabField) = QQ

characteristic(::QQabField) = 0

################################################################################
# display
################################################################################

function show(io::IO, ::QQabField)
  print(io, "ℚᵃᵇ")
end

################################################################################
# arithmetic
################################################################################

*(a::QQabField, ::QQabField) = a

*(::RootOfUnityGroup, b::QQabField) = b

*(a::QQabField, ::RootOfUnityGroup) = a

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

@doc Markdown.doc"""
    qqab_order(a::cf_elem)

Return the order of given $\mathbb{Q}(\zeta_n)$.
"""
qqab_order(a::cf_elem) = a.order

@doc Markdown.doc"""
    qqab_degree(a::cf_elem)

Return the degree of the given $\mathbb{Q}(\zeta_n)$.
"""
qqab_degree(a::cf_elem) = degree(parent(a.realization))

################################################################################
# display
################################################################################

function show(io::IO, a::cf_elem)
  print(io, a.realization)
end

needs_parentheses(a::cf_elem) = needs_parentheses(a.realization)

isnegative(a::cf_elem) = isnegative(a.realization)

show_minus_one(::Type{cf_elem}) = show_minus_one(nf_elem)

################################################################################
# creation
################################################################################

function deepcopy_internal(a::cf_elem, stackdict::IdDict)
  if haskey(stackdict, a)
    return stackdict[a]
  end

  b = cf_elem(a.order, deepcopy_internal(a.realization, stackdict))
  stackdict[a] = b
  return b
end

@doc Markdown.doc"""
    cf_elem(order::Int, a::cf_elem)

Return an cycloatomic field element as an element in the cyclotomic field of
larger order. We require that the new order is divisble by the old order.
"""
function cf_elem(order::Int, a::cf_elem)
  order == a.order && return a

  divisible(order, a.order) || throw(DomainError((order, a.order), ("order must divide order")))

  conv = map_from_cyclotomic_tower(QQabTower, QQabSubfield(a.order), QQabSubfield(order))
  return cf_elem(order, conv(nf_elem(a)))
end

copy(a::cf_elem) = cf_elem(a.order, copy(nf_elem(a)))

(parent::QQabField)() = zero(parent)

(::QQabField)(a::Union{Int,UInt,Integer,fmpz,fmpq}) = cf_elem(2, QQabSubfield(2)(a))

(::QQabField)(a::cf_elem) = a

zero(parent::QQabField) = parent(zero(QQ))

one(parent::QQabField) = parent(one(QQ))

function root_of_unity(::QQabField, n::Union{Int,UInt,fmpz})
  if isodd(n)
    cf_elem(2*n, gen(QQabSubfield(2*n))^2)
  else
    cf_elem(n, gen(QQabSubfield(n)))
  end
end

function set!(c::cf_elem, a::cf_elem)
  c.order = a.order
  set!(c.realization, a.realization)
end

################################################################################
# conversion
################################################################################

@doc Markdown.doc"""
    (::QQabField)(a::nf_elem)

If cyclotomic type, then return element with parent of `QQabField`. Else
"""
function (::QQabField)(a::nf_elem)
  (flag,m) = iscyclotomic_type(parent(a))
  if QQabSubfield(m) != parent(a)
    throw(DomainError("element not in $(QQab)"))
  end
  return cf_elem(m,deepcopy(a))
end

function (::QQabField)(a::RootOfUnity)
  n = order(a)
  if isodd(n)
    n *= 2
  end
  muinf_embedding!(QQabTower, n)
  return cf_elem(n, QQabTower(QQabSubfield(n),a))
end


function nf_elem(a::cf_elem)
  a.realization
end

function nf_elem(n::Int, a::cf_elem)
  if n == qqab_order(a)
    return nf_elem(a)
  else
    return nf_elem(cf_elem(n,a))
  end
end

function (CC::AcbField)(a::cf_elem)
  z = root_of_unity(CC, qqab_order(a))
  zn = one(CC)
  acc = zero(CC)
  for n in 0:qqab_degree(a)-1
    acc += coeff(a.realization,n)*zn
    zn *= z
  end
  return acc
end

function (mspace::AcbMatSpace)(m::M) where {M <: MatElem{cf_elem}}
  (ncols(mspace) != ncols(m) || nrows(mspace) != nrows(m)) &&
      throw(DomainError(m, "incompatible dimensions"))
  mcc = acb_mat(nrows(m), ncols(m))
  CC = base_ring(mspace)
  mcc.base_ring = CC
  for ix in 1:nrows(m), jx in 1:ncols(m)
    mcc[ix,jx] = CC(m[ix,jx])
  end
  return mcc
end

function matrix(CC::AcbField, m::M) where {M <: MatElem{cf_elem}}
  MatrixSpace(CC, nrows(m), ncols(m))(m)
end

################################################################################
# auxiliar functions
################################################################################

@doc Markdown.doc"""
    unify_qqab_subfields(a::cf_elem, b::cf_elem)

Return the unified subfields,
"""
function unify_qqab_subfields(a::cf_elem, b::cf_elem)
  if a.order == b.order
    return (a.order,a,b)
  elseif a.order % b.order == 0
    return (a.order,a,cf_elem(a.order,b))
  elseif b.order % a.order == 0
    return (b.order,cf_elem(b.order,a),b)
  else
    o = lcm(a.order,b.order)
    return (o,cf_elem(o,a),cf_elem(o,b))
  end
end

function unify_qqab_subfields(ordera::Int, a::Vector{Ta}, orderb::Int, b::Vector{Tb}) where {Ta,Tb}
  if ordera == orderb
    return (ordera,a,b)
  elseif divisible(ordera,orderb)
    return (ordera,a,cf_elem(ordera,b.realization))
  elseif divisible(orderb,ordera)
    return (orderb,cf_elem(orderb,a),b)
  else
    o = lcm(ordera,orderb)
    return (o, cf_elem(o,a), cf_elem(o,b))
  end
end

################################################################################
# comparison
################################################################################

isone(a::cf_elem) = isone(a.realization)

iszero(a::cf_elem) = iszero(a.realization)

isunit(a::cf_elem) = !iszero(a)

function ==(a::cf_elem, b::cf_elem) 
  (_, a, b) = unify_qqab_subfields(a, b)
  return a.realization == b.realization
end

==(a::cf_elem, b::Union{Int,UInt,fmpq,fmpz}) = a.realization == b

==(a::Union{Int,UInt,fmpq,fmpz}, b::cf_elem) = a == b.realization

function isless(a::cf_elem, b::cf_elem)
  (o, a, b) = unify_qqab_subfields(a, b)
  for dx in 0:degree(parent(a.realization))-1
    ac = coeff(a.realization,dx)
    bc = coeff(b.realization,dx)
    ac < bc && return true
    ac > bc && return false
  end
  return false
end

################################################################################
# coefficients
################################################################################

coeff(a::cf_elem, ix::Int) = coeff(a.realization, ix)

function coeffvec(order::Int, a::cf_elem)
  if order != qqab_order(a)
    a = cf_elem(order,a)
  end
  return fmpq[coeff(a.realization,n) for n in 0:qqab_degree(a)-1]
end

################################################################################
# automorphisms
################################################################################

function conj(a::cf_elem)
  ord = qqab_order(a)
  sum([QQab(MuInf((ord-n)//ord)) * coeff(a.realization,n)
      for n in 0:qqab_degree(a)-1])
end

norm(a::cf_elem) = coeff(a*conj(a),0)

function conj(m::M) where {M <: Array{cf_elem}}
  map(conj, m)
end

function conj(m::M) where {M <: MatElem{cf_elem}}
  cm = parent(m)()
  for ix in 1:nrows(m), jx in 1:ncols(m)
    cm[ix,jx] = conj(m[ix,jx])
  end
  return cm
end

################################################################################
# arithmetic
################################################################################

@doc Markdown.doc"""
    +(a::cf_elem, b::cf_elem)

Return
"""
function +(a::cf_elem, b::cf_elem) 
  (o, a, b) = unify_qqab_subfields(a, b)
  return cf_elem(o, a.realization+b.realization)
end

function -(a::cf_elem, b::cf_elem) 
  (o, a, b) = unify_qqab_subfields(a, b)
  return cf_elem(o, a.realization-b.realization)
end

-(a::cf_elem) = cf_elem(a.order, -a.realization)


function *(a::cf_elem, b::cf_elem) 
  (o, a, b) = unify_qqab_subfields(a, b)
  return cf_elem(o, a.realization*b.realization)
end

function //(a::cf_elem, b::cf_elem) 
  (o, a, b) = unify_qqab_subfields(a, b)
  return cf_elem(o, a.realization//b.realization)
end

function divexact(a::cf_elem, b::cf_elem)
  (o, a, b) = unify_qqab_subfields(a, b)
  return cf_elem(o, divexact(a.realization, b.realization))
end

Base.inv(a::cf_elem) = cf_elem(a.order, inv(a.realization))

^(a::cf_elem,n::Int) = cf_elem(a.order, a.realization^n)

^(a::cf_elem,n::fmpz) = a^Int(n)

# unsafe arithmetic

zero!(a::cf_elem) = cf_elem(a.order, zero!(a.realization))

function add!(r::cf_elem, a::cf_elem, b::cf_elem)
  (o, a, b) = unify_qqab_subfields(a, b)
  if o == r.order
    add!(r.realization, a.realization, b.realization)
  else
    r.order = o
    r.realization = a.realization + b.realization
  end
  return r
end

function addeq!(a::cf_elem, b::cf_elem)
  if a.order != b.order
    (o, au, bu) = unify_qqab_subfields(a, b)
    a.order = o
    a.realization = (au+bu).realization
  else
    addeq!(a.realization, b.realization)
  end
  return a
end

function mul!(r::cf_elem, a::cf_elem, b::cf_elem)
  (o, a, b) = unify_qqab_subfields(a, b)
  if o == r.order
    mul!(r.realization, a.realization, b.realization)
  else
    r.order = o
    r.realization = a.realization * b.realization
  end
  return r
end

# embedding of fmpz and fmpq into QQab

function +(a::cf_elem, b::Union{Int, UInt, fmpz, Rational{Int}, fmpq})
  cf_elem(a.order, a.realization+b)
end

function +(a::Union{Int, UInt, fmpz, Rational{Int}, fmpq}, b::cf_elem)
  cf_elem(b.order, a+b.realization)
end

function -(a::cf_elem, b::Union{Int, UInt, fmpz, Rational{Int}, fmpq})
  cf_elem(a.order, a.realization-b)
end

function -(a::Union{Int, UInt, fmpz, Rational{Int}, fmpq}, b::cf_elem)
  cf_elem(b.order, a-b.realization)
end

function *(a::cf_elem, b::Union{Int, UInt, fmpz, Rational{Int}, fmpq})
  cf_elem(a.order, a.realization*b)
end

function *(a::Union{Int, UInt, fmpz, Rational{Int}, fmpq}, b::cf_elem)
  cf_elem(b.order, a*b.realization)
end

# Need macro instead of type union, since otherwise Nemo definition yield
# ambiguous resolutions.
for T in [:Int, :UInt, :fmpz, :(Rational{Int}), :fmpq]
  @eval begin
    //(a::cf_elem, b::$T) = cf_elem(a.order, a.realization//b)

    //(a::$T, b::cf_elem) = cf_elem(b.order, a//b.realization)
  end
end

# embedding MuInf into QQab

+(a::RootOfUnity, b::RootOfUnity) = QQab(a) + QQab(b)
-(a::RootOfUnity, b::RootOfUnity) = QQab(a) - QQab(b)

# +(a::RootOfUnity, b::Union{Int,fmpz,Rational{Int},fmpq,cf_elem}) = QQab(a) + b
# -(a::RootOfUnity, b::Union{Int,fmpz,Rational{Int},fmpq,cf_elem}) = QQab(a) - b
# 
# +(a::Union{Int,fmpz,Rational{Int},fmpq,cf_elem}, b::RootOfUnity) = a + QQab(b)
# -(a::Union{Int,fmpz,Rational{Int},fmpq,cf_elem}, b::RootOfUnity) = a - QQab(b)
# 
# *(a::Union{Int,fmpz,Rational{Int},fmpq}, b::RootOfUnity) = a * QQab(b)
# *(a::RootOfUnity, b::Union{Int,fmpz,Rational{Int},fmpq}) = QQab(a) * b

################################################################################
# square roots
################################################################################

qqab_sqrt(a::Int) = qqab_sqrt(ZZ(a))

function qqab_sqrt(a::fmpz)
  iszero(a) && return zero(QQab)

  b = one(QQab)
  for (p,e) in factor(a)
    (ehalf,even) = divrem(e,2)
    b *= p^ehalf
    if even == 1
      if p == 2
        b *= act(MuInf(1//8), (1 - QQab(MuInf(1//4))))
      else
        p = Int(p)
        sqs = BitSet(mod(a^2,p) for a in 1:p-1)
        b *= sum((a in sqs ? 1 : -1) * QQab(MuInf(a//p)) for a in 1:p-1)
      end
    end
  end

  prec = 30
  CC = AcbField(30)
  local rot
  while true
    rot = angle(CC(b))*4//(2*const_pi(CC))
    (unique,rot) = unique_integer(rot)
    unique && break
    prec *= 2
  end

  if a > 0
    return act(MuInf(-rot//4), b)
  else
    return act(MuInf((1-rot)//4), b)
  end
end

################################################################################
# random
################################################################################

rand(::QQabField, complexity) = rand(GLOBAL_RNG, QQab, complexity)
rand(r::AbstractRNG, ::QQabField, complexity::Int) = rand(GLOBAL_RNG, QQab, 1:complexity)

function rand(r::AbstractRNG, ::QQabField, complexity::UnitRange{Int})
  if complexity[end] < 10
    order = 2
  else
    order = fld(rand(complexity), 5)
    while order <= 0
      order = fld(rand(complexity), 5)
    end
    if isodd(order)
      order += 1
    end
  end
  cf_elem(order, fmpq[rand(QQ, complexity) for _ in 1:order])
end

################################################################################
################################################################################
# default instances
################################################################################
################################################################################

QQab = QQabField()
