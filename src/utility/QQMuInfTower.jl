################################################################################
# creation
################################################################################

function QQMuInfTower()
  QQMuInfTower(
      Dict{Int,Dict{RootOfUnity,nf_elem}}(),
      Dict{Tuple{Int,Int},fmpz_mat}(),
      Dict{Tuple{Int,Int},Tuple{Vector{Int},fmpz_mat,fmpz_mat}}()
     )
end

cyclotomic_tower(::FlintRationalField) = QQabTower

cyclotomic_tower(::QQabField) = QQabTower

################################################################################
# comparison
################################################################################

==(::QQMuInfTower, ::QQMuInfTower) = true

################################################################################
# properties
################################################################################

base_ring(::QQMuInfTower) = QQ

limit_ring(::QQMuInfTower) = QQab

characteristic(f::QQMuInfTower) = 0

# QQMuInfTower is not an instance of AbstractAlgebra.Set
elem_type(::QQMuInfTower) = nf_elem

function muinf_embedding(
    f::QQMuInfTower,
    fQQ::AnticNumberField,
    n::Int
   )
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)
  @assert divisible(m,n)
  return f.muinf_embeddings[m]
end

function muinf_embedding!(f::QQMuInfTower, n::Int)
  if haskey(f.muinf_embeddings, n)
    return f.muinf_embeddings[n]
  end

  muinf_embedding = Dict{RootOfUnity,nf_elem}()
  K = QQabSubfield(n)
  a = gen(K)
  ad = one(K)
  for d in 0:n-2
    muinf_embedding[MuInf(d//n)] = ad
    ad *= a
  end
  muinf_embedding[MuInf((n-1)//n)] = ad

  f.muinf_embeddings[n] = muinf_embedding
  return f.muinf_embeddings[n]
end

function embedding_matrix(f::QQMuInfTower, m::Int, n::Int)
  if haskey(f.field_embeddings, (m,n))
    return f.field_embeddings[m,n]
  end

  fm = muinf_ring(f, m)
  fn = muinf_ring(f, n)

  # the ix-th row corresponds to the coefficient of zeta_m^(ix-1)
  # the jx-th col corresponds to the coefficient of zeta_n^(jx-1)
  mat = zero_matrix(ZZ, degree(fm), degree(fn))

  (k,kmod) = fldmod(n,m)
  kmod == 0 || error("no embedding of m-th (=$m) into n-th (=$n) cyclotomic field")

  # the d*k-th power of zeta_n is zeta_m^d
  for d in 0:div(degree(fn)-1,k)
    mat[d+1,d*k+1] = one(ZZ)
  end

  zeta_n_k = gen(fn)^k
  a = zeta_n_k^(div(degree(fn)-1,k)+1)
  for ix in div(degree(fn)-1,k)+2:degree(fm)
    for jx in 1:degree(fn)
      mat[ix,jx] = numerator(coeff(a,jx-1))
    end
    if ix != degree(fn)
      a *= zeta_n_k
    end
  end

  f.field_embeddings[m,n] = mat
  return mat
end

function retraction_matrix(f::QQMuInfTower, m::Int, n::Int)
  if haskey(f.field_retractions, (m,n))
    return f.field_retractions[m,n]
  end

  fm = muinf_ring(f, m)
  fn = muinf_ring(f, n)

  mat = embedding_matrix(f, n, m)
  mataug = zero_matrix(ZZ,nrows(mat),nrows(mat)+ncols(mat))
  for ix in 1:nrows(mat), jx in 1:ncols(mat)
    mataug[ix,jx] = mat[ix,jx]
  end
  for ix in 1:nrows(mat)
    mataug[ix,ncols(mat)+ix] = one(ZZ)
  end

  r,matrref,den = rref(mataug)
  @assert r == nrows(mataug)

  pivots = Int[]
  pivotrels = zero_matrix(ZZ,nrows(mat),ncols(mat)-nrows(mat))
  let px = 1
    for ix in 1:nrows(mat)
      while iszero(matrref[ix,px])
        for ixx in 1:ix-1
          pivotrels[ixx,px-(ix-1)] = matrref[ixx,px]
        end
        px += 1
      end
      push!(pivots,px)
      px += 1
    end

    while px <= ncols(mat)
      for ixx in 1:nrows(mat)
        pivotrels[ixx,px-nrows(mat)] = matrref[ixx,px]
      end
      px += 1
    end
  end

  matinv = sub(matrref, 1:nrows(mat), ncols(mat)+1:ncols(matrref))

  f.field_retractions[m,n] = (pivots,matinv,den,pivotrels)
  return f.field_retractions[m,n]
end

################################################################################
# extensions in tower
################################################################################

function in(::FlintRationalField, f::QQMuInfTower)
  return true
end

function in(::QQabField, f::QQMuInfTower)
  return true
end

function in(fQQ::AnticNumberField, f::QQMuInfTower)
  (flag,m) = iscyclotomic_type(fQQ)
  flag && QQabSubfield(m) == fQQ
end

function has_muinf_order(f::QQMuInfTower, fQQ::FlintRationalField, n::Int)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  return n == 1 || n == 2
end

function has_muinf_order(f::QQMuInfTower, fQQ::AnticNumberField, n::Int)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)
  return mod(m,n) == 0
end

function has_muinf_order(f::QQMuInfTower, fQQ::QQabField, n::Int)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  return true
end

has_muinf_ring(f::QQMuInfTower, n::Int) = n > 0

function muinf_ring(f::QQMuInfTower, n::Int)
  if isodd(n)
    n = 2*n
  end
  muinf_embedding!(f, n)
  return QQabSubfield(n)
end

function muinf_ring(
    f::QQMuInfTower,
    fQQs::FlintRationalField...
   )
  return QQ
end

function muinf_ring(
    f::QQMuInfTower,
    fQQs::Union{FlintRationalField,AnticNumberField}...
   )
  n = 1
  for fQQ in fQQs
    fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
    if typeof(fQQ) == AnticNumberField
      n = lcm(n,iscyclotomic_type(fQQ)[2])
    end
  end
  return muinf_ring(f, n)
end

function muinf_extension(f::QQMuInfTower, fQQ::AnticNumberField, n::Int)
  n > 0 || throw(DomainError("negative order"))
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)

  if isodd(n) 
    n = 2*n
  end
  n = lcm(m,n)
  if n == m
    # Nothing to convert to we return nothing instead of (fQQ,identity)
    return nothing
  end

  fQQn = muinf_ring(f,n)
  conv = _map_from_cyclotomic_tower(f, m, n)
  return (fQQn, x::nf_elem -> conv(x))
end

function muinf_extension(f::QQMuInfTower, fQQ::FlintRationalField, n::Int)
  n > 0 || throw(DomainError("negative order"))
  if isodd(n) 
    n = 2*n
  end
  fQQn = muinf_ring(f,n)
  return (fQQn, x::fmpq -> fQQn(x))
end

@doc Markdown.doc"""
> The maximal order of roots of unity contained in this field. Zero if
> infinitely many roots of unity are contained.
"""
function muinf_order(f::QQMuInfTower, fQQ::AnticNumberField)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)
  @assert iseven(m)
  return m
end

################################################################################
# conversion
################################################################################

function embedding(
    f     :: QQMuInfTower,
    m     :: Int,
    n     :: Int;
    check :: Bool = true
   )
  mat = embedding_matrix(f,m,n)
  if check
    parent_check = muinf_ring(f,m)
    return (a::nf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      av = zero_matrix(ZZ, 1, degree(muinf_ring(f, m)))
      den = ZZ()
      Nemo.elem_to_mat_row!(av, 1, den, a)
      return Nemo.elem_from_mat_row(muinf_ring(f, n), av*mat, 1, den)
     end
     )
  else
    return (a::nf_elem -> begin
      av = zero_matrix(ZZ, 1, degree(muinf_ring(f, m)))
      den = ZZ()
      Nemo.elem_to_mat_row!(av, 1, den, a)
      return Nemo.elem_from_mat_row(muinf_ring(f, n), av*mat, 1, den)
     end
     )
  end
end

function retraction(
    f     :: QQMuInfTower,
    m     :: Int,
    n     :: Int;
    check :: Bool = false
   )
  (pivots, projmat, tden, _) = retraction_matrix(f,m,n)
  if check
    parent_check = muinf_ring(f,m)
    return (a::nf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of retraction"))
      end
      av = zero_matrix(ZZ, 1, degree(muinf_ring(f, m)))
      den = ZZ()
      Nemo.elem_to_mat_row!(av, 1, den, a)
      avp = zero_matrix(ZZ, 1, length(pivots))
      for (ix,px) in enumerate(pivots)
        avp[1,ix] = av[1,px]
      end
      return Nemo.elem_from_mat_row(muinf_ring(f, n), avp*projmat, 1, den*tden)
     end
     )
  else
    return (a::nf_elem -> begin
      av = zero_matrix(ZZ, 1, degree(muinf_ring(f, m)))
      den = ZZ()
      Nemo.elem_to_mat_row!(av, 1, den, a)
      avp = zero_matrix(ZZ, 1, length(pivots))
      for (ix,px) in enumerate(pivots)
        avp[1,ix] = av[1,px]
      end
      return Nemo.elem_from_mat_row(muinf_ring(f, n), avp*projmat, 1, den*tden)
     end
     )
  end
end

function (f :: QQMuInfTower)(
    a     :: fmpq;
    check :: Bool = false
   )
  # We can ignore the check flag, since there is a unique QQ field.
  return muinf_ring(f,1)(a)
end

function (f :: QQMuInfTower)(
    a     :: cf_elem;
    check :: Bool = false
   )
  # We can ignore the check flag, since there is a unique QQab field.
  return nf_elem(a)
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
          :: FlintRationalField,
    fQQ   :: AnticNumberField;
    check :: Bool = false
    )
  if !(fQQ in f)
    throw(DomainError(fQQ, "field does not lie in tower"))
  end
  if check
    parent_check = QQ
    return (a::fmpq -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fQQ(a)
     end
     )
  else
    return (a::fmpq -> fQQ(a))
  end
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
          :: FlintRationalField,
    fQQab :: QQabField;
    check :: Bool = false
    )
  if check
    parent_check = QQ
    return (a::fmpq -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fQQab(a)
     end
     )
  else
    return (a::fmpq -> fQQab(a))
  end
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    fQQ   :: AnticNumberField,
    fQQab :: QQabField;
    check :: Bool = false
   )
  if !(fQQ in f)
    throw(DomainError(fQQ, "field does not lie in tower"))
  end
  if check
    parent_check = fQQ
    return (a::nf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fQQab(a)
     end
     )
  else
    return (a::nf_elem -> fQQab(a))
  end
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    fQQ1  :: AnticNumberField,
    fQQ2  :: AnticNumberField;
    check :: Bool = false
    )
  if !(fQQ1 in f)
    throw(DomainError(fQQ1, "field does not lie in tower"))
  end
  if !(fQQ2 in f)
    throw(DomainError(fQQ2, "field does not lie in tower"))
  end
  (_,m) = iscyclotomic_type(fQQ1)
  (_,n) = iscyclotomic_type(fQQ2)
  return _map_from_cyclotomic_tower(f,m,n; check)
end

function _map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    m     :: Int,
    n     :: Int;
    check :: Bool = false
   )
  if divisible(m,n)
    return retraction(f,m,n)
  elseif divisible(n,m)
    return embedding(f,m,n)
  end

  mn = gcd(m,n)
  ret = retraction(f,m,mn; check)
  emb = embedding(f,mn,n; check = false)
  return a -> emb(ret(a))
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    fQQ   :: AnticNumberField,
          :: FlintRationalField;
    check :: Bool = false
    )
  if !(fQQ in f)
    throw(DomainError(fQQ1, "field does not lie in tower"))
  end
  if check
    parent_check = fQQ
    return (a::nf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return coeff(a,0)
     end
     )
  else
    return (a::nf_elem -> coeff(a,0))
  end
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    fQQab :: QQabField,
    fQQ   :: AnticNumberField;
    check :: Bool = false
    )
  if check
    parent_check = fQQab
    o = muinf_order(fQQ)
    return (a::cf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return realization(cf_elem(o,a))
     end
     )
  else
    o = muinf_order(fQQ)
    return (a::cf_elem -> realization(cf_elem(o,a)))
  end
end

function map_from_cyclotomic_tower(
    f     :: QQMuInfTower,
    fQQab :: QQabField,
          :: FlintRationalField;
    check :: Bool = false
    )
  if check
    parent_check = fQQab
    return (a::cf_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return coeff(realization(a),0)
     end
     )
  else
    return (a::cf_elem -> coeff(realization(a),0))
  end
end


function in(a::nf_elem, f::QQMuInfTower, fQQ::AnticNumberField)
  parent(a) in f || throw(DomainError(a, "element does not lie in tower"))
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(parent(a))
  (_,n) = iscyclotomic_type(fQQ)

  if divisible(n,m)
    return true
  elseif !divisible(m,n)
    n = gcd(m,n)
    fQQ = muinf_ring(f, n)
  end

  (pivots, _, tden, rels) = retraction_matrix(f,m,n)

  deg = degree(parent(a))
  den = denominator(a)
  acoeffs = zero_matrix(ZZ, 1, length(pivots))
  rcoeffs = zero_matrix(ZZ, 1, ncols(rels))
  let
    ax = 1; rx = 1
    for d in 0:deg-1
      if ax <= length(pivots) && pivots[ax] == d+1
        acoeffs[1,ax] = numerator(den*coeff(a,d))
        ax += 1
      else
        rcoeffs[1,rx] = numerator(den*coeff(a,d))
        rx += 1
      end
    end
  end

  return acoeffs * rels == tden*rcoeffs
end

function in(a::nf_elem, f::QQMuInfTower, fQQ::FlintRationalField)
  parent(a) in f || throw(DomainError(a, "element does not lie in tower"))
  deg = degree(parent(a))
  return all(iszero(coeff(a,d)) for d in 1:deg-1)
end

function in(a::nf_elem, f::QQMuInfTower, fQQ::QQabField)
  parent(a) in f || throw(DomainError(a, "element does not lie in tower"))
  return true
end

################################################################################
# roots of unity
################################################################################

function root_of_unity(f::QQMuInfTower, fQQ::AnticNumberField, n::Int)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)
  return f.muinf_embeddings[m][MuInf(1//n)]
end

function (f::QQMuInfTower)(fQQ::AnticNumberField, a::RootOfUnity)
  fQQ in f || throw(DomainError(fQQ, "field does not lie in tower"))
  (_,m) = iscyclotomic_type(fQQ)
  return f.muinf_embeddings[m][a]
end

function (f::QQMuInfTower)(::FlintRationalField, a::RootOfUnity)
  if isone(a)
    return one(QQ)
  elseif order(a) == 2
    return QQ(-1)
  else
    throw(DomainError(a, "root of unity not in field"))
  end
end

function (f::QQMuInfTower)(::QQabField, a::RootOfUnity)
  QQab(a)
end

function act(a::RootOfUnity, b::fmpq, f::QQMuInfTower)
  parent(b) in f || throw(DomainError(b, "element does not lie in tower"))
  if order(a) == 1
    return b
  elseif order(a) == 2
    return -b
  else
    throw(DomainError(a, "root of unity not contained in field"))
  end
end

function act(a::RootOfUnity, b::nf_elem, f::QQMuInfTower)
  parent(b) in f || throw(DomainError(b, "element does not lie in tower"))
  if !divisible(muinf_order(f,parent(b)),order(a))
    throw(DomainError(a, "root of unity not contained in field"))
  end
  return f(parent(b),a) * b
end

function act(a::RootOfUnity, b::cf_elem, f::QQMuInfTower)
  return act(a, b)
end

################################################################################
################################################################################
# default instances
################################################################################
################################################################################

QQabTower = QQMuInfTower()
