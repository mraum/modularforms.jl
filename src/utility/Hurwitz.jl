@doc Markdown.doc"""
    hurwitz_numbers(prc::Int, ring::NmodRing)
> Return the generating series of $12*H(n) \in \mathrm{ring}$ up to precision
> $\mathrm{prc}$, where $H(n)$ is the $n$-th Hurwitz number.
"""
function hurwitz_numbers(prc::Int, ring::NmodRing)
  R,hw = PowerSeriesRing(ring,prc,"x")
  fit!(hw, prc)
  hw.prec = prc
  hw.val = 0
  hw.length = prc
  ccall((:hurwitz_numbers, libmodularforms_jll), Nothing,
        (Ptr{UInt}, Int, UInt),
        hw.coeffs, prc, modulus(ring))
  return hw
end


@doc Markdown.doc"""
    hurwitz_numbers(prc::Int, ring::R = ZZ) where {R <: Ring}
> Return the generating series of $12*H(n) \in \mathbb{Z}$ up to precision
> $\mathrm{prc}$, where $H(n)$ is the $n$-th Hurwitz number.
"""
function hurwitz_numbers(prc::Int, ring::A = ZZ) where {A <: Ring}
  R,x = PowerSeriesRing(ring, prc, "x")
  prc <= 0 && return O(x^prc)

  # use Eichler recursions to compute Hurwitz class numbers

  # sigma(n) = sum_{d \isdiv n} d
  # lambda(n) = sum_{d \isdiv n} \min{d,n/d}
  lambda = [zero(ring) for n in 1:prc+3]
  sigma = [zero(ring) for n in 1:prc+3]

  nd = one(ring)
  for d in 1:prc+3
    one!(nd)
    for n in d:d:min(d^2,prc+3)
      addeq!(lambda[n], ZZ(nd))
      addeq!(nd,1)
      addeq!(sigma[n], d)
    end
    for n in d*(d+1):d:prc+3
      addeq!(lambda[n], d)
      addeq!(sigma[n], d)
    end
  end

  hw = [zero(ring) for n in 1:prc]

  # The following code is equivalent to the for-loop body
  # hw[nn+1] =   24*sigma[div(nn,4)]
  #            - 12*lambda[div(nn,4)]
  #            - 2*sum(hw[nn-s^2+1] for s in 1:isqrt(nn))
  # hw[nn+4] =   4*sigma[nn+3]
  #            - 6*lambda[nn+3]
  #            - 2*sum(hw[nn+3-s^2+1] for s in 1:isqrt(nn+3))
  if prc >= 1
    hw[1] = -1
    h = -2*hw[1]
    for s in 2:2:isqrt(prc-1)
      addeq!(hw[1+s^2], h)
    end
  end

  if prc >= 4
    hw[4] = 4
    h = -2*hw[4]
    for s in 1:isqrt(prc-4)
      addeq!(hw[4+s^2], h)
    end

    for nn in 4:4:prc-4
      addeq!(hw[nn+1], 24*sigma[div(nn,4)] - 12*lambda[div(nn,4)])
      h = -2*hw[nn+1]
      if prc > nn
        for s in 2:2:isqrt(prc-nn-1)
          addeq!(hw[nn+1+s^2], h)
        end
      end
  
      addeq!(hw[nn+4], 4*sigma[nn+3] - 6*lambda[nn+3])
      h = -2*hw[nn+4]
      if prc > nn+3
        for s in 1:isqrt(prc-nn-4)
          addeq!(hw[nn+4+s^2], h)
        end
      end
    end

    # process the remaining cases so that nn runs through 4:4:prc
    nn = (prc-4 - mod(prc-4,4)) + 4
    if nn + 1 <= prc
      addeq!(hw[nn+1], 24*sigma[div(nn,4)] - 12*lambda[div(nn,4)])
    end
    if nn + 4 <= prc
      addeq!(hw[nn+4], 4*sigma[nn+3] - 6*lambda[nn+3])
    end
  end

  return R(hw, prc, prc, 0)
end


function hurwitz_c(prc::Int, mod::Int)
  hw = Vector{UInt}(undef, prc)
  ccall((:hurwitz_numbers, libmodularforms_jll), Nothing,
        (Ref{UInt}, Int, Int),
        hw, prc, mod)
  return hw
end
