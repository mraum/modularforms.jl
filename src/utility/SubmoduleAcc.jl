################################################################################
# creation
################################################################################

function SubmoduleAcc(coeffring::R, moduledim::Int, capacity::C = nothing) where {R <: Ring, C <: Union{Nothing,Int}}
  if isnothing(capacity)
    # TODO: this can certainly be optimized, but is probably not worth it given
    # that Submodule in Nemo will get better
    capacity = 2*dim
  end

  return SubmoduleAcc{elem_type(coeffring)}(
      zero_matrix(coeffring, capacity, moduledim),
      0, 0
     )
end

################################################################################
# conversion
################################################################################

function change_base_ring(
    f,
    sm::SubmoduleAcc{D};
    codomain::Union{Nothing,C} = nothing
   ) where {
    D <: RingElem, C <: Ring
   }
  if isnothing(codomain)
    @assert f isa Ring
    codomain = f
  end
  SubmoduleAcc{elem_type(codomain)}(
      change_base_ring(x -> f(x), sm.mat;
                       parent = MatrixSpace(codomain, nrows(sm.mat), ncols(sm.mat))),
      0, sm.rkbd+sm.rkinc)
end

################################################################################
# properties
################################################################################

function dim(m::SubmoduleAcc{A}) where {A <: RingElem}
  return ncols(m.mat)
end

function capacity(m::SubmoduleAcc{A}) where {A <: RingElem}
  return nrows(m.mat)
end

function rank_lower_bound(m::SubmoduleAcc{A}) where {A <: RingElem}
  return m.rkbd
end

function base_ring(m::SubmoduleAcc{A}) where {A <: RingElem}
  return base_ring(m.mat)
end

################################################################################
# display
################################################################################

function show(io::IO, m::SubmoduleAcc{A}) where {A}
  print(io, "Accumulating submodule of free module over $(base_ring(m.mat)) of dimension $(dim(m))")
end

################################################################################
# iteration
################################################################################

function Base.length(m::SubmoduleAcc{A}) where {A <: RingElem}
  return m.rkbd
end

# NOTE: this does not copy the elements
function Base.iterate(m::SubmoduleAcc{A}, ix::Int = 1) where {A <: RingElem}
  m.rkbd >= ix || return nothing

  return ([m.mat[ix,jx] for jx in 1:dim(m)], ix+1)
end

################################################################################
# accumulation
################################################################################

function empty!(m::SubmoduleAcc{A}) where {A <: RingElem}
  m.mat = zero(parent(m.mat))
  m.rkbd = 0
  m.rkinc = 0

  return m
end

function reduce!(m::SubmoduleAcc{A}) where {A <: RingElem}
  m.rkinc == 0 && return m

  if A == fmpq
    # Without this type assertion the compiler speed breaks down.
    m.rkbd = rref!(m.mat::fmpq_mat)
  else
    m.rkbd = rref!(m.mat)
  end
  m.rkinc = 0

  return m
end

function insert!(m::SubmoduleAcc{A}, v::Vector{A}) where {A <: RingElem}
  dim(m) == length(v) || error("incompatible vector length")

  if m.rkbd + m.rkinc == capacity(m)
    reduce!(m)
  end 
  if rank_lower_bound(m) == capacity(m)
    throw(DomainError("capacity of accumulating submodule exceeded"))
  end

  m.rkinc += 1
  for (jx,a) in enumerate(v)
    m.mat[m.rkbd+m.rkinc, jx] = deepcopy(a)
  end

  return m
end
