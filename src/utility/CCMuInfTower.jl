################################################################################
# creation
################################################################################

cyclotomic_tower(f::AcbField) = CCMuInfTower(f)

################################################################################
# conversion
################################################################################

AcbField(f::CCMuInfTower) = f.acb_field

################################################################################
# properties
################################################################################

base_ring(f::CCMuInfTower) = AcbField(f)

limit_ring(f::CCMuInfTower) = AcbField(f)

characteristic(f::CCMuInfTower) = 0

# CCMuInfTower is not an instance of AbstractAlgebra.Set
elem_type(::CCMuInfTower) = acb

function muinf_embedding(f::CCMuInfTower, fCC::AcbField, ::Int)
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  f.muinf_embedding
end

function muinf_embedding!(f::CCMuInfTower, n::Int)
  if !haskey(f.muinf_embedding, MuInf(1//n))
    for a in 0:n-1
      if !haskey(f.muinf_embedding, MuInf(a//n))
        f.muinf_embedding[MuInf(a//n)] = AcbField(f)(MuInf(a//n))
      end
    end
  end

  return f.muinf_embedding
end

################################################################################
# extensions in tower
################################################################################

function in(fCC::AcbField, f::CCMuInfTower)
  AcbField(f) == fCC
end

function muinf_ring(f::CCMuInfTower, n::Int)
  muinf_embedding!(f, n)
  return AcbField(f)
end

function muinf_ring(f::CCMuInfTower, fCCs::AcbField...)
  for fCC in fCCs
    fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  end
  return base_ring(f)
end

function has_muinf_order(f::CCMuInfTower, fCC::AcbField, n::Int)
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  return true
end

has_muinf_ring(f::CCMuInfTower, n::Int) = n > 0

function muinf_extension(f::CCMuInfTower, fCC::AcbField, n::Int)
  n > 0 || throw(DomainError(n, "negative order"))
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  muinf_embedding!(f, n)
  # Nothing to convert, so we return nothing instead of (fCC,identity)
  return nothing
end

@doc Markdown.doc"""
> The maximal order of roots of unity contained in this field. Zero if
> infinitely many roots of unity are contained.
"""
function muinf_order(f::CCMuInfTower, fCC::AcbField)
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  return 0
end

################################################################################
# conversion
################################################################################

function (f :: CCMuInfTower)(
    a     :: acb;
    check :: Bool = false
   )
  if check && !(parent(a) === base_ring(f))
    throw(DomainError(a, "element does not lie in tower"))
  end
  return a
end

function map_from_cyclotomic_tower(
    f::CCMuInfTower,
    f1CC::AcbField,
    f2CC::AcbField;
    check :: Bool = false
   )
  if !(f1CC in f)
    throw(DomainError(f1CC, "fields do not lie in tower"))
  end
  if !(f2CC in f)
    throw(DomainError(f2CC, "fields do not lie in tower"))
  end
  if check
    parent_check = f1CC
    (a::acb -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of conversion"))
      end
      return a
     end
     )
  else
    return identity
  end
end

function in(a::acb, f::CCMuInfTower, fCC::AcbField)
  return fCC in f && parent(a) == fCC
end

################################################################################
# roots of unity
################################################################################

function root_of_unity(f::CCMuInfTower, fCC::AcbField, n::Int)
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  return f.muinf_embedding[MuInf(1//n)]
end

function (f::CCMuInfTower)(fCC::AcbField, a::RootOfUnity)
  fCC in f || throw(DomainError(fCC, "field does not lie in tower"))
  return f.muinf_embedding[a]
end

function act(a::RootOfUnity, b::acb, f::CCMuInfTower)
  parent(b) in f || throw(DomainError(a, "element does not lie in tower"))
  return act(a,b)
end
