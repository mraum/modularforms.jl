################################################################################
# creation
################################################################################

function GFpMuInfTower(base_ring::Union{GaloisField,GaloisFmpzField})
  p = characteristic(base_ring)
  try p = Int(p) catch ; end
  FF,_ = FiniteField(p,1,"a")
  return GFpMuInfTower(base_ring,
             Dict(1 => FF),
             Dict(1 => 1),
             Dict(1 => 1),
             Dict(1 => Dict(one(MuInf) => one(FF)))
            )
end

function cyclotomic_tower(base_ring::Union{GaloisField,GaloisFmpzField})
  GFpMuInfTower(base_ring)
end

################################################################################
# properties
################################################################################

base_ring(f::GFpMuInfTower) = f.base_ring

function limit_ring(f::GFpMuInfTower)
  error("algebraic closure of $(base_ring(f)) not implemented")
end

characteristic(f::GFpMuInfTower) = characteristic(base_ring(f))

# GFpMuInfTower is not an instance of AbstractAlgebra.Set
function elem_type(f::GFpMuInfTower)
  if isa(f.base_ring, GaloisField)
    return Nemo.fq_nmod
  else
    return Nemo.fq
  end
end

function muinf_embedding(
    f::GFpMuInfTower,
    fFF::Union{FqNmodFiniteField,FqFiniteField},
    n::Int
   )
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  return muinf_embedding!(f,n,degree(fFF))
end

@doc Markdown.doc"""
> Compute roots of unity of the given order and ensure
> that the minimal extension of the base field with
> these roots is available.
"""
function muinf_embedding!(f::GFpMuInfTower, n::Int)
  d = muinf_degree(f,n)
  if haskey(f.muinf_orders,d)
    if divisible(f.muinf_orders[d],n)
      return f.muinf_embeddings[d]
    end

    dmax = maximum(keys(f.muinf_orders))
    nmax = f.muinf_orders[dmax]

    # If the maximal field in the tower has the corresponding roots of unity,
    # we merely need to convert them down to correct degree.
    if divisible(nmax,n)
      return muinf_embedding!(f,n,d; convert_only = true)
    end
  end


  dmax = maximum(keys(f.muinf_orders))
  nmax = f.muinf_orders[dmax]
  # The maximal field in the tower contains all roots of unity. zetamax is the
  # currently maximal root of unity, which we need to extend now.
  zetamax = f.muinf_embeddings[dmax][MuInf(1//nmax)]

  # In order to ensure that we always have compatible choices of roots
  # of unity, we keep one maximal element in tower. If the degree d associated
  # with n does not divide the maximal one in the tower, we have to create a
  # new extension.
  # fFF is the maximal field in the tower after extension.
  nnew = lcm(n,nmax)
  if divisible(dmax,d)
    dnew = dmax
    fFF = f.fields[dnew]
  else
    dnew = lcm(d,dmax)
    fFF = _add_muinf_ring_by_degree!(f,dnew)
  end
  muinf_embedding_new = f.muinf_embeddings[dnew]
  @assert dnew == muinf_degree(f,nnew)
  f.muinf_orders[dnew] = nnew

  if nmax == 1
    testexps = [divexact(n,p) for (p,_) in factor(ZZ(n))]
    fFF1exp = divexact(order(fFF)-1,n)
    while true
      zetanew = rand(fFF)^fFF1exp
      if all(!isone(zetanew^e) for e in testexps)
        break
      end
    end
  else # nmax != 1 || d != 1
    # In order to find a root of unity that is compatible with the previous one,
    # we factor the corresponding cyclotomic polynomial and the root equation for
    # the previous one.
    R,x = PolynomialRing(fFF,"x")
    RZZ,xZZ = PolynomialRing(ZZ,"x")
    if dmax == dnew
      zeta_poly = x^divexact(nnew,nmax) - zetamax
    else
      zeta_poly = x^divexact(nnew,nmax) - fFF(zetamax)
    end
    cyclo_poly = change_base_ring(fFF, cyclotomic(nnew,xZZ); parent=R)
    zetanew = -coeff(first(first(factor(gcd(zeta_poly, cyclo_poly)))),0)
  end

  # Given the new root of unity, we build up the dictionary of roots of unity
  # for fFF.
  c = one(fFF)
  for a in 0:nnew-2
    b = MuInf(a//nnew)
    if !haskey(muinf_embedding_new,b)
      muinf_embedding_new[b] = c
    end
    c *= zetanew
  end
  muinf_embedding_new[MuInf((nnew-1)//nnew)] = c

  # If the requested degree differs from the new maximal one, we need to
  # convert all required roots of unity down to that field.
  if d == dnew
    return muinf_embedding_new
  else
    return muinf_embedding!(f,n,d; convert_only = true)
  end
end

@doc Markdown.doc"""
> Compute roots of unity of the given order and ensure
> that the extension of degree `d` with these roots of unity is given.
>
> If convert_only is true, then we assume that the roots of unity have already
> be computed, and only need to be converted into the right field.
"""
function muinf_embedding!(
    f::GFpMuInfTower, n::Int, d::Int;
    convert_only::Bool = false
   )
  if !divisible(d,muinf_degree(f,n))
    throw(DomainError((n,d), "roots of unity not contained in field of requested degree"))
  end

  if !convert_only
    muinf_embedding!(f,n)
  end

  if haskey(f.muinf_orders,d)
    fFF = f.fields[d]
  else
    # If the degree d field is not in the tower, we create it.
    fFF = _add_muinf_ring_by_degree!(f,d)
  end
  muinf_embedding = f.muinf_embeddings[d]

  if divisible(f.muinf_orders[d],n)
    return muinf_embedding
  end

  dmax = maximum(keys(f.muinf_orders))
  @assert divisible(dmax,d)
  muinf_embedding_max = f.muinf_embeddings[dmax]

  nnew = lcm(f.muinf_orders[d],n)
  for a in 0:nnew-1
    b = MuInf(a//nnew)
    if !haskey(muinf_embedding,b)
      muinf_embedding[b] = fFF(muinf_embedding_max[b])
    end
  end

  return muinf_embedding
end

function _add_muinf_ring_by_degree!(f::GFpMuInfTower, d::Int)
  # This function does not guarantee that all roots of unity are defined for
  # the maximal field of an extension. This must be taken care of in
  # the function muinf_embedding!.
  p = characteristic(f)
  try p = Int(p) catch ; end
  fFF,_ = FiniteField(p,d,"a")
  f.fields[d] = fFF
  f.muinf_embeddings[d] = Dict(one(MuInf) => one(fFF))
  f.muinf_orders[d] = 1
  return fFF
end

################################################################################
# extensions in tower
################################################################################

function muinf_degree(f::GFpMuInfTower, n::Int)
  if haskey(f.muinf_degrees,n)
    return f.muinf_degrees[n]
  end

  d = 1
  p = characteristic(f)
  pd = ResidueRing(ZZ,n)(p)
  while !isone(pd)
    pd *= p
    d += 1
  end

  f.muinf_degrees[n] = d
  return d
end

function has_muinf_order(
    f::GFpMuInfTower,
    fFF::Union{GaloisField,GaloisFmpzField,
               FqNmodFiniteField,FqFiniteField},
    n::Int)
  return divisible(muinf_order(f,fFF),n)
end

function has_muinf_ring(f::GFpMuInfTower, n::Int)
  n > 0 && isone(gcd(n,characteristic(f)))
end

function in(fFF::Union{GaloisField,GaloisFmpzField}, f::GFpMuInfTower)
  fFF == base_ring(f)
end

function in(fFF::Union{FqNmodFiniteField,FqFiniteField}, f::GFpMuInfTower)
  haskey(f.fields, degree(fFF)) && f.fields[degree(fFF)] == fFF
end

function muinf_ring(f::GFpMuInfTower, n::Int)
  if divisible(fmpz(n),characteristic(f))
    throw(DomainError(n, "characteristic may not divide cyclotomic order"))
  end
  muinf_embedding!(f,n)
  return f.fields[muinf_degree(f,n)]
end

function muinf_ring(
    f::GFpMuInfTower,
    fFFs::R...
   ) where {
    R <: Union{GaloisField, GaloisFmpzField}
   }
  return muinf_ring_by_degree(f, 1)
end

function muinf_ring(
    f::GFpMuInfTower,
    fFFs::R...
   ) where {
    R <: Union{GaloisField, GaloisFmpzField, FqNmodFiniteField, FqFiniteField}
   }
  d = 1
  for fFF in fFFs
    fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
    if typeof(fFF) == FqNmodFiniteField || typeof(fFF) == FqFiniteField
      d = lcm(d, degree(fFF))
    end
  end

  return muinf_ring_by_degree(f, d)
end

function muinf_ring_by_degree(f::GFpMuInfTower, d::Int)
  if haskey(f.fields, d)
    return f.fields[d]
  end

  n = maximum(values(f.muinf_orders))
  muinf_embedding!(f,n,d)
  return f.fields[d]
end

function muinf_extension(
    f::GFpMuInfTower,
    fFF::Union{FqNmodFiniteField,FqFiniteField},
    n::Int
   )
  if !(fFF in f)
    throw(DomainError(fFF, "field does not lie in tower"))
  end

  d = muinf_degree(f,n)
  if divisible(degree(fFF),d)
    muinf_embedding!(f,n,d)
    # Nothing to convert, so we return nothing instead of (fFF, identity)
    return nothing
  end

  d = lcm(d,degree(fFF))
  muinf_embedding!(f,n,d)
  fFFn = f.fields[d]
  if isa(fFF, FqNmodFiniteField)
    return (fFFn, x::fq_nmod -> fFFn(x))
  else
    return (fFFn, x::fq -> fFFn(x))
  end
end

function muinf_extension(
    f::GFpMuInfTower,
    fFF::Union{GaloisField,GaloisFmpzField},
    n::Int
   )
  fFF == base_ring(f) || throw(DomainError(fFF, "field does not lie in tower"))

  d = muinf_degree(f,n)
  muinf_embedding!(f,n,d)
  fFFn = f.fields[d]

  conv = map_from_cyclotomic_tower(f, fFF, fFFn)
  if fFF isa GaloisField
    return (fFFn, x::Nemo.gfp_elem -> conv(x))
  else
    return (fFFn, x::Nemo.gfp_fmpz_elem -> conv(x))
  end
end

@doc Markdown.doc"""
> The maximal order of roots of unity contained in this field. Zero if
> infinitely many roots of unity are contained.
"""
function muinf_order(
    f::GFpMuInfTower,
    fFF::Union{GaloisField,GaloisFmpzField,
               FqNmodFiniteField,FqFiniteField},
   )
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  return order(fFF)-1
end

##############################################################
### conversion
##############################################################

function (f :: GFpMuInfTower)(
    a     :: Union{gfp_elem,gfp_fmpz_elem};
    check :: Bool = false
   )
  if check && !(parent(a) === base_ring(f))
    throw(DomainError(a, "element does not lie in tower"))
  end
  return muinf_ring(f,1)(a.data)
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFFp::GaloisField,
    fFF::FqNmodFiniteField;
    check :: Bool = false
   )
  if fFFp != base_ring(f)
    throw(DomainError(fFFp, "field does not lie in tower"))
  end
  if !(fFF in f)
    throw(DomainError(fFF, "field does not lie in tower"))
  end
  if check
    parent_check = fFFp
    return (a::Nemo.gfp_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fFF(a.data)
     end
     )
  else
    # In the case of GaloisField, a.data is of type UInt.
    return (a::Nemo.gfp_elem -> fFF(a.data))
  end
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFFp::GaloisFmpzField,
    fFF::FqFiniteField;
    check :: Bool = false
   )
  if fFFp != base_ring(f)
    throw(DomainError(fFFp, "field does not lie in tower"))
  end
  if !(fFF in f)
    throw(DomainError(fFF, "field does not lie in tower"))
  end
  if check
    parent_check = fFFp
    return (a::Nemo.gfp_elem -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fFF(a.data)
     end
     )
  else
    # In the case of GaloisFmpzField, a.data it is of type fmpz.
    return (a::Nemo.gfp_fmpz_elem -> fFF(a.data))
  end
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFF1::FqNmodFiniteField,
    fFF2::FqNmodFiniteField;
    check :: Bool = false
   )
  if !(fFF1 in f)
    throw(DomainError(fFF1, "field does not lie in tower"))
  end
  if !(fFF2 in f)
    throw(DomainError(fFF2, "field does not lie in tower"))
  end
  if check
    parent_check = fFF1
    return (a::Nemo.fq_nmod -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of conversion"))
      end
      return fFF2(a)
     end
     )
  else
    return (a::Nemo.fq_nmod -> fFF2(a))
  end
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFF1::FqFiniteField,
    fFF2::FqFiniteField;
    check :: Bool = false
   )
  if !(fFF1 in f)
    throw(DomainError(fFF1, "field does not lie in tower"))
  end
  if !(fFF2 in f)
    throw(DomainError(fFF2, "field does not lie in tower"))
  end
  if check
    parent_check = fFF1
    return (a::Nemo.fq -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of conversion"))
      end
      return fFF2(a)
     end
     )
  else
    return (a::Nemo.fq -> fFF2(a))
  end
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFF::FqNmodFiniteField,
    fFFp::GaloisField;
    check :: Bool = false
   )
  if !(fFF in f)
    throw(DomainError(fFF, "field does not lie in tower"))
  end
  if fFFp != base_ring(f)
    throw(DomainError(fFFp, "field does not lie in tower"))
  end
  if check
    parent_check = fFF
    return (a::Nemo.fq_nmod -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of conversion"))
      end
      return fFFp(coeff(a,0))
     end
     )
  else
    return (a::Nemo.fq_nmod -> fFFp(coeff(a,0)))
  end
end

function map_from_cyclotomic_tower(
    f::GFpMuInfTower,
    fFF::FqFiniteField,
    fFFp::GaloisFmpzField;
    check :: Bool = false
   )
  if !(fFF in f)
    throw(DomainError(fFF, "field does not lie in tower"))
  end
  if fFFp != base_ring(f)
    throw(DomainError(fFFp, "field does not lie in tower"))
  end
  if check
    parent_check = fFF
    return (a::Nemo.fq -> begin
      if !(parent(a) === parent_check)
        throw(DomainError(a, "incorrect parent of preimage of embedding"))
      end
      return fFFp(coeff(a,0))
     end
     )
  else
    return (a::Nemo.fq -> fFFp(coeff(a,0)))
  end
end

function in(
    a::Nemo.gfp_elem,
    f::GFpMuInfTower,
    fFF::Union{FqNmodFiniteField,FqFiniteField}
   )
  if parent(a) != base_ring(f)
    throw(DomainError(parent(a), "element does not lie in tower"))
  end
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  return true
end

function in(
    a::Union{Nemo.fq_nmod,Nemo.fq},
    f::GFpMuInfTower,
    fFF::Union{GaloisField,GaloisFmpzField}
   )
  fFF == base_ring(f) || throw(DomainError(fFF, "field does not lie in tower"))
  parent(a) in f || throw(DomainError(a, "element does not lie in tower"))
  return all(iszero(coeff(a,n)) for n in 1:degree(parent(a))-1)
end

function in(
    a::Union{Nemo.fq_nmod,Nemo.fq},
    f::GFpMuInfTower,
    fFF::Union{FqNmodFiniteField,FqFiniteField}
   )
  parent(a) in f || throw(DomainError(a, "element does not lie in tower"))
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))

  if divisible(degree(fFF),degree(parent(a)))
    return true
  end

  if degree(parent(a)) == degree(fFF)
    try
      fFF(a)
    catch
      return false
    end
    return true
  else
    d = gcd(degree(parent(a)), degree(fFF))
    muinf_embedding!(f,1,d)
    try
      f.fields[d](a)
    catch
      return false
    end
    return true
  end
end

# conversion from QQMuInfTower

function (f :: GFpMuInfTower)(
    g     :: QQMuInfTower,
    a     :: fmpq,
    check :: Bool = false
   )
  base_ring(f)(a)
end

function (f :: GFpMuInfTower)(
    g     :: QQMuInfTower,
    a     :: nf_elem,
    check :: Bool = false
   )
  parent(a) in g || throw(DomainError(parent(a), "field does not lie in tower"))
  (_,m) = iscyclotomic_type(parent(a))
  zeta = root_of_unity(f,m)
  fFF = parent(zeta)
  return sum(zeta^ix * fFF(coeff(a,ix)) for ix in 0:degree(parent(a))-1)
end

function (f :: GFpMuInfTower)(
    fFF   :: Union{FqNmodFiniteField,FqFiniteField},
    g     :: QQMuInfTower,
    a     :: nf_elem,
    check :: Bool = false
   )
  parent(a) in g || throw(DomainError(parent(a), "field does not lie in tower"))
  (_,m) = iscyclotomic_type(parent(a))
  zeta = root_of_unity(f,fFF,m)
  return sum(zeta^ix * fFF(coeff(a,ix)) for ix in 0:degree(parent(a))-1)
end

function (f :: GFpMuInfTower)(
    g     :: QQMuInfTower,
    a     :: cf_elem,
    check :: Bool = false
   )
  f(g(a); check)
end

function lift(
    f :: GFpMuInfTower,
    a :: Union{Nemo.gfp_elem,Nemo.gfp_fmpz_elem},
      :: QQMuInfTower,
      :: FlintRationalField
   )
  QQ(lift(a))
end

function lift(
    f   :: GFpMuInfTower,
    a   :: Union{Nemo.fq_nmod,Nemo.fq},
    g   :: QQMuInfTower,
    gQQ :: AnticNumberField
   )
  (_,m) = iscyclotomic_type(gQQ)
  zetaQQ = gen(gQQ)
  zetaFF = root_of_unity(f, parent(a), m)

  GF = base_ring(f)
  vec = zero_matrix(GF, degree(parent(a)), 1)
  for ix in 1:degree(parent(a))
    vec[ix,1] = GF(coeff(a,ix-1))
  end
  mat = zero_matrix(GF, degree(parent(a)), degree(parent(a)))
  for jx in 1:degree(parent(a))
    b = zetaFF^(jx-1)
    for ix in 1:degree(parent(a))
      mat[ix,jx] = GF(coeff(b,ix-1))
    end
  end

  zeta_coeffs = solve(mat,vec)
  return sum(zetaQQ^(ix-1) * lift(zeta_coeffs[ix,1])
             for ix in 1:degree(parent(a)))
end

##############################################################
# roots of unity
##############################################################

function root_of_unity(
    f::GFpMuInfTower,
    n::Int
   )
  return f(muinf_ring(f,n), MuInf(1//n))
end

function root_of_unity(
    f::GFpMuInfTower,
    fFF::Union{GaloisField,GaloisFmpzField,
               FqNmodFiniteField,FqFiniteField},
    n::Int
   )
  return f(fFF, MuInf(1//n))
end

function (f::GFpMuInfTower)(
    fFF::Union{GaloisField,GaloisFmpzField},
    a::RootOfUnity
   )
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  # As opposed to the towers over CC and QQ, we might need to compute roots of
  # unity.
  return fFF(coeff(muinf_embedding!(f,order(a),1)[a],0))
end

function (f::GFpMuInfTower)(
    fFF::Union{FqNmodFiniteField,FqFiniteField},
    a::RootOfUnity
   )
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  # As opposed to the towers over CC and QQ, we might need to compute roots of
  # unity.
  return muinf_embedding!(f,order(a),degree(fFF))[a]
end

function act(
    a::RootOfUnity,
    b::Union{Nemo.gfp_elem,Nemo.gfp_fmpz_elem, Nemo.fq_nmod,Nemo.fq},
    f::GFpMuInfTower
   )
  parent(b) in f || throw(DomainError(b, "element does not lie in tower"))
  if !divisible(muinf_order(f,parent(b)),order(a))
    throw(DomainError(a, "root of unity not contained in field"))
  end
  return f(parent(b),a)*b
end

##############################################################
# reduction
##############################################################

function (f::GFpMuInfTower)(
    fFF::Union{FqNmodFiniteField,FqFiniteField},
    elt::nf_elem)
  fFF in f || throw(DomainError(fFF, "field does not lie in tower"))
  K = parent(elt)
  ord = get_attribute(K, :cyclo)
  ord != nothing || throw(DomainError(elt, "element not in a cyclotomic field"))
  zeta_FF = root_of_unity(f, fFF, ord)

  num = zero(fFF)
  zeta_pow = one(fFF)
  coeff = fmpz()
  for i in 0:degree(K)-1
    Nemo.num_coeff!(coeff, elt, i)
    num = addeq!(num, fFF(coeff)*zeta_pow)
    zeta_pow *= zeta_FF
  end
  return inv(fFF(denominator(elt))) * num
end

function (f::GFpMuInfTower)(
    fFF::Union{FqNmodFiniteField,FqFiniteField},
    elt::cf_elem)
  return f(fFF, nf_elem(elt))
end
