################################################################################
# display
################################################################################

function show(io::IO, f::MuInfTower)
  print(io,"Cyclotomic tower over $(base_ring(f))")
end

################################################################################
# conversion
################################################################################

# NOTE: RB and R stand for the types of the base field and the fields in the
# tower.
function muinf_ring_with_maps(
    f::MuInfTower,
    ffs::Union{RB,R}...
   ) where {RB <: Field, R <: Field}
  for ff in ffs
    ff in f || throw(DomainError(ff, "field does not lie in tower"))
  end

  if isempty(ffs)
    return nothing
  end

  ff1 = ffs[1]
  if all(ff == ff1 for ff in ffs[2:end])
    return nothing
  end

  ffall = muinf_ring(f, ffs...)
  return (ffall,
          tuple([ff == ffall ? identity : map_from_cyclotomic_tower(f, ff, ffall)
                 for ff in ffs]...))
end
