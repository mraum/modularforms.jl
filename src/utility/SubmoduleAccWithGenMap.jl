################################################################################
# creation
################################################################################

function SubmoduleAccWithGenMap(
    coeffring    :: R,
    genparent    :: G,
    moduledim    :: Int,
    capacity     :: Int,
    gencoefflift :: Union{Nothing,Function} = nothing
   ) where {
    R <: Ring, A, G <: Union{Ring,Module{A}}
   }
  SubmoduleAccWithGenMap(coeffring, elem_type(genparent),
      moduledim, capacity, gencoefflift)
end

function SubmoduleAccWithGenMap(
    coeffring :: R,
    gentype   :: Type,
    moduledim :: Int,
    capacity  :: Int,
    gencoefflift :: Union{Nothing,Function} = nothing
   ) where {R <: Ring}
  return SubmoduleAccWithGenMap{elem_type(coeffring), gentype}(
      zero_matrix(coeffring, capacity, moduledim+capacity),
      [], [], gencoefflift)
end

################################################################################
# conversion
################################################################################

function change_base_ring(
    f        :: Any,
    sm       :: SubmoduleAccWithGenMap{D,G},
    codomain :: Union{Nothing,C} = nothing
   ) where {
    D <: RingElem, G, C <: Ring
   }
  if isnothing(codomain)
    @assert f isa Ring
    codomain = f
  end
  mat = change_base_ring(x -> f(x), sm.mat;
                         parent = MatrixSpace(codomain, nrows(sm.mat), ncols(sm.mat)))
  return SubmoduleAccWithGenMap{elem_type(codomain),G}(
             mat, G[], vcat(sm.gensacc, sm.gens), sm.gencoefflift)
end

function map_generators(
    f  :: Any,
    GC :: Type,
    sm :: SubmoduleAccWithGenMap{A,GD}
   ) where {A, GD}
  SubmoduleAccWithGenMap{A,GC}(sm.mat,
      map(f,sm.gens), map(f,sm.gensacc), sm.gencoefflift)
end

function map_generators(
    f  :: Map{GDP,GCP},
    sm :: SubmoduleAccWithGenMap{A,GD}
   ) where {
    A,
    GD <: SetElem,
    GDP <: Nemo.Set,
    GCP <: Nemo.Set
   }
  SubmoduleAccWithGenMap{A,elem_type(codomain(f))}(sm.mat,
      map(f,sm.gens), map(f,sm.gensacc), sm.gencoefflift)
end

################################################################################
# properties
################################################################################

function dim(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  return ncols(m.mat) - nrows(m.mat)
end

function capacity(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  return nrows(m.mat)
end

function rank_lower_bound(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  return length(m.gens)
end

function base_ring(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  return base_ring(m.mat)
end

################################################################################
# display
################################################################################

function show(io::IO, m::SubmoduleAccWithGenMap{A,G}) where {A,G}
  print(io, "Accumulating submodule with generator map of free module over $(base_ring(m.mat)) of dimension $(dim(m))")
end
################################################################################
# iteration
################################################################################

function Base.length(
    m::SubmoduleAccWithGenMap{A,G}
   ) where {A <: FieldElem, G}
  return length(m.gens)
end

function Base.eltype(
     ::Type{SubmoduleAccWithGenMap{A,G}}
   ) where {A <: FieldElem, G}
  Tuple{Vector{A},G}
end

# NOTE: this does not copy the elements
function Base.iterate(
    m::SubmoduleAccWithGenMap{A,G},
    ix::Int = 1
   ) where {A <: FieldElem, G <: Union{RingElem,ModuleElem}}
  length(m.gens) >= ix || return nothing

  if isnothing(m.gencoefflift)
    return (([m.mat[ix,jx] for jx in 1:dim(m)],
            sum(m.mat[ix,dim(m)+capacity(m)-(jx-1)] * m.gens[jx]::G
                for jx in 1:length(m.gens))),
           ix+1)
  else
    return (([m.mat[ix,jx] for jx in 1:dim(m)],
            sum(m.gencoefflift(m.mat[ix,dim(m)+capacity(m)-(jx-1)]) * m.gens[jx]::G
                for jx in 1:length(m.gens))),
           ix+1)
  end
end

function Base.iterate(
    m::SubmoduleAccWithGenMap{A,Vector{G}},
    ix::Int = 1
   ) where {A <: FieldElem, G <: Union{RingElem,ModuleElem}}
  length(m.gens) >= ix || return nothing

  if isnothing(m.gencoefflift)
    return (([m.mat[ix,jx] for jx in 1:dim(m)],
            sum([m.mat[ix,dim(m)+capacity(m)-(jx-1)] * g for g in m.gens[jx]]
                for jx in 1:length(m.gens))),
           ix+1)
  else
    return (([m.mat[ix,jx] for jx in 1:dim(m)],
            sum([m.gencoefflift(m.mat[ix,dim(m)+capacity(m)-(jx-1)]) * g
                 for g in m.gens[jx]]
                for jx in 1:length(m.gens))),
           ix+1)
  end
end

################################################################################
# accumulation
################################################################################

function empty!(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  m.mat = zero(parent(m.mat))
  empty!(m.gens)
  empty!(m.gensacc)

  return m
end

function reduce!(m::SubmoduleAccWithGenMap{A,G}) where {A <: FieldElem, G}
  length(m.gensacc) == 0 && return m

  if A == fmpq
    # Without this type assertion the compiler speed breaks down.
    rref!(m.mat::fmpq_mat)
  else
    rref!(m.mat)
  end

  ring = base_ring(m.mat)
  pivotcols = Vector{Vector{elem_type(ring)}}()

  rkprev = length(m.gens)
  zeroix = findfirst(ix -> all(iszero(m.mat[ix,jx]) for jx in 1:dim(m)),
                     length(m.gens)+1:length(m.gens)+length(m.gensacc))
  rkinc = isnothing(zeroix) ? length(m.gensacc) : zeroix - 1

  # This is the first row past the ones that contribute to the span. All other
  # rows have zero entries with index between 1 and dim(m).
  ix = rkprev + rkinc + 1
  # The column index minus one corresopnding to the first element in m.gensacc.
  jx = dim(m) + capacity(m) - length(m.gens) - length(m.gensacc)
  for g in reverse!(m.gensacc)
    jx += 1

    # A nonzero entry indicates a generator that does not contribute to the
    # reduced basis, since all entries to the left of [ix,dim(m)+jx] are zero.
    # Vice verse we can check for zeros to detect generators from genacc that
    # contribute.
    if ix > capacity(m) || iszero(m.mat[ix,jx])
      insert!(m.gens, rkprev+1, g)
      insert!(pivotcols, 1, [m.mat[ixx,jx] for ixx in 1:rkprev+rkinc])

      # set columns zero that will not be overwritten later
      if jx <= dim(m) + capacity(m) - rkprev - rkinc
        for ix in 1:rkprev+rkinc
          m.mat[ix,jx] = zero(ring)
        end
      end
    else
      # set row zero; column is already zero since matrix is in rref
      for jxx in jx:dim(m)+capacity(m)
        m.mat[ix,jxx] = zero(ring)
      end

      ix += 1
    end
  end
  empty!(m.gensacc)

  # write new columns in reverse order
  for (cx,c) in enumerate(pivotcols)
    for ix in 1:rkprev+rkinc
      m.mat[ix,dim(m)+capacity(m)-rkprev-(cx-1)] = c[ix]
    end
  end

  return m
end

function insert!(m::SubmoduleAccWithGenMap{A,G}, v::Vector{A}, g::G) where {A <: FieldElem, G}
  dim(m) == length(v) || throw(DomainError(v, "incompatible vector length"))

  if length(m.gens) + length(m.gensacc) == capacity(m)
    reduce!(m)
  end 
  if rank_lower_bound(m) == capacity(m)
    throw(DomainError(m, "capacity of accumulating submodule exceeded"))
  end

  push!(m.gensacc, g)
  m.mat[length(m.gens)+length(m.gensacc),
        ncols(m.mat)+1-length(m.gensacc)-length(m.gens)] = one(base_ring(m.mat))
  for (jx,a) in enumerate(v)
    m.mat[length(m.gens)+length(m.gensacc), jx] = deepcopy(a)
  end

  return m
end
