# Additions to Hecke, which should be upstreamed

Base.eltype(::Hecke.TorQuadMod) = Hecke.TorQuadModElem

Hecke.primary_part(T::Hecke.TorQuadMod, p::I) where {I<:Integer} = Hecke.primary_part(T, fmpz(p))

zero(T::Hecke.TorQuadMod) = T(zero(abelian_group(T)))
