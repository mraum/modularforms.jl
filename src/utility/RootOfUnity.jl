################################################################################
# default instances
################################################################################

MuInf = RootOfUnityGroup()

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

dim(::RootOfUnityGroup) = 1

################################################################################
# comparison
################################################################################

==(::RootOfUnityGroup, ::RootOfUnityGroup) = true

################################################################################
# display
################################################################################

function show(io::IO, ::RootOfUnityGroup)
  print(io, "μ_∞")
end

################################################################################
# arithmetic
################################################################################

*(a::RootOfUnityGroup, ::RootOfUnityGroup) = a

################################################################################
# tensor products
################################################################################

dual(a::RootOfUnityGroup) = a

tensor_product(a::RootOfUnityGroup, ::RootOfUnityGroup) = a

tensor_product(a::RootOfUnityGroup, ::TrivialGroup) = a

tensor_product(::TrivialGroup, a::RootOfUnityGroup) = a

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

@doc Markdown.doc"""
    exponent(a::RootOfUnity)

Return exponent of root of unity.
"""
exponent(a::RootOfUnity) = a.exponent

@doc Markdown.doc"""
    order(a::RootOfUnity)

Return order of root of unity.
"""
order(a::RootOfUnity) = Int(denominator(a.exponent))

tr(a::RootOfUnity) = QQab(a)

################################################################################
# display
################################################################################

function show(io::IO, a::RootOfUnity)
  print(io, "e($(exponent(a)))")
end

################################################################################
# comparison
################################################################################

hash(a::RootOfUnity) = hash(a.exponent)

function ==(a::RootOfUnity,b::RootOfUnity)
  return a.exponent == b.exponent
end

isone(a::RootOfUnity) = iszero(a.exponent)

function isless(a::RootOfUnity, b::RootOfUnity)
  isless(exponent(a)*order(b), exponent(b)*order(a))
end

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    one(::RootOfUnityGroup)

Return $\zeta_N^0$.
"""
one(::RootOfUnityGroup) = RootOfUnity(zero(QQ))

copy(a::RootOfUnity) = RootOfUnity(a.exponent)

function deepcopy_internal(a::RootOfUnity, stackdict::IdDict)
  return RootOfUnity(deepcopy_internal(exponent(a), stackdict))
end

@doc Markdown.doc"""
    root_of_unity(n::Union{Int, UInt, fmpz})

Return $\zeta_n$.
"""
root_of_unity(n::Union{Int, UInt, fmpz}) = root_of_unity(MuInf, n)

@doc Markdown.doc"""
    root_of_unity(::RootOfUnityGroup, n::fmpz)

Return $\zeta_n$.
"""
function root_of_unity(::RootOfUnityGroup, n::fmpz)
  isone(n) && return RootOfUnity(zero(QQ))
  n <= 0 && throw("non-positive modulus")
  RootOfUnity(one(ZZ)//n)
end

@doc Markdown.doc"""
    root_of_unity(::RootOfUnityGroup, n::Union{Int, UInt})

Return $\zeta_n$.
"""
root_of_unity(::RootOfUnityGroup, n::Union{Int, UInt}) = root_of_unity(MuInf, ZZ(n))

################################################################################
# conversion
################################################################################

(::RootOfUnityGroup)(q::RootOfUnity) = q

function (::RootOfUnityGroup)(q::Rational{Int})
  0 <= q < 1 && return RootOfUnity(QQ(q))
  return RootOfUnity( QQ(mod(numerator(q),denominator(q))//denominator(q)) )
end

function (::RootOfUnityGroup)(q::fmpq)
  0 <= q < 1 && return RootOfUnity(q)
  return RootOfUnity(mod(numerator(q),denominator(q))//denominator(q))
end

(::RootOfUnityGroup)(::TrivialGroupElem) = one(MuInf)

@doc Markdown.doc"""
    (CC::Nemo.AcbField)(a::RootOfUnity)

Convert root of unity to a complex number via $\zeta_N^k = e^{i 2 \pi k / N}$.
"""
(CC::AcbField)(a::RootOfUnity) = cispi(2*CC(exponent(a)))

matrix(::QQabField, a::RootOfUnity) = matrix(QQab, fill(QQab(a),1,1))

matrix(CC::AcbField, a::RootOfUnity) = matrix(CC, fill(CC(a),1,1))

function matrix(f::MuInfTower, fFF::Field, a::RootOfUnity)
  fFF in f || throw(DomainError("field not in tower"))
  matrix(fFF, fill(f(fFF,a),1,1))
end

################################################################################
# arithmetic
################################################################################

@doc Markdown.doc"""
    *(a::RootOfUnity, b::RootOfUnity)

Return $\zeta_M^n \zeta_N^n$.
"""
function *(a::RootOfUnity, b::RootOfUnity)
  e = a.exponent + b.exponent
  e >= 1 && (e -= 1)
  return RootOfUnity(e)
end

function mul!(c::RootOfUnity, a::RootOfUnity, b::RootOfUnity)
  c.exponent = add!(c.exponent, a.exponent, b.exponent)
  c.exponent >= 1 && (c.exponent -= 1)
  return c
end

function Base.inv(a::RootOfUnity)
  iszero(a.exponent) && return a
  return RootOfUnity(one(QQ)-a.exponent)
end

function ^(a::RootOfUnity, n::Int)
  (n == 0 || isone(a)) && return one(MuInf)

  d = denominator(a.exponent)
  e = mod(abs(n)*numerator(a.exponent),d)

  e == 0 && return one(MuInf)
  if n > 0
    return RootOfUnity(e//d)
  else
    return RootOfUnity((d-e)//d)
  end
end

function ^(a::RootOfUnity, n::Rational{Int})
  (n == 0 || isone(a)) && return one(MuInf)

  an = a.exponent*n
  den = denominator(an)
  num = mod(numerator(an), den)

  return RootOfUnity(num//den)
end

conj(a::RootOfUnity) = inv(a)

################################################################################
# actions
################################################################################

# action on itself

act(a::RootOfUnity, b::RootOfUnity) = a * b

# action on CC and QQab

function act(a::RootOfUnity, b::R) where {
    R <: Union{acb,cf_elem}
   }
 parent(b)(a)*b
end

function act(a::R, b::RootOfUnity) where {
    R <: Union{acb,cf_elem}
   }
  a*parent(a)(b)
end

function act!(c::R, a::RootOfUnity, b::R) where {
    R <: Union{acb,cf_elem}
   }
  c = mul!(c, parent(c)(a), b)
end

function act!(c::R, a::R, b::RootOfUnity) where {
    R <: Union{acb,cf_elem}
   }
  c = mul!(c, a, parent(c)()b)
end

function addacteq!(c::R, a::RootOfUnity, b::R) where {
    R <: Union{acb,cf_elem},
   }
  addeq!(c,act(a,b))
end

# action arrays

function act(a::RootOfUnity, b::AbstractArray{R}) where {R <: RingElement}
  size(b,1) == 1 || throw(DomainError(b, "incompatible array size"))
  [act(a,e) for e in b]
end

function act(
    a::RootOfUnity,
    b::AbstractArray{R},
    ix::Int
   ) where {R <: RingElement}
  size(b,ix) == 1 || throw(DomainError(b, "incompatible array size"))
  [act(a,e) for e in b]
end

# action on Laurent and Puiseux series

function act(a::RootOfUnity, b::R) where {
    R <: Union{
        AbstractAlgebra.Generic.LaurentSeriesElem,
        AbstractAlgebra.Generic.PuiseuxSeriesElem}
   }
   base_ring(parent(b))(a)*b
end

function act!(c::R, a::RootOfUnity, b::R) where {
    R <: Union{
        AbstractAlgebra.Generic.LaurentSeriesElem,
        AbstractAlgebra.Generic.PuiseuxSeriesElem}
   }
  mul!(c, base_ring(parent(b))(a), b)
end

################################################################################
# tensor products
################################################################################

dual(a::RootOfUnity) = conj(a)

tensor_product(a::RootOfUnity, b::RootOfUnity) = a*b

tensor_product(a::RootOfUnity, ::TrivialGroupElem) = a

tensor_product(::TrivialGroupElem, a::RootOfUnity) = a

################################################################################
# random
################################################################################

rand(::RootOfUnityGroup, complexity::Union{Int,UnitRange{Int}}) = rand(GLOBAL_RNG, MuInf, complexity)

rand(r::AbstractRNG, ::RootOfUnityGroup, complexity::Int) = rand(r, MuInf, 0:complexity)

function rand(r::AbstractRNG, ::RootOfUnityGroup, complexity::UnitRange{Int})
  if first(complexity) > 0
    modulus = rand(complexity)
  else
    modulus = rand(1:last(complexity))
  end
  RootOfUnity(rand(0:(modulus-1))//ZZ(modulus))
end
