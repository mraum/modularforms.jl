################################################################################
# general
################################################################################

function check_equal_parents(a::SetElem, b::SetElem)
  parent(a) == parent(b) || throw("parents must coincide")
end

function power_by_squaring(
    a::T,
    b::Int
   ) where {
    T <: Union{AbstractAlgebra.Generic.GroupElem, AbstractAlgebra.Generic.RingElem}
   }
  b < 0 && throw(DomainError())
  return power_by_squaring(a, UInt(b))
end

function power_by_squaring(
    a::T,
    b::UInt
   ) where {
    T <: Union{AbstractAlgebra.Generic.GroupElem, AbstractAlgebra.Generic.RingElem}
   }
  b == 0 && return one(parent(a))
  b == 1 && return deepcopy(a)
  b == 2 && return a*a
  b == 3 && return a*a*a

  bit = ~((~UInt(0)) >> 1)
  while (UInt(bit) & b) == 0
     bit >>= 1
  end

  #WARNING: It might be tempting to instead use inplace arithmetic here, i. e.
  #mul!, but it introduces aliasing that eventually yields the wrong results!
  z = deepcopy(a)
  bit >>= 1
  while bit != 0
     z = z*z
     if (UInt(bit) & b) != 0
        z *= a
     end
     bit >>= 1
  end

  return z
end

function multiple_by_doubling(a::T, b::Integer) where {T <: GroupElem}
  b < 0 && throw(DomainError())
  b == 0 && return zero(parent(a))

  bit = ~((~UInt(0)) >> 1)
  while (UInt(bit) & b) == 0
     bit >>= 1
  end

  z = a
  bit >>= 1
  while bit != 0
     z = z+z
     if (UInt(bit) & b) != 0
        z += a
     end
     bit >>= 1
  end

  return z
end

(::Type{Rational{Int}})(a::fmpq) = Int(numerator(a))//Int(denominator(a))

+(a::RingElement) = a

function addmuleq!(a::A, b::A, c::A) where {A <: RingElement}
  addeq!(a,b*c)
end

divisible(a::Int, b::Int) = iszero(a%b)

################################################################################
# Int UInt
################################################################################

iszero(a::Union{Int,UInt,Rational{Int},Rational{UInt}}) = a == 0

isone(a::Union{Int,UInt,Rational{Int},Rational{UInt}}) = a == 1

################################################################################
# fmpz fmpq
################################################################################

function one!(a::fmpz)
  ccall((:fmpz_one, Nemo.libflint), Nothing,
        (Ref{fmpz},), a)
  return a
end

function set!(a::fmpz, b::Int)
  ccall((:fmpz_set_si, Nemo.libflint), Nothing,
        (Ref{fmpz}, Int), a, b)
  return a
end

function set!(a::fmpz, b::fmpz)
  ccall((:fmpz_set, Nemo.libflint), Nothing,
        (Ref{fmpz}, Ref{fmpz}), a, b)
  return a
end

function neg!(x::fmpz, y::fmpz)
  ccall((:__fmpz_neg, Nemo.libflint), Nothing, (Ref{fmpz}, Ref{fmpz}), x, y)
  return x
end

function sub!(z::fmpz, x::fmpz, y::UInt)
   ccall((:fmpz_sub_ui, Nemo.libflint), Nothing,
         (Ref{fmpz}, Ref{fmpz}, Int), z, x, y)
   return z
end

function addeq!(z::fmpz, c::Int)
  if c >= 0
     ccall((:fmpz_add_ui, Nemo.libflint), Nothing,
           (Ref{fmpz}, Ref{fmpz}, Int), z, z, c)
  else
     ccall((:fmpz_sub_ui, Nemo.libflint), Nothing,
           (Ref{fmpz}, Ref{fmpz}, Int), z, z, -c)
  end
  return z
end

function divrem!(q::fmpz, r::fmpz, x::fmpz, y::fmpz)
  iszero(y) && throw(DivideError())
  ccall((:fmpz_tdiv_qr, libflint), Nothing,
        (Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}), q, r, x, y)
  return (q,r)
end

function fmma!(r::fmpz, a::fmpz, b::fmpz, c::fmpz, d::fmpz)
  ccall((:fmpz_fmma, libflint), Nothing,
        (Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}), r, a, b, c, d)
  return r
end

function fmms!(r::fmpz, a::fmpz, b::fmpz, c::fmpz, d::fmpz)
  ccall((:fmpz_fmms, libflint), Nothing,
        (Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}, Ref{fmpz}), r, a, b, c, d)
  return r
end

fld(a::fmpz, b::fmpz) = fdiv(a,b)
fldmod(a::fmpz, b::fmpz) = fdivrem(a,b)

issquarefree(a::fmpz) = all([e == 1 for (_,e) in factor(a)])

rand(::Type{fmpz}) = rand(GLOBAL_RNG, fmpz)
rand(::Type{fmpz}, complexity) = rand(GLOBAL_RNG, fmpz, complexity)

function rand(r::AbstractRNG, ::Type{fmpz}, complexity::Int)
  rand(r, fmpz, -complexity:complexity)
end

rand(::FlintIntegerRing, complexity) = rand(GLOBAL_RNG, ZZ, complexity)
rand(r::AbstractRNG, ::FlintIntegerRing, complexity::Int) = rand(r, ZZ, 1:complexity)

rand(::FlintRationalField, complexity) = rand(GLOBAL_RNG, QQ, complexity)
rand(r::AbstractRNG, ::FlintRationalField, complexity::Int) = rand(r, QQ, 1:complexity)

function rand(r::AbstractRNG, ::FlintRationalField, complexity::UnitRange{Int})
  num = rand(r, complexity)
  den = rand(r, complexity)
  while iszero(den)
    den = rand(r, complexity)
  end
  return QQ(num, den) :: fmpq
end

cyclotomic_order(K::FlintRationalField) = 1
cyclotomic_order(K::AnticNumberField) = get_attribute(K, :cyclo)

################################################################################
# arb / acb
################################################################################

function upper_bound_integer(a::arb)
  b = Nemo.arf_struct(0,0,0,0)
  ccall((:arf_init, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct},),
        b)
  ccall((:arb_get_ubound_arf, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct}, Ref{arb}, Int),
        b, a, parent(a).prec)
  c = fmpz()
  ccall((:arf_get_fmpz, Nemo.Nemo.libarb),
        Int, (Ref{fmpz}, Ref{Nemo.arf_struct}, Int),
        c, b, 3) # 3 == ARF_RND_CEIL
  ccall((:arf_clear, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct},),
        b)
  return c
end

function lower_bound_integer(a::arb)
  b = Nemo.arf_struct(0,0,0,0)
  ccall((:arf_init, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct},),
        b)
  ccall((:arb_get_lbound_arf, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct}, Ref{arb}, Int),
        b, a, parent(a).prec)
  c = fmpz()
  ccall((:arf_get_fmpz, Nemo.Nemo.libarb),
        Int, (Ref{fmpz}, Ref{Nemo.arf_struct}, Int),
        c, b, 2) # 2 == ARF_RND_FLOOR
  ccall((:arf_clear, Nemo.Nemo.libarb),
        Nothing, (Ref{Nemo.arf_struct},),
        b)
  return c
end

set!(c::acb, a::acb) = Nemo._acb_set(c,a)

rand(CC::AcbField, complexity) = rand(GLOBAL_RNG, CC, complexity)

function rand(r::AbstractRNG, CC::AcbField, complexity)
  CC(rand(r,QQ,complexity))
end

################################################################################
# nf_elem
################################################################################

function set!(c::nf_elem, a::nf_elem)
   parent(a) != parent(a) && error("incompatible number fields")
   ccall((:nf_elem_set, Nemo.libantic), Nothing,
       (Ref{nf_elem}, Ref{nf_elem}, Ref{AnticNumberField}), c, a, parent(a))
end

function Polynomial(a::nf_elem)
  p = fmpq_poly()
  p.parent = parent(parent(a).pol)
  aparent = parent(a)

  ccall((:nf_elem_get_fmpq_poly, Nemo.Nemo.libflint), Nothing,
        (Ref{fmpq_poly}, Ref{nf_elem}, Ref{AnticNumberField}),
        p, a, aparent)
  return p
end

function rand(r::AbstractRNG, nf::AnticNumberField, complexity)
  poly_parent = parent(nf.pol)
  nf(poly_parent([rand(r, QQ, complexity) for dx in 1:degree(nf)]))
end

################################################################################
# fmpq_poly
################################################################################

rand(parent::P, complexity) where {T, P <: Nemo.PolyElem{T}} = rand(GLOBAL_RNG, parent, complexity)
rand(r::AbstractRNG, parent::P, complexity::Int) where {T, P <: Nemo.PolyElem{T}} = rand(GLOBAL_RNG, parent, 1:complexity)

function rand(r::AbstractRNG, parent::P, complexity::UnitRange{Int}) where {T, P <: Nemo.PolyElem{T}}
  parent([rand(base_ring(parent), complexity) for ix in 1:rand(r, complexity)])
end

################################################################################
# fmpz_rel_series
################################################################################

(par::FmpzRelSeriesRing)(a::Array{fmpz, 1}) =
  par(a, max_precision(par))

(par::FmpzRelSeriesRing)(a::Array{fmpz, 1}, prec::Int) =
  par(a, length(a), prec, 0)

function force_convert(par::FmpzRelSeriesRing, a::fmpz_rel_series)
  par == parent(a) && return a

  precision(a) - valuation(a) > max_precision(par) && error("incompatible precision")

  z = fmpz_rel_series()
  z.prec = precision(a)
  z.val = valuation(a)
  z.parent = par
  ccall((:fmpz_poly_set, Nemo.libflint), Nothing,
        (Ref{fmpz_rel_series},Ref{fmpz_rel_series}), z, a)

  return z
end

################################################################################
# fmpq_rel_series
################################################################################

(par::FmpqRelSeriesRing)(a::Union{Rational{Int},Rational{UInt}}) =
  par(fmpq(a))

(par::FmpqRelSeriesRing)(a::Array{fmpq, 1}) =
  par(a, max_precision(par))

(par::FmpqRelSeriesRing)(a::Array{fmpq, 1}, prec::Int) =
  par(a, length(a), prec, 0)

function (par::FmpqRelSeriesRing)(a::fmpq_poly)
  z = fmpq_rel_series()
  z.prec = max_precision(par)
  z.val = 0
  z.parent = par
  ccall((:fmpq_poly_set, Nemo.libflint), Nothing,
        (Ref{fmpq_rel_series},Ref{fmpq_poly}), z, a)
end

function force_convert(par::FmpqRelSeriesRing, a::fmpq_rel_series)
  par == parent(a) && return a

  precision(a) - valuation(a) > max_precision(par) && error("incompatible precision")

  z = fmpq_rel_series()
  z.prec = precision(a)
  z.val = valuation(a)
  z.parent = par
  ccall((:fmpq_poly_set, Nemo.libflint), Nothing,
        (Ref{fmpq_rel_series},Ref{fmpq_rel_series}), z, a)

  return z
end

function force_convert(par::FmpqRelSeriesRing, a::fmpz_rel_series)
  max_precision(par) >= precision(a) || error("incompatible precision")

  z = fmpq_rel_series()
  z.prec = a.prec
  z.val = a.val
  z.parent = par
  ccall((:fmpq_poly_set_fmpz_poly, Nemo.libflint), Nothing,
        (Ref{fmpq_rel_series},Ref{fmpz_rel_series}), z, a)

  return z
end

for op in [:+,:-]
  for T in [:Int,:UInt,:fmpz,
            :(Rational{Int}),:(Rational{UInt}),:fmpq,
            :fmpq_poly]
    @eval begin
      $op(a::fmpq_rel_series, b::$T) =
        $op(a,parent(a)(b))

      $op(a::$T, b::fmpq_rel_series) =
        $op(parent(b)(a), b)
    end
  end
end

*(a::fmpq_rel_series, b::Union{Rational{Int},Rational{UInt}}) = a*fmpq(b)

*(a::Union{Rational{Int},Rational{UInt}}, b::fmpq_rel_series) = fmpq(a)*b

rand(parent::R, complexity...) where {T, R <: SeriesRing{T}} = rand(GLOBAL_RNG, parent, complexity...)

function rand(r::AbstractRNG, par::R, compshift, compcoeff) where {T, R <: SeriesRing{T}}
  x = gen(par)
  coeffring = base_ring(par)
  a = sum(rand(coeffring, compshift) * x^ix
          for ix in 1:max_precision(par))
  return shift_left(a, rand(r, Int, compshift))
end

################################################################################
# RelSeries
################################################################################

+(a::AbstractAlgebra.Generic.RelSeries{T}) where T = a

################################################################################
# matrix spaces
################################################################################

nrows(ms::Nemo.Generic.MatSpace{T}) where T = ms.nrows

ncols(ms::Nemo.Generic.MatSpace{T}) where T = ms.ncols

function (ms::FmpzMatSpace)(m::MatElem{fmpq})
  nrows(ms) == nrows(parent(m)) || throw("number of rows must be equals")
  ncols(ms) == ncols(parent(m)) || throw("number of cols must be equals")

  mZZ = ms()
  for ix in 1:nrows(ms), jx in 1:ncols(ms)
    isone(denominator(m[ix,jx])) || throw("cannot convert matrix with denominator")
    mZZ[ix,jx] = numerator(m[ix,jx])
  end

  return mZZ
end

MatrixSpace(a, b::Int) = MatrixSpace(a, b, b)

function matrix(a::R, m::M) where {R <: Ring, S <: RingElement, M <: MatElem{S}}
  matrix(a, [m[ix,jx] for ix in 1:nrows(m), jx in 1:ncols(m)])
end

function Vector{T}(m::M) where {T, M <: MatElem{T}}
  ncols(m) == 1 || throw("incompatible matrix dimension")
  T[m[ix,1] for ix in 1:nrows(m)]
end

function Array{T,2}(m::M) where {T, M <: MatElem{T}}
  T[m[i,j] for i in 1:nrows(m), j in 1:ncols(m)]
end

function elementary_matrix(ring::R, r::Int, c::Int, i::Int, j::Int) where {R <: Ring}
  matrix(ring,
      [ii==i && jj==j ? one(ring) : zero(ring)
       for ii in 1:r, jj in 1:c]
       )
end

function elementary_symmetric_matrix(ring::R, r::Int, c::Int, i::Int, j::Int) where {R <: Ring}
  matrix(ring,
      [(ii==i && jj==j) || (ii==j && jj==i) ? one(ring) : zero(ring)
       for ii in 1:r, jj in 1:c]
       )
end

function elementary_symmetric_matrix(ring::R, r::Int, c::Int, i::Int) where {R <: Ring}
  matrix(ring,
      [ii==i && jj==i ? one(ring) : zero(ring)
       for ii in 1:r, jj in 1:c]
       )
end

function change_base_ring(f::Function, M::MatElem; parent::MatSpace)
  N = parent()
  @assert nrows(N) == nrows(M) && ncols(N) == ncols(M)
  for i = 1:nrows(M), j = 1:ncols(M)
     N[i,j] = f(M[i,j])
  end
  return N
end

function mul_classical(m::MatElem{T}, v::Vector{T}) where T <: RingElem
  [sum(m[ix,jx]*v[jx] for jx in 1:ncols(m)) for ix in 1:nrows(m)]
end

rand(::Type{MatSpace}, coeffset::M, complexity...) where {M <: Set} = rand(GLOBAL_RNG, MatSpace, coeffset, complexity...)
rand(r::AbstractRNG, ::Type{MatSpace}, coeffset::M, complexity::Int) where {M <: Set} = rand(r, MatSpace, coeffset, 1:complexity)

function rand(r::AbstractRNG, ::Type{MatSpace}, coeffset::M, complexity::UnitRange{Int}) where {M <: Set}
  nrows = rand(r, complexity)
  ncols = rand(r, complexity)
  return MatrixSpace(coeffset, nrows, ncols)
end

function rand(r::AbstractRNG, ::Type{MatSpace}, coeffset::M, complexity_rows::UnitRange{Int}, complexity_cols::UnitRange{Int}) where {M <: Set}
  nrows = rand(r, complexity_rows)
  ncols = rand(r, complexity_cols)
  return MatrixSpace(coeffset, nrows, ncols)
end

rand(parent::MS, complexity) where {T, MS <: Nemo.MatSpace{T}} = rand(GLOBAL_RNG, parent, complexity)
rand(r::AbstractRNG, parent::MS, complexity::Int) where {T, MS <: Nemo.MatSpace{T}} = rand(r, parent, 1:complexity)

function rand(r::AbstractRNG, parent::MS, complexity::UnitRange{Int}) where {T, MS <: Nemo.MatSpace{T}}
  parent(elem_type(base_ring(parent))[
          rand(r, base_ring(parent), complexity)
          for i in 1:nrows(parent), j in 1:ncols(parent)])
end

rand(parent::MatSpace{cf_elem}, complexity) = rand(GLOBAL_RNG, parent, complexity)
rand(r::AbstractRNG, parent::MatSpace{cf_elem}, complexity::Int) = rand(r, parent, 1:complexity)

function rand(r::AbstractRNG, parent::MatSpace{cf_elem}, complexity::UnitRange{Int})
  order = rand(r,complexity)
  while order <= 0
    order = rand(r,complexity)
  end
  if isodd(order)
    order += 1
  end
  cfsf = ModularForms.QQabSubfield(Int(order))
  parent([cf_elem(order, rand(r, cfsf, complexity))
          for i in 1:nrows(parent), j in 1:ncols(parent)])
end

################################################################################
# free modules
################################################################################

rank(m::AbstractAlgebra.Generic.Submodule{A}) where A = ngens(m)

dim(m::AbstractAlgebra.Generic.Submodule{A}) where A = ngens(m)

length(a::FreeModuleElem{A}) where A = rank(parent(a))

getindex(a::FreeModuleElem{A}, jx::Int) where A = a.v[1,jx]

function iterate(a::FreeModuleElem{A}, ix :: Union{Nothing,Int} = nothing) where A
  if isnothing(ix)
    ix = 1
  end
  if ix > rank(parent(a))
    return nothing
  else
    return (a[ix],ix+1)
  end
end

################################################################################
# conversion among series rings with different precision
################################################################################

function convert_precision(
    ring::AbstractAlgebra.Generic.RelSeriesRing{T},
    a::AbstractAlgebra.Generic.RelSeries{T}
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")
  return ring(a.coeffs, a.length, a.prec, a.val)
end

function convert_precision(
    ring::Nemo.FmpzRelSeriesRing,
    a::Nemo.fmpz_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")

  b = fmpz_rel_series(a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.FmpqRelSeriesRing,
    a::Nemo.fmpq_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")

  b = fmpq_rel_series(a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.NmodRelSeriesRing,
    a::Nemo.nmod_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  b = nmod_rel_series(a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.FmpzModRelSeriesRing,
    a::Nemo.fmpz_mod_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  b = fmpz_mod_rel_series(a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.FqNmodRelSeriesRing,
    a::Nemo.fq_nmod_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  b = fq_nmod_rel_series(base_ring(ring), a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.FqRelSeriesRing,
    a::Nemo.fq_rel_series,
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  b = fq_rel_series(base_ring(ring), a)
  b.prec = a.prec
  b.val = a.val
  b.parent = ring

  return b
end

function convert_precision(
    ring::Nemo.FmpzLaurentSeriesRing,
    a::Nemo.fmpz_laurent_series
   )
  var(ring) == var(parent(a)) || error("variables must be identical")

  b = fmpz_laurent_series(a)
  b.prec = a.prec
  b.val = a.val
  b.scale = a.scale
  b.parent = ring

  return b
end

function convert_precision(
    ring::AbstractAlgebra.Generic.LaurentSeriesRing{T},
    a::AbstractAlgebra.Generic.LaurentSeriesRingElem{T}
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  return ring(a.coeffs, a.length, a.prec, a.val, a.scale, false)
end

function convert_precision(
    ring::AbstractAlgebra.Generic.LaurentSeriesField{T},
    a::AbstractAlgebra.Generic.LaurentSeriesFieldElem{T}
   ) where T
  var(ring) == var(parent(a)) || error("variables must be identical")
  base_ring(ring) == base_ring(parent(a)) || error("incompatible base rings")

  return ring(a.coeffs, a.length, a.prec, a.val, a.scale, false)
end

function convert_precision(
    ring::AbstractAlgebra.Generic.PuiseuxSeriesRing{T},
    a::AbstractAlgebra.Generic.PuiseuxSeriesRingElem{T}
   ) where T
  return ring(convert_precision(laurent_ring(ring), a.data), a.scale)
end

function convert_precision(
    ring::AbstractAlgebra.Generic.PuiseuxSeriesField{T},
    a::AbstractAlgebra.Generic.PuiseuxSeriesFieldElem{T}
   ) where T
  return ring(convert_precision(laurent_ring(ring), a.data), a.scale)
end

function convert_precision(
    ring::Nemo.FlintPuiseuxSeriesRing{T},
    a::Nemo.FlintPuiseuxSeriesRingElem{T}
   ) where T
  return ring(convert_precision(laurent_ring(ring), a.data), a.scale)
end

################################################################################
# change of base rings
################################################################################

# TODO: These functions secretely convert the precision of the parent ring.
# This needs a better interface.

function change_base_ring(a::AbstractAlgebra.Generic.LaurentSeriesRingElem{C}, f::Any, ring::AbstractAlgebra.Generic.LaurentSeriesRing{D}) where {C,D}
  return ring(elem_type(base_ring(ring))[f(c) for c in a.coeffs],
              a.length, a.prec, a.val, a.scale, true)
end

function change_base_ring(a::AbstractAlgebra.Generic.LaurentSeriesFieldElem{C}, f::Any, ring::AbstractAlgebra.Generic.LaurentSeriesField{D}) where {C,D}
  return ring(elem_type(base_ring(ring))[f(c) for c in a.coeffs],
              a.length, a.prec, a.val, a.scale, true)
end

function change_base_ring(a::AbstractAlgebra.Generic.PuiseuxSeriesRingElem{C}, f::Any, ring::AbstractAlgebra.Generic.PuiseuxSeriesRing{D}) where {C,D}
  return ring(change_base_ring(a.data, f, laurent_ring(ring)), a.scale)
end

function change_base_ring(a::AbstractAlgebra.Generic.PuiseuxSeriesFieldElem{C}, f::Any, ring::AbstractAlgebra.Generic.PuiseuxSeriesField{D}) where {C,D}
  return ring(change_base_ring(a.data, f, laurent_ring(ring)), a.scale)
end
