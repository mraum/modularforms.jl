################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# displaying
################################################################################

function show(io::IO, G::DirichletGroup)
  print(io, "Gχ($(G.q))")
end

function show(io::IO, G::DirichletGroupPrimitive)
  print(io, "Gχ⁰($(G.q))")
end

################################################################################
# creation
################################################################################

DirichletGroup(q::Int) = DirichletGroup(UInt(q))

subgroup(dg::DirichletGroup, q::Int) = DirichletGroup(dg,UInt(q))

################################################################################
# comparison
################################################################################

hash(gp::DirichletGroup, h::UInt) = xor(hash(DirichletGroup, h), hash(modulus(gp)))

==(gp_l::DirichletGroup, gp_r::DirichletGroup) = modulus(gp_l) == modulus(gp_r)

################################################################################
# iteration interface
################################################################################

length(G::DirichletGroup) = Int(G.phi_q)

eltype(::Type{DirichletGroup}) = DirichletCharacter

function iterate(G::DirichletGroup)
  return (one(G), one(G))
end

function iterate(G::DirichletGroup, state::DirichletCharacter)
  chi = DirichletCharacter(state)
  ix = ccall((:dirichlet_char_next, Nemo.libarb), Int,
             (Ref{DirichletCharacter}, Ref{DirichletGroup}),
             chi, G)

  # fixme: 
  # We have to test for ix > DG.num since in some cases
  # Arb seems to return 32 bit integers, even though
  # data structures relying on the bitsize work.
  if ix < 0 || ix > G.num
    return nothing
  else
    return (chi,chi)
  end
end

################################################################################
# iteration interface for primitive Characters
################################################################################

function primitive_characters(dg::DirichletGroup)
  len = Int(ccall((:dirichlet_group_num_primitive, Nemo.libarb), UInt,
                  (Ref{DirichletGroup},),
                  dg))
  DirichletGroupPrimitive(dg, len)
end

group(dg::DirichletGroupPrimitive) = dg.group

length(dg::DirichletGroupPrimitive) = dg.length

eltype(::Type{DirichletGroupPrimitive}) = DirichletCharacter

function iterate(dg::DirichletGroupPrimitive)
  length(dg) == 0 && return nothing

  chi = DirichletCharacter(group(dg))
  ccall((:dirichlet_char_first_primitive, Nemo.libarb), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}),
        chi, dg.group)
  return (chi, chi)
end

function iterate(dg::DirichletGroupPrimitive, state::DirichletCharacter)
  chi = DirichletCharacter(state)
  ix = ccall((:dirichlet_char_next_primitive, Nemo.libarb), Int,
             (Ref{DirichletCharacter}, Ref{DirichletGroup}),
             chi, dg.group)

  # fixme: 
  # We have to test for ix > DG.num since in some cases
  # Arb seems to return 32 bit integers, even though
  # data structures relying on the bitsize work.
  if ix < 0 || ix > length(dg)
    return nothing
  else
    return (chi,chi)
  end
end

################################################################################
# indexing interface
################################################################################

function getindex(G::DirichletGroup, i::Int)
  1 <= i <= G.phi_q || throw(BoundsError(G, i))
  chi = DirichletCharacter(G)
  ccall((:dirichlet_char_index, Nemo.libarb), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}, UInt),
        chi, G, UInt(i-1))
  return chi
end

################################################################################
# properties
################################################################################

modulus(dg::DirichletGroup) = Int(dg.q)

order(G::DirichletGroup) = Int(G.phi_q)

exponent(G::DirichletGroup) = Int(G.expo)

################################################################################
# random
################################################################################

rand(::Type{DirichletGroup}, complexity) = rand(GLOBAL_RNG, DirichletGroup, complexity)
rand(r::AbstractRNG, ::Type{DirichletGroup}, complexity::Int) = rand(r, DirichletGroup, 1:complexity)

function rand(r::AbstractRNG, ::Type{DirichletGroup}, complexity::UnitRange{Int})
  return DirichletGroup(rand(complexity))
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

hash(chi::DirichletCharacter, h::UInt) = xor(hash(parent(chi),h), hash(chi.n))

function isone(chi::DirichletCharacter)
  return order(chi) == 1
end

function ==(chi_l::DirichletCharacter, chi_r::DirichletCharacter)
  # NOTE: arb compares Dirichlet characters ignoring the modulus, which can
  # yield unintended results for characters that are not primitive.
  parent(chi_l) == parent(chi_r) || return false
  return Bool(ccall((:dirichlet_char_eq_wrapped, libmodularforms_jll), Int,
                    (Ref{DirichletCharacter}, Ref{DirichletCharacter}),
                    chi_l, chi_r))
end

function isless(a::DirichletCharacter, b::DirichletCharacter)
  if modulus(a) < modulus(b)
    return true
  elseif modulus(a) > modulus(b)
    return false
  end
    
  if conductor(a) < conductor(b)
    return true
  elseif conductor(a) > conductor(b)
    return false
  end

  return a.n < b.n
end

################################################################################
# displaying
################################################################################

function show(io::IO, chi::DirichletCharacter)
  print(io, "χ_$(chi.parent.q)($(chi.n),•)")
end

################################################################################
# creation
################################################################################

function DirichletCharacter(chi::DirichletCharacter)
  chi_n = DirichletCharacter(chi.parent)
  ccall((:dirichlet_char_set_wrapped, libmodularforms_jll), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}),
        chi_n, chi.parent, chi)
  return chi_n
end

function one(G::DirichletGroup)
  chi = DirichletCharacter(G)
  ccall((:dirichlet_char_one, Nemo.libarb), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}),
        chi, G)
  return chi
end

function copy(a::DirichletCharacter)
  parent(a)(a)
end

function deepcopy_internal(a::DirichletCharacter, stackdict::IdDict)
  return parent(a)(a)
end

################################################################################
# conversion
################################################################################

function primitive(chi::DirichletCharacter)
  isprimitive(chi) && return chi
  return DirichletGroup(conductor(chi))(chi)
end

function (dg::DirichletGroup)(chi::DirichletCharacter)
  if divisible(modulus(dg), modulus(chi))
    nchi = DirichletCharacter(dg)
    ccall((:dirichlet_char_lift, Nemo.libarb), Nothing,
          (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}, Ref{DirichletGroup}),
          nchi, parent(nchi), chi, parent(chi))
    return nchi
  elseif divisible(modulus(dg), conductor(chi))
    if divisible(modulus(chi), modulus(dg))
      nchi = DirichletCharacter(dg)
      ccall((:dirichlet_char_lower, Nemo.libarb), Nothing,
            (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}, Ref{DirichletGroup}),
            nchi, parent(nchi), chi, parent(chi))
      return nchi
    else
      dg(primitive(chi))
    end
  else
    error("conversion not possible")
  end
end

################################################################################
# basic properties
################################################################################

function order(chi::DirichletCharacter)
  o = ccall((:dirichlet_order_char, Nemo.libarb), UInt,
            (Ref{DirichletGroup}, Ref{DirichletCharacter}),
            chi.parent, chi)
  return Int(o)
end

modulus(chi::DirichletCharacter) = modulus(parent(chi))

function conductor(chi::DirichletCharacter)
  c = ccall((:dirichlet_conductor_char, Nemo.libarb), UInt,
            (Ref{DirichletGroup}, Ref{DirichletCharacter}),
            chi.parent, chi)
  return Int(c)
end

function parity(chi::DirichletCharacter)
  p = ccall((:dirichlet_parity_char, Nemo.libarb), UInt,
            (Ref{DirichletGroup}, Ref{DirichletCharacter}),
            chi.parent, chi)
  return Int(p)
end

iseven(chi::DirichletCharacter) = 0 == parity(chi)

isodd(chi::DirichletCharacter) = 1 == parity(chi)

function isreal(chi::DirichletCharacter)
  return 0 == ccall((:dirichlet_char_is_real, Nemo.libarb), Int,
                    (Ref{DirichletGroup}, Ref{DirichletCharacter}),
                    chi.parent, chi)
end

function isprimitive(chi::DirichletCharacter)
  return modulus(chi) == conductor(chi)
end

################################################################################
# arithmetic
################################################################################

function *(chi_l::DirichletCharacter, chi_r::DirichletCharacter)
  if parent(chi_l) != parent(chi_r)
    dg = DirichletGroup(lcm(modulus(chi_l),modulus(chi_r)))
    chi_l = dg(chi_l)
    chi_r = dg(chi_r)
  end

  chi_n = DirichletCharacter(chi_l.parent)
  ccall((:dirichlet_char_mul, Nemo.libarb), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}, Ref{DirichletCharacter}),
        chi_n, chi_l.parent, chi_l, chi_r)
  return chi_n
end

function mul!(c::DirichletCharacter, a::DirichletCharacter, b::DirichletCharacter)
  if parent(a) != parent(c) || parent(b) != parent(c)
    throw(DomainError((c,a,b), "incompatible parents"))
  end

  ccall((:dirichlet_char_mul, Nemo.libarb), Nothing,
        (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}, Ref{DirichletCharacter}),
        c, c.parent, a, b)
  return c
end

function ^(chi::DirichletCharacter, n::UInt)
  # TODO: Arb calls nmod_mul with second factor n, which might not be reduced;
  # I think this is the reason why the result sometimes is incorrect. 
  # When fixing this also reenable call to this function in from n::Int.
  #  dg = Gχ(44)
  #  chi = χ_44(31,•) # = dg[17}
  #  chi * inv(chi) = χ_44(1,•)
  #  conductor(chi * inv(chi)) = 11
  #
  # chi_n = DirichletCharacter(chi.parent)
  # ccall((:dirichlet_char_pow, Nemo.libarb), Nothing,
  #       (Ref{DirichletCharacter}, Ref{DirichletGroup}, Ref{DirichletCharacter}, UInt),
  #       chi_n, chi.parent, chi, n)
  # return chi_n
  return power_by_squaring(chi,n)
end

function ^(chi::DirichletCharacter, n::Int)
  isone(chi) && return chi

  if n < 0
    n = mod(n,euler_phi(conductor(chi)))
  end
  return chi^UInt(n)
end

Base.inv(chi::DirichletCharacter) = chi^(UInt(euler_phi(conductor(chi))-1))

conj(chi::DirichletCharacter) = inv(chi)

################################################################################
# evaluation
################################################################################

function evaluate__exponent(chi::DirichletCharacter, n::UInt)
  return ccall((:dirichlet_chi, Nemo.libarb), UInt,
               (Ref{DirichletGroup}, Ref{DirichletCharacter}, UInt),
               chi.parent, chi, n)
end

evaluate__exponent(chi::DirichletCharacter, n::Int) = evaluate__exponent(chi, UInt(n))

function evaluate__root_of_unity(chi::DirichletCharacter, n::UInt)
  e = evaluate__exponent(chi,n)
  e == typemax(UInt) && throw("only nonzero values allowed")
  RootOfUnity(exponent(chi.parent),e)
end

function (chi::DirichletCharacter)(n::UInt)
  e = evaluate__exponent(chi,n)
  e == typemax(UInt) && return zero(QQab)
  root_of_unity(exponent(chi.parent))^Int(e)
end

(chi::DirichletCharacter)(n::Union{Int,fmpz}) = chi(UInt(mod(n, modulus(chi))))

################################################################################
# Gauss sums
################################################################################

function gauss_sum(chi::DirichletCharacter, a::RootOfUnity, ring::R = QQab) where {R <: Ring}
  N = modulus(chi)
  N == 1 && return one(ring)

  acc = ring(a)
  apown = a
  for chin in map(chi,2:N-1)
    apown *= a
    if chin isa RootOfUnity
      acc += ring(chin*apown)
    end
  end
  return acc
end

function gauss_sum(chi::DirichletCharacter, ring::R = QQab) where {R <: Ring}
  gauss_sum(chi, root_of_unity(modulus(chi)), ring)
end

function gauss_sum(chi::DirichletCharacter, a::Int, ring::R = QQab) where {R <: Ring}
  gauss_sum(chi,MuInf(a//modulus(chi)),ring)
end

################################################################################
# random
################################################################################

rand(dg::Union{DirichletGroup,DirichletGroupPrimitive}) = rand(GLOBAL_RNG, dg)

function rand(r::AbstractRNG, dg::Union{DirichletGroup,DirichletGroupPrimitive})
  rand(collect(dg))
end

rand(::Type{DirichletCharacter}, complexity) = rand(GLOBAL_RNG, DirichletCharacter, complexity)

function rand(r::AbstractRNG, ::Type{DirichletCharacter}, complexity)
  return rand(r, rand(r, DirichletGroup, complexity))
end
