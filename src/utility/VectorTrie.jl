################################################################################
# properties
################################################################################

value(trie :: VectorTrie{K,A}) where {K,A} = trie.val

subtries(trie :: VectorTrie{K,A}) where {K,A} = trie.subtries

################################################################################
# creation
################################################################################

VectorTrie() = VectorTrie{Any,Any}()

function VectorTrie(
    kvs :: AbstractVector{Pair{VK,A}};
    check :: Bool = true
   ) where {K, A, VK <: AbstractVector{K}}
  trie = VectorTrie{K,A}()
  if check
    for (k,v) in kvs
      if haskey(trie,k)
        throw(DomainError(k, "duplicate key"))
      end
      trie[k] = v
    end
  else
    for (k,v) in kvs
      trie[k] = v
    end
  end
  return trie
end

function VectorTrie(
    kvs::AbstractVector{Tuple{VK,A}};
    check :: Bool = true
   ) where {K, A, VK <: AbstractVector{K}}
  trie = VectorTrie{K,A}()
  if check
    for (k,v) in kvs
      if haskey(trie,k)
        throw(DomainError(k, "duplicate key"))
      end
      trie[k] = v
    end
  else
    for (k,v) in kvs
      trie[k] = v
    end
  end
  return trie
end

function VectorTrie(
    ks :: AbstractVector{VK},
    vs :: AbstractVector{A};
    check :: Bool = true
   ) where {K, A, VK <: AbstractVector{K}}
  trie = VectorTrie{K,A}()
  if check
    for (k,v) in zip(ks,vs)
      if haskey(trie,k)
        throw(DomainError(k, "duplicate key"))
      end
      trie[k] = v
    end
  else
    for (k,v) in zip(ks,vs)
      trie[k] = v
    end
  end
  return trie
end

function VectorTrie(
    kvs :: AbstractDict{VK,A}
   ) where {K, A, VK <: AbstractVector{K}}
  trie = VectorTrie{K,A}()
  for (k,v) in zip(ks,vs)
    trie[k] = v
  end
  return trie
end

################################################################################
# comparison
################################################################################

function ==(
    triel :: VectorTrie{K,A},
    trier :: VectorTrie{K,A}
   ) where {K, A}
  triel.val == trier.val && triel.subtries == trier.subtries
end

################################################################################
# subtries and element access
################################################################################

function subtrie(
    trie :: VectorTrie{K,A},
    key :: VK
   ) where {K, A, VK <: AbstractVector{K}}
  for k in key
    if !haskey(trie.subtries, k)
      return nothing
    end
    trie = trie.subtries[k]
  end
  return trie
end

function Base.haskey(
    trie :: VectorTrie{K,A},
    key :: VK
   ) where {K, A, VK <: AbstractVector{K}}
  trie = subtrie(trie, key)
  return !isnothing(trie) && !isnothing(trie.val)
end

function Base.setindex!(
    trie :: VectorTrie{K,A},
    val
   ) where {K, A}
  trie.val = val
end

function Base.setindex!(
    trie :: VectorTrie{K,A},
    val,
    key :: VK
   ) where {K, A, VK <: AbstractVector{K}}
  for k in key
    if !haskey(trie.subtries, k)
      trie.subtries[k] = VectorTrie{K,A}()
    end
    trie = trie.subtries[k]
  end
  trie.val = val
end

function Base.getindex(
    trie :: VectorTrie{K,A},
    key :: VK
   ) where {K, A, VK <: AbstractVector{K}}
  trie = subtrie(trie, key)
  if !isnothing(trie) && !isnothing(trie.val)
    return trie.val
  end
  throw(KeyError("key $key not found"))
end

function Base.get(
    trie    :: VectorTrie{K,A},
    key     :: VK,
    default :: Union{Nothing,A} = nothing
   ) where {K, A, VK <: AbstractVector{K}}
  trie = subtrie(trie, key)
  if !isnothing(trie) && !isnothing(trie.val)
    return trie.val
  end
  return default
end

function Base.delete!(
    trie    :: VectorTrie{K,A},
    key     :: VK
   ) where {K, A, VK <: AbstractVector{K}}
  trie_root = trie
  prune_dict = nothing
  prune_key = nothing

  (nk,) = size(key)
  for (kx,k) in enumerate(key)
    if !haskey(trie.subtries, k)
      return trie_root
    end
    nexttrie = trie.subtries[k]

    if isnothing(prune_dict)
      # Check whether the next trie needs to be pruned in case we delete its
      # value or a subtrie.
      if kx == nk
        # The next trie is the final one. We will delete its value. If it has
        # no subtries, we need to remove it from the currect list.
        if isempty(nexttrie.subtries)
          prune_dict = trie.subtries
          prune_key = k
        end
      else
        # There are several steps before we reach the final trie. Only if the
        # next trie has no value and a unique subtrie it has to be pruned.
        if isnothing(nexttrie.val) && length(nexttrie.subtries) == 1
          prune_dict = trie.subtries
          prune_key = k
        end
      end
    else
      # Check whether the next trie needs to preserved.
      if kx == nk
        # The next trie is the final one. If is has no subtries, it will be
        # pruned.
        if !isempty(nexttrie.subtries)
          prune_dict = nothing
        end
      else
        # There are several steps before we reach the final trie. Only if the
        # next trie has no value and a unique subtrie it has to be pruned.
        if !isnothing(nexttrie.val) || length(nexttrie.subtries) != 1
          prune_dict = nothing
        end
      end
    end

    trie = nexttrie
  end

  trie.val = nothing
  if !isnothing(prune_dict)
    delete!(prune_dict, prune_key)
  end

  return trie_root
end

################################################################################
# iteration
################################################################################

function Base.keys(
    trie   :: VectorTrie{K,A},
    prefix :: VK = K[],
    keys   :: AbstractVector{VK} = Vector{K}[]
   ) where {K, A, VK <: AbstractVector{K}}
  if !isnothing(trie.val)
      push!(keys, copy(prefix))
  end
  for (k,subtrie) in trie.subtries
      push!(prefix,k)
      Base.keys(subtrie, prefix, keys)
      pop!(prefix)
  end
  return keys
end

# depth first iteration
function Base.collect(
    trie    :: VectorTrie{K,A},
    prefix  :: VK = K[],
    keyvals :: AbstractVector{Pair{VK,A}} = Pair{Vector{K},A}[]
   ) where {K, A, VK <: AbstractVector{K}}
  if !isnothing(trie.val)
    push!(keyvals, copy(prefix) => trie.val)
  end
  for (k,subtrie) in trie.subtries
    push!(prefix,k)
    collect(subtrie, prefix, keyvals)
    pop!(prefix)
  end
  return keyvals
end

function Base.IteratorSize(::Type{VectorTrie{K,A}}) where {K, A}
  Base.SizeUnknown()
end

function Base.eltype(::Type{VectorTrie{K,A}}) where {K, A}
  Pair{Vector{K},A}
end

function Base.iterate(
    trie :: VectorTrie{K,A}
   ) where {K, A}
  if isempty(trie.subtries)
    if isnothing(trie.val)
      return nothing
    else
      state = VectorTrieIteratorState{K,A}([], Int[], K[])
      return (K[] => trie.val, state)
    end
  else
    state = VectorTrieIteratorState{K,A}([trie.subtries], Int[], K[])
    if isnothing(trie.val)
      return iterate(trie, state)
    else
      return (K[] => trie.val, state)
    end
  end
end

function Base.iterate(
    trie  :: VectorTrie{K,A},
    state :: VectorTrieIteratorState{K,A}
   ) where {K, A}
  while true
    if isempty(state.dicts)
      return nothing
    elseif length(state.dicts) != length(state.states)
      # In this case, we have that dicts is one item longer, and we need to
      # initialize the iteration of that dictionary.
      it = iterate(state.dicts[end])
      if isnothing(it)
        # If there is nothing to iterate in the last dict, we are done with
        # this iteration level.
        pop!(state.dicts)
      else
        ((k,subtrie), dict_state) = it
        push!(state.dicts, subtrie.subtries)
        push!(state.states, dict_state)
        push!(state.k,k)

        if isnothing(subtrie.val)
          # If there is no value stored, we simply continue the iteration with
          # the next dictionary of subtries
        else
          # If the node contains a value, we return this and prepare the
          # iteration of the corresponding subtries.
          return (copy(state.k) => subtrie.val, state)
        end
      end
    else # length(state.dicts) == length(state.states)
      # In this case, we need to continue the iteration of the current
      # dictionary.
      it = iterate(state.dicts[end], state.states[end])
      if isnothing(it)
        # The iteration of the last dictionary has ended. We step back. Since
        # we know that this branch is only taken if dicts[end] successfully
        # allowed for at least one iteration, we also need to truncate the key.
        pop!(state.dicts)
        pop!(state.states)
        pop!(state.k)
      else
        ((state.k[end], subtrie), state.states[end]) = it
        push!(state.dicts, subtrie.subtries)

        if isnothing(subtrie.val)
          # If there is no value stored, we simply continue the iteration with
          # the next dictionary of subtries
        else
          # If the node contains a value, we return this and prepare the
          # iteration of the corresponding subtries.
          return (copy(state.k) => subtrie.val, state)
        end
      end
    end
  end
end

################################################################################
# copy
################################################################################

function Base.copy(
    trie::VectorTrie{K,A}
   ) where {K,A}
  VectorTrie{K,A}(trie.val,
      Dict{K,VectorTrie{K,A}}(k => copy(subtrie)
                              for (k,subtrie) in trie.subtries))
end

function Base.deepcopy_internal(
    trie::VectorTrie{K,A},
    stackdict::IdDict
   ) where {K,A}
  if !haskey(stackdict, trie)
    stackdict[trie] = VectorTrie{K,A}(
        deepcopy_internal(trie.val, stackdict),
        deepcopy_internal(trie.subtries, stackdict))
  end
  return stackdict[trie]
end

################################################################################
# map, merge, map_reduce
################################################################################

function Base.map(
    f,
    trie::VectorTrie{K,A};
    value_type :: Type = Nothing
   ) where {K,A}
  if value_type === Nothing
    value_type = A
  end
  VectorTrie{K,value_type}(
      isnothing(trie.val) ? nothing : f(trie.val),
      Dict{K,VectorTrie{K,value_type}}(
          k => map(f, subtrie; value_type)
          for (k,subtrie) in trie.subtries))
end

function Base.map!(
    f,
    trie::VectorTrie{K,A}
   ) where {K,A}
  if !isnothing(trie.val)
    trie.val = f(trie.val)
  end
  for (k,subtrie) in trie.subtries
    map!(f, subtrie)
  end
  return trie
end

function map_keys(
    f,
    trie::VectorTrie{K,A};
    key_type :: Type = Nothing
   ) where {K,A}
  if key_type === Nothing
    key_type = K
  end
  VectorTrie{key_type,A}(
      trie.val,
      Dict{key_type,VectorTrie{key_type,A}}(
          f(k) => map_keys(f, subtrie; key_type)
          for (k,subtrie) in trie.subtries))
end

function Base.mergewith(
    f,
    triel::VectorTrie{K,A},
    trier::VectorTrie{K,A}
   ) where {K,A}
  mergewith!(f, copy(triel), trier)
end

function Base.mergewith!(
    f,
    triel::VectorTrie{K,A},
    trier::VectorTrie{K,A}
   ) where {K,A}
  if !isnothing(trier.val)
    if isnothing(triel.val)
      triel.val = trier.val
    else
      triel.val = f(triel.val, trier.val)
    end
  end

  for (k,subtrie) in trier.subtries
    if haskey(triel.subtries, k)
      mergewith!(f, triel.subtries[k], subtrie)
    else
      triel.subtries[k] = copy(subtrie)
    end
  end

  return triel
end

@doc Markdown.doc"""
    map_suffix_accumulate(
        trie       :: VectorTrie{K,A},
        map        :: Function,
        accumulate :: Function,
        combine    :: Function
       )

Apply map to the values of the trie, accumulate subtries, and combine values
and subtries.
"""
function map_reduce(
    trie    :: VectorTrie{K,A},
    map     :: Function,
    reduce  :: Function,
    combine :: Function;
    init = nothing
   ) where {K, A}
  if isempty(trie.subtries)
    if isnothing(trie.val)
      return init
    else
      return map(trie.val)
    end
  else
    subtries = reduce(
        ((k, map_reduce(subtrie, map, reduce, combine))
         for (k,subtrie) in trie.subtries))
    if isnothing(trie.val)
      return subtries
    else
      return combine(map(trie.val), subtries)
    end
  end
end

function map_extend_union(
    trie   :: VectorTrie{K,A},
    map    :: Function,
    extend :: Function;
    init = nothing
   ) where {K, A}
  map_reduce(trie,
      c -> Base.Set([map(c)]),
      subtries_withkey -> reduce(union,
          [Base.Set([extend(k,t) for t in subtrie])
           for (k,subtrie) in subtries_withkey]),
      union;
      init = Base.Set([init]))
end
