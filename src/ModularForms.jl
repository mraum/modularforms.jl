__precompile__()

module ModularForms

function __init__()
  # Respect the -q flag
  isquiet = Bool(Base.JLOptions().quiet)

  # Check if were are non-interactive
  bt = Base.process_backtrace(Base.backtrace())
  isinteractive_manual = all(sf -> sf[1].func != :_tryrequire_from_serialized, bt)

  show_banner = !isquiet && isinteractive_manual && isinteractive() &&
                get(ENV, "MODULARFORMS_PRINT_BANNER", "true") != "false"

  if show_banner
    println("")
    print("Welcome to \n")
    printstyled(" __  __ ", bold=true, color=:red)
    print("        _      _          ")
    printstyled("___", bold=true, color=:red)
    print("\n")
    printstyled("|  \\/  |", bold=true, color=:red)
    print("___  __| |_  _| |__ _ _ _")
    printstyled("| __|", bold=true, color=:red)
    print("__ _ _ _ __  ___\n")
    printstyled("| |\\/|", bold=true, color=:red)
    print(" / _ \\/ _' | || | / _' | '_")
    printstyled("| _/", bold=true, color=:red)
    print(" _ \\ '_| '  \\(_-‹\n")
    printstyled("|_|  |_", bold=true, color=:red)
    print("\\___/\\__,_|\\_,_|_\\__,_|_| ")
    printstyled("|_|", bold=true, color=:red)
    print("\\___/_| |_|_|_/__/\n")
    println()
    print("Version ")
    printstyled("$VERSION_NUMBER", color = :red)
    println("\n(c) 2018-2021 by Martin Raum, Tobias Magnusson and Albin Ahlbäck\n")
  end
end

################################################################################
################################################################################
# Version number
################################################################################
################################################################################

import Pkg
deps = Pkg.dependencies()
if haskey(deps, Base.UUID("1591e58e-be0c-5971-b103-0733cc033d6a"))
  ver = Pkg.dependencies()[Base.UUID("1591e58e-be0c-5971-b103-0733cc033d6a")]
  if occursin("/dev/", ver.source)
    global VERSION_NUMBER = VersionNumber("$(ver.version)-dev")
  else
    global VERSION_NUMBER = VersionNumber("$(ver.version)")
  end
else
  global VERSION_NUMBER = "building"
end

################################################################################
################################################################################
# Includes
################################################################################
################################################################################

include("Exports.jl")
include("Using.jl")
include("Imports.jl")
include("Types.jl")

## adjustment of external libraries
include("utility/Base.jl")
include("utility/Nemo.jl")

## basic functionality
include("utility/VectorTrie.jl")
include("utility/TensorProduct.jl")
include("utility/LinearAlgebra.jl")
include("utility/SubmoduleAcc.jl")
include("utility/SubmoduleAccWithGenMap.jl")
include("utility/Orbit.jl")
include("utility/ContinuedFraction.jl")
include("utility/Hurwitz.jl")

## cyclotomic aritmetic
include("utility/RootOfUnity.jl")
include("utility/QQab.jl")
include("utility/MuInfTower.jl")
include("utility/CCMuInfTower.jl")
include("utility/QQMuInfTower.jl")
include("utility/GFpMuInfTower.jl")

## various base objects
include("utility/Dirichlet.jl")
include("utility/TorQuadMod.jl")

include("modular_group/Generators.jl")
include("modular_group/SL2Z/Cusp.jl")
include("modular_group/SL2Z/FareySymbol_MODULE.jl")
include("modular_group/SL2Z/PermStab_MODULE.jl")
include("modular_group/SL2Z/ParabolicCoset.jl")
include("modular_group/SL2Z/Subgroup.jl")
include("modular_group/SL2Z/SubgroupCover.jl")
include("modular_group/SL2Z/Hecke.jl")
include("modular_group/Heisenberg.jl")
include("modular_group/Jacobi.jl")
include("modular_group/SpnZ.jl")

include("analytic_type/SL2R.jl")

include("arithmetic_type/image/TrivialGroup.jl")
include("arithmetic_type/image/MatrixGroup.jl")
include("arithmetic_type/image/SymTransformation.jl")
include("arithmetic_type/image/WeilTransformation.jl")
include("arithmetic_type/image/TwistedPerm.jl")

include("arithmetic_type/General.jl")
include("arithmetic_type/Trivial.jl")
include("arithmetic_type/Generic.jl")
include("arithmetic_type/Dirichlet.jl")
include("arithmetic_type/Matrix.jl")
include("arithmetic_type/Weil.jl")
include("arithmetic_type/Restriction.jl")
include("arithmetic_type/Pullback.jl")
include("arithmetic_type/Pushforward.jl")
include("arithmetic_type/Tensor.jl")
include("arithmetic_type/Induction.jl")
include("arithmetic_type/SL2Z.jl")
include("arithmetic_type/SL2ZCover.jl")
include("arithmetic_type/Hecke.jl")
# TODO: reactivate when Jacobi groups are stable
# include("arithmetic_type/Jacobi.jl")
include("arithmetic_type/TranslationDecomposition.jl")

include("arithmetic_type/MoonshineLike.jl")

include("arithmetic_type/homomorphism/Orbit.jl")
include("arithmetic_type/homomorphism/Generic.jl")
include("arithmetic_type/homomorphism/Isotrivial.jl")


include("modular_form/Generic.jl")
include("modular_form/Symbolic.jl")
include("modular_form/SL2Z/FourierExpansion_MODULE.jl")
include("modular_form/SL2Z/Symbolic.jl")
include("modular_form/SL2Z/EisensteinCongruence_MODULE.jl")
include("modular_form/SL2Z/CuspFormCongruence_MODULE.jl")
include("modular_form/SL2Z/CongruenceType_MODULE.jl")
include("modular_form/SL2Z/CongruenceType.jl")
include("modular_form/SL2Z/Hecke.jl")
include("modular_form/SL2Z/PeriodPoly.jl")
include("modular_form/SL2Z/EisensteinSymmetric_MODULE.jl")
include("modular_form/SL2Z/EisensteinSndOrder_MODULE.jl")

end
