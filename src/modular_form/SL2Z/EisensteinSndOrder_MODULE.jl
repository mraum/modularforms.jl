module ModularFormSL2ZEisensteinSndOrder_MODULE

using Nemo
using ModularForms

using ModularForms:
  _act_symmetric_power_polynomial,
  integral_basis,
  PeriodPolySL2Z,
  SL2zElem,
  SL2zElem_from_bottom_row

using ModularForms.ModularFormSL2ZFourierExpansion_MODULE:
  fourier_expansion_ring


function eisenstein_sndorder_fourier_expansion(
    k      :: Int,
    phiS   :: PeriodPolySL2Z{R},
    j      :: Int,
    prc    :: Int,
    CC     :: AcbField;
    maxerr :: Union{arb,Vector{arb}}
   ) where R

  RR = ArbField(precision(CC))
  l = degree(parent(phiS))+2

  if maxerr isa arb
    maxerr = fill(maxerr,prc)
  end
  C = 0
  err_pre = ( inv( RR(k+2j-2l+2) * gamma(RR(k)) )
            * RR(2)^j * const_e(RR)^2 * gamma(RR(l-1))^2 * (2*const_pi(RR))^(k + j - l)
            * zeta(RR(l-1)//2)^2
            )
  err = nothing
  while isnothing(err) || any(err .> maxerr)
    C += 5
    # we use C > 1
    err = [err_pre * RR(n)^(k + j - 1) * inv(RR(C)^(k+2j-2l+3))
           for n in 0:prc-1]
  end

  return eisenstein_sndorder_fourier_expansion(k, phiS, j, prc, CC, C)
end

# Fourier expansion of given precision of the generalized second order
# Eisenstein series associated with the dual cocycle of the cocycle ϕ with
# period polynomial ϕ(S) and (X-τ)^j. Summation over all cosets with |c| < C.
function eisenstein_sndorder_fourier_expansion(
    k       :: Int,
    phiS    :: PeriodPolySL2Z{R},
    j       :: Int,
    prc     :: Int,
    CC      :: AcbField,
    C       :: Int
   ) where R
  @warn ( "When testing the evaluation for second order Eisenstein series "
        * "associated with the Δ-cocyle, we get errors in some of the very high "
        * "digits. It is currently not clear where the mistake originates." )
  fering,_ = fourier_expansion_ring(CC, prc; level = 1)
  res = zero(fering)
  for (c,phiSbasis) in zip(coordinates(phiS), integral_basis(parent(phiS)))
    res = addeq!(res,
            CC(c) * eisenstein_sndorder_fourier_expansion(k, phiSbasis, j, prc, CC, C))
  end
  return res
end

function eisenstein_sndorder_fourier_expansion(
    k       :: Int,
    phiSinv :: PeriodPolySL2Z{fmpz},
    j       :: Int,
    prc     :: Int,
    CC      :: AcbField,
    C       :: Int
   )
  C > 0 || throw(DomainError(C, "incorrect bound C"))
  C == 1 && error("not implemented")

  ga = SL2zElem(0, -1, 1, 0)
  deg = degree(parent(phiSinv))

  psi_cache = Dict{NTuple{2,Int}, Vector{fmpz}}()
  psi_cache[(1,0)] = coefficients(phiSinv)

  # We compute with the right cocycle ψ(γ) = ϕ(γ^-1)
  @assert iseven(deg)
  psiSneg = coefficients(phiSinv)

  # We cache binomial coefficients and their inverse
  binomial_cache = zeros(CC,deg+1,deg+1)
  binomial_inv_cache = zeros(CC,deg+1,deg+1)
  for j in 0:deg, i in 0:j
    binomial_cache[j+1,i+1] = CC(binomial(ZZ(j),ZZ(i)))
    binomial_inv_cache[j+1,i+1] = inv(binomial_cache[j+1,i+1])
  end

  # constants to scale cs in the next loop
  cs_scales = zeros(CC,j+1)
  for r in 0:j
    cs_scales[r+1] = ( binomial_cache[1+j,1+r] * (2*const_pi(CC)*onei(CC))^(k+r)
                    // CC(factorial(ZZ(k+r-1))) )
  end

  # powers of n which otherwise are computed in the very inner loop
  npws = [CC(n)^(k-1+r) for n in 0:prc-1, r in 0:j]

  # variable for inplace arithmetic
  tmp = zero(CC)

  coeffs = zeros(CC, prc)
  for c in 1:C-1, d in 0:c-1
    ga = SL2zElem_from_bottom_row(c,d; return_nothing=true)
    isnothing(ga) && continue
    ga_dbyc = CC(ga.d//ga.c)
    ga_dbyc_pws = acb[ga_dbyc^i for i in 0:deg]
    ga_cinv_const = CC(1//ga.c)^(k+2*j-deg)

    # We compute via cocycle relations
    # psi_ga = ModularForms.right_cocycle_value(phiSinv, ga)

    if c == 1
      psi_ga = psi_cache[(1,0)]
    else
      # We write γ = - δ T^n S, i.e., -c = δd+nδc, with δc = d and δd < d
      n,δc = fldmod(-ga.c,ga.d)
      psi_ga = _act_symmetric_power_polynomial(psi_cache[(d,δc)], SL2zElem(-n,1,-1,0)) .+ psiSneg
      psi_cache[(c,d)] = psi_ga
    end
    psi_ga_CC = map(CC, psi_ga)

    # The coefficients except for the contribution involving n
    cs = zeros(CC, j+1)
    for r in 0:j
      for i in 0:deg-j+r
        tmp = iseven(i) ? CC(1) : CC(-1)
        tmp = mul!(tmp, tmp, binomial_inv_cache[1+deg,1+i])
        tmp = mul!(tmp, tmp, binomial_cache[1+deg-j+r,1+i])
        tmp = mul!(tmp, tmp, ga_dbyc_pws[1+deg-j+r-i])
        tmp = mul!(tmp, tmp, psi_ga_CC[1+deg-i])
        cs[r+1] = addeq!(cs[r+1], tmp)
      end
      if iseven(j-k)
        cs[r+1] = mul!(cs[r+1], cs[r+1], ga_cinv_const)
      else
        cs[r+1] = mul!(cs[r+1], cs[r+1], -ga_cinv_const)
      end
      cs[r+1] = mul!(cs[r+1], cs[r+1], cs_scales[r+1])
    end

    zetac = root_of_unity(CC,ga.c)
    twist_n = zero(CC)
    twists = zeros(CC, ga.c)
    twists[1] = zetac
    for n in 2:ga.c
      twists[n] = mul!(twists[n], twists[n-1], zetac)
    end
    for n in 0:prc-1
      for r in 0:j
        tmp = mul!(tmp, cs[r+1], twists[mod1(ga.d*n,ga.c)])
        tmp = mul!(tmp, tmp, npws[n+1,r+1])
        coeffs[n+1] = addeq!(coeffs[n+1], tmp)
      end
    end
  end

  RR = ArbField(precision(CC))
  l = deg+2
  err_pre = ( inv( RR(k+2j-2l+2) * gamma(RR(k)) * RR(C)^(k+2j-2l+2) )
            * RR(2)^j * const_e(RR)^2 * gamma(RR(l-1))^2 * (2*const_pi(RR))^(k + j - l)
            * zeta(RR(l-1)//2)^2
            )
  for n in 0:prc-1
    coeffs[n+1] += ball(zero(RR), err_pre * RR(n)^(k + j - 1))
  end

  fering,_ = fourier_expansion_ring(CC, prc; level = 1)
  return fering(laurent_ring(fering)(coeffs, prc, prc, 0, 1), 1)
end

function eisenstein_sndorder_evaluate(
    k      :: Int,
    phiS   :: PeriodPolySL2Z{R},
    j      :: Int,
    tau    :: acb;
    maxerr :: Union{Nothing,arb} = nothing
   ) where R

  CC = parent(tau)
  RR = ArbField(precision(CC))
  y = imag(tau)
  l = degree(parent(phiS))+2

  if isnothing(maxerr)
    maxerr = radius(abs(tau))
    if maxerr <= 0
      maxerr = RR("1e-10")
    end
  end

  prc = 0
  feerr_pre = ( RR(2)^j * const_e(RR)^2 * (k+2j-2l+3) * gamma(RR(l-1))^2
              * zeta(RR(l-1)//2)^2
              * inv( (k+2j-2l+2) * gamma(RR(k)) * (2*const_pi(RR))^l * y^(k+j) )
              )
  feerr = nothing
  while isnothing(feerr) || feerr > maxerr//2
    prc += 10
    feerr = feerr_pre * gamma(RR(k+j), 2*const_pi(RR) * (prc-1) * y)
  end

  # We augment the precision to avoid unnecessary accumulation of arithmetic errors
  CCaug = AcbField(precision(CC)+64)
  RRaug = ArbField(precision(CC)+64)
  fe = eisenstein_sndorder_fourier_expansion(k, phiS, j, prc, CCaug;
           maxerr = arb[maxerr//(2*(prc-1)) * exp(2*const_pi(RRaug)*n*RRaug(y))
                        for n in 0:prc-1])

  return ( sum(coeff(fe,n) * cispi(2*n*tau) for n in 0:prc-1)
         + ball(zero(RR), feerr)
         )
end

end

import .ModularFormSL2ZEisensteinSndOrder_MODULE
