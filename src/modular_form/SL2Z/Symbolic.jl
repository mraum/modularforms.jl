################################################################################
################################################################################
# type functions
################################################################################
################################################################################

function parent_type(
    ::Type{ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}}
   ) where {RT,RB,R}
  ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}
end

function parent(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  f.parent
end

function elem_type(
    ::Type{ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}}
   ) where {RT,RB,R}
  ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
end

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

function ==(
    f::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R},
    g::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}
   ) where {RT,RB,R}
  coefficient_ring_tower(f) == coefficient_ring_tower(g)
end

################################################################################
# properties
################################################################################

function coefficient_ring_tower(
    f::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}
   ) where {RT,RB,R}
  f.coefftower
end

################################################################################
# display
################################################################################

function show(
    io::IO,
    f::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}
   ) where {RT,RB,R}
  print(io, "Symbolic ring of modular forms for $SL2Z with coefficients in ")
  show(io, coefficient_ring_tower(f))
  println(io, "")
end

################################################################################
# creation
################################################################################

function ModularFormsSL2ZCongruenceSymbolicRing(
    coefftower::RT
   ) where RT <: MuInfTower
   # TODO: cache this
  RB = elem_type(base_ring(coefftower))
  R = elem_type(coefftower)
  ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}(coefftower)
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

function coefficient_ring(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  f.coeffring
end

function term_trie(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  f.terms
end

function weight(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R};
   ) where {RT,RB,R}
  wts = map_extend_union(term_trie(f),
            _ -> 0,
            (g,wt) -> weight(g) + wt,
            init = 0)
  if length(wts) == 1
    return pop!(wts)
  else
    # In case of multi weight modular forms, which do appear in applications,
    # we do not want to sacrifice information by supressing this.
    return wts
  end
end

function level(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  lcm(collect(map_extend_union(term_trie(f),
          _ -> 1,
          (g,lvl) -> lcm(level(g), lvl),
          init = 1)))
end

function symbolic_fourier_coefficient_muinf_order(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  lcm(collect(map_extend_union(
      term_trie(f),
      c -> 1,
      (g,ord) -> lcm(fourier_coefficient_muinf_order(g), ord),
      init = 1)))
end

function fourier_coefficient_ring(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  coefftower = coefficient_ring_tower(parent(f))
  coeffring = coefficient_ring(f)
  sfe_muinf_order = symbolic_fourier_coefficient_muinf_order(f)
  if has_muinf_order(coefftower, coeffring, sfe_muinf_order)
    return coeffring
  else
    (fering, _) =
        muinf_extension(coefftower, coeffring, sfe_muinf_order)
    return fering
  end
end

function fourier_coefficient_ring(
    fs :: Vector{ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}};
    coefficient_ring :: Union{Nothing,Ring} = nothing
   ) where {RT,RB,R}
  if isempty(fs)
    throw(DomainError(fs, "empty vector of symbolic modular forms"))
  end
  coefftower = coefficient_ring_tower(parent(first(fs)))
  if isnothing(coefficient_ring)
    fering = muinf_ring(coefftower,
                        map(ModularForms.coefficient_ring, fs)...)
  else
    fering = muinf_ring(coefftower,
                        coefficient_ring,
                        map(ModularForms.coefficient_ring, fs)...)
  end
  fe_muinf_order = lcm(map(symbolic_fourier_coefficient_muinf_order, fs))
  if !has_muinf_order(coefftower, fering, fe_muinf_order)
    fering, _ = muinf_extension(coefftower, fering, fe_muinf_order)
  end
  return fering
end

################################################################################
# display
################################################################################

function expressify(
    f :: ModularFormsSL2ZCongruenceSymbolicRingElem;
    context = nothing
   )
  sum = Expr(:call, :+)
  for (ess,c) in term_trie(f)
    if !iszero(c)
      if isempty(ess)
        if isone(c)
          push!(sum.args, Expr(:call, :*, 1))
        else
          push!(sum.args, Expr(:call, :*, expressify(c, context = context)))
        end
      else
        prod = Expr(:call, :*)
        for es in ess
          push!(prod.args, expressify(es, context = context))
        end
        if isone(c)
           push!(sum.args, Expr(:call, :*, prod))
        else
           push!(sum.args, Expr(:call, :*, expressify(c, context = context), prod))
        end
      end
    end
  end
  return sum
end

@enable_all_show_via_expressify ModularFormsSL2ZCongruenceSymbolicRingElem

function needs_parentheses(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  if iszero(f)
    return needs_parentheses(zero(coefficient_ring(f)))
  else
    return true
  end
end

function isnegative(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  false
end

function show_minus_one(
    ::Type{ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}}
   ) where {RT,RB,R}
  true
end

################################################################################
# creation
################################################################################

function deepcopy_internal(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    stackdict::IdDict
   ) where {RT,RB,R}
  parent(f)(
    coefficient_ring(f),
    deepcopy_internal(term_trie(f), stackdict);
    check = false)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})() where {RT,RB,R}
  sring(muinf_ring(coefficient_ring_tower(sring),1); check = false)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    coeffring :: Ring;
    check :: Bool = true
   ) where {RT,RB,R}
  sring(coeffring, VectorTrie{ModularFormSymbolic,R}(); check)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    coeffring :: Ring,
    term_trie :: VectorTrie{ModularFormSymbolic,R};
    check :: Bool = true
   ) where {RT,RB,R}
  if check && !(coeffring in coefficient_ring_tower(sring))
    throw(DomainError(coeffring, "ring must lie in coeffring ring tower"))
  end
  if check && any(parent(c) != coeffring for (_,c) in term_trie)
    throw(DomainError(term_trie,
        "coefficients must lie in coefficient ring $coeffring"))
  end
  ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}(sring, coeffring, term_trie)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    coeffring :: Ring,
    terms :: Vector{Pair{Vector{ModularFormSymbolic},R}};
    check :: Bool = true
   ) where {RT,RB,R}
  sring(coeffring, VectorTrie(terms); check)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    coeffring :: Ring,
    terms :: Dict{Vector{ModularFormSymbolic},R};
    check :: Bool = true
   ) where {RT,RB,R}
  sring(coeffring, VectorTrie{ModularFormSymbolic,R}(terms); check)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    s::ModularFormSymbolic;
    check :: Bool = true
   ) where {RT,RB,R}
  # TODO: We need to modify the type parameter to allow for a finite family of
  # element types. Then we can use the base ring here.
  sring(muinf_ring(coefficient_ring_tower(sring), 1), s;
        check = false)
end

function (sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R})(
    coeffring::Ring,
    s::ModularFormSymbolic;
    check :: Bool = true
   ) where {RT,RB,R}
  if check
    if !(coeffring in coefficient_ring_tower(sring))
      throw(DomainError(coeffring, "coefficient ring tower must contain ring"))
    end
  end
  return sring(coeffring, [ModularFormSymbolic[s] => one(coeffring)];
               check = false)
end

function zero(sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}) where {RT,RB,R}
  sring()
end

function one(sring::ModularFormsSL2ZCongruenceSymbolicRing{RT,RB,R}) where {RT,RB,R}
  coeffring = muinf_ring(coefficient_ring_tower(sring),1)
  sring(coeffring, [ModularFormSymbolic[] => one(coeffring)])
end

################################################################################
# conversion
################################################################################

function map_coefficients(
    coefficient_map  :: Any,
    f                :: ModularFormsSL2ZCongruenceSymbolicRingElem{RTD,RBD,RD};
    parent           :: Union{Nothing,SR} = nothing,
    coefficient_ring :: Union{Nothing,Ring} = nothing,
    check            :: Bool = true
   ) where {
    RTD, RBD, RD, RTC, RBC, RC,
    SR <: Union{Nothing, ModularFormsSL2ZCongruenceSymbolicRing{RTC,RBC,RC}}
   }
  if isnothing(parent)
    parent = AbstractAlgebra.parent(a)
    value_type = RD
  else
    value_type = RC
  end
  if isnothing(coefficient_ring)
    coefficient_ring = ModularForms.coefficient_ring(f)
  end
  parent(coefficient_ring, map(coefficient_map, term_trie(f); value_type); check)
end

################################################################################
# arithmetic: auxiliary functions
################################################################################

function _unify_coefficient_ring(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    gs::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}...
   ) where {RT,RB,R}
  for g in gs
    if parent(f) != parent(g) 
      throw(DomainError((f,g), ("incompatible parents")))
    end
  end

  return muinf_ring_with_maps(
      coefficient_ring_tower(parent(f)),
      coefficient_ring(f),
      map(coefficient_ring, gs)...)
end

function _unify_coefficient_ring(
    c::Union{RB,R},
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  if !(parent(c) in coefficient_ring_tower(parent(f)))
    throw(DomainError((f,g), ("incompatible parents")))
  end
  
  return muinf_ring_with_maps(
      coefficient_ring_tower(parent(f)),
      coefficient_ring(f),
      parent(c))
end

function _unify_coefficient_ring(
    c::RootOfUnity,
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    gs::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}...
   ) where {RT,RB,R}
  coefftower = coefficient_ring_tower(parent(f))

  cvts_pre = _unify_coefficient_ring(f, gs...)
  if isnothing(cvts_pre)
    coeffring = coefficient_ring(f)
    if has_muinf_order(coefftower, coeffring, order(c))
      return nothing
    end
    (coeffring, cvtc) = muinf_extension(coefftower, coeffring, order(c))
    cvts = tuple((cvtc for _ in 1:length(gs)+1)...)
    return (coeffring, cvts)
  else
    (coeffring, cvts) = cvts_pre

    cvtc_pre = muinf_extension(coefftower, coeffring, order(c))
    if isnothing(cvtc_pre)
      return (coeffring, cvts)
    else
      (coeffring, cvtc) = cvtc_pre
      return (coeffring, map(cvt -> cvtc ∘ cvt, cvts))
    end
  end
end

################################################################################
# arithmetic
################################################################################

+(f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}) where {RT,RB,R} = f

function -(f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map(-, term_trie(f)); check = false)
end

function +(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  ucr = _unify_coefficient_ring(f,g)
  if isnothing(ucr)
    return parent(f)(coefficient_ring(f),
        mergewith!(
            (cf,cg) -> let
              cfg = addeq!(cf,cg)
              iszero(cfg) ? nothing : cfg
            end,
            deepcopy(term_trie(f)),
            term_trie(g));
        check = false)
  else
    (coeffring, (cvtf, cvtg)) = ucr
    return parent(f)(coeffring,
        mergewith!(
            (cf,cg) -> let
              cfg = addeq!(cf,cg)
              iszero(cfg) ? nothing : cfg
            end,
            map(cvtf, term_trie(f)),
            map(cvtg, term_trie(g)));
        check = false)
  end
end

function addeq!(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  ucr = _unify_coefficient_ring(f,g)
  if isnothing(ucr)
    mergewith!(
        (cf,cg) -> let
          cfg = addeq!(cf,cg)
          iszero(cfg) ? nothing : cfg
        end,
        term_trie(f),
        term_trie(g))
    return f
  else
    (f.coeffring, (cvtf, cvtg)) = ucr
    mergewith!(
        (cf,cg) -> let
          cfg = addeq!(cf,cg)
          iszero(cfg) ? nothing : cfg
        end,
        map!(cvtf, term_trie(f)),
        map(cvtg, term_trie(g)))
    return f
  end
end

function -(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  return addeq!(-g,f)
end

function *(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  ucr = _unify_coefficient_ring(f,g)
  if isnothing(ucr)
    return parent(f)(coefficient_ring(g),
        [vcat(esf,esg) => cf*cg
         for (esf,cf) in term_trie(f)
         for (esg,cg) in term_trie(g)];
        check = false)
  else
    (coeffring, (cvtf, cvtg)) = ucr
    return parent(f)(coeffring,
        [vcat(esf,esg) => cf*cg
         for (esf,cf) in map(cvtf, term_trie(f))
         for (esg,cg) in map(cvtg, term_trie(g))];
        check = false)
  end
end

# ad-hoc arithmetic

function *(
    c::Integer,
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map(cf -> c*cf, term_trie(f)); check = false)
end

# NOTE: We need to provide a separate method for fmpz to avoid ambiguity if the
# RB = fmpz.
function *(
    c::fmpz,
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map(cf -> c*cf, term_trie(f)); check = false)
end

function *(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::Integer
   ) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map(cf -> c*cf, term_trie(f)); check = false)
end

# NOTE: We need to provide a separate method for fmpz to avoid ambiguity if the
# RB = fmpz.
function *(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::fmpz
   ) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map(cf -> c*cf, term_trie(f)); check = false)
end

# NOTE: We need to specialize the signature in order to avoid ambiguity with
# general functions from AbstractAlgebra.
function *(
    c::Union{RB,R},
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {
    RT <: MuInfTower,
    RB <: RingElem,
    R <: RingElem
   }
  ucr = _unify_coefficient_ring(c,f)
  if isnothing(ucr)
    return parent(f)(coefficient_ring(f), map(cf -> c*cf, term_trie(f));
                     check = false)
  else
    (coeffring, (cvtf, cvtc)) = ucr
    c = cvtc(c)
    return parent(f)(coeffring, map(cf -> c*cvtf(cf), term_trie(f));
                     check = false)
  end
end

# NOTE: We need to specialize the signature in order to avoid ambiguity with
# general functions from AbstractAlgebra.
function *(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::Union{RB,R}
   ) where {
    RT <: MuInfTower,
    RB <: RingElem,
    R <: RingElem
   }
  ucr = _unify_coefficient_ring(c,f)
  if isnothing(ucr)
    return parent(f)(coefficient_ring(f), map(cf -> cf*c, term_trie(f));
                     check = false)
  else
    (coeffring, (cvtf, cvtc)) = ucr
    c = cvtc(c)
    return parent(f)(coeffring, map(cf -> cvtf(cf)*c, term_trie(f));
                     check = false)
  end
end

################################################################################
# actions
################################################################################

function act(
    c::TrivialGroupElem,
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  f
end

function act!(
    ::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::TrivialGroupElem,
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  g
end

function addacteq!(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    ::TrivialGroupElem,
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  addeq!(f,g)
end


function act(
    c::RootOfUnity,
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  ucr = _unify_coefficient_ring(c,f)
  coefftower = coefficient_ring_tower(parent(f))
  if isnothing(ucr)
    return parent(f)(coefficient_ring(f),
                     map(cf -> act(c,cf,coefftower), term_trie(f));
                     check = false)
  else
    (coeffring, (cvtf,)) = ucr
    return parent(f)(coeffring,
                     map(cf -> act(c,cvtf(cf),coefftower), term_trie(f));
                     check = false)
  end
end

function act!(
    ::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::RootOfUnity,
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  return act(c,g)
end

function addacteq!(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    c::RootOfUnity,
    g::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  ucr = _unify_coefficient_ring(c,f,g)
  coefftower = coefficient_ring_tower(parent(f))
  if isnothing(ucr)
    mergewith!(
        (cf,cg) -> let
          cfg = addeq!(cf,cg)
          iszero(cfg) ? nothing : cfg
        end,
        term_trie(f),
        map(cg -> act(c,cg,coefftower), term_trie(g)))
    return f
  else
    (f.coeffring, (cvtf,cvtg)) = ucr
    mergewith!(
        (cf,cg) -> let
          cfg = addeq!(cf,cg)
          iszero(cfg) ? nothing : cfg
        end,
        map!(cvtf, term_trie(f)),
        map(cg -> act(c,cvtg(cg),coefftower), term_trie(g)))
    return f
  end
end

################################################################################
# action of translation matrix [1 1; 0 1]
################################################################################

function translation_action(
    f::ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R}
   ) where {RT,RB,R}
  parent(f)(coefficient_ring(f), map_keys(translation_action, term_trie(f));
            check = false)
end

################################################################################
# Fourier expansion
################################################################################

function fourier_expansion(
    f   :: ModularFormsSL2ZCongruenceSymbolicRingElem{RT,RB,R},
    prc :: Union{Int,Rational{Int}};
    coefficient_ring                  :: Union{Nothing,Ring} = nothing,
    fourier_expansion_level           :: Union{Nothing,Int}  = nothing,
    fourier_expansion_cache           :: Union{Nothing,Dict} = nothing,
    fourier_expansion_cache_converted :: Union{Nothing,Dict} = nothing
   ) where {RT,RB,R}
  coefftower = coefficient_ring_tower(parent(f))

  if isnothing(coefficient_ring)
    coefficient_ring = ModularForms.fourier_coefficient_ring(f)
  end
  coeffconvert = map_from_cyclotomic_tower(coefftower,
                     ModularForms.coefficient_ring(f), coefficient_ring)

  if isnothing(fourier_expansion_level)
    fourier_expansion_level = level(f)
  end

  if isnothing(fourier_expansion_cache)
    fourier_expansion_cache =
        Dict{ModularFormSymbolic,PuiseuxSeriesElem}()
  end

  if isnothing(fourier_expansion_cache_converted)
    fourier_expansion_cache_converted =
        Dict{ModularFormSymbolic,PuiseuxSeriesElem}()
  end

  # We need to take a Fourier expansion ring with potentially higher level than
  # the final one to accommodate all factors of Eisenstein series.
  fering, _ = ModularFormSL2ZFourierExpansion_MODULE.
                  fourier_expansion_ring(coefficient_ring, prc;
                                         level = fourier_expansion_level)

  feexp(s) = _fourier_expansion(s, prc;
                  fourier_expansion_ring = fering,
                  coefficient_ring_tower = coefftower,
                  fourier_expansion_cache,     
                  fourier_expansion_cache_converted)

  map_reduce(term_trie(f),
    coeffconvert,
    # TODO: addeq! for PuiseuxSeriesElem seems still broken
    # s_fe_iter -> reduce(addeq!, imap(((s,fe),) -> feexp(s)*fe, s_fe_iter)),
    s_fe_iter -> reduce(+, imap(((s,fe),) -> feexp(s)*fe, s_fe_iter)),
    # ((c,fe),) -> addeq!(fe,c);
    +;
    init = O(gen(fering)^prc)) + O(gen(fering)^prc)
 # TODO: we don't need the last addition
end

################################################################################
# evaluation
################################################################################

function evaluate(
    f   :: ModularFormsSL2ZCongruenceSymbolicRingElem{QQMuInfTower,fmpq,nf_elem},
    tau :: acb;
    cc_muinf_tower   :: Union{Nothing,CCMuInfTower} = nothing,
    evaluation_cache :: Union{Nothing,Dict}         = nothing
   )
  CC = parent(tau)
  if isnothing(cc_muinf_tower)
    cc_muinf_tower = cyclotomic_tower(CC)
  elseif !(CC in cc_muinf_tower)
    throw(DomainError(cc_muinf_tower, "tower must contain field of tau"))
  end

  sring = ModularFormsSL2ZCongruenceSymbolicRing(cc_muinf_tower)

  qqab_emb = map_from_cyclotomic_tower(
      coefficient_ring_tower(parent(f)), coefficient_ring(f), QQab)

  return evaluate(
      map_coefficients(CC ∘ qqab_emb, f;
                       parent = sring, coefficient_ring = CC, check = false),
      tau;
      cc_muinf_tower,
      evaluation_cache)
end

function evaluate(
    f   :: ModularFormsSL2ZCongruenceSymbolicRingElem{CCMuInfTower,acb,acb},
    tau :: acb;
    cc_muinf_tower   :: Union{Nothing,CCMuInfTower} = nothing,
    evaluation_cache :: Union{Nothing,Dict}         = nothing
   )
  CC = parent(tau)
  if isnothing(cc_muinf_tower)
    cc_muinf_tower = cyclotomic_tower(CC)
  elseif !(CC in cc_muinf_tower)
    throw(DomainError(cc_muinf_tower, "tower must contain field of tau"))
  end

  if isnothing(evaluation_cache)
    evaluation_cache = Dict{ModularFormSymbolic, acb}()
  end

  eval(s) = _evaluate(s, tau; cc_muinf_tower, evaluation_cache)

  map_reduce(term_trie(f),
    identity,
    s_ev_iter -> reduce(addeq!, imap(((s,ev),) -> eval(s)*ev, s_ev_iter)),
    (c,ev) -> addeq!(ev, c);
    init = zero(CC))
end
