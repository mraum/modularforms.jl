# This is included in of ModularFormSL2ZCongruenceType_MODULE

################################################################################
################################################################################
# Iterators weights and levels associated with pairs of Eisenstein series whose
# tensor product maps to a given arithmetic type.
################################################################################
################################################################################

################################################################################
# The weight and level iterators are employed when iterating pairs of
# Eisenstein series. They are usually constructed via the function
#   eisenstein_factors_weight_level,
# which fixes the the pairs of weights that can occur and the levels lvl1 of
# the first factors. Possible levels of the second factor are determined during
# iteration for each given lvl1.
#
# If the flag single_factor_flag is provided as well, then single Eisenstein
# series of given weight and level are iterated through in the beginning.
################################################################################

struct EisensteinFactorsWeightLevelIterator
  k                     :: SL2RCoverWeight
  lvl                   :: Int
  lvl_fac               :: Nemo.Fac{fmpz}
  k12s                  :: Vector{NTuple{2,SL2RCoverWeight}}
  lvl1s
  single_factor_flag    :: Bool
  muinf_order_predicate :: Function
end

mutable struct EisensteinFactorsWeightLevelIteratorState
  lvl1val :: Int
  lvl2val :: Int
  lvl2iter
  lvl1
  lvl2
  k12

  function EisensteinFactorsWeightLevelIteratorState()
    new(0, 0, nothing, nothing, nothing, nothing)
  end
end

################################################################################
# creation
################################################################################

function eisenstein_factors_weight_level(
    k                     :: SL2RCoverWeight,
    lvl                   :: Int;
    single_factor_flag    :: Bool,
    level_bound           :: Union{Nothing,Int},
    muinf_order_predicate :: Function
    )
  kqq = Rational{Int}(k)

  if kqq < 2
    throw(DomainError(k, join("spanning sets for spaces of modular forms ",
                              "via Eisenstein series are only ",
                              "available for weight at least 2")))
  end

  # Weight k1 of the first factor in the product of Eisenstein series. The
  # contributions of various combinations of weights are associated with the
  # vanishing of specific coefficients of the period polynomial. Iterating
  # through all k1 and k2 seems inefficient.
  if isinteger(kqq)
    # This works per Raum, Raum-Xia.
    if k == SL2RCoverWeight(2)
      k12s = let k1 = SL2RCoverWeight(1)
               [(k1, k1)]
             end
    else
      k12s = let k1 = SL2RCoverWeight(1)
               [(k1, k ⊗ dual(k1)), (k ⊗ dual(k1), k1)]
             end
    end
  elseif isinteger(2*kqq)
    # This works per Williams
    k12s = let k1 = SL2RCoverWeight(1//2)
             [(k1, k ⊗ dual(k1)), (k ⊗ dual(k1), k1)]
           end
    error("Implement iteration correctly")
  else
    throw(DomainError(k, join("spanning sets for spaces of modular forms ",
                              "via Eisenstein series are only ",
                              "available for half-integral weights")))
  end

  if !isnothing(level_bound) && !divisible(level_bound, lvl)
    throw(DomainError(level_bound, join("bound for level must be divisible by level")))
  end

  if isnothing(level_bound)
    lvl1_iterator = Iterators.countfrom(lvl,lvl)
  else
    lvl1_iterator = (lvl*d for d in divisors(div(level_bound,lvl)))
  end

  EisensteinFactorsWeightLevelIterator(
    k, lvl, factor(ZZ(lvl)),
    k12s, lvl1_iterator,
    single_factor_flag,
    muinf_order_predicate
   )
end

################################################################################
# iteration
################################################################################

Base.IteratorSize(::Type{EisensteinFactorsWeightLevelIterator}) = Base.SizeUnknown()

function Base.eltype(::Type{EisensteinFactorsWeightLevelIterator})
  Union{
    Tuple{NTuple{1,SL2RCoverWeight}, NTuple{1,Int}},
    Tuple{NTuple{2,SL2RCoverWeight}, NTuple{2,Int}}
    }
end

function Base.iterate(
    iter :: EisensteinFactorsWeightLevelIterator
   )
  if iter.single_factor_flag
    return (((iter.k,), (iter.lvl,)), EisensteinFactorsWeightLevelIteratorState())
  else
    return iterate(iter, EisensteinFactorsWeightLevelIteratorState())
  end
end

function Base.iterate(
    iter  :: EisensteinFactorsWeightLevelIterator,
    state :: EisensteinFactorsWeightLevelIteratorState
   )
  # We use the out loop to avoid recursion (no tail-call optimization) while
  # enforcing predicates on the muinf order.
  while true
    if isnothing(state.lvl2)
      iterret = iterate(iter.lvl1s)
      if isnothing(iterret)
        return nothing
      else
        (state.lvl1val,state.lvl1) = iterret
      end

      iter.muinf_order_predicate(state.lvl1val) || continue

      # We construct the set of all possible lvl2 for Gamma1 Eisenstein series.
      # In order to have nontrivial homomorphisms from ρ1 ⊗ ρ2 to ρ at each prime
      # p we must have the following condition:
      # (1) If p^m divides lvl exactly, then p^m must either divide lvl1 or lvl2.
      # (2) If p^n divides lcm(lvl1,lvl2) exactly and n > m, then it must divide
      # both lvl1 and lvl2.

      lvl2base = 1
      lvl2s = Int[1]

      lvl1_fac = factor(ZZ(state.lvl1val))
      for (p,e) in iter.lvl_fac
        if p in lvl1_fac
          if lvl1_fac[p] == e
            # In this case lvl2 can be divisible by p^e and any of its divisors.
            p = Int(p)
            lvl2s_p = copy(lvl2s)
            for _ in 1:e
              lvl2s_p .*= p
              append!(lvl2s, lvl2s_p)
            end
          elseif lvl1_fac[p] < e
            # In this case lvl2 must be exactly divisible by p^e.
            lvl2base *= Int(p^e)
          else
            # In this case lvl2 must be exactly divisible by p^lvl1_fac[p]
            lvl2base *= Int(p^lvl1_fac[p])
          end
        else
          # In this case lvl2 must be exactly divisible by p^e.
          lvl2base *= Int(p^e)
        end
      end

      state.lvl2iter = lvl2base .* lvl2s
    end

    if isnothing(state.k12)
      iterret = isnothing(state.lvl2) ?
                    iterate(state.lvl2iter) : iterate(state.lvl2iter,state.lvl2)
      if isnothing(iterret)
        state.lvl2 = nothing
        continue
      else
        (state.lvl2val, state.lvl2) = iterret
      end

      iter.muinf_order_predicate(state.lvl2val) || continue
    end

    iterret = isnothing(state.k12) ?
                  iterate(iter.k12s) : iterate(iter.k12s, state.k12)
    if isnothing(iterret)
      state.k12 = nothing
      continue
    else
      ((k1,k2), state.k12) = iterret
    end

    return (((k1,k2), (state.lvl1val, state.lvl2val)), state)
  end
end

################################################################################
################################################################################
# Iterators of pairs of Eisenstein series for Gamma0 and Gamma1 together with
# their weight and type whose tensor product maps to a given arithmetic type.
################################################################################
################################################################################

################################################################################
# We leverage the iteration of pairs of weights and level. For Gamma0 we need
# to amend the iteration of Dirichlet characters, while for Gamma1 we can
# directly map to Eisenstein series.
#
# If the flag single_factor_flag is provided as well, then single Eisenstein
# series are also provided.
################################################################################

struct EisensteinFactorsGamma1Iterator
  klvls :: EisensteinFactorsWeightLevelIterator
end

const EisensteinFactorsGamma1IteratorState = EisensteinFactorsWeightLevelIteratorState

struct EisensteinFactorsGamma0Iterator
  klvls             :: EisensteinFactorsWeightLevelIterator
  muinf_order_bound :: Union{Nothing,Int}
end

mutable struct EisensteinFactorsGamma0IteratorState
  klvls    :: Union{Nothing,EisensteinFactorsWeightLevelIteratorState}
  ks       :: NTuple{N,SL2RCoverWeight} where N
  chis_set :: Set{NTuple{N,DirichletCharacter}} where N

  function EisensteinFactorsGamma0IteratorState()
    new(nothing, tuple(), Set{Tuple{}}())
  end
end

################################################################################
# creation
################################################################################

function eisenstein_factors(
    k                     :: SL2RCoverWeight,
    lvl                   :: Int;
    single_factor_flag    :: Bool = true,
    level_bound           :: Union{Nothing,Int} = nothing,
    muinf_order_bound     :: Union{Nothing,Int} = nothing,
    muinf_order_predicate :: Function = (order -> true),
    eisenstein_groups     :: Symbol = :gamma0
    )
    if eisenstein_groups === :gamma1
      return EisensteinFactorsGamma1Iterator(
                 eisenstein_factors_weight_level(
                     k, lvl;
                     single_factor_flag,
                     level_bound,
                     muinf_order_predicate
                     ))
    elseif eisenstein_groups === :gamma0
      return EisensteinFactorsGamma0Iterator(
                 eisenstein_factors_weight_level(
                     k, lvl;
                     single_factor_flag,
                     level_bound,
                     muinf_order_predicate
                     ),
                 muinf_order_bound)
    else
      throw(DomainError(eisenstein_groups, "kind of group not recognized"))
    end
end

################################################################################
# iteration
################################################################################

function Base.IteratorSize(
    ::Union{Type{EisensteinFactorsGamma1Iterator},
            Type{EisensteinFactorsGamma0Iterator}}
   )
  Base.SizeUnknown()
end

function Base.eltype(
    ::Union{Type{EisensteinFactorsGamma1Iterator},
            Type{EisensteinFactorsGamma0Iterator}}
   )
  Union{
    NTuple{1,Tuple{Vector,SL2RCoverWeight,ArithType}},
    NTuple{2,Tuple{Vector,SL2RCoverWeight,ArithType}}
    }
end

function Base.iterate(
    iter::EisensteinFactorsGamma1Iterator,
    state :: Union{Nothing,EisensteinFactorsGamma1IteratorState} = nothing
   )
  if isnothing(state)
    klvls_pre = iterate(iter.klvls)
  else
    klvls_pre = iterate(iter.klvls, state)
  end
  if isnothing(klvls_pre)
    return nothing
  end
  ((ks,lvls), state) = klvls_pre
  return (zip((let
                 es, rho = eisenstein_gamma1_type(Int(k), lvl)
                 (es, k, rho)
               end
               for (k,lvl) in zip(ks,lvls))...),
          state)
end

function Base.iterate(
    iter::EisensteinFactorsGamma0Iterator
    )
  iterate(iter, EisensteinFactorsGamma0IteratorState())
end

function Base.iterate(
    iter::EisensteinFactorsGamma0Iterator,
    state :: EisensteinFactorsGamma0IteratorState
   )
  while true
    if isempty(state.chis_set)
      if isnothing(state.klvls)
        klvls_pre = iterate(iter.klvls)
      else
        klvls_pre = iterate(iter.klvls, state.klvls)
      end
      if isnothing(klvls_pre)
        return nothing
      end
      ((ks,lvls), state.klvls) = klvls_pre
      state.ks = ks

      state.chis_set = Set(Iterators.product(
                           (collect(DirichletGroup(lvl)) for lvl in lvls)...))
    end

    chis = pop!(state.chis_set)
    if !isnothing(iter.muinf_order_bound) &&
           any(!divisible(iter.muinf_order_bound, order(chi)) for chi in chis)
      continue
    end

    return (zip((let
                   es, rho = eisenstein_gamma0_type(Int(k), chi)
                   (es, k, rho)
                 end
                 for (k,chi) in zip(state.ks,chis))...),
            state)
  end
end

################################################################################
################################################################################
# Iterator through the images of a basis of homomorphisms from products of
# symbolic Eisenstein series to an arithmetic type.
################################################################################
################################################################################

################################################################################
# Iteration through Eisenstein series, the preimages, is performed via one of
# the EisensteinFactors iterators. No assumption the number of factors is made.
# This iterator creates the associated homspaces and applys their basis
# elements.
################################################################################

mutable struct SymbolicProductHomomorphismIterator
  arithtype            :: ArithType
  arithtype_components :: Vector{Int}
  symbolic_ring        :: ModularFormsSL2ZCongruenceSymbolicRing
  symbolic_factors     :: Any
end

mutable struct SymbolicProductHomomorphismIteratorState
  symbolic_factors_state :: Any
  homomorphisms          :: Union{Nothing,Vector}
  homomorphisms_state    :: Union{Nothing,Any}
  symbprod               :: Vector{ModularFormsSL2ZCongruenceSymbolicRingElem}

  function SymbolicProductHomomorphismIteratorState()
    new(nothing, nothing, nothing, [])
  end
end

################################################################################
# creation
################################################################################

# Iterator through products of a single Eisenstein series, effectively
# computing Eisenseries in the image.
function symbolic_modular_forms_from_single_eisenstein_and_homomorphisms(
    k                        :: SL2RCoverWeight,
    rho                      :: ArithType,
    symbolic_ring            :: ModularFormsSL2ZCongruenceSymbolicRing;
    eisenstein_groups        :: Symbol = :gamma0,
    return_translation_orbit_representatives :: Bool = true
   )
  coefftower = coefficient_ring_tower(symbolic_ring)

  lvl = level(rho)
  if isnothing(lvl)
    throw(DomainError(rho, join("spanning sets for spaces of modular forms ",
                                "via Eisenstein series are only ",
                                "available for congruence types")))
  end

  if eisenstein_groups === :gamma1
    es, sigma = eisenstein_gamma1_type(Int(k), lvl)
    es_k_sigmas = [((es,), (k,), (sigma,))]
  elseif eisenstein_groups === :gamma0
    es_k_sigmas = [let
                   es, sigma = eisenstein_gamma0_type(Int(k), chi)
                   ((es,), (k,), (sigma,))
                 end
                 for chi in DirichletGroup(lvl)]
  else
    throw(DomainError(eisenstein_groups, "kind of group not recognized"))
  end

  SymbolicProductHomomorphismIterator(
      rho,
      return_translation_orbit_representatives ?
          representatives(translation_decomposition(rho)) :
          collect(1:dim(rho)),
      symbolic_ring,
      es_k_sigmas
      )
end

# Iterator through products of up to two Eisenstein series. If the
# single_factor_flag is true, products with one factor, i.e. Eisenstein series,
# are also iterated through.
function symbolic_modular_forms_from_eisenstein_products_and_homomorphisms(
    k                        :: SL2RCoverWeight,
    rho                      :: ArithType,
    symbolic_ring            :: ModularFormsSL2ZCongruenceSymbolicRing;
    single_factor_flag       :: Bool = true,
    eisenstein_groups        :: Symbol = :gamma0,
    level_bound              :: Union{Nothing,Int} = nothing,
    return_translation_orbit_representatives :: Bool = true
   )
  coefftower = coefficient_ring_tower(symbolic_ring)

  lvl = level(rho)
  if isnothing(lvl)
    throw(DomainError(rho, join("spanning sets for spaces of modular forms ",
                                "via Eisenstein series are only ",
                                "available for congruence types")))
  end

  SymbolicProductHomomorphismIterator(
      rho,
      return_translation_orbit_representatives ?
          representatives(translation_decomposition(rho)) :
          collect(1:dim(rho)),
      symbolic_ring,
      eisenstein_factors(
          k, lvl;
          single_factor_flag,
          eisenstein_groups,
          level_bound,
          muinf_order_predicate = (order -> has_muinf_ring(coefftower, order))
         ))
end

################################################################################
# creation
################################################################################

Base.IteratorSize(::Type{SymbolicProductHomomorphismIterator}) = Base.SizeUnknown()

function Base.eltype(::Type{SymbolicProductHomomorphismIterator})
  Vector{ModularFormsSL2ZCongruenceSymbolicRingElem}
end

function Base.iterate(iter::SymbolicProductHomomorphismIterator)
  iterate(iter, SymbolicProductHomomorphismIteratorState())
end

function Base.iterate(
    iter  :: SymbolicProductHomomorphismIterator,
    state :: SymbolicProductHomomorphismIteratorState
   )
  while true
    if isnothing(state.symbolic_factors_state) || isnothing(state.homomorphisms_state)
      if isnothing(state.symbolic_factors_state)
        symbfactors_pre = iterate(iter.symbolic_factors)
      else
        symbfactors_pre = iterate(iter.symbolic_factors, state.symbolic_factors_state)
      end

      if isnothing(symbfactors_pre)
        return nothing
      end
      ((symbfactors, _, sigmafactors), state.symbolic_factors_state) = symbfactors_pre
      symbfactors = map(f -> map(iter.symbolic_ring, f), symbfactors)

# FIXME: slight source of slowness
      symbprod = reduce(kronecker_product, symbfactors)
      state.symbprod = symbprod
  
      sigma = reduce(⊗, sigmafactors)
# FIXME: slight source of slowness
      if !ModularForms._issl2z(group(iter.arithtype))
        sigma = restriction(group(iter.arithtype), sigma)
      end
      state.homomorphisms = basis(homspace(sigma, iter.arithtype))
  
      hompre = iterate(state.homomorphisms)
    else
      symbprod = state.symbprod
      hompre = iterate(state.homomorphisms, state.homomorphisms_state)
    end
  
    if isnothing(hompre)
      state.homomorphisms_state = nothing
      continue
    end
    (hom, state.homomorphisms_state) = hompre
  
# FIXME: absolutely dominant source slowness
    svec = hom(symbprod)
    return (svec[iter.arithtype_components], state)
  end
end
