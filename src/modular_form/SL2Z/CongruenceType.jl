################################################################################
################################################################################
# type functions
################################################################################
################################################################################

function parent_type(
    ::Type{ModularFormSL2ZCongruenceType{I,R}}
   ) where {I,R}
  ModularFormsSpaceSL2ZCongruenceType{I,R}
end

function parent(a::ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  a.parent
end

function elem_type(
    ::ModularFormsSpaceSL2ZCongruenceType{I,R}
   ) where {I,R}
  ModularFormSL2ZCongruenceType{I,R}
end

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

function base_ring_tower(mf::ModularFormsSpaceSL2ZCongruenceType)
  mf.coefftower
end

function base_ring(mf::ModularFormsSpaceSL2ZCongruenceType)
  mf.coeffring
end

function symbolic_ring(mf::ModularFormsSpaceSL2ZCongruenceType)
  ModularFormsSL2ZCongruenceSymbolicRing(base_ring_tower(mf))
end

function fourier_expansion_ring(
    mfs :: ModularFormsSpaceSL2ZCongruenceType{I,R},
    prc :: Union{Int,Rational{Int}}
   ) where {I,R}
  ModularForms.ModularFormSL2ZFourierExpansion_MODULE.
      fourier_expansion_ring(base_ring(mfs), prc; level=level(arithtype(mfs)))
end

@doc Markdown.doc"""
    weight(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}

Return the weight of the modular forms space.
"""
weight(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R} = mfs.weight

@doc Markdown.doc"""
    arithtype(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}

Return the arithmetic type of the modular forms space.
"""
arithtype(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R} = mfs.arithtype

function isscalar_valued(
    mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}
   ) where {I,R}
  mfs.scalar_valued
end

@doc Markdown.doc"""
    basis(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}

Return the basis of the modular forms space.
"""
basis(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R} = mfs.basis

@doc Markdown.doc"""
    dim(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}

Return the dimension of the modular forms space.
"""
function dim(mfs::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I,R}
  if ismissing(basis(mfs))
    error("not implmented")
  else
    return length(basis(mfs))
  end
end

@doc Markdown.doc"""
    sturm_bound(ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I, R}

Return Sturm bound for the modular forms space.
"""
function sturm_bound(
    mfs :: ModularFormsSpaceSL2ZCongruenceType{I,R}
   ) where {I, R}
  sturm_bound(weight(mfs), arithtype(mfs), base_ring(mfs))
end

@doc Markdown.doc"""
    sturm_bound(k::SL2RCoverWeight, gp::SL2ZGroup, coeffring::R = QQab) where {R <: Ring}

Return Sturm bound of given combination of weight, group and coefficient ring.
"""
function sturm_bound(
    k            :: SL2RCoverWeight,
    gp           :: SL2ZGroup,
    coeffambient :: R = QQab
   ) where {R <: Union{Ring,MuInfTower}}
  # NOTE: we cannot simply redirect to sturm_bound(k, gp,
  # base_ring(coefftower)), since an optimal Sturm bound for the base ring
  # might be less than the one of the limit ring. But the limit ring is not
  # defined in all cases. So we depend here on the implementation detail that
  # the Sturm bound are in fact the ones for QQab and Fpbar.
  if R <: MuInfTower
    coeffambient = base_ring(coeffambient)
  end

  if characteristic(coeffambient) > 0 && Int(k) < 2
    error("not implemented")
  end

  k = _issl2z(gp) ? Int(k) : index(SL2Z,gp) * Int(k)
  # NOTE: Precisions are given by strict upper bound on the exponents, but the
  # correct vanishing bounds are not strict. Adding any positive number to k
  # will yield correct bounds.
  return (k+1)//12
end

@doc Markdown.doc"""
    sturm_bound(k::SL2RCoverWeight, rho::ArithType{G,I}, ::R) where {G, I <: Union{TrivialGroupElem,RootOfUnity, TwistedPerm,WeilTransformation}, R <: Union{FlintIntegerRing,FlintRationalField, AnticNumberField,QQabField, AcbField,FqNmodFiniteField}}

Return the Sturm bound for given weight, arithmetic type and ring.
"""
function sturm_bound(
    k            :: SL2RCoverWeight,
    rho          :: ArithType{G,I},
    coeffambient :: R = QQab
   ) where {
    G,
    I <: Union{TrivialGroupElem,RootOfUnity,
               TwistedPerm,WeilTransformation},
    R <: Union{Ring, MuInfTower}
   }
  # The arithmetic type factors through a finite quotient. Therefore, we can
  # employ the Sturm bound for the associated group, if representation decends
  # to the coefficint ring.
  if (  characteristic(coeffambient) > 0 && I == WeilTransformation
     && isdivisible_by(level(rho), characteristic(coeffambient)) )
    error("not implemented")
  end
  sturm_bound(k, group(rho), coeffambient)
end

# TODO: is there a better name for this?
function isfull_space(
    mfs::ModularFormsSpaceSL2ZCongruenceType{I, R}
   ) where {I, R}
  ismissing(mfs.basis) && return true
  if isone(arithtype(mfs)) || base_ring(mfs) == QQab
    mfspace_dim = ModularFormSL2ZCongruenceType_MODULE.mfspace_dim
    return dim(mfs) == mfspace_dim(weight(mfs), arithtype(mfs))
  else
    error("not implemented")
  end
end

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    ModularFormsSpaceSL2ZCongruenceType(weight::SL2RCoverWeight, rho::ArithType{SL2ZElem,I}, coefftower::MuInfTower) where I

Return the modular forms space $\mathcal{M}_k(\rho; R)$ where $R$ is the
coefficient tower.
"""
function ModularFormsSpaceSL2ZCongruenceType(
    weight        :: SL2RCoverWeight,
    rho           :: ArithType{G,I},
    coeffring     :: Ring,
    coefftower    :: MuInfTower
   ) where {
    G <: SL2ZElement, I
   }
  if !(coeffring in coefftower)
    throw(DomainError((coeffring,coefftower), "ring not contaiend in tower"))
  end
  ModularFormsSpaceSL2ZCongruenceType{I,elem_type(coeffring)}(
      weight, rho, false, coeffring, coefftower, missing)
end

function ModularFormsSpaceSL2ZCongruenceType(
    weight        :: SL2RCoverWeight,
    rho           :: ArithType{G,I},
    scalar_valued :: Bool,
    coeffring     :: Ring,
    coefftower    :: MuInfTower
   ) where {
    G <: SL2ZElement, I
   }
  if !(coeffring in coefftower)
    throw(DomainError((coeffring,coefftower), "ring not contaiend in tower"))
  end
  if scalar_valued && dim(rho) != 1
    throw(DomainError(rho, "scalar-valued modular forms must have one-dimensional type"))
  end
  ModularFormsSpaceSL2ZCongruenceType{I,elem_type(coeffring)}(
      weight, rho, scalar_valued, coeffring, coefftower, missing)
end

function setscalar_valued(
    mfs :: ModularFormsSpaceSL2ZCongruenceType{I,R},
    scalar_valued :: Bool
   ) where {I,R}
  if scalar_valued && dim(arithtype(mfs)) != 1
    throw(DomainError(rho, "scalar-valued modular forms must have one-dimensional type"))
  end
  mfs_new = ModularFormsSpaceSL2ZCongruenceType(
                weight(mfs), arithtype(mfs), scalar_valued,
                base_ring(mfs), base_ring_tower(mfs))
  if !ismissing(mfs.basis)
    mfs_new.basis = [_change_parent(f, mfs_new) for f in mfs.basis]
  end
  return mfs_new
end

################################################################################
# creation: Eisenstein space
################################################################################

function EisensteinSeriesSpace(
    k                      :: Int,
    gp                     :: SL2ZGroup = SL2Z;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(SL2RCoverWeight(k), gp; cached)
end

function EisensteinSeriesSpace(
    k                      :: SL2RCoverWeight,
    gp                     :: SL2ZGroup = SL2Z;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(k, TrivialArithmeticType(gp); scalar_valued = true, cached)
end

function EisensteinSeriesSpace(
    k             :: Int,
    rho           :: ArithType{G,I};
    scalar_valued :: Bool = false,
    cached        :: Bool = true
   ) where {
    G <: SL2ZElement, I
   }
  return EisensteinSeriesSpace(SL2RCoverWeight(k), rho; scalar_valued, cached)
end

@doc Markdown.doc"""
    EisensteinSeriesSpace(k::SL2RCoverWeight, rho::ArithType{SL2ZSubgroupElem,I}; cached = true) where I

Return the space of Eisenstein series of weight $k$ and arithmetic type $\rho$.
"""
function EisensteinSeriesSpace(
    k             :: SL2RCoverWeight,
    rho           :: ArithType{G,I};
    scalar_valued :: Bool = false,
    cached        :: Bool = true
   ) where {
    G <: SL2ZElement, I
   }
  return EisensteinSeriesSpace(k, rho, QQab; scalar_valued, cached)
end

function EisensteinSeriesSpace(
    k                      :: Int,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(SL2RCoverWeight(k), SL2Z,
      coefficient_ring, coefficient_ring_tower; cached)
end

function EisensteinSeriesSpace(
    k                      :: SL2RCoverWeight,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(k, SL2Z, coefficient_ring, coefficient_ring_tower; cached)
end

function EisensteinSeriesSpace(
    k                      :: Int,
    gp                     :: SL2ZGroup,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(SL2RCoverWeight(k), gp,
      coefficient_ring, coefficient_ring_tower; cached)
end

function EisensteinSeriesSpace(
    k                      :: SL2RCoverWeight,
    gp                     :: SL2ZGroup,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  EisensteinSeriesSpace(k, TrivialArithmeticType(gp),
      coefficient_ring, coefficient_ring_tower; scalar_valued = true, cached)
end

function EisensteinSeriesSpace(
    k                      :: Int,
    rho                    :: ArithType{G,I},
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    scalar_valued          :: Bool = false,
    cached                 :: Bool = true
   ) where {
    G <: SL2ZElement, I, R <: Ring
   }
  EisensteinSeriesSpace(SL2RCoverWeight(k), rho,
      coefficient_ring, coefficient_ring_tower; scalar_valued, cached)
end

@doc Markdown.doc"""
    EisensteinSeriesSpace(k::SL2RCoverWeight, rho::ArithType{SL2ZSubgroupElem,I}, coeffring::R; cached = true) where {I, R <: Ring}

Return the space of Eisenstein series over of weight $k$ and arithmetic type
$\rho$ whose coefficients lay in the given ring.
"""
function EisensteinSeriesSpace(
    k                      :: SL2RCoverWeight,
    rho                    :: ArithType{G,I},
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    scalar_valued          :: Bool = false,
    cached                 :: Bool = true
   ) where {
    G <: SL2ZElement, I, R <: Ring
   }
  MF_MODULE = ModularFormSL2ZCongruenceType_MODULE
  MFE_MODULE = ModularFormSL2ZEisensteinCongruence_MODULE

  if !isintegral(k)
    throw(DomainError(gp, "modular forms only implemented for integral weights"))
  end

  # FIXME: check this
  # if !iscongruence(rho)
  #   throw(DomainError(gp, "modular forms only implemented for congruence types"))
  # end


  if scalar_valued && dim(rho) != 1
    throw(DomainError(rho, "scalar-valued modular forms must have one-dimensional type"))
  end

  if ismissing(coefficient_ring_tower)
    coefficient_ring_tower = cyclotomic_tower(coefficient_ring)
  end


  if cached && haskey(MF_MODULE.eisenstein_space_cache,
                      (k, rho, coefficient_ring, coefficient_ring_tower))
    mfs = MF_MODULE.eisenstein_space_cache[
              k, rho, coefficient_ring, coefficient_ring_tower]
    if !scalar_valued
      return mfs
    else
      return setscalar_valued(mfs, true)
    end
  end


  mfs = ModularFormsSpaceSL2ZCongruenceType(
            k, rho, coefficient_ring, coefficient_ring_tower)


  # Since the group is an SL2Z group, modular forms must have integral weight.
  if !isintegral(k)
    mfs.basis = []

  # We use a special basis for modular forms of level 1
  elseif isone(rho) && _issl2z(group(rho))

    if characteristic(coefficient_ring) != 0
      error("not implemented")
    end

    if Int(k) <= 2 || isodd(Int(k))
      mfs.basis = []
    else
      symbring = symbolic_ring(mfs)
      mf = ModularFormSL2ZCongruenceType(mfs)
      mf.symbolic = [symbring(MFE_MODULE.EisensteinSL2Z(Int(k)))]
      mfs.basis = [mf]
    end

  else # isintegral(k) && (!isone(rho) || !_issl2z(group(rho)))

    if !has_muinf_order(coefficient_ring_tower, coefficient_ring, 
                        MF_MODULE.fourier_coefficient_muinf_order(k, rho))
      throw(DomainError((coefficient_ring,
                         MF_MODULE.fourier_coefficient_muinf_order(k, rho)),
                        join("coefficient ring must contain required roots of unity")))
    end

    symbfevecs = MF_MODULE._eisenstein_with_fourier_expansion(
                   k, rho, coefficient_ring, coefficient_ring_tower)
    mfs.basis = []
    for (svec,fevec) in symbfevecs
      mf = ModularFormSL2ZCongruenceType(mfs)
      mf.symbolic = svec
      mf.fourier_expansion = fevec
      push!(mfs.basis, mf)
    end
  end

  if cached
    MF_MODULE.eisenstein_space_cache[
        k, rho, coefficient_ring, coefficient_ring_tower] = mfs
  end

  if scalar_valued
    return setscalar_valued(mfs, true)
  else
    return mfs
  end
end

################################################################################
# creation: Modular forms space
################################################################################

@doc Markdown.doc"""
    ModularFormsSpace(k::Union{Int, SL2RCoverWeight}, gp::SL2ZGroup; cached::Bool = true)

Return modular forms space $\mathcal{M}_k(\Gamma)$.
"""
function ModularFormsSpace(
    k      :: Union{Int, SL2RCoverWeight},
    gp     :: SL2ZGroup;
    cached :: Bool = true
   )
  error("documentation prototype must be overwritten")
end

@doc Markdown.doc"""
    ModularFormsSpace(k::Int; cached::Bool=true)

Return modular forms space $\mathcal{M}_k(\Gamma)$, where $\Gamma =
\mathrm{SL}_2(\mathbb{Z})$ by default.
"""
function ModularFormsSpace(
    k      :: Int,
    gp     :: SL2ZGroup = SL2Z;
    cached :: Bool = true
   )
  ModularFormsSpace(SL2RCoverWeight(k), gp; cached)
end

function ModularFormsSpace(
    k      :: SL2RCoverWeight,
    gp     :: SL2ZGroup = SL2Z;
    cached :: Bool = true
   )
  ModularFormsSpace(k, TrivialArithmeticType(gp); scalar_valued = true, cached)
end

function ModularFormsSpace(
    k             :: Int,
    rho           :: ArithType{G,I};
    scalar_valued :: Bool = false,
    cached        :: Bool = true
   ) where {
    G <: SL2ZElement, I
   }
  return ModularFormsSpace(SL2RCoverWeight(k), rho; scalar_valued, cached)
end

@doc Markdown.doc"""
    ModularFormsSpace(k::SL2RCoverWeight, rho::ModularForms.ArithType{G,I}; cached = true) where {G <: SL2ZElement, I}

Return the modular forms space $\mathcal{M}_k(\rho)$ where $\rho$ is an
arithmetic type defined over $\Gamma \subseteq \mathrm{SL}_2(\mathbb{Z})$.
"""
function ModularFormsSpace(
    k             :: SL2RCoverWeight,
    rho           :: ArithType{G,I};
    scalar_valued :: Bool = false,
    cached        :: Bool = true
   ) where {
    G <: SL2ZElement, I
   }
  return ModularFormsSpace(k, rho, QQab; scalar_valued, cached)
end

function ModularFormsSpace(
    k                      :: Int,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  ModularFormsSpace(SL2RCoverWeight(k), SL2Z,
      coefficient_ring, coefficient_ring_tower; cached)
end

function ModularFormsSpace(
    k                      :: SL2RCoverWeight,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  ModularFormsSpace(k, SL2Z, coefficient_ring, coefficient_ring_tower; cached)
end

function ModularFormsSpace(
    k                      :: Int,
    gp                     :: SL2ZGroup,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  ModularFormsSpace(SL2RCoverWeight(k), gp,
      coefficient_ring, coefficient_ring_tower; cached)
end

function ModularFormsSpace(
    k                      :: SL2RCoverWeight,
    gp                     :: SL2ZGroup,
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    cached                 :: Bool = true
   )
  ModularFormsSpace(k, TrivialArithmeticType(gp),
      coefficient_ring, coefficient_ring_tower; scalar_valued = true, cached)
end

function ModularFormsSpace(
    k                      :: Int,
    rho                    :: ArithType{G,I},
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    scalar_valued          :: Bool = false,
    cached                 :: Bool = true
   ) where {
    G <: SL2ZElement, I, R <: Ring
   }
  ModularFormsSpace(SL2RCoverWeight(k), rho,
      coefficient_ring, coefficient_ring_tower; scalar_valued, cached)
end

@doc Markdown.doc"""
    ModularFormsSpace(k::SL2RCoverWeight, rho::ArithType{G,I}, coefficients::Union{Ring,ModularForms.MuInfTower}; cached = true) where {G <: SL2ZElement, I}

Return the modular forms space $\mathcal{M}_k(\rho; R)$ where $\rho$ is an
arithmetic type defined over $\Gamma \subseteq \mathrm{SL}_2(\mathbb{Z})$ and
$R$ is the coefficient ring.
"""
function ModularFormsSpace(
    k                      :: SL2RCoverWeight,
    rho                    :: ArithType{G,I},
    coefficient_ring       :: Ring,
    coefficient_ring_tower :: Union{Missing,MuInfTower} = missing;
    scalar_valued          :: Bool = false,
    cached                 :: Bool = true
   ) where {
    G <: SL2ZElement, I, R <: Ring
   }
  MF_MODULE = ModularFormSL2ZCongruenceType_MODULE
  MFE_MODULE = ModularFormSL2ZEisensteinCongruence_MODULE
  MFC_MODULE = ModularFormSL2ZCuspFormCongruence_MODULE


  # FIXME: check this
  # if !iscongruence(rho)
  #   throw(DomainError(gp, "modular forms only implemented for congruence types"))
  # end


  if scalar_valued && dim(rho) != 1
    throw(DomainError(rho, "scalar-valued modular forms must have one-dimensional type"))
  end

  if ismissing(coefficient_ring_tower)
    coefficient_ring_tower = cyclotomic_tower(coefficient_ring)
  end

  if cached && haskey(MF_MODULE.modular_forms_space_cache,
                      (k, rho, coefficient_ring, coefficient_ring_tower))
    mfs = MF_MODULE.modular_forms_space_cache[
              k, rho, coefficient_ring, coefficient_ring_tower]
    if !scalar_valued
      return mfs
    else
      return setscalar_valued(mfs, true)
    end
  end


  mfs = ModularFormsSpaceSL2ZCongruenceType(
            k, rho, coefficient_ring, coefficient_ring_tower)


  # Since the group is an SL2Z group, modular forms must have integral weight.
  if !isintegral(k)
    mfs.basis = []

  # We use a special basis for modular forms of level 1
  elseif isone(rho) && _issl2z(group(rho))

    if characteristic(coefficient_ring) != 0 && characteristic(coefficient_ring) <= 3
      error("not implemented")
    end

    symbring = symbolic_ring(mfs)
    mfs.basis = []
    if !isintegral(k) || Int(k) < 0 || isodd(Int(k))
    elseif Int(k) == 0
      mf = ModularFormSL2ZCongruenceType(mfs)
      mf.symbolic = [one(symbring)]
      push!(mfs.basis, mf)
    else
      e4 = symbring(MFE_MODULE.EisensteinSL2Z(4))
      e6 = symbring(MFE_MODULE.EisensteinSL2Z(6))
      delta = symbring(MFC_MODULE.RamanujanDeltaSL2Z())
      km12 = Int(k) % 12
      if km12 == 0
        basemf = one(symbring)
      elseif km12 == 2
        basemf = e4^2*e6
      elseif km12 == 4
        basemf = e4
      elseif km12 == 6
        basemf = e6
      elseif km12 == 8
        basemf = e4^2
      elseif km12 == 10
        basemf = e4*e6
      end

      d = MF_MODULE.mfspace_dim(k, rho)
      for i in 0:d-1
        mf = ModularFormSL2ZCongruenceType(mfs)
        mf.symbolic = [basemf*e6^(2*(d-1-i))*delta^i]
        push!(mfs.basis, mf)
      end
    end

  else # isintegral(k) && (!isone(rho) || !_issl2z(group(rho)))

    if !has_muinf_order(coefficient_ring_tower, coefficient_ring, 
                        MF_MODULE.fourier_coefficient_muinf_order(k, rho))
      throw(DomainError((coefficient_ring,
                         MF_MODULE.fourier_coefficient_muinf_order(k, rho)),
                        join("coefficient ring must contain required roots of unity")))
    end

    symbfevecs = MF_MODULE._modular_forms_with_fourier_expansion_via_products(
                   k, rho, coefficient_ring, coefficient_ring_tower)
    mfs.basis = []
    for (svec,fevec) in symbfevecs
      mf = ModularFormSL2ZCongruenceType(mfs)
      mf.symbolic = svec
      mf.fourier_expansion = fevec
      push!(mfs.basis, mf)
    end
  end


  if cached
    ModularFormSL2ZCongruenceType_MODULE.
        modular_forms_space_cache[k, rho, coefficient_ring, coefficient_ring_tower] = mfs
  end

  if scalar_valued
    return setscalar_valued(mfs, true)
  else
    return mfs
  end
end

################################################################################
# comparison
################################################################################

function ==(
    mfs1::ModularFormsSpaceSL2ZCongruenceType{I, R},
    mfs2::ModularFormsSpaceSL2ZCongruenceType{I, R}
   ) where {I, R}
  base_ring(mfs1) == base_ring(mfs2) || return false
  weight(mfs1) == weight(mfs2) || return false
  arithtype(mfs1) == arithtype(mfs2) || return false
  isscalar_valued(mfs1) == isscalar_valued(mfs2) || return false

  if isfull_space(mfs1) && isfull_space(mfs2)
    return true
  else
    return hnf(_fourier_expansion_matrix(mfs1)) == hnf(_fourier_expansion_matrix(mfs2))
  end
end

################################################################################
# show
################################################################################

function show(
    io::IO,
    mfs :: ModularFormsSpaceSL2ZCongruenceType{I,R}
   ) where {I,R}
  if isfull_space(mfs)
    space = "Full space"
  else
    space = "Space"
  end
  if isone(arithtype(mfs))
    print(io, "$(space) of modular forms of weight $(weight(mfs)) " *
              "for $(group(arithtype(mfs)))")
  else
    print(io, "$(space) of modular forms of weight $(weight(mfs)) " *
              "of type $(arithtype(mfs))")
  end
end

################################################################################
# arithmetic
################################################################################

function *(
  fs::ModularFormsSpaceSL2ZCongruenceType{TrivialGroupElem,R},
  gs::ModularFormsSpaceSL2ZCongruenceType{TrivialGroupElem,R}
  ) where R
  if !isscalar_valued(fs) && !isscalar_valued(gs)
    throw(DomainError((fs,gs),
                      "multiplication is defined only for " *
                      "scalar-valued modular forms " *
                      "(use tensor products instead)"))
  end
  if base_ring_tower(fs) != base_ring_tower(gs)
    throw(DomainError((base_ring_tower(fs), base_ring_tower(gs)),
                      "incompatible coefficient ring towers"))
  end
  if base_ring(fs) != base_ring(gs)
    throw(DomainError((base_ring(fs), base_ring(gs)),
                      "incompatible base rings"))
  end
  ModularFormsSpaceSL2ZCongruenceType(
      weight(fs) ⊗ weight(gs),
      arithtype(fs) ⊗ arithtype(gs),
      isscalar_valued(fs) && isscalar_valued(gs),
      base_ring(fs),
      base_ring_tower(fs))
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    ModularFormSL2ZCongruenceType(parent::ModularFormsSpaceSL2ZCongruenceType{I,R}) where {I, R}

Return a modular form given its parent.
"""
function ModularFormSL2ZCongruenceType(
    parent :: ModularFormsSpaceSL2ZCongruenceType{I,R}
   ) where {I, R}
  ModularFormSL2ZCongruenceType{I,R}(parent, missing, missing)
end

function _change_parent(
    f::ModularFormSL2ZCongruenceType{R,I},
    mfs::ModularFormsSpaceSL2ZCongruenceType{R,I}
   ) where {R,I}
  g = ModularFormSL2ZCongruenceType(mfs)
  g.symbolic = f.symbolic
  g.fourier_expansion = f.fourier_expansion
  return g
end

@doc Markdown.doc"""
    eisenstein_series(k::Int; coeffring = missing)

Return $E_k$ defined on $\mathrm{SL}_2(\mathbb{Z})$, with the coefficient ring as an
optional argument whose fallback is $\mathbb{Q}$.
"""
function eisenstein_series(k::Int; coeffring = QQ)
  mfs = ModularFormsSpace(k, SL2Z, coeffring)
  mf = ModularFormSL2ZCongruenceType(mfs)
  mf.symbolic = [ symbolic_ring(mfs)(
                      ModularFormSL2ZEisensteinCongruence_MODULE.EisensteinSL2Z(k))
                ]
  return mf
end

@doc Markdown.doc"""
    ramanujan_delta(coeffring = missing)

Return the Ramanujan delta function $\Delta$, also called the discriminant
function, with coefficient ring as an optional argument, whose fallback is
$\mathbb{Q}$.
"""
function ramanujan_delta(coeffring = QQ)
  mfs = ModularFormsSpace(12, SL2Z, coeffring)
  basering = base_ring(base_ring_tower(mfs))
  symbring = symbolic_ring(mfs)
  mf = ModularFormSL2ZCongruenceType(mfs)
  mf.symbolic = [symbring(ModularFormSL2ZCuspFormCongruence_MODULE.RamanujanDeltaSL2Z())]
  return mf
end

function deepcopy_internal(
    f::ModularFormSL2ZCongruenceType{I, R},
    stackdict::IdDict
   ) where {I, R}
  ModularFormSL2ZCongruenceType{I,R}(
      parent(f),
      deepcopy_internal(f.symbolic, stackdict),
      deepcopy_internal(f.fourier_expansion, stackdict))
end

################################################################################
# comparison
################################################################################

function ==(
    f::ModularFormSL2ZCongruenceType{I, R},
    g::ModularFormSL2ZCongruenceType{I, R}
   ) where {I, R}
  # TODO: Does equality in AbstractAlgebra imply equality of parents? If so, we
  # are in trouble, because we want arbitrary subspaces to accommodate symbolic
  # expressions without computing their coordinates.
  weight(f) == weight(g) || return false
  arithtype(f) == arithtype(g) || return false
  # NOTE: We ignore the scalar_valued attribute
  base_ring(parent(f)) == base_ring(parent(g)) || return false
  # NOTE: We ignore the base ring tower
  sbd = sturm_bound(weight(f), arithtype(f), base_ring(parent(f)))
  return fourier_expansion(f, sbd) == fourier_expansion(g, sbd)
end

################################################################################
# properties
################################################################################

function isscalar_valued(f::ModularFormSL2ZCongruenceType{I, R}) where {I, R}
  isscalar_valued(parent(f))
end

function iszero(f::ModularFormSL2ZCongruenceType{I, R}) where {I, R}
  iszero(fourier_expansion(f, sturm_bound(parent(f))))
end

################################################################################
# symbolic expressions
################################################################################

function symbolic_expression_deflated(
    f        :: ModularFormSL2ZCongruenceType{I, R};
    asvector :: Bool = false
   ) where {I, R}
  if ismissing(f.symbolic)
    return missing
  end

  if isscalar_valued(f) && !asvector
    return f.symbolic[1]
  else
    return Vector{elem_type(symbolic_ring(parent(f)))}(f.symbolic)
  end
end

function symbolic_expression(
    f::ModularFormSL2ZCongruenceType{I,R};
    asvector :: Bool = false
   ) where {I, R}
  if ismissing(f.symbolic)
    return missing
  end

  if isscalar_valued(f) && !asvector
    return f.symbolic[1]
  end

  mfs = parent(f)
  svec = Vector{elem_type(symbolic_ring(mfs))}(undef, dim(arithtype(mfs)))
  for (ix,o) in enumerate(translation_decomposition(arithtype(mfs)))
    s = f.symbolic[ix]
    svec[representative(o)] = s
    for (jx,tw) in nonrepresentatives_with_transitions(o)
      s = translation_action(s)
      svec[jx] = act(tw, s)
    end
  end

  return svec
end

################################################################################
# conversion
################################################################################

function (mfs::ModularFormsSpaceSL2ZCongruenceType{I,R})(
    fe::S
   ) where {I,R, S <: SeriesElem{R}}
  if isscalar_valued(mfs)
    return mfs(S[fe])
  else
    throw(DomainError(mfs, "modular forms in this space are vector-valued"))
  end
end

function (mfs::ModularFormsSpaceSL2ZCongruenceType{I,R})(
    fes::Vector{S}
   ) where {I,R, S <: SeriesElem{R}}
  # To cover the full case, we have to check that accommodate coefficients of
  # rational index
  isone(arithtype(mfs)) || error("not implmeented")

  cs = coordinates(mfs, fes)
  f = sum(cs[n,1]*f for (n,f) in enumerate(basis(mfs)))

  # If any component provides more coefficients than the Sturm bound, we have
  # to check the Fourier expansion.
  sbd = ceil(Int,sturm_bound(mfs))
  prc = maximum(precision(fe) for fe in fes)
  if sbd < prc
    ffes = fourier_expansion(f, prc; asvector = true)
    for (ix,(ffe, fe)) in enumerate(zip(ffes, fes))
      for n in sbd:precision(fe)-1
        if coeff(ffe,n) != coeff(fe,n)
          throw(DomainError((ix,n, coeff(ffe,n),coeff(fe,n)),
                            "Fourier expansion does not represent " *
                            "a modular form in the given space"))
        end
      end
    end
  end

  return f
end

function coordinates(
    mfs::ModularFormsSpaceSL2ZCongruenceType{I,R},
    fe::S
   ) where {I,R, S <: SeriesElem{R}}
  if isscalar_valued(mfs)
    return coordinates(mfs, S[fe])
  else
    throw(DomainError(mfs, "modular forms in this space are vector-valued"))
  end
end

function coordinates(
    mfs::ModularFormsSpaceSL2ZCongruenceType{I,R},
    fes::Vector{S}
   ) where {I,R, S <: SeriesElem{R}}
  # To cover the full case, we have to check that accommodate coefficients of
  # rational index
  isone(arithtype(mfs)) || error("not implmeented")

  d = dim(arithtype(mfs))
  if length(fes) != d
    throw(DomainError(fes, "incorrect number of components"))
  end
  sbd = ceil(Int,sturm_bound(mfs))
  v = matrix(base_ring(mfs), d*sbd, 1, [coeff(fe,n) for fe in fes for n in 0:sbd-1])
  return solve(_fourier_expansion_matrix(mfs,sbd; transposed = true), v)
end

################################################################################
# show
################################################################################

function show(
    io::IO,
    f :: ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  if ismissing(symbolic_expression_deflated(f))
    output = fourier_expansion(f; asvector = true)
  else
    output = symbolic_expression(f; asvector = true)
  end

  for c in output[1:end-1]
    println(io, c)
  end
  print(io, output[end])
end

################################################################################
# arithmetic
################################################################################

function +(
    f::ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  return f
end

function -(
    f::ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  g = ModularFormSL2ZCongruenceType(parent(f))
  if !ismissing(f.symbolic)
    g.symbolic = -f.symbolic
  end
  if !ismissing(f.fourier_expansion)
    g.fourier_expansion = -f.fourier_expansion
  end
  return g
end

function +(
    f::ModularFormSL2ZCongruenceType{I,R},
    g::ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  parent(f) == parent(g) || throw(DomainError((f,g), "incompatible parents"))
  mfs = parent(f)
  fg = ModularFormSL2ZCongruenceType(mfs)

  if !ismissing(f.symbolic) && !ismissing(g.symbolic)
    fg.symbolic = f.symbolic .+ g.symbolic
    if !ismissing(f.fourier_expansion) && !ismissing(g.fourier_expansion)
      fg.fourier_expansion = f.fourier_expansion .+ g.fourier_expansion
    end
    return fg
  end

  if !ismissing(f.symbolic) && ismissing(g.symbolic)
    prc = mimimum(precision.(g.fourier_expansion))
  elseif ismissing(f.symbolic) && !ismissing(g.symbolic)
    prc = mimimum(precision.(f.fourier_expansion))
  else
    prc = min(mimimum(precision.(f.fourier_expansion)),
              mimimum(precision.(g.fourier_expansion)))
  end
  fg.fourier_expansion = fourier_expansion(f,prc) .+ fourier_expansion(g,prc)

  return fg
end

function -(
    f::ModularFormSL2ZCongruenceType{I,R},
    g::ModularFormSL2ZCongruenceType{I,R}
   ) where {I,R}
  parent(f) == parent(g) || throw(DomainError((f,g), "incompatible parents"))
  mfs = parent(f)
  fg = ModularFormSL2ZCongruenceType(mfs)

  if !ismissing(f.symbolic) && !ismissing(g.symbolic)
    fg.symbolic = f.symbolic .- g.symbolic
    if !ismissing(f.fourier_expansion) && !ismissing(g.fourier_expansion)
      fg.fourier_expansion = f.fourier_expansion .- g.fourier_expansion
    end
    return fg
  end

  if !ismissing(f.symbolic) && ismissing(g.symbolic)
    prc = mimimum(precision.(g.fourier_expansion))
  elseif ismissing(f.symbolic) && !ismissing(g.symbolic)
    prc = mimimum(precision.(f.fourier_expansion))
  else
    prc = min(mimimum(precision.(f.fourier_expansion)),
              mimimum(precision.(g.fourier_expansion)))
  end
  fg.fourier_expansion = fourier_expansion(f,prc) .- fourier_expansion(g,prc)

  return fg
end

function *(
    f::ModularFormSL2ZCongruenceType{TrivialGroupElem,R},
    g::ModularFormSL2ZCongruenceType{TrivialGroupElem,R}
   ) where R
  isone(arithtype(parent(f))) || error("not implmeented")

  mfs = parent(f) * parent(g)
  fg = ModularFormSL2ZCongruenceType(mfs)

  if !ismissing(f.symbolic) && !ismissing(g.symbolic)
    fg.symbolic = f.symbolic .* g.symbolic
    if !ismissing(f.fourier_expansion) && !ismissing(g.fourier_expansion)
      fg.fourier_expansion = f.fourier_expansion .* g.fourier_expansion
    end
    return fg
  end

  if !ismissing(f.symbolic) && ismissing(g.symbolic)
    prc = mimimum(precision.(g.fourier_expansion))
  elseif ismissing(f.symbolic) && !ismissing(g.symbolic)
    prc = mimimum(precision.(f.fourier_expansion))
  else
    prc = min(mimimum(precision.(f.fourier_expansion)),
              mimimum(precision.(g.fourier_expansion)))
  end
  fg.fourier_expansion = fourier_expansion(f,prc) .* fourier_expansion(g,prc)

  return fg
end

^(f::ModularFormSL2ZCongruenceType{I,R}, n::Int) where {I,R} = power_by_squaring(f,n)

# ad-hoc arithmetic

function *(
    c::Integer,
    f::ModularFormSL2ZCongruenceType{I,R}
   ) where {I, R<:RingElem}
  mfs = parent(f)

  g = ModularFormSL2ZCongruenceType(mfs)

  if !ismissing(f.symbolic)
    g.symbolic = c .* f.symbolic
  end

  if !ismissing(f.fourier_expansion)
    g.fourier_expansion = c .* f.fourier_expansion
  end

  return g
end

function *(
    c::R,
    f::ModularFormSL2ZCongruenceType{I,R}
   ) where {I, R<:RingElem}
  mfs = parent(f)
  parent(c) == base_ring(mfs) || throw(DomainError((c,f), "incompatible parents"))

  g = ModularFormSL2ZCongruenceType(mfs)

  if !ismissing(f.symbolic)
    coefftower = base_ring_tower(parent(f))
    if R != elem_type(coefftower) && R != elem_type(base_ring(coefftower))
      g.symbolic = coefftower(c) .* f.symbolic
    else
      g.symbolic = c .* f.symbolic
    end
  end

  if !ismissing(f.fourier_expansion)
    g.fourier_expansion = c .* f.fourier_expansion
  end

  return g
end

# NOTE: When uncommenting this code, we trigger a runtime error in the Julia
# type inference (Version 1.6.3). Check on later version whether we can
# activate this. The following did trigger it:
#   using ModularForms
#   eisenstein_series(4) * eisenstein_series(4) * eisenstein_series(6)

# function *(
#     f::ModularFormSL2ZCongruenceType{I,R},
#     c::R
#    ) where {I, R<:RingElem}
#   c*f
# end

################################################################################
# Fourier expansion
################################################################################

@doc Markdown.doc"""
    fourier_expansion(f::ModularFormSL2ZCongruenceType{I, R}, prc::Int; coefficient_ring::Union{Nothing,Ring} = nothing, fourier_expansion_cache::Union{Nothing,Dict} = nothing) where {I <: GroupElem, R <: RingElem}

Return the Fourier series expansion up to given precision of the modular form.
"""
function fourier_expansion(
    f        :: ModularFormSL2ZCongruenceType{I, R},
    prc      :: Union{Int,Rational{Int}};
    asvector :: Bool = false,
    fourier_expansion_cache :: Union{Nothing,Dict} = nothing
   ) where {
      I <: GroupElem, R <: RingElem
   }
  mfs = parent(f)
  fering, q = fourier_expansion_ring(mfs, prc)


  if !ismissing(f.fourier_expansion) &&
     all(precision(fe) >= prc for fe in f.fourier_expansion)
    if isscalar_valued(f) && !asvector
      return convert_precision(fering, f.fourier_expansion[1]) + O(q^prc)
    else
      return inflate([convert_precision(fering, fe) + O(q^prc)
                      for fe in f.fourier_expansion],
                 translation_decomposition(arithtype(f)),
                 base_ring_tower(parent(f))
                 )
    end
  end


  svec = symbolic_expression_deflated(f; asvector = true)

  # TODO: Why would one expand over the intermediate ring?
  # The fourier expansion function needs to accept the Fourier expanson ring.
  intermediate_coefficient_ring = fourier_coefficient_ring(svec)
  fevec = [fourier_expansion(s, prc;
               coefficient_ring = intermediate_coefficient_ring,
               fourier_expansion_cache = fourier_expansion_cache)
           for s in svec]

  if intermediate_coefficient_ring != base_ring(mfs)
    cvt = map_from_cyclotomic_tower(
              base_ring_tower(mfs), intermediate_coefficient_ring, base_ring(mfs))
    fevec = [change_base_ring(fe, cvt, fering) for fe in fevec]
  end
  f.fourier_expansion = fevec


  if isscalar_valued(f) && !asvector
    return fevec[1]
  else
    return inflate(fevec,
               translation_decomposition(arithtype(f)),
               base_ring_tower(parent(f))
               )
  end
end

@doc Markdown.doc"""
    _fourier_expansion_matrix(mfs::ModularFormSL2ZCongruenceType{I, R}, prec::Int; transposed::Bool = false) where {I,R}

Return a matrix with the Fourier coefficients corresponding to each basis vector
laying on each column, respectively. Can be transposed.
"""
function _fourier_expansion_matrix(
    mfs        :: ModularFormsSpaceSL2ZCongruenceType{I, R},
    prc        :: Union{Int,Rational{Int}};
    transposed :: Bool = false
   ) where {I,R}
  # To cover the full case, we have to check that accommodate coefficients of
  # rational index
  isone(arithtype(mfs)) || error("not implemented")
  prc = ceil(Int,prc)

  prc <= 0 && throw(DomainError(prc, "nonpositive precision"))


  dmfs= dim(mfs)
  drho = dim(arithtype(mfs))
  d = dmfs*drho

  if transposed
    mat = MatrixSpace(base_ring(mfs), prc, d)()
  else
    mat = MatrixSpace(base_ring(mfs), d, prc)()
  end

  if prc == 0 || d == 0
    return mat
  end

  if transposed
    for j in 1:dmfs
      fe = fourier_expansion(basis(mfs)[j], prc; asvector = true)
      for k in 1:drho
        fek = fe[k]
        for i in 1:prc
          setindex!(mat, coeff(fek,i-1), i, (j-1)*drho+k)
        end
      end
    end
  else
    for j in 1:dmfs
      fe = fourier_expansion(basis(mfs)[j], prc; asvector = true)
      for k in 1:drho
        fek = fe[k]
        for i in 1:prc
          setindex!(mat, coeff(fek,i-1), (j-1)*drho+k, i)
        end
      end
    end
  end

  return mat
end

################################################################################
# evaluation
################################################################################

@doc Markdown.doc"""
    evaluate(f::ModularFormSL2ZCongruenceType{I,R}, tau::acb; CCtower::Union{Nothing,CCMuInfTower} = nothing, evaluation_cache::Union{Nothing,Dict} = nothing ) where {I <: GroupElem, R <: RingElem}

Returns the numerical evaluation of $f(\tau)$.
"""
function evaluate(
    f        :: ModularFormSL2ZCongruenceType{I,R},
    tau      :: acb;
    asvector :: Bool = false,
    CCtower          :: Union{Nothing,CCMuInfTower} = nothing,
    evaluation_cache :: Union{Nothing,Dict}         = nothing
   ) where {
    I <: GroupElem, R <: RingElem
   }
  symb = symbolic_expression(f)
  if ismissing(symb)
    throw(DomainError(f, join("numerical evaluation only possible ",
                              "if symbolic expression is available")))
  end

  if isscalar_valued(f) && !asvector
    return evaluate(symb, tau)
  else
    return evaluate.(symb, tau)
  end
end
