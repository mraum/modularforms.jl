module ModularFormSL2ZCongruenceType_MODULE

using AbstractAlgebra
using AbstractAlgebra.Generic:
  PuiseuxSeriesElem, PuiseuxSeriesFieldElem, PuiseuxSeriesRingElem
using AbstractAlgebra:
  PuiseuxSeriesRing, PuiseuxSeriesField

using Nemo
using Nemo:
  ZZ, QQ, Ring

using Hecke:
  PrimesSet

using ModularForms
using ModularForms:
  ArithType,
  GenArithType,
  ModularFormsSL2ZCongruenceSymbolicRing,
  ModularFormsSL2ZCongruenceSymbolicRingElem,
  MuInfTower,
  RootOfUnity,
  SL2ZCoverElement,
  SL2ZElement,
  SL2ZGroup,
  SubmoduleAccWithGenMap,
  TrivialGroupElem,
  base_type,
  coefficient_ring,
  deflate_fourier_expansion,
  fourier_coefficient_ring,
  inflate_fourier_expansion,
  inv,
  rank_lower_bound,
  representatives,
  symbolic_fourier_coefficient_muinf_order,
  twist

using ModularForms.ModularFormSL2ZFourierExpansion_MODULE:
  fourier_expansion_ring

using ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE:
  eisenstein_gamma0_type,
  eisenstein_gamma1_type

################################################################################
# includes
################################################################################

include("CongruenceType/EisensteinProducts.jl")

################################################################################
# dimension
################################################################################

function esspace_dim(k::SL2RCoverWeight, rho::ArithType{G,I}) where {
    G <: SL2ZElement, I
   }
 return esspace_dim(Int(numerator(k.k))//Int(denominator(k.k)), rho)
end

function esspace_dim(k::K, rho::ArithType{G,I}) where
      {K <: Union{Int,Rational{Int}}, G <: SL2ZCoverElement, I}
  gp = group(rho)
  if ModularForms._issl2z(gp)
    isinteger(k) || return zero(ZZ)
  elseif ModularForms._ismp1z(gp)
    isinteger(2*k) || return zero(ZZ)
  elseif gp isa SL2ZGroup
    return esspace_dim(k, induction(SL2Z, rho))
  elseif !ModularForms._issl2z(base_group(gp))
    return esspace_dim(k, induction(change_base_group(SL2Z, gp), rho))
  else
    error("not implemented")
  end

  k >= 2 || error("not implemented")
  lvl = level(rho) # this ensures that rho is unitary

  (s,t) = gens(group(rho))
  t = rho(t)
  z = rho(s^2)
  z2 = z^2
  z3 = z2*z

  nprec = 10
  sufficientprec, dint = false, missing
  while !sufficientprec
    CC = AcbField(nprec)
    nprec > 200 && error("required precision in dimension compuation too high")

    traces = acb[]
    tpw = one(image_parent(rho))
    for _ in 0:lvl-1
      push!(traces, CC(tr(tpw)))
      push!(traces, onei(CC)^(2*k) * CC(tr(z*tpw)))
      push!(traces, onei(CC)^(4*k) * CC(tr(z2*tpw)))
      push!(traces, onei(CC)^(6*k) * CC(tr(z3*tpw)))
      tpw *= t
    end
    d = sum(traces) // (4*lvl)

    (sufficientprec,dint) = unique_integer(d)
    nprec *= 2
  end

  if k == 2
    dint -= dim(homspace(ModularForms.TrivialArithmeticType(group(rho)), rho))
  end

  return dint
end

# Provide that rho(Z) acts by e(-k/2)
#     Fischer, An Approach to the Selberg Trace Formula via the Selberg
#     Zeta-Function, Theorem 2.5.5
# shows that dim equals
#   dim(rho)*(1+k/12) - ev(e(-k//6)*rho(S*T)^-1) - ev(e(k//4)*rho(S)) - ev(rho(T)),
# where ev(m) = sum a_i for a matrix m with eigenvalues e(a_i), 0 \le a_i < 1.
# Use
#   ev(m) = \sum_{j(N)} j/N 1/N \sum_{l(N)} e(-jl/N) tr(m^l)
#   ev(m) = \sum_{l(N)} tr(m^l)  1/N \sum_{l(N)} j/N e(-jl/N),
# where N is the order of m. If l = 0 the inner sum is
#   (N-1)/2/N,
# otherwise
#   1//(e(-j//N)-1))/N.
# To obtain independence of the eigenvalues of Z, insert Z^h into each trace use the sum
#   sum_{h=0}^4 e(k*j/2) * (....).

# dimension for the full modular group
function mfspace_dim(k::SL2RCoverWeight)
  return mfspace_dim(Int(numerator(k.k))//Int(denominator(k.k)))
end

function mfspace_dim(k::SL2RCoverWeight, rho::ArithType{G,I}) where {
    G <: SL2ZElement, I
   }
 return mfspace_dim(Int(numerator(k.k))//Int(denominator(k.k)), rho)
end

# dimension for the full modular group
function mfspace_dim(k::K) where {K <: Union{Int,Rational{Int}}}
  if k < 0 || !isone(denominator(k)) || isodd(numerator(k))
    return 0
  end
  (kd12,km12) = fldmod(k,12)
  if km12 == 2
    return kd12
  else
    return kd12 + 1
  end
end

function mfspace_dim(k::K, rho::ArithType{G,I}) where
      {K <: Union{Int,Rational{Int}}, G <: SL2ZCoverElement, I}
  gp = group(rho)
  if ModularForms._issl2z(gp)
    isinteger(k) || return zero(ZZ)
  elseif ModularForms._ismp1z(gp)
    isinteger(2*k) || return zero(ZZ)
  elseif gp isa SL2ZGroup
    return mfspace_dim(k, induction(SL2Z, rho))
  elseif !ModularForms._issl2z(base_group(gp))
    # TODO: can also be computed using Borcherds's formula directly using the
    # geometry the Farey symbol directly
    return mfspace_dim(k, induction(change_base_group(SL2Z, gp), rho))
  else
    error("not implemented")
  end

  if isone(rho) && ModularForms._issl2z(gp)
    return mfspace_dim(k)
  end

  k >= 2 || error("not implemented")

  cord = centerorder(gp)

  nprec = 10
  sufficientprec, dint = false, missing
  while !sufficientprec
    CC = AcbField(nprec)
    nprec > 200 && error("required precision in dimension compuation too high")
    d = sum(cispi(CC(j*k)) * mfspace_dim_borcherds_psi(k,rho,j,CC)
            for j in 0:cord-1) // cord
    (sufficientprec,dint) = unique_integer(d)
    nprec *= 2
  end
  return dint
end

function mfspace_dim_borcherds_psi(k::K, rho::ArithType{G,I}, j::Int, CC) where
      {K <: Union{Int,Rational{Int}}, G <: SL2ZCoverElement, I
      }
  # assuming that group(rho) is SL2Z or Mp1Z
  (s,t) = gens(group(rho))
  z = s^2
  r = t*s
  rhozj = rho(z^j)

  return CC((k-1)//12) * CC(tr(rhozj)) +
         mfspace_dim_borchers_delta(3, MuInf(k//6), rho(r),      rhozj, CC) +
         mfspace_dim_borchers_delta(2, MuInf(k//4), rho(s),      rhozj, CC) +
         mfspace_dim_borchers_delta(0, one(MuInf),  rho(inv(t)), rhozj, CC)
end

function mfspace_dim_borchers_delta(
    nu::Int,
    Xtw::RootOfUnity,
    X::I,
    g::I,
    CC::AcbField
   ) where {
    I <: GroupElem
   }
  Xpwtraces = acb[]
  Xtwpw = Xtw
  Xpw = X

  if nu == 0
    while !isone(Xpw)
      push!(Xpwtraces, CC(Xtwpw) * CC(tr(Xpw*g)))
      Xtwpw *= Xtw
      Xpw *= X
    end

    if isempty(Xpwtraces)
      return CC(tr(g))//2
    else
      N = length(Xpwtraces)+1
      return CC(tr(g))//(2*N) +
          sum(Xpwtr//(1-cispi(2*CC(j//N)))
              for (j,Xpwtr) in enumerate(Xpwtraces)) // N
    end
  else
    for _ in 1:nu-1
      push!(Xpwtraces, CC(Xtwpw) * CC(tr(Xpw*g)))
      Xtwpw *= Xtw
      Xpw *= X
    end

    return sum(Xpwtr//(1-cispi(2*CC(j//nu)))
               for (j,Xpwtr) in enumerate(Xpwtraces)) // nu
  end
end


################################################################################
# coefficient field of the Fourier expansion
################################################################################

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::ArithType{G,TrivialGroupElem}
  ) where {
   G <: SL2ZElement
  }
  return level(rho)
end

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::ArithType{G,RootOfUnity}
  ) where {
   G <: SL2ZElement
  }
  return lcm(level(rho),
             lcm(order(g) for g in generator_images(rho)))
end

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::ArithType{G,TwistedPerm{TrivialGroupElem}}
  ) where {
   G <: SL2ZElement
  }
  return level(rho)
end

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::ArithType{G,TwistedPerm{RootOfUnity}}
  ) where {
   G <: SL2ZElement,
   H <: SL2ZElement
  }
  if rho isa ModularForms.IndArithType &&
         ModularForms.base_type(rho) isa ModularForms.DirichletType
    return lcm(level(rho), order(character(base_type(rho))))
  else
    return lcm(level(rho),
               lcm((order(t) for g in generator_images(rho)
                             for t in twists(g))...))
  end
end

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::WeilTransformation{GW}
  ) where {
   G <: SL2ZElement,
   GW <: SL2ZElement
  }
  return 1
end

function fourier_coefficient_muinf_order(
   k::SL2RCoverWeight,
   rho::ArithType{G,TwistedPermGroup{T}}
  ) where {
   G <: SL2ZElement,
   GW <: SL2ZElement,
   T <: Union{RootOfUnity, WeilTransformation{GW}}
  }
  return lcm(level(rho),
             lcm([lcm([order(t) for t in twists(g)])
                  for g in generator_images(rho)]))
end

################################################################################
# cached spaces of modular forms
################################################################################

eisenstein_space_cache =
    Dict{Tuple{SL2RCoverWeight,ArithType,Ring,MuInfTower},ModularFormsSpace}()

modular_forms_space_cache =
    Dict{Tuple{SL2RCoverWeight,ArithType,Ring,MuInfTower},ModularFormsSpace}()

################################################################################
# Spanning sets of spaces of modular forms
################################################################################

# Space of Eisenstein series, computed via homomorphisms from Eisenstein series
# of type Ind_Gamma1(N) 1 or Int_Gamma0(N) chi.
function _eisenstein_with_fourier_expansion(
    k          :: SL2RCoverWeight,
    rho        :: A,
    coeffring  :: Ring,
    coefftower :: MuInfTower
   ) where {
    G <: SL2ZElement, I,
    A <: ArithType{G,I}
   }
  symbring = ModularForms.ModularFormsSL2ZCongruenceSymbolicRing(coefftower)
  # TODO: We pass a far too large dimension bound. Compute the correct dimension.
  _accumulate_modular_forms_via_fourier_expansion(
      k, rho, coeffring, coefftower, symbring,
      symbolic_modular_forms_from_single_eisenstein_and_homomorphisms(
          k, rho, symbring),
      esspace_dim(k,rho)
      )
end

# Spanning sets of spaces of modular forms, computed via products of Eisenstein
# series.
function _modular_forms_with_fourier_expansion_via_products(
    k           :: SL2RCoverWeight,
    rho         :: A,
    coeffring   :: Ring,
    coefftower  :: MuInfTower;
    level_bound :: Union{Nothing,Int} = nothing
   ) where {
    G <: SL2ZElement, I,
    A <: ArithType{G,I}
   }
  symbring = ModularForms.ModularFormsSL2ZCongruenceSymbolicRing(coefftower)

  # If we are in characteristic zero and have enough roots of unity, then we
  # first try to compute Fourier expansion modulo p.
  lvl = level(rho)
  muinf_order = lcm(lvl, euler_phi(lvl))
  if characteristic(coefftower) == 0 &&
     has_muinf_order(coefftower, coeffring, muinf_order)

    # We iterate through primes that are congurent to 1 modulo muinf_order.
    primes = PrimesSet(rand(2^50:2^55),typemax(Int64), muinf_order,1)
    for p in Iterators.take(primes, 50)
      # NOTE: It is important to use the GF constructor instead of FiniteField,
      # since matrices over GF(p) are stored and computed with more efficiently.
      coefficient_ring_tower_modp = cyclotomic_tower(Nemo.GF(p))
      symbvecs = _accumulate_modular_forms_via_fourier_expansion(
          k, rho, coeffring, coefftower, symbring,
          symbolic_modular_forms_from_eisenstein_products_and_homomorphisms(
              k, rho, symbring; level_bound = level_bound),
          mfspace_dim(k,rho)
          )
      if !isnothing(symbvecs)
        return symbvecs
      end
    end
  end

  return _accumulate_modular_forms_via_fourier_expansion(
      k, rho, coeffring, coefftower, symbring,
      symbolic_modular_forms_from_eisenstein_products_and_homomorphisms(
          k, rho, symbring; level_bound = level_bound),
      mfspace_dim(k,rho)
      )
end

# Iterate through a symbolic modular forms, accumulate a submodule via their
# Fourier expansion until either the dimension given by the dimension bound is
# attained or the iterator is exhausted. The iterator function receives one
# argument, the symbolic ring. The final Fourier expansions are defined over
# coeffring.
# If coefftower_modp is nontrivial, then all symbols must have expansions over
# its base ring after reduction.
function _accumulate_modular_forms_via_fourier_expansion(
    k               :: SL2RCoverWeight,
    rho             :: A,
    coeffring       :: Ring,
    coefftower      :: MuInfTower,
    symbring        :: Ring,
    symb_iterator   :: Any,
    mfdim_bound     :: fmpz;
    coefftower_modp :: Union{Nothing,MuInfTower} = nothing
   ) where {
    G <: SL2ZElement, I,
    A <: ArithType{G,I}
   }
  lvl = level(rho)

  # Since we will compute with Fourier expansions, we need Sturm bounds. We
  # cannot use the Sturm bound for the Fourier coefficient base ring, even if
  # one is passed in, since during the accumulation we might use any of the
  # fields in the tower. Therefore, we have to use the potentially worth Sturm
  # bound associated with the coefficient tower.
  prcfe = ModularForms.sturm_bound(k, rho, coefftower)


  # The dimension of the space which we want to span.
  if mfdim_bound == 0
    if isnothing(coefftower_modp)
      (fering,_) = fourier_expansion_ring(coeffring, prcfe; level = lvl)
      return Tuple{Vector{elem_type(symbring)}, Vector{elem_type(fering)}}[]
    else
      return Tuple{Vector{elem_type(symbring),Nothing}}[]
    end
  end


  if !isnothing(coefftower_modp)
    symbring_modp = ModularForms.ModularFormsSL2ZCongruenceSymbolicRing(coefftower_modp)
  end

  # We will evaluate Fourier expansions of many Eisenstein series, which we
  # cache.
  fe_cache = Dict()

  # Depending on the weight and the type, we determine the least possible order
  # of roots of unity that appear in the Fourier expansion. Individual terms in
  # the contribution from Eisenstein product might require further roots of
  # unity.
  fe_muinf_order = fourier_coefficient_muinf_order(k, rho)

  # The fespan_base_ring will change during the computation. We initiate it in
  # such a way that it can accommodate the Fourier expansion of the target
  # space and the Fourier expansion of Eisenstein series that we employ.
  if isnothing(coefftower_modp)
    # We accumulate Fourier expansions of modular forms, until we reach the
    # dimension of the space of modular forms, mfdim_bound.
    # TODO: We do not reduce in each step, which might delay completion of
    # the computation. To optimize performance we need to balance the
    # capacity of fespan with the maximal final dimension mfdim_bound.
    fespan_base_ring = muinf_ring(coefftower, fe_muinf_order)
    fespan = SubmoduleAccWithGenMap(
                 fespan_base_ring, Vector{elem_type(symbring)},
                 number_of_fourier_coefficients(rho, prcfe), Int(mfdim_bound+2))
  else
    fespan_base_ring = base_ring(coefftower_modp)
    fespan = SubmoduleAccWithGenMap(
                 fespan_base_ring, Vector{elem_type(symbring)},
                 number_of_fourier_coefficients(rho, prcfe), Int(mfdim_bound+2),
                 a -> lift(coefftower_modp, a, coefftower, base_ring(coefftower)))
  end


  for svec in symb_iterator
    if isnothing(coefftower_modp)
      fespan_base_ring = fourier_coefficient_ring(
                             svec; coefficient_ring = base_ring(fespan))

      if base_ring(fespan) != fespan_base_ring
        # If the base ring of the submodule cannot accommodate all roots of unity
        # needed, we extend it.
        cvt = map_from_cyclotomic_tower(coefftower, base_ring(fespan), fespan_base_ring)
        fering, _ = ModularForms.ModularFormSL2ZFourierExpansion_MODULE.
                    fourier_expansion_ring(fespan_base_ring, prcfe; level = lvl)
        fespan = change_base_ring(cvt, fespan, fespan_base_ring)
        fe_cache = Dict(((s, change_base_ring(fe, cvt, fering))
                         for (s,fe) in fe_cache))
      end
    else
      # If we compute modulo p, we are guaranteed that Fourier expansions of
      # all modules are defined over the tower base field. In particular, we do
      # not need to change fespan_base_ring, and the coefficient_ring for
      # reduced symbolic expressions is muinf_ring(coefftower_modp,1).
      svec_modp = [
          map_coefficients(a -> coefftower_modp(coefftower,a), s;
                           parent = symbring_modp,
                           coefficient_ring = muinf_ring(coefftower_modp,1))
          for s in svec]
    end

    # TODO: We compute the Fourier expansion in the fespan_base_ring, but
    # should test the possibility of first computing it in the smallest
    # possible ring and then converting it.
    fevec = [fourier_expansion(f, prcfe;
                coefficient_ring = fespan_base_ring,
                fourier_expansion_level = lvl,
                fourier_expansion_cache_converted = fe_cache)
             for f in (isnothing(coefftower_modp) ? svec : svec_modp)]

    # Insert the Eisenstein representation and the Fourier expansion into the
    # fespan.
    insert!(fespan, deflate_fourier_expansion(fevec, rho, prcfe), svec)

    # Check whether fespan exhausts all modular forms. Insertion implicitely
    # reduces the module, when its capacity is exceeded. This check is a proper
    # check only after such reduction has been performed.
    if rank_lower_bound(fespan) == mfdim_bound
      @goto accumulate_modular_forms_via_fourier_expansion__return
    end
  end

  if !isnothing(coefftower_modp)
    return nothing
  end


  @label accumulate_modular_forms_via_fourier_expansion__return
  reduce!(fespan)

  # Symbolic expressions might not be defined over the coeffring, but may
  # indeed contain coefficients in fespan_base_ring. But since the echelon
  # basis for the accumulating submodule is computed on the Fourier expansion
  # and since we have spanned the whole space, we know that all Fourier
  # expansions lie in the minimal possible field.

  if isnothing(coefftower_modp)
    (fering,_) = fourier_expansion_ring(coeffring, prcfe; level = lvl)
    if fespan_base_ring == coeffring
      return Tuple{Vector{elem_type(symbring)},Vector{elem_type(fering)}}[
                 (svec, inflate_fourier_expansion(v, rho, fering, prcfe))
                 for (v,svec) in fespan]
    else
      coeffconvert = map_from_cyclotomic_tower(coefftower,
                         fespan_base_ring, coeffring)
      return Tuple{Vector{elem_type(symbring)},Vector{elem_type(fering)}}[
                 (svec, inflate_fourier_expansion(map(coeffconvert,v),
                            rho, fering, prcfe))
                 for (v,svec) in fespan]
    end
  else
    return Tuple{Vector{elem_type(symbring),Nothing}}[
               (svec,nothing) for (_,svec) in fespan]
  end
end

end # module ModularFormSL2ZCongruenceType_MODULE

import .ModularFormSL2ZCongruenceType_MODULE
