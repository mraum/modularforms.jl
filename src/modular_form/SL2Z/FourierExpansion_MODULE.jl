module ModularFormSL2ZFourierExpansion_MODULE

using AbstractAlgebra

using Nemo
using Nemo:
  Ring

################################################################################
# cached Fourier expansion rings in (generalized) level N
################################################################################

# TODO: This is a relict of Nemo comparing power series by ===. We can we use
# caching in Nemo instead.
fourier_expansion_ring_lvlN_cache = Dict{Tuple{Ring,Int},Any}()

function fourier_expansion_ring(
    coeffring :: R,
    prc       :: Union{Int,Rational{Int}};
    level     :: Int
   ) where R <: Ring
  prc = ceil(Int,prc*level)

  global fourier_expansion_ring_lvlN_cache
  if !haskey(fourier_expansion_ring_lvlN_cache, (coeffring,prc))
    prc <= 0 && error("nonpositive precision")
    fering = PuiseuxSeriesRing(coeffring, prc, "q")
    fourier_expansion_ring_lvlN_cache[coeffring,prc] = fering
    return fering
  end

  return fourier_expansion_ring_lvlN_cache[coeffring,prc]
end

end

import .ModularFormSL2ZFourierExpansion_MODULE
