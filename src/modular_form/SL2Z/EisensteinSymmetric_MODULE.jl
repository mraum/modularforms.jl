module ModularFormSL2ZEisensteinSymmetric_MODULE

using Nemo
using ModularForms

using ModularForms.ModularFormSL2ZFourierExpansion_MODULE:
  fourier_expansion_ring


# Fourier expansion of the Eisenstein series for (X-τ)^j of type sym^d. The
# components are the expansions associated with (X-τ)^r, in ascending order.
function eisenstein_symmetric_fourier_expansion(
    k   :: Int,
    d   :: Int, j :: Int,
    prc :: Int,
    CC  :: AcbField
   )
  RR = ArbField(precision(CC))

  fering,_ = fourier_expansion_ring(CC, prc; level = 1)
  fevec = [zero(fering) for r in 0:j-1]

  for r in j:d
    coeff_pre = ( binomial(ZZ(d-j),ZZ(r-j))
                * (-2 * const_pi(CC) * onei(CC))^(k+j+r-d)
                * inv( gamma(RR(k+j+r-d)) * zeta(k+2j-d,RR) )
                )
    coeffs = zeros(CC, prc)
    if r == j
      coeffs[1] += one(CC)
    end
    for m in 1:prc-1
      a = coeff_pre * CC(m)^(k+2j-d-1)
      for n in m:m:prc-1
        coeffs[n+1] += CC(n)^(r-j) * a
      end
    end

    push!(fevec, fering(laurent_ring(fering)(coeffs, prc, prc, 0, 1), 1))
  end

  return fevec
end

# Evaluation of the Eisenstein series for (X-τ)^j of type sym^d. The components
# are the expansions associated with (X-τ)^r (X-\ov{τ})^(d-r).
function eisenstein_symmetric_evaluate(
    k      :: Int,
    d      :: Int, j :: Int,
    tau    :: acb;
    maxerr :: Union{Nothing,arb} = nothing
   )
  CC = parent(tau)
  RR = ArbField(precision(CC))
  y = imag(tau)

  evvec_Xτ = eisenstein_symmetric_evaluate_Xτ(k, d, j, tau; maxerr=maxerr)
  # We use that
  # 1 = i/(2y) * ((X-τ) - (X-\ov{τ}))
  evvec = zeros(CC, d+1)
  for r in j:d
    for rr in r:d
      evvec[rr+1] += ( evvec_Xτ[r+1]
                     * (onei(CC)//(2*y))^(d-r)
                     * (-1)^(d-rr) * binomial(ZZ(d-r),ZZ(rr-r))
                     )
    end
  end
  return evvec
end

#Evaluation of E_k(τ;D,j) in the (X-τ)^r basis
function eisenstein_symmetric_evaluate_Xτ(
    k      :: Int,
    d      :: Int, j :: Int,
    tau    :: acb;
    maxerr :: Union{Nothing,arb} = nothing
   )
  k > 2 + d || throw(DomainError(k,"k must be greater than 2 + d"))
  j >= 0 || throw(DomainError(j, "j must be nonnegative"))
  j <= d || throw(DomainError(j,"j must be less or equal to d"))

  CC = parent(tau)
  RR = ArbField(precision(CC))
  y = imag(tau)

  if isnothing(maxerr)
    maxerr = radius(abs(tau))
    if maxerr <= 0
      maxerr = RR("1e-10")
    end
  end

  prc = 0
  feerr_pre = ( binomial(ZZ(d-j), floor(fmpz,(d-j)//2))
              * (2 * const_pi(RR))^(k+j)
              * inv( gamma(RR(k+j)) * zeta(k+2*j-d,RR) )
              * (k+2j-d-1) * inv(k+2j-d-2)
              )
  feerr = nothing
  while isnothing(feerr) || feerr > maxerr
    prc += 10
    if 2 * const_pi(RR) * y >= 1
      feerr = ( feerr_pre
              * gamma(RR(k+j), 2*const_pi(RR) * prc * y)
              * inv((2 * const_pi(RR) * y)^(k+2*j-d))
              )
    else
      feerr = ( feerr_pre
              * maximum(
                gamma(RR(r+k+j-d), 2 * const_pi(RR) * prc * y)
              * inv((2 * const_pi(RR) * y)^(r+k+j-d))
                for r in j:d)
              )
    end
  end

  fevec = eisenstein_symmetric_fourier_expansion(k, d, j, prc, CC)

  # This is the expansion with respect to the basis (X-τ)^r
  evvec_Xτ = [( sum(coeff(fe,n) * cispi(2*n*tau) for n in 0:prc-1)
              + ball(zero(RR), feerr)
              )
              for fe in fevec]

  return evvec_Xτ
end

end

import .ModularFormSL2ZEisensteinSymmetric_MODULE
