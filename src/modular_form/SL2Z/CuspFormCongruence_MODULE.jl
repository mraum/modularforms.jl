module ModularFormSL2ZCuspFormCongruence_MODULE

import Base:
  ==,
  hash,
  show

using AbstractAlgebra

using Nemo

using ModularForms
using ModularForms:
  CCMuInfTower,
  ModularFormSymbolic,
  MuInfTower
import ModularForms:
  _evaluate,
  _fourier_expansion,
  fourier_coefficient_muinf_order,
  level,
  translation_action,
  weight

using ModularForms.ModularFormSL2ZFourierExpansion_MODULE:
  fourier_expansion_ring

using ModularForms.ModularFormSL2ZEisensteinCongruence_MODULE:
  EisensteinSL2Z

struct RamanujanDeltaSL2Z <: ModularFormSymbolic
end

################################################################################
# properties
################################################################################

weight(a::RamanujanDeltaSL2Z) = 12

level(a::RamanujanDeltaSL2Z) = 1

fourier_coefficient_muinf_order(::RamanujanDeltaSL2Z) = 1

################################################################################
# display
################################################################################

function show(io::IO, a::RamanujanDeltaSL2Z)
  print(io, "Δ(SL(2,ℤ))")
end

################################################################################
# comparison
################################################################################

hash(a::RamanujanDeltaSL2Z, h::UInt) = xor(hash(RamanujanDeltaSL2Z,h), hash(12))

==(::RamanujanDeltaSL2Z, ::RamanujanDeltaSL2Z) = true

################################################################################
# translation action
################################################################################

translation_action(f::RamanujanDeltaSL2Z) = f

################################################################################
################################################################################
# Fourier expansions
################################################################################
################################################################################

function _fourier_expansion(
    es         :: RamanujanDeltaSL2Z,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower
   ) where {
    R <: RingElement
   }
  char = characteristic(base_ring(coefftower))
  if char != 2 && char != 3
    e6sq = _fourier_expansion(EisensteinSL2Z(6),prc,coefftower)^2
    e4cb = _fourier_expansion(EisensteinSL2Z(4),prc,coefftower)^3
    return inv(base_ring(parent(e6sq))(1728)) * (e4cb - e6sq)
  elseif char == 3
    (fering,q) = fourier_expansion_ring(muinf_ring(coefftower, 1), prc; level=1)
    return q * prod(1-q^(3*n) for n in 1:prc-1)^8
  elseif char == 2
    (fering,q) = fourier_expansion_ring(muinf_ring(coefftower, 1), prc; level=1)
    return q * prod(1-q^(8*n) for n in 1:prc-1)^3
  end
end

################################################################################
################################################################################
# evaluation
################################################################################
################################################################################

function _evaluate(
    es             :: RamanujanDeltaSL2Z,
    tau            :: acb,
    cc_muinf_tower :: CCMuInfTower
   )

  e6sq = _evaluate(EisensteinSL2Z(6),tau,cc_muinf_tower)^2
  e4cb = _evaluate(EisensteinSL2Z(4),tau,cc_muinf_tower)^3
  return inv(parent(tau)(1728)) * (e4cb - e6sq)
end


end

import .ModularFormSL2ZCuspFormCongruence_MODULE
