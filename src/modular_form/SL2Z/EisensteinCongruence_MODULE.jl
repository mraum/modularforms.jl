module ModularFormSL2ZEisensteinCongruence_MODULE

import Base:
  ==,
  hash,
  show

using AbstractAlgebra

using Nemo
using Nemo:
  ZZ, QQ, Ring

using Hecke:
  Zlattice,
  discriminant_group
 
using ModularForms
using ModularForms:
  CCMuInfTower,
  ModularFormSymbolic,
  MuInfTower
import ModularForms:
  _evaluate,
  _fourier_expansion,
  character,
  fourier_coefficient_muinf_order,
  level,
  translation_action,
  weight

using ModularForms.ModularFormSL2ZFourierExpansion_MODULE:
  fourier_expansion_ring

################################################################################
################################################################################
# symbolic expressions
################################################################################
################################################################################

################################################################################
# types
################################################################################

struct EisensteinSL2Z <: ModularFormSymbolic
  k   :: Int
end

struct EisensteinSL2ZGamma0 <: ModularFormSymbolic
  k   :: Int
  chi :: DirichletCharacter
  c   :: Int
  d   :: Int
end

struct EisensteinSL2ZGamma1 <: ModularFormSymbolic
  k   :: Int
  lvl :: Int
  c   :: Int
  d   :: Int
end

################################################################################
# properties
################################################################################

weight(a::EisensteinSL2Z) = a.k
weight(a::EisensteinSL2ZGamma0) = a.k
weight(a::EisensteinSL2ZGamma1) = a.k

character(a::EisensteinSL2ZGamma0) = a.chi

level(a::EisensteinSL2Z) = 1
level(a::EisensteinSL2ZGamma0) = modulus(a.chi)
level(a::EisensteinSL2ZGamma1) = a.lvl

fourier_coefficient_muinf_order(::EisensteinSL2Z) = 1

function fourier_coefficient_muinf_order(es::EisensteinSL2ZGamma0)
  lcm(level(es), euler_phi(conductor(character(es))))
end

fourier_coefficient_muinf_order(es::EisensteinSL2ZGamma1) = level(es)

################################################################################
# display
################################################################################

function show(io::IO, a::EisensteinSL2Z)
  print(io, "E($(a.k),SL(2,ℤ))")
end

function show(io::IO, a::EisensteinSL2ZGamma0)
  print(io, "E($(a.k),Γ_0($(modulus(a.chi))),$(a.chi);$(a.c),$(a.d))")
end

function show(io::IO, a::EisensteinSL2ZGamma1)
  print(io, "E($(a.k),Γ_1($(a.lvl));$(a.c),$(a.d))")
end

################################################################################
# comparison
################################################################################

hash(a::EisensteinSL2Z, h::UInt) = xor(hash(EisensteinSL2Z,h), hash(weight(a)))

function hash(a::EisensteinSL2ZGamma0, h::UInt)
  xor(hash(EisensteinSL2ZGamma0,h),
      hash(weight(a)),
      hash(character(a)),
      hash(a.c),
      hash(a.d)
     )
end

function hash(a::EisensteinSL2ZGamma1, h::UInt)
  xor(hash(EisensteinSL2ZGamma1,h),
      hash(weight(a)),
      hash(level(a)),
      hash(a.c),
      hash(a.d)
     )
end

==(a::EisensteinSL2Z, b::EisensteinSL2Z) = weight(a) == weight(b)

function ==(a::EisensteinSL2ZGamma0, b::EisensteinSL2ZGamma0)
  weight(a) == weight(b) && character(a) == character(b) &&
      a.c == b.c && a.d == b.d
end

function ==(a::EisensteinSL2ZGamma1, b::EisensteinSL2ZGamma1)
  weight(a) == weight(b) && level(a) == level(b) &&
      a.c == b.c && a.d == b.d
end

################################################################################
# translation action
################################################################################

translation_action(es::EisensteinSL2Z) = es

function translation_action(es::EisensteinSL2ZGamma1)
  EisensteinSL2ZGamma1(es.k, es.lvl, es.c, mod1(es.c+es.d,level(es)))
end

function translation_action(es::EisensteinSL2ZGamma0)
  EisensteinSL2ZGamma0(es.k, es.chi, es.c, mod1(es.c+es.d,level(es)))
end

################################################################################
################################################################################
# vector valued Eisenstein series
################################################################################
################################################################################

function eisenstein_sl2z_type(k::Int)
  ([EisensteinSL2Z(k)], TrivialArithmeticType(SL2Z))
end

function eisenstein_gamma0_type(k::Int, chi::DirichletCharacter)
  rho = right_induction(ArithmeticType(chi))
  lvl = modulus(chi)
  mod1(a::fmpz,b::Int) = Base.mod1(Int(a),b)
  es = [EisensteinSL2ZGamma0(k, chi, mod1(g.c,lvl), mod1(g.d,lvl))
        for g in coset_representatives(rho)]
  return (es, rho)
end

function eisenstein_gamma1_type(k::Int, lvl::Int)
  rho = right_induction(TrivialArithmeticType(Gamma1SL2Z(lvl)))
  mod1(a::fmpz,b::Int) = Base.mod1(Int(a),b)
  es = [EisensteinSL2ZGamma1(k, lvl, mod1(g.c,lvl), mod1(g.d,lvl))
        for g in coset_representatives(rho)]
  return (es, rho)
end

# TODO: move to a symbolic espression
# function eisenstein_weil_hyperbolic_plane_type(k::Int, lvl::Int) where {R <: Ring}
function eisenstein_weil_hyperbolic_plane_type(
    k         :: Int,
    lvl       :: Int,
    prc       :: Union{Int,Rational{Int}},
    coeffring :: R
   ) where {R <: Ring}
  lat = Zlattice(gram = matrix(ZZ, [0 lvl; lvl 0]))
  disc = discriminant_group(lat)
  return ([eisenstein_weil_hyperbolic_plane_type_fourier_expansion(
               k, lvl, Int(numerator(lvl*c)), Int(numerator(lvl*h)),
               prc, coeffring)
           for (c,h) in map(lift,disc)],
          weil_type(SL2Z, lat)
         )
end

################################################################################
################################################################################
# Fourier expansions
################################################################################
################################################################################

# Eisenstein series for SL2Z

function _fourier_expansion(
    es         :: EisensteinSL2Z,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower
   ) where {
    R <: RingElement
   }
  eisenstein_sl2z_fourier_expansion(
      es.k, prc, coefftower, muinf_ring(coefftower, 1), 1)
end

function eisenstein_sl2z_fourier_expansion(
    k           :: Int,
    prc         :: Union{Int,Rational{Int}},
    coefftower  :: MuInfTower,
    coeffring   :: R,
    fepseudolvl :: Union{Nothing,Int}
   ) where {
    R <: Ring
   }
  prc = ceil(Int,prc)
  es = [zero(coeffring) for n in 1:prc]

  es[1] = one(coeffring)
  for n in 1:prc-1
    inc = coeffring(n)^(k-1)
    for ix in n+1:n:prc
      es[ix] = addeq!(es[ix], inc)
    end
  end

  h = coeffring(-2*k) * inv(coeffring(bernoulli(k)))
  for ix in 2:length(es)
    es[ix] = es[ix] * h
  end

  (fering,_) = fourier_expansion_ring(coeffring, prc; level=fepseudolvl)
  return fering(laurent_ring(fering)(es, prc, prc, 0, 1), 1)
end

# Eisenstein series for SL2Z of weight 2

function eisenstein_weight2_fourier_expansion!(
    es          :: Vector{R},
    coeff       :: C,
    prc         :: Union{Int,Rational{Int}},
    fepseudolvl :: Int
   ) where {R <: RingElem, C <: Union{Nothing,R}}
  length(es) >= ceil(Int,fepseudolvl*prc) || error("too short es vector")
  if isempty(es)
    return
  end

  if isnothing(coeff)
    coeffring = parent(first(es))
  else
    coeffring = parent(coeff)
  end

  inc = coeffring(-1//12)
  if !isnothing(coeff)
    inc *= coeff
  end
  addeq!(es[1], inc)

  for n in 1:ceil(Int,prc)-1
    inc = 2*coeffring(n)
    if !isnothing(coeff)
      inc *= coeff
    end
    for nc in n*fepseudolvl+1:n*fepseudolvl:ceil(Int,prc*fepseudolvl)
      addeq!(es[nc], inc)
    end
  end
end

# Eisenstein series for Gamma0

function _fourier_expansion(
    es         :: EisensteinSL2ZGamma0,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower
   ) where {
    R <: RingElement
   }
  eisenstein_gamma0_type_fourier_expansion(
      es.k, es.chi, es.c, es.d,
      prc,
      coefftower, muinf_ring(coefftower, fourier_coefficient_muinf_order(es)),
      modulus(es.chi))
end

function eisenstein_gamma0_type_fourier_expansion(
    k           :: Int,
    chi         :: DirichletCharacter,
    c0          :: Int,
    d0          :: Int,
    prc         :: Union{Int,Rational{Int}},
    coefftower  :: MuInfTower,
    coeffring   :: R,
    fepseudolvl :: Union{Nothing,Int}
   ) where {
    R <: Ring
   }
  lvl = modulus(chi)
  if isnothing(fepseudolvl)
    fepseudolvl = lvl
  else
    @assert mod(fepseudolvl,lvl) == 0
  end

  es = [zero(coeffring) for _ in 1:ceil(Int,prc*lvl)]

  if k != 2 || lvl != 1
    for h in 1:lvl
      gcd(h,lvl) == 1 || continue
      eisenstein_gamma1_type_fourier_expansion!(
          es, coefftower(coeffring, conj(chi(h))),
          k, lvl, mod(h*c0,lvl), mod(h*d0,lvl),
          prc, coefftower)
    end

    if k == 2 && isone(chi)
      consttermsum = eisenstein_gamma1_type_wt2_constant_term_sum(lvl, coeffring)
      rhodim = coeffring(prod(p^(n-1) * (p+1) for (p,n) in factor(ZZ(lvl))))
      eisenstein_weight2_fourier_expansion!(
          es, 12 * consttermsum // rhodim,
          prc, lvl)
    end
  end

  (fering,_) = fourier_expansion_ring(coeffring, prc; level = fepseudolvl)
  return  fering(laurent_ring(fering)(
                   es, ceil(Int,prc*lvl), ceil(Int,prc*lvl), 0, 1), lvl)
end

# Eisenstein series for Gamma1

function _fourier_expansion(
    es         :: EisensteinSL2ZGamma1,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower
   ) where {
    R <: RingElement
   }
  eisenstein_gamma1_type_fourier_expansion(
      es.k, es.lvl, es.c, es.d,
      prc,
      coefftower, muinf_ring(coefftower, fourier_coefficient_muinf_order(es)))
end

function eisenstein_gamma1_type_fourier_expansion(
    k          :: Int,
    lvl        :: Int,
    c0         :: Int,
    d0         :: Int,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower,
    coeffring  :: R
   ) where {
    R <: Ring,
    A <: RingElem
   }
  es = [zero(coeffring) for _ in 1:ceil(Int,prc*lvl)]

  if k != 2 || lvl != 1
    eisenstein_gamma1_type_fourier_expansion!(
        es, nothing,
        k, lvl, c0, d0,
        prc, coefftower)

    if k == 2
      consttermsum = eisenstein_gamma1_type_wt2_constant_term_sum(lvl, coeffring)
      rhodim = coeffring(prod(p^(2*n-2) * (p^2-1) for (p,n) in factor(ZZ(lvl))))
      eisenstein_weight2_fourier_expansion!(
          es, 12 * consttermsum // rhodim,
          prc, lvl)
    end
  end

  (fering,_) = fourier_expansion_ring(coeffring, prc; level = lvl)
  fe = fering(laurent_ring(fering)(
                  es, ceil(Int,prc*lvl), ceil(Int,prc*lvl), 0, 1), lvl)

  return fe
end

function eisenstein_gamma1_type_fourier_expansion!(
    es         :: Vector{R},
    coeff      :: C,
    k          :: Int,
    lvl        :: Int,
    c0         :: Int,
    d0         :: Int,
    prc        :: Union{Int,Rational{Int}},
    coefftower :: MuInfTower
   ) where {R <: RingElem, C <: Union{Nothing,R}}
  @assert k > 0

  if c0 < 0 || c0 >= lvl
    c0 = c0 % lvl
  end
  if d0 < 0 || d0 >= lvl
    d0 = d0 % lvl
  end

  c0m = c0 == 0 ? 0 : lvl - c0
  d0m = d0 == 0 ? 0 : lvl - d0
  
  length(es) >= ceil(Int,lvl*prc) || error("too short es vector")
  if isempty(es)
    return
  end

  if isnothing(coeff)
    coeffring = parent(first(es))
  else
    coeffring = parent(coeff)
  end

  # constant term
  inc = nothing
  if c0 == 0
    if d0 == 0
      if iseven(k)
        inc = coeffring(-divexact(bernoulli(k), k))
      end
    else
      if k != 1
        N = ZZ(lvl)
        inc = coeffring( - N^(k-1) // k
                           * sum(  binomial(k,h)
                                 * bernoulli(k-h)
                                 * sum(coefftower(coeffring,MuInf(d*d0//lvl)) * d^h
                                       for d in 1:lvl) // QQ(N^h)
                                 for h in 0:k )
                       )
      else
        inc = coeffring(coeffring(QQ(1,2)) -
              sum(coefftower(coeffring,MuInf(d*d0//lvl)) * d
                  for d in 1:lvl) // lvl)
      end
    end
  elseif k == 1
    inc = coeffring(QQ((lvl-2*c0), 2*lvl))
  end
  if !isnothing(inc)
    if !isnothing(coeff)
      inc *= coeff
    end
    addeq!(es[1], inc)
  end

  # positive Fourier indices
  let
    # c0 occurs as a factor of the Fourier index, where we need nonzero contributions
    if c0 == 0   c0  = lvl end
    if c0m == 0  c0m = lvl end

    if k != 1
      for n in 1:ceil(Int,lvl*prc)-1
        npow = coeffring(n)^(k-1)

        inc = npow * coefftower(coeffring,MuInf(d0*n//lvl))
        if !isnothing(coeff)
          inc *= coeff
        end
        for nc in n*c0+1:n*lvl:ceil(Int,lvl*prc)
          addeq!(es[nc], inc)
        end

        if iseven(k)
          inc = npow * coefftower(coeffring,MuInf(d0m*n//lvl))
        else
          inc = - npow * coefftower(coeffring,MuInf(d0m*n//lvl))
        end
        if !isnothing(coeff)
          inc *= coeff
        end
        for nc in n*c0m+1:n*lvl:ceil(Int,lvl*prc)
          addeq!(es[nc], inc)
        end
      end
    else
      for n in 1:ceil(Int,lvl*prc)-1
        inc = coefftower(coeffring,MuInf(d0*n//lvl))
        if !isnothing(coeff)
          inc *= coeff
        end
        for nc in n*c0+1:n*lvl:ceil(Int,lvl*prc)
          addeq!(es[nc], inc)
        end

        inc = -coefftower(coeffring,MuInf(d0m*n//lvl))
        if !isnothing(coeff)
          inc *= coeff
        end
        for nc in n*c0m+1:n*lvl:ceil(Int,lvl*prc)
          addeq!(es[nc], inc)
        end
      end
    end
  end
end

function eisenstein_gamma1_type_wt2_constant_term_sum(
    lvl       :: Int,
    coeffring :: Ring
   )
   if lvl == 1
     lvlsf = 1
     lvls = 1
   else
     lvlfact = factor(ZZ(lvl))
     lvlsf = Int(prod(iseven(n) ? p^2 : p for (p,n) in lvlfact))
     lvls = Int(prod(iseven(n) ? p^(n>>1-1) : p^(n>>1) for (p,n) in lvlfact))
   end

   return coeffring(
       QQ(lvls^4,2*lvlsf)
     * sum(d * (lvlsf-d)
           * divexact(euler_phi(lvlsf), euler_phi(divexact(lvlsf, gcd(d,lvlsf))))
           * moebius_mu(divexact(lvlsf, gcd(d,lvlsf)))
           for d in 1:lvlsf)
     )
end

# Eisenstein series for Weil representation of hyperbolic plane

function eisenstein_weil_hyperbolic_plane_type_fourier_expansion(
    k         :: Int,
    lvl       :: Int,
    c0        :: Int,
    h0        :: Int,
    prc       :: Union{Int,Rational{Int}},
    coeffring :: R
   ) where {
    R <: Ring
   }
  @assert k > 2

  # sum e(d0*h0/N) G(c0,d0) / N
  if c0 < 0 || c0 >= lvl
    c0 = c0 % lvl
  end
  if h0 < 0 || h0 >= lvl
    h0 = h0 % lvl
  end
  
  c0m = c0 == 0 ? 0 : lvl - c0
  h0m = h0 == 0 ? 0 : lvl - h0


  es = [zero(coeffring) for _ in 1:ceil(Int,lvl*prc)]


  if c0 == 0
    if iseven(k)
      es[1] = coeffring(-divexact(bernoulli(k), k))
    end

    N = ZZ(lvl)
    es[1] = coeffring(- QQ(N^(k-1)) // k
                      * sum( binomial(k,i) * bernoulli(k-i)
                      * ((N-h0m)//N)^i
                      for i in 0:k )
                    )
  end

  let
    # c0 and h0 occur as a factor of the Fourier index, where we need nonzero contributions
    if c0 == 0   c0  = lvl end
    if c0m == 0  c0m = lvl end
    if h0 == 0   h0  = lvl end
    if h0m == 0  h0m = lvl end

    for n in h0:lvl:ceil(Int,lvl*prc)-1
      inc = coeffring(n)^(k-1)
      for nc in n*c0+1:n*lvl:ceil(Int,lvl*prc)
        addeq!(es[nc], inc)
      end
    end

    for n in h0m:lvl:ceil(Int,lvl*prc)-1
      inc = iseven(k) ? coeffring(n)^(k-1) : - coeffring(n)^(k-1)
      for nc in n*c0m+1:n*lvl:ceil(Int,lvl*prc)
        addeq!(es[nc], inc)
      end
    end
  end


  (fering,_) = fourier_expansion_ring(coeffring, prc; level=lvl)
  return fering(laurent_ring(fering)(
      es, ceil(Int,lvl*prc), ceil(Int,lvl*prc), 0, 1), lvl)
end

# Zaiger Eisenstein series and theta series

# TODO: rename to jacobi_theta_fourier_expansion
function jacobi_theta_weil_type(coeffring::R, prc::Int) where {R <: Ring}
  lat = Zlattice(gram = matrix(ZZ, fill(2,1,1)))
  disc = discriminant_group(lat)

  # TODO: implement this more efficiently
  th0 = fill(zero(coeffring), prc)
  th1 = fill(zero(coeffring), 4*prc)
  for n in -isqrt(4*prc-1):isqrt(4*prc-1)
    if iseven(n)
      th0[1+(n*n)>>2] += 1
    else
      th1[1+(n*n-1)>>2] += 1
    end
  end

  (fering,_) = fourier_expansion_ring(coeffring, prc; level=4)
  return ([iszero(h) ? fering(laurent_ring(fering)(th0, prc, prc, 0, 1), 1) :
                       fering(laurent_ring(fering)(th1, prc, 4*prc, 1, 4), 4)
           for h in disc],
          SL2RCoverWeight(1//2),
          weil_type(Mp1Z, lat)
          )
end

# TODO: rename to zagier_eisenstein_fourier_expansion
function zagier_eisenstein_weil_type(coeffring::R, prc::Int) where {R <: Ring}
  lat = Zlattice(gram = matrix(ZZ, fill(-2,1,1)))
  disc = discriminant_group(lat)

  hw = hurwitz_numbers(4*prc, coeffring)
  hw0 = [coeff(hw,4*n) for n in 0:prc-1]
  hw3 = [coeff(hw,4*n+3) for n in 0:prc-1]

  (fering,_) = fourier_expansion_ring(coeffring, prc; level=4)
# FIXME: the return type has different order than the other functions; also the
# half integgral case returns the weight. Find a consensus one when to return
# the weight and when not.
  return (weil_type(Mp1Z, lat),
          [iszero(h) ? fering(laurent_ring(fering)(hw0, prc, prc, 0, 1), 1) :
                       fering(laurent_ring(fering)(hw3, prc, 4*prc, 3, 4), 4)
           for h in disc]
         )
end

################################################################################
################################################################################
# evaluation and error estimates
################################################################################
################################################################################

function _evaluate(
    es             :: EisensteinSL2Z,
    tau            :: acb,
    cc_muinf_tower :: CCMuInfTower
   )
  k = weight(es)

  CC = parent(tau)
  if isodd(k) || k < 0 || k == 2
    return zero(CC)
  elseif k == 0
    return one(CC)
  end

  # we renormalize to account for the constant term
  return (-CC(k)//bernoulli(k)
         * _evaluate(EisensteinSL2ZGamma1(k,1,0,0), tau, cc_muinf_tower))
end

function _evaluate(
    es             :: Union{EisensteinSL2ZGamma0,EisensteinSL2ZGamma1},
    tau            :: acb,
    cc_muinf_tower :: CCMuInfTower
   )
  lvl = level(es)
  (err, prc) = _fourier_expansion_tail_error(es, tau; maxerr = radius(abs(tau)))

  # TODO: Why do we need to adjust the precision by a factor lvl? This is a
  # hint that we are internally not working with precisions or that we compute
  # too much.
  fe = _fourier_expansion(es, div(prc-1,lvl)+1, cc_muinf_tower)
  s = AbstractAlgebra.Generic.scale(fe)
  feeval = sum(coeff(fe, n//s) * cispi(2*n*tau//s)
               for n in 0:s*precision(fe)-1)

  return feeval + err
end

function _fourier_expansion_tail_error(
    es             :: EisensteinSL2ZGamma1,
    tau            :: acb,
    cc_muinf_tower :: CCMuInfTower
   )
  lvl = level(es)
  (err, prc) = _fourier_expansion_tail_error(es, tau; maxerr = radius(abs(tau)))

  # TODO: Why do we need to adjust the precision by a factor lvl? This is a
  # hint that we are internally not working with precisions or that we compute
  # too much.
  fe = _fourier_expansion(es, div(prc-1,lvl)+1, cc_muinf_tower)
  s = AbstractAlgebra.Generic.scale(fe)
  feeval = sum(coeff(fe, n//s) * cispi(2*n*tau//s)
               for n in 0:s*precision(fe)-1)

  return feeval + err
end

function _fourier_expansion_tail_error(
    es     :: EisensteinSL2ZGamma0,
    tau    :: acb;
    maxerr :: arb
   ) :: Tuple{arb, Int}
  k = weight(es)
  lvl = level(es)

  if k != 2 || !isone(character(es))
    (err, prc) = _eisenstein_fourier_expansion_tail_error(k, lvl, tau; maxerr)
    # NOTE: Since we define Eisenstein series for characters as a sum of
    # Eisenstein series for Gamma1, the error bound needs to be adjusted
    # correspondingly.
    return (euler_phi(lvl) * err, prc)
  elseif lvl != 1 # k == 2
    # We split up the allowed error between the two quasimodular Eisenstein
    # series.
    maxerr *= 1//2
    (err_lvl, prc_lvl) = _eisenstein_fourier_expansion_tail_error(k, lvl, tau; maxerr)
    (err_1, prc_1) = _eisenstein_fourier_expansion_tail_error(k, 1, tau; maxerr)
    return (euler_phi(lvl) * err_lvl + err_1, max(prc_lvl, prc_1))
  else # k == 2 && lvl == 1
    RR = parent(imag(tau))
    return (zero(RR), 1)
  end
end

function _fourier_expansion_tail_error(
    es     :: EisensteinSL2ZGamma1,
    tau    :: acb;
    maxerr :: arb
   ) :: Tuple{arb, Int}
  k = weight(es)
  lvl = level(es)

  if k != 2
    return _eisenstein_fourier_expansion_tail_error(k, lvl, tau; maxerr)
  elseif lvl != 1 # k == 2
    # We split up the allowed error between the two quasimodular Eisenstein
    # series.
    maxerr *= 1//2
    (err_lvl, prc_lvl) = _eisenstein_fourier_expansion_tail_error(k, lvl, tau; maxerr)
    (err_1, prc_1) = _eisenstein_fourier_expansion_tail_error(k, 1, tau; maxerr)
    return (err_lvl + err_1, max(prc_lvl, prc_1))
  else # k == 2 && lvl == 1
    RR = parent(imag(tau))
    return (zero(RR), 1)
  end
end

# Computes a expansion depth prc and the evaluation bound that it implies.
# This is based on a comparison with
#   \sum_{n=prc}^\infty \sigma_{k-1}(n) e(n \tau / lvl) .
function _eisenstein_fourier_expansion_tail_error(
    k      :: Int,
    lvl    :: Int,
    tau    :: acb;
    maxerr :: arb
    ) :: Tuple{arb,Int}
  # Since we take the trace over the action of the center, each component is
  # bounded by 2 \sum_{n=n_0} ...
  maxerr *= 1//2

  CC = parent(tau)
  RR = parent(imag(tau))

  if maxerr <= 0
    maxerr = RR("1e-10")
  end



  err_pre = lvl // (2*const_pi(RR)*imag(tau))
  err = nothing

  prc = ceil(-err_pre * log(maxerr))
  prc = Int(unique_integer(midpoint(prc)+radius(prc))[2])
  prc = max(16, prc) - lvl

  while isnothing(err) || err > maxerr
    # We can add lvl, since we divide by lvl later when computing the Fourier
    # expansion.
    prc += lvl

    if k >= 2
      eps = log(log(log(RR(prc)))) // log(RR(prc))
      if prc < 1 + (k-1+eps) * err_pre
        err = nothing
      else
        err = ( 1.7813 * err_pre^(k+eps) * gamma(RR(k+eps), RR(prc-1)//err_pre)
              + 0.6359 * err_pre^k * gamma(RR(k), RR(prc-1)//err_pre)
              )
      end
    else
      # we use the trivial estimate \simga_0(n) \le n
      err = err_pre^2 * gamma(RR(2), RR(prc-1)//err_pre)
    end
  end

  return (ball(zero(RR), err), prc)
end

end

import .ModularFormSL2ZEisensteinCongruence_MODULE
