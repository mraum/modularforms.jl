function hecke(n::Union{Int,fmpz}, wt::SL2RCoverWeight, rho::ArithType{G,I}, f::Vector{A}) where {
    G, I, A <: ModularFormVectorEntry}
  hecke(hecke_matrices(n, group(rho)), wt, rho, f)
end

function hecke_new(n::Union{Int,fmpz}, wt::SL2RCoverWeight, rho::ArithType{G,I}, f::Vector{A}) where {
    G, I, A <: ModularFormVectorEntry}
  hecke(hecke_new_matrices(n, group(rho)), wt, rho, f)
end

function hecke(hecke_mats::Vector{fmpz_mat}, wt::SL2RCoverWeight, rho::ArithType{G,I}, f::Vector{A}) where {
    G <: SL2ZCoverElement, I,
    T <: Union{cf_elem, acb},
    A <: Union{PuiseuxSeriesElem{T},
               PuiseuxSeriesFieldElem{T}}
   }
  # we need triangular representatives
  _issl2z(base_group(group(rho))) || error("not implemented")

  if isempty(f)
    return elemtype(f)[]
  end

  fering = parent(first(f))
  q = gen(fering)
  coeffring = base_ring(fering)

  heckef = Vector{A}(undef, length(hecke_mats)*dim(rho))
  for (mx,m) in enumerate(hecke_mats)
    a,b,d = Int(m[1,1]), Int(m[1,2]), Int(m[2,2])
    if isone(denominator(wt.k))
      mnormalization = QQ(a^(Int(numerator(wt.k))-1)) // d
    elseif isone(denominator(2*wt.k))
      mnormalization = qqab_sqrt(a)^(Int(numerator(2*wt.k))-2) // d
    else
      error("only half-integral weights are implemented")
    end
    mnormalization = coeffring(mnormalization)

    for ix in 1:dim(rho)
      s = AbstractAlgebra.Generic.scale(f[ix])
      # TODO: implement fast variant
      if precision(f[ix]) > valuation(f[ix])
        heckef[(mx-1)*dim(rho) + ix] =
            sum(mnormalization * coeffring(MuInf(b*n//(d*s))) * coeff(f[ix],n//s)
                * q^(a*n//(d*s))
                for n in Int(s*valuation(f[ix])):Int(s*precision(f[ix])-1)
                if !iszero(coeff(f[ix],n//s)))
      else
        heckef[(mx-1)*dim(rho) + ix] = zero(fering)
      end
    end
  end

  return heckef
end
