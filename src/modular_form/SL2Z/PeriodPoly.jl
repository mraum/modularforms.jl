@doc Markdown.doc"""
    period_polynomial_space(degree::Int)

Return the space of possible periodic polynomials with the basis on respective
column.
"""
function PeriodPolynomials(
    deg       :: Int,
    base_ring :: Ring = QQ
   )
  if characteristic(base_ring) != 0 || !(typeof(base_ring) <: AbstractAlgebra.Field)
    error("not implemented")
  end

  R, x = PolynomialRing(ZZ, "x")

  # We use the invariant pairing to implement the period polynomial relations.
  relmat = zero_matrix(QQ, deg + 2, deg + 1)
  for j in 0:deg ÷ 2
    # p | 1 + U + U^2 = 0
    p = x^j + (-x + 1)^(deg - j) + (-x + 1)^j * (-x)^(deg - j)
    for i in 0 : deg
      # The scalar factor arises from the pairing.
      relmat[j + 1, i + 1] = (-1)^i * inv(QQ(binomial(deg,i))) * coeff(p, deg-i)
    end
  end
  for j in deg ÷ 2:deg
    # p | 1 + S = 0
    p = x^j + (-1)^j * x^(deg - j)
    for i in 0:deg
      relmat[j + 2, i + 1] = coeff(p, i)
    end
  end

  basis = nullspace(change_base_ring(ZZ, denominator(relmat) * relmat))[2]
  return PeriodPolySL2ZSpace(base_ring, deg, basis)
end

base_ring(pps :: PeriodPolySL2ZSpace) = pps.coeff_ring

dim(pps :: PeriodPolySL2ZSpace) = ncols(pps.basis)

degree(pps :: PeriodPolySL2ZSpace) = nrows(pps.basis)-1

# TODO: find better name/interface and then export
function from_coefficients(
    pps :: PeriodPolySL2ZSpace{R},
    cs  :: Vector{R}
   ) where R
  d = degree(pps)
  l = length(cs)
  if l > d+1
    throw(DomainError(cs, "too many initial coefficients"))
  end

  pps_mat = matrix(base_ring(pps),
                   vcat([transpose(coefficients(phi)[1:l]) for phi in basis(pps)]...))
  rk,pps_matrref = rref(pps_mat)
  if rk == l
    coords = solve_left(pps_mat, matrix(base_ring(pps), transpose(cs)))
  else
    pivots = [first(j for j in 1:l if !contains_zero(pps_matrref[i,j])) for i in 1:rk]
    coords = solve_left(pps_mat[:,pivots],
                        matrix(base_ring(pps), transpose(cs[pivots])))
    # TODO: this is not quite the correct test over inexact fields
    if coords * pps_mat != matrix(base_ring(pps), transpose(cs))
      throw(DomainError(cs, "element does not lie space"))
    end
  end

  return PeriodPolySL2Z(pps, [coords[1,i] for i in 1:dim(pps)])
end

parent(pp :: PeriodPolySL2Z) = pp.parent

function basis(pps :: PeriodPolySL2ZSpace)
  [PeriodPolySL2Z(pps, unitvec(base_ring(pps), dim(pps), i)) for i in 1:dim(pps)]
end

# TODO: This is not an ideal interface. We need a possibility to write a period
# polynomial as a linear combination of integral ones. This is relevant when
# evaluating cocycles at reasonably large SL(2,Z) elements.
function integral_basis(pps :: PeriodPolySL2ZSpace{R}) where R
  basis(PeriodPolySL2ZSpace(ZZ, pps.degree,pps.basis))
end

coordinates(pp :: PeriodPolySL2Z{R}) where R = pp.coord

function coefficients(pp :: PeriodPolySL2Z{R}) where R
  d = dim(parent(pp))
  if d == 0
    return [zero(base_ring(parent(pp))) for i in 0:degree(parent(pp))]
  end
  b = parent(pp).basis
  c = pp.coord
  return [sum(b[i,j]*c[j,1] for j in 1:d) for i in 1:degree(parent(pp))+1]
end

function coeff(pp :: PeriodPolySL2Z{R}, n::Int) where R
  d = dim(parent(pp))
  d == 0 && return zero(base_ring(parent(pp)))

  b = parent(pp).basis
  c = pp.coord
  return sum(b[n+1,j]*c[j,1] for j in 1:d)
end

# value of the right cocycle associated with a period polynomial
function right_cocycle_value(pp :: PeriodPolySL2Z{R}, ga :: SL2zElem) where R
  # Given a parabolic double coset word (l,[n1,...],r) for ga, we use
  # phi(ga) = phi(S) (((T^n2 S + 1) T^n3 + 1) ...) T^r
  (_,ns,r) = _word_in_tns(ga)

  if isempty(ns)
    return [zero(base_ring(parent(pp))) for d in 0:degree(parent(pp))]
  end

  pp =  coefficients(pp)
  val = copy(pp)
  for n in ns[2:end]
    # This should be the value at T^n1 S T^n2 S ... T^ni S
    val = pp .+ _act_symmetric_power_polynomial(val, SL2zElem(n, -1, 1, 0))
  end
  val = _act_symmetric_power_polynomial(val, SL2zElem(1, r, 0, 1))
  return val
end

function _act_symmetric_power_polynomial(
    cs :: Vector{C},
    ga :: SL2zElem
   ) where {C <: RingElem}
  isempty(cs) && return C[]

  # Write δ for the degree.
  # The action of [a b; c d] on X^j is given by (a X + b)^j (c X + d)^(deg-j).
  # For the contribution of X^m this yields
  #   (a X + b)^m (c X + d)^(δ-m)
  # = ∑_{i=0}^m ∑_{j=0}^{δ-m} binom(m,i) binom(δ-m,j) a^i b^n-i c^j d^δ-n-j X^i+j
  # Hence the contribution to the coefficient of X^n, n = i+j is
  # ∑_{m=0}^δ \sum_{i=0}^m \chi_{j=n-i, 0 ≤ j \le δ-m} ...
  # The summation over i hence required i ≤ n and i ≥ n+m - δ

  R = parent(first(cs))
  deg = length(cs)-1

  a,b,c,d = map(ZZ, (ga.a,ga.b,ga.c,ga.d))
  apw = zeros(ZZ,deg+1)
  bpw = zeros(ZZ,deg+1)
  cpw = zeros(ZZ,deg+1)
  dpw = zeros(ZZ,deg+1)
  apw[1] = 1; apw[2] = a
  bpw[1] = 1; bpw[2] = b
  cpw[1] = 1; cpw[2] = c
  dpw[1] = 1; dpw[2] = d
  for i in 2:deg
    apw[i+1] = mul!(apw[i+1], apw[i], a)
    bpw[i+1] = mul!(bpw[i+1], bpw[i], b)
    cpw[i+1] = mul!(cpw[i+1], cpw[i], c)
    dpw[i+1] = mul!(dpw[i+1], dpw[i], d)
  end

  binomial_cache = zeros(ZZ,deg+1,deg+1)
  for n in 0:deg, i in 0:n
    binomial_cache[n+1,i+1] = binomial(ZZ(n),ZZ(i))
  end

  if R != ZZ
    apw = map(R, apw)
    bpw = map(R, bpw)
    cpw = map(R, cpw)
    dpw = map(R, dpw)
    binomial_cache = map(R, binomial_cache)
  end

  tmp = zero(R)

  res = zeros(R,deg+1)
  for n in 0:deg
    for m in 0:deg, i in max(0,n+m-deg):min(m,n)
      j = n-i;
      tmp = mul!(tmp, cs[m+1], binomial_cache[1+m,1+i])
      tmp = mul!(tmp, tmp, binomial_cache[1+deg-m,1+j])
      tmp = mul!(tmp, tmp, apw[1+i])
      tmp = mul!(tmp, tmp, bpw[1+m-i])
      tmp = mul!(tmp, tmp, cpw[1+j])
      tmp = mul!(tmp, tmp,  dpw[1+deg-m-j])
      res[n+1] = addeq!(res[n+1], tmp)
    end
  end
  return res
end

function pairing(
    cs1 :: PeriodPolySL2Z{fmpq},
    cs2 :: acb_poly
  )
  CC = base_ring(parent(cs2))
  deg = degree(parent(cs1))
  return sum((-1)^(deg-ix)*CC(binomial(ZZ(deg),ZZ(ix)))^(-1)*CC(coeff(cs1,ix))*coeff(cs2,deg-ix) for ix in 0:deg)
end
