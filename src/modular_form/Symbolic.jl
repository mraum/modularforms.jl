################################################################################
# Fourier expansion
################################################################################

function fourier_coefficient_muinf_order(
    f :: ModularFormSymbolic
   ) where {RT,R}
  error("not implemented for symbolic expresions of type $(typeof(f))")
end

function _fourier_expansion(
    f   :: ModularFormSymbolic,
    prc :: Union{Int,Rational{Int}};
    fourier_expansion_ring            :: Ring,
    coefficient_ring_tower            :: MuInfTower,
    fourier_expansion_cache           :: Dict,
    fourier_expansion_cache_converted :: Dict
   )
  if haskey(fourier_expansion_cache_converted, f)
    return fourier_expansion_cache_converted[f]
  end

  # We compute the Fourier expansion in the smaller ring and then convert.
  if !haskey(fourier_expansion_cache, f)
    fourier_expansion_cache[f] =
        _fourier_expansion(f, prc, coefficient_ring_tower)
  end

  if parent(fourier_expansion_cache[f]) == fourier_expansion_ring
    fourier_expansion_cache_converted[f] = fourier_expansion_cache[f]
  else
    coeffconvert = map_from_cyclotomic_tower(coefficient_ring_tower,
                       base_ring(parent(fourier_expansion_cache[f])),
                       base_ring(fourier_expansion_ring))
    fourier_expansion_cache_converted[f] =
        change_base_ring(fourier_expansion_cache[f],
                         coeffconvert, fourier_expansion_ring)
  end
  
  return fourier_expansion_cache_converted[f]
end

function _fourier_expansion(
    f                      :: ModularFormSymbolic,
    prc                    :: Union{Int,Rational{Int}},
    coefficient_ring_tower :: MuInfTower
   )
  error("need to overwrite this function for each symbolic modular form")
end

################################################################################
# evaluation
################################################################################

function _evaluate(
    f                :: ModularFormSymbolic,
    tau              :: acb;
    cc_muinf_tower   :: CCMuInfTower,
    evaluation_cache :: Dict
   )
  if !haskey(evaluation_cache, f)
    evaluation_cache[f] = _evaluate(f, tau, cc_muinf_tower)
  end

  return evaluation_cache[f]
end

function _evaluate(
    f              :: ModularFormSymbolic,
    tau            :: acb,
    cc_muinf_tower :: CCMuInfTower
   )
  error("need to overwrite this function for each symbolic modular form")
end
