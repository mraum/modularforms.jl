################################################################################
# properties
################################################################################

function group(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  if ismissing(rho.group)
    # We pullback via left conjucation, so the group is right conjucated.
    rho.group = right_conjugate(pullback_elements(rho)..., base_group(rho))
  end
  rho.group
end

function base_type(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  rho.base_type
end

function base_group(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  group(base_type(rho))
end

function image_parent(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  rho.image_parent
end

function pullback_elements(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  (rho.pullback_elem, rho.pullback_elem_inv)
end

################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    pullback(f::Map{D,C}, rho::ArithType{G,I}) where {D,C,G,I}

> Pullback of an arithmetic type $G \rightarrow \mathrm{GL}(V)$ via a
> homomorphisms $H \rightarrow G$.
"""
function pullback(f::Map{D,C}, rho::ArithType{G,I}) where {D,C,G,I}
  if codomain(f) != group(rho)
    throw(DomainError(codomain(f), "incompatible groups"))
  end
  return ArithmeticType(domain(f), image_parent(rho),
             [rho(f(g)) for g in gens(domain(f))])
end

function left_pullback(g::G, rho::ArithType{G,I}) where {G,I}
  if parent(g) != group(rho)
    throw(DomainError(g, join("when pulling back in an ambient group, ",
                              "the domain group has to be passed as an argument")))
  end
  return left_pullback(g, group(rho), rho)
end

function right_pullback(g::G, rho::ArithType{G,I}) where {G,I}
  if parent(g) != group(rho)
    throw(DomainError(g, join("when pulling back in an ambient group, ",
                              "the domain group has to be passed as an argument")))
  end
  return right_pullback(g, group(rho), rho)
end

@doc Markdown.doc"""
    pullback(g::GH, domain_grp::Union{Missing, Group}, rho::ArithType{G,I}) where {G, GH <: GroupElem, I}

  Pull back an arithmetic type along the map $h \mapsto g h g^{-1}$.
"""
function left_pullback(
    g          :: GH,
    domain_grp :: Union{Type, Group},
    rho        :: ArithType{G,I}
   ) where {
    G, GH <: GroupElem, I
   }
  left_pullback(g, inv(g), domain_grp, rho)
end

function left_pullback(
    g          :: GH,
    ginv       :: GH,
    domain_grp :: Union{Type, Group},
    rho        :: ArithType{G,I}
   ) where {
    G, GH <: GroupElem, I
   }
  ginv = inv(g)
  if domain_grp isa Type
    return PullbackConjArithType{G,domain_grp,GH,I}(missing, rho, g, ginv)
  else
    return PullbackConjArithType{G,elem_type(domain_grp),GH,I}(
               domain_grp, rho, g, ginv)
  end
end

function right_pullback(
    g          :: GH,
    domain_grp :: Union{Type, Group},
    rho        :: ArithType{G,I}
   ) where {
    G, GH <: GroupElem, I
   }
  left_pullback(inv(g), g, domain_grp, rho)
end

function right_pullback(
    g          :: GH,
    ginv       :: GH,
    domain_grp :: Union{Type, Group},
    rho        :: ArithType{G,I}
   ) where {
    G, GH <: GroupElem, I
   }
  left_pullback(ginv, g, domain_grp, rho)
end

function GenericType(rho::PullbackConjArithType{G,H,GH,I}) where {G,H,GH,I}
  h, hinv = pullback_elements(rho)
  hgrp = parent(h)
  grp = base_group(rho)
  return pullback(
      map_from_func(g -> grp(h * hgrp(g; check = false) * hinv; check = false),
                    group(rho), grp),
      base_type(rho))
end

################################################################################
# evaluation
################################################################################

function (rho::PullbackConjArithType{G1,H,GH,I})(g::G2) where {G1,G2,H,GH,I}
  h, hinv = pullback_elements(rho)
  return base_type(rho)(h * parent(h)(g) * hinv)
end
