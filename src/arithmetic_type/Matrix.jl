################################################################################
# creation
################################################################################

function MatrixType(
    group::Group,
    gen_images::Vector{Matrix{R}}
   ) where {
    G, R <: RingElem
   }
  isempty(gen_images) && throw(DomainError(gen_images, "gen_images may not be empty"))
  (r,c) = size(gen_images[1])
  (r != c || r == 0) && throw(DomainError(gen_images, "matrices must be positve size square"))
  for g in gen_images[2:end]
    size(g) == (r,r) || throw(DomainError(g, "matrices must be of same size"))
  end

  ring = parent(gen_images[1][1,1])
  matgroup = MatrixGroup(ring, r)

  return ArithmeticType(group, matgroup, map(matgroup, gen_images))
end
