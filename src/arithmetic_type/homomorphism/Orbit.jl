################################################################################
# orbits with stabilizer for twisted permutations
################################################################################

function orbits(
    tperms::Vector{TwistedPerm{T}};
    check::Bool = false
   ) where {G,T}
  if check && isempty(tperms)
    throw(DomainError(tperms, "orbits for empty list of twisted permutations"))
  end

  tgp = parent(tperms[1])
  tsz = permsize(tgp)
  if check
    for tp in tperms[2:end]
      if parent(tp) != tgp
        throw(DomainError(tp, "incompatible parent"))
      end
    end
  end

  # We store permutations as a matrix, whose columns hold the image
  # of a given indices under all generators. In other words, the rows
  # correspond to the elements of gens.
  perms = vcat([reshape(ModularForms.perms(tp), 1, tsz) for tp in tperms]...) :: Matrix{Int}

  # The result variable, whose elements are tuples with the following components:
  # First: An orbit
  # Second: A distinguished element of the orbit.
  res = Tuple{Vector{Int}, Int}[]

  unprocessed = Base.Set{Int}(1:tsz)
  while !isempty(unprocessed)
    ix = pop!(unprocessed)

    orbit_unprocessed = Base.Set{Int}([ix])
    orbit = Int[ix]
    orbit_init = ix

    while !isempty(orbit_unprocessed)
      ix = pop!(orbit_unprocessed)
      for gx in 1:ngens
        jx = perms[gx,ix]
        ojx = searchsorted(orbit, jx)
        if isempty(ojx)
          ojx = first(ojx)
          insert!(orbit, ojx, jx)
          push!(orbit_unprocessed, jx)
          delete!(unprocessed, jx)
        end
      end
    end

    push!(res, (orbit, orbit_init))
  end

  return res
end

################################################################################
# orbits with stabilizer for twisted permutations
################################################################################

function extended_orbits(
    tperms::Vector{TwistedPerm{T}};
    check::Bool = false
   ) where T
  # NOTE: Since we need stabilizer_twists and orbit_twists, there is very
  # little to save by a separate implementation that discards stabilizer_gens
  # and orbit_gens.
  return [(orbit, orbit_init, nothing, stabilizer_twists, nothing, orbit_twists)
      for (orbit, orbit_init,
           stabilizer_gens, stabilizer_twists,
           orbit_gens, orbit_twists) in
      extended_orbits(fill(TrivialGroupElem(), length(tperms)), tperms; check = check)
      ]
end

function extended_orbits(
    gens::Vector{G},
    tperms::Vector{TwistedPerm{T}};
    check::Bool = false
   ) where {G,T}
  if check && isempty(tperms)
    throw(DomainError(tperms, "orbits for empty list of twisted permutations"))
  end

  ngens = length(gens)
  if check && ngens != length(tperms)
    throw(DomainError(gens, "incompatible number of generators"))
  end

  tgp = parent(tperms[1])
  tsz = permsize(tgp)
  if check
    for tp in tperms[2:end]
      if parent(tp) != tgp
        throw(DomainError(tp, "incompatible parent"))
      end
    end
  end

  gp = parent(gens[1])
  if check
    for g in gens
      if parent(g) != gp
        throw(DomainError(g, "incompatible parent"))
      end
    end
  end

  # We store permutations and twists as a matrix, whose columns hold the image
  # of a given indices under all generators. In other words, the rows
  # correspond to the elements of gens.
  perms = vcat([reshape(ModularForms.perms(tp), 1, tsz) for tp in tperms]...) :: Matrix{Int}
  twists = vcat([reshape(ModularForms.twists(tp), 1, tsz) for tp in tperms]...) :: Matrix{T}

  # The result variable, whose elements are tuples with the following components:
  # First: An orbit
  # Second: A distinguished element of the orbit.
  # Third: Generators for the stabilizer of the distinguished element.
  # Fourth: Generators for the twists associated with the stabilizer
  # Fifth: Twists associated with each element of the orbit, unique up to
  # twists associated with the stabilizer.
  res = Tuple{Vector{Int}, Int, Vector{G}, Vector{T}, Vector{G}, Vector{T}}[]

  unprocessed = Base.Set{Int}(1:tsz)
  while !isempty(unprocessed)
    ix = pop!(unprocessed)

    orbit_unprocessed = Base.Set{Int}([ix])
    orbit = Int[ix]
    orbit_init = ix
    stabilizer_gens = G[]
    stabilizer_twists = T[]
    orbit_gens = G[one(gp)]
    orbit_twists = T[one(twist_parent(tgp))]

    while !isempty(orbit_unprocessed)
      ix = pop!(orbit_unprocessed)
      for gx in 1:ngens
        jx = perms[gx,ix]
        oix = searchsortedfirst(orbit, ix)
        ojx = searchsorted(orbit, jx)
        if isempty(ojx)
          ojx = first(ojx)
          insert!(orbit, ojx, jx)
          insert!(orbit_twists, ojx,
                  twists[gx,ix] * orbit_twists[oix])
          insert!(orbit_gens, ojx,
                  gens[gx] * orbit_gens[oix])
          push!(orbit_unprocessed, jx)
          delete!(unprocessed, jx)
        else
          ojx = first(ojx)
          push!(stabilizer_twists,
                inv(orbit_twists[ojx]) * twists[gx,ix] * orbit_twists[oix])
          push!(stabilizer_gens,
                inv(orbit_gens[ojx]) * gens[gx] * orbit_gens[oix])
        end
      end
    end

    push!(res,
        (orbit, orbit_init,
         stabilizer_gens, stabilizer_twists,
         orbit_gens, orbit_twists)
       )
  end

  return res
end

