################################################################################
################################################################################
# type functions
################################################################################
################################################################################

function elem_type(
    ::Type{GenArithTypeHomSpace{D,C}}
   ) where {D,C}
  GenArithTypeHom{D,C}
end

function parent_type(
    ::Type{GenArithTypeHom{D,C}}
   ) where {D,C}
  GenArithTypeHomSpace{D,C}
end

function parent(
    a::GenArithTypeHom{D,C}
   ) where {D,C}
  a.parent
end

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# creation
################################################################################

function (hspace::GenArithTypeHomSpace{D,C})(
    orbits_init   :: Vector{Int},
    orbits_cosets :: Vector{Vector{G}},
    mat           :: Union{Nothing, MatElem} = nothing
   ) where {
    D, C, G
   }
  store_type = _image_store_type(hspace)
  if isnothing(mat)
    @assert store_type == :twist_trivial || store_type == :twist_root_of_unity
  else
    @assert store_type == :twist_matrix
  end
  return GenArithTypeHom{D,C}(hspace, orbits_init, orbits_cosets, mat)
end

function (hspace::GenArithTypeHomSpace{D,C})(
    orbits_init   :: Int,
    orbits_cosets :: Vector{G},
    mat          :: Union{Nothing, MatElem} = nothing
   ) where {
    D, C, G
   }
  hspace([orbits_init], [orbits_cosets], mat)
end

function (hspace::GenArithTypeHomSpace{D,C})(
    mat :: MatElem
   ) where {
    D, C,
   }
  hspace(Int[], Vector{elem_type(group(domain(hspace)))}[], mat)
end


################################################################################
# creation : twists by roots of unity
################################################################################

function homspace(
    rho::TrivialArithType{G,I},
    sigma::Union{ArithType{G,TwistedPerm{Tsigma}},ArithType{G, RootOfUnity}}
   ) where {
    G,
    I <: Union{RootOfUnity,TrivialGroupElem},
    Tsigma <: Union{RootOfUnity,TrivialGroupElem}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
    [homspace(oi,ocs) for (oi,ocs) in _isotrivial_subtype(sigma)]
  return homspace
end

function homspace(
    rho::Union{ArithType{G,TwistedPerm{Trho}},ArithType{G, RootOfUnity}},
    sigma::TrivialArithType{G,Isigma}
   ) where {
    G,
    Trho <: Union{RootOfUnity,TrivialGroupElem},
    Isigma <: Union{RootOfUnity,TrivialGroupElem}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
    [homspace(oi,ocs) for (oi,ocs) in _isotrivial_subtype(dual(rho))]
  return homspace
end

#FIXME: Basis doesn't work for hom(chi, chi)
function homspace(
    rho::  Union{ArithType{G,TwistedPerm{Trho}},  ArithType{G,RootOfUnity}},
    sigma::Union{ArithType{G,TwistedPerm{Tsigma}},ArithType{G,RootOfUnity}}
   ) where {
    G,
    Trho <: Union{RootOfUnity,TrivialGroupElem},
    Tsigma <: Union{RootOfUnity,TrivialGroupElem}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
    [homspace(oi,ocs) for (oi,ocs) in _isotrivial_subtype(dual(rho) ⊗ sigma)]
  return homspace
end

################################################################################
# creation : twists by matrices
################################################################################

function homspace(
    rho::TrivialArithType{G,Irho},
    sigma::ArithType{G,Isigma}
  ) where {
   G,
   Irho <: Union{RootOfUnity,TrivialGroupElem},
   Isigma <: Union{MatrixGroupElem,WeilTransformation{G}}
  }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
    [homspace(deepcopy(mat)) for mat in _isotrivial_subtype(sigma)]
  return homspace
end

function homspace(
    rho::TrivialArithType{G,Irho},
    sigma::ArithType{G,TwistedPerm{Tsigma}}
   ) where {
    G,
    Irho <: Union{RootOfUnity,TrivialGroupElem},
    Tsigma <: Union{MatrixGroupElem,WeilTransformation{G}}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
      [homspace(oi,ocs,deepcopy(mat))
        for (oi,ocs,mat) in _isotrivial_subtype(sigma)]
  return homspace
end

function homspace(
    rho::ArithType{G,Irho},
    sigma::TrivialArithType{G,Isigma}
   ) where {
    G,
    Irho <: Union{MatrixGroupElem,WeilTransformation{G}},
    Isigma <: Union{RootOfUnity,TrivialGroupElem}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
      [homspace(deepcopy(transpose(mat)))
        for mat in _isotrivial_subtype(dual(rho))]
  return homspace
end

function homspace(
    rho::ArithType{G,TwistedPerm{Trho}},
    sigma::TrivialArithType{G,Isigma}
   ) where {
    G,
    Trho <: Union{MatrixGroupElem,WeilTransformation{G}},
    Isigma <: Union{RootOfUnity,TrivialGroupElem}
   }
  homspace = GenArithTypeHomSpace(rho, sigma)

  homspace.basis =
      [homspace(oi,ocs,transpose(deepcopy(mat)))
        for (oi,ocs,mat) in _isotrivial_subtype(dual(rho))]
  return homspace
end

# We need to instantiate three variants in order to avoid ambiguity with the
# variants twisted by roots of unity.
for (_Trho,_Tsigma) in [
    (:(Union{RootOfUnity,TrivialGroupElem}),
     :(Union{MatrixGroupElem,WeilTransformation{G}})),
    (:(Union{MatrixGroupElem,WeilTransformation{G}}),
     :(Union{RootOfUnity,TrivialGroupElem})),
    (:(Union{MatrixGroupElem,WeilTransformation{G}}),
     :(Union{MatrixGroupElem,WeilTransformation{G}}))
   ]
@eval begin

function homspace(
    rho::ArithType{G,Irho},
    sigma::ArithType{G,Isigma}
   ) where {
    G,
    Irho <: $_Trho,
    Isigma <: $_Tsigma
   }
  function _reshape(mat::MatElem, nr, nc)
    @assert ncols(mat) == 1
    @assert nrows(mat) == nr*nc
    matrix(base_ring(mat), [mat[ix+(jx-1)*nr,1] for ix in 1:nr, jx in 1:nc])
  end

  dimrho = dim(image_parent(rho))
  dimsigma = dim(image_parent(sigma))

  # We convert dual(rho) ⊗ sigma to GenArithType, since invariants can anyway
  # only be determined by evaluating the Kronecker product.
  # REMARK: In my tests, this was actually slower anyway.
  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
      [homspace(_reshape(deepcopy(mat), dimsigma, dimrho))
        for mat in _isotrivial_subtype(dual(rho) ⊗ sigma)]
  return homspace
end

function homspace(
    rho::ArithType{G,Irho},
    sigma::ArithType{G,TwistedPerm{Tsigma}}
   ) where {
    G,
    Irho <: $_Trho,
    Tsigma <: $_Tsigma
   }
  function _reshape(mat::MatElem, nr, nc)
    @assert ncols(mat) == 1
    @assert nrows(mat) == nr*nc
    matrix(base_ring(mat), [mat[ix+(jx-1)*nr,1] for ix in 1:nr, jx in 1:nc])
  end

  dimrho = dim(image_parent(rho))
  dimsigma = dim(twist_parent(image_parent(sigma)))

  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
      [homspace(oi,ocs, _reshape(deepcopy(mat), dimsigma, dimrho))
        for (oi,ocs,mat) in _isotrivial_subtype(dual(rho) ⊗ sigma)]
  return homspace
end

function homspace(
    rho::ArithType{G,TwistedPerm{Trho}},
    sigma::ArithType{G,Isigma}
   ) where {
    G,
    Trho <: $_Trho,
    Isigma <: $_Tsigma
   }
  function _reshape(mat::MatElem, nr, nc)
    @assert ncols(mat) == 1
    @assert nrows(mat) == nr*nc
    matrix(base_ring(mat), [mat[ix+(jx-1)*nr,1] for ix in 1:nr, jx in 1:nc])
  end

  dimrho = dim(twist_parent(image_parent(rho)))
  dimsigma = dim(image_parent(sigma))

  homspace = GenArithTypeHomSpace(rho, sigma)
  homspace.basis =
      [homspace(oi,ocs, _reshape(deepcopy(mat), dimsigma, dimrho))
        for (oi,ocs,mat) in _isotrivial_subtype(dual(rho) ⊗ sigma)]
  return homspace
end

function homspace(
    rho::ArithType{G,TwistedPerm{Trho}},
    sigma::ArithType{G,TwistedPerm{Tsigma}}
   ) where {
    G,
    Trho <: $_Trho,
    Tsigma <: $_Tsigma
   }
  function _reshape(mat::MatElem, nr, nc)
    @assert ncols(mat) == 1
    @assert nrows(mat) == nr*nc
    matrix(base_ring(mat), [mat[ix+(jx-1)*nr,1] for ix in 1:nr, jx in 1:nc])
  end

  dimrho = dim(twist_parent(image_parent(rho)))
  dimsigma = dim(twist_parent(image_parent(sigma)))

  homspace = GenArithTypeHomSpace(rho, sigma)

  homspace.basis =
      [homspace(oi, ocs, _reshape(deepcopy(mat), dimsigma, dimrho))
        for (oi,ocs,mat) in _isotrivial_subtype(dual(rho) ⊗ sigma)]
  return homspace
end

end # @eval
end # for

################################################################################
# properties
################################################################################

domain(homspace::GenArithTypeHomSpace{D,C}) where {D,C} = homspace.domain

codomain(homspace::GenArithTypeHomSpace{D,C}) where {D,C} = homspace.codomain

basis(homspace::GenArithTypeHomSpace{D,C}) where {D,C} = homspace.basis

function dim(homspace::ArithTypeHomSpace{D,C}) where {D,C}
  length(collect(basis(homspace)))
end

################################################################################
# display
################################################################################

function show(io::IO, homspace::GenArithTypeHomSpace{D,C}) where {D,C}
  print(io, "Hom(")
  show(io, domain(homspace))
  print(io, ", ")
  show(io, codomain(homspace))
  print(io, ")")
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

domain(hom::ArithTypeHom{D,C}) where {D,C} = domain(parent(hom))

codomain(hom::ArithTypeHom{D,C}) where {D,C} = codomain(parent(hom))

function _image_store_type(hspace::GenArithTypeHomSpace{D,C}) where {D,C}
  DI = elem_type(image_parent(domain(hspace)))
  CI = elem_type(image_parent(codomain(hspace)))
  _istwist_type0(I::Type, T::Type) = (I <: T || I <: TwistedPerm{T})
  _istwist_type1(I::Type, T::Type) = (I <: (T{M} where M) || I <: (TwistedPerm{T{M}} where M))
  if _istwist_type0(DI,TrivialGroupElem) &&
     _istwist_type0(CI,TrivialGroupElem)
    return :twist_trivial
  elseif any(_istwist_type0(DI,T) for T in [TrivialGroupElem, RootOfUnity]) &&
         any(_istwist_type0(CI,T) for T in [TrivialGroupElem, RootOfUnity])
    return :twist_root_of_unity
  elseif (any(_istwist_type0(DI,T) for T in [TrivialGroupElem, RootOfUnity]) ||
          any(_istwist_type1(DI,T) for T in [MatrixGroupElem, WeilTransformation])) &&
         (any(_istwist_type0(CI,T) for T in [TrivialGroupElem, RootOfUnity]) ||
          any(_istwist_type1(CI,T) for T in [MatrixGroupElem, WeilTransformation]))
    return :twist_matrix
  else
    throw(DomainError((DI,CI), "types not implemented"))
  end
end
#
################################################################################
# display
################################################################################

function show(io::IO, h::ArithTypeHom{D,C}) where {D,C}
  print(io, matrix(QQab,h))
end

function show(io::IO, mime::MIME"text/plain", h::ArithTypeHom{D,C}) where {D,C}
  show(io, mime, matrix(QQab,h))
end

################################################################################
# conversion
################################################################################

function matrix(QQab::QQabField, h::ArithTypeHom{D,C}) where {D,C}
  d = dim(domain(h))
  return matrix(QQab, hcat((h(unitvec(QQab,d,jx)) for jx in 1:d)...))
end

################################################################################
# application
################################################################################

function (hom::GenArithTypeHom{D,C})(
    vec::Vector{A}
   ) where {
    G,
    TD,
    TC,
    D <: Union{ArithType{G,TD}, ArithType{G,TwistedPerm{TD}}},
    C <: Union{ArithType{G,TC}, ArithType{G,TwistedPerm{TC}}},
    A <: RingElem
   }
  dim(domain(hom)) == length(vec) || throw(DomainError(vec, "incompatible vector length"))

  if isempty(hom.orbits_gens)
    # If orbits_gens is empty, there are not twisted permutations among the
    # tensor componentents. In that case we know that there is a matrix transformation.
    return mul_classical(hom.mat,vec)
  end

  # We subdivide vec, on which elements of type D act, into chunks that
  # correspond to elements of the of type TD acting. In other words, the
  # permutation size of the domain of determines the number of chunks and the
  # dimension of the twist parent determines their size. Analogously for the
  # size. Much of the logic in this function is owed to the fact that the
  # tensor product of a twisted permutation with a twisted permutation is
  # itself a twisted permutation, whose permutation size is the product of the
  # previous two permutatoin sizes. In particular, we need to untangle these
  # two contrubtions. In addition, domain and codomain can be tensor products
  # of permutation types.

  # determine vector chunk dimensions and permutation sizes
  if domain(hom) isa TensorArithType
    domain_components = components(domain(hom))
  else
    domain_components = [domain(hom)]
  end
  domain_chunkdims = [image_parent(c) isa TwistedPermGroup ?
                        dim(twist_parent(image_parent(c))) :
                        dim(image_parent(c))
                      for c in domain_components]
  domain_permsizes = [permsize(image_parent(c))
                      for c in domain_components
                      if image_parent(c) isa TwistedPermGroup]
  if image_parent(domain(hom)) isa TwistedPermGroup
    domain_chunkdim = dim(twist_parent(image_parent(domain(hom))))
    domain_permsize = permsize(image_parent(domain(hom)))
  else
    domain_chunkdim = dim(image_parent(domain(hom)))
    domain_permsize = 1
  end
  # number of components and twisted perm components
  domain_ncs = length(domain_chunkdims)
  domain_ntperms = length(domain_permsizes)
  # The index shifts for vec to access the next coordinate in one of the
  # twisted perm components.
  domain_permshifts = [domain_chunkdim*prod(domain_permsizes[px+1:end])
                       for px in 1:length(domain_permsizes)]

  if codomain(hom) isa TensorArithType
    codomain_components = components(codomain(hom))
  else
    codomain_components = [codomain(hom)]
  end
  codomain_chunkdims = [image_parent(c) isa TwistedPermGroup ?
                        dim(twist_parent(image_parent(c))) :
                        dim(image_parent(c))
                        for c in codomain_components]
  codomain_permsizes = [permsize(image_parent(c))
                      for c in codomain_components
                      if image_parent(c) isa TwistedPermGroup]
  if image_parent(codomain(hom)) isa TwistedPermGroup
    codomain_chunkdim = dim(twist_parent(image_parent(codomain(hom))))
    codomain_permsize = permsize(image_parent(codomain(hom)))
  else
    codomain_chunkdim = dim(image_parent(codomain(hom)))
    codomain_permsize = 1
  end
  codomain_ncs = length(codomain_chunkdims)
  codomain_ntperms = length(codomain_permsizes)
  codomain_permshifts = [codomain_chunkdim*prod(codomain_permsizes[px+1:end])
                         for px in 1:length(codomain_permsizes)]

  hom_components = vcat(domain_components, codomain_components)
  ncs = domain_ncs + codomain_ncs
  ntperms = domain_ntperms + codomain_ntperms


  ring = parent(first(vec))
  res = [zero(ring) for _ in 1:dim(codomain(hom))]
  # The orbit gens are group elements that map the inital element of an orbit
  # (stored in hom.orbits_init) to the other elements. Each element is in the
  # stabilizer of all previous components, so when applying them from the left
  # we can take their product to map to the tuple of components that they
  # reference.
  for orbit_gens in Iterators.product(hom.orbits_gens...)
    g = length(orbit_gens) != 1 ? prod(orbit_gens) : orbit_gens[1]

    twists = Vector(undef,ncs)
    coords = Vector{Int}(undef,ntperms)
    # tcx is index in the list of twisted permutation components as opposed to
    # all components.
    # FIXME: the evaluation c(g) is the cause of slowness
    let tcx = 1
      for (cx,c) in enumerate(hom_components)
        if image_parent(c) isa TwistedPermGroup
          if cx <= domain_ncs
            (px,tw) = act(dual(c(g)), hom.orbits_init[tcx])
          else
            (px,tw) = act(c(g), hom.orbits_init[tcx])
          end
          twists[cx] = tw
          coords[tcx] = px
          tcx += 1
        else
          if cx <= domain_ncs
            twists[cx] = dual(c(g))
          else
            twists[cx] = c(g)
          end
        end
      end
    end
    domain_coords = view(coords, 1:domain_ntperms)
    codomain_coords = view(coords, domain_ntperms+1:ntperms)

    # The indices in vec and res which arise from the twisted permutations
    ix = domain_ntperms == 0 ? 1 :
             1 + sum((c-1)*sh for (c,sh) in zip(domain_coords,domain_permshifts))
    jx = codomain_ntperms == 0 ? 1 :
             1 + sum((c-1)*sh for (c,sh) in zip(codomain_coords,codomain_permshifts))

    if isnothing(hom.mat)
      # This implies that all twists are 1-dimensional.
      res[jx] = addacteq!(res[jx], reduce(tensor_product, twists), vec[ix])
      continue
    end

    # Get chunks of vec to which we can apply all matrix transformations. This
    # a multidimensional array, whose (tensor) components correspond to the
    # components in the tensor decomposition of the domain.
    vec_chunk = reshape(view(vec, ix:ix+domain_chunkdim-1), domain_chunkdims...)

    # Apply twists to the respective dimension of the domain
    for cx in 1:domain_ncs
      vec_chunk = act(dual(inv(twists[cx])), vec_chunk, cx)
    end

    # Apply the basic transformation matrix and then reshape to match the
    # result chunk.
    vec_chunk = reshape(mul_classical(hom.mat, reshape(vec_chunk,:)),
                        codomain_chunkdims...)

    # Apply twists to the respective dimension of the codomain
    for cx in 1:codomain_ncs
      vec_chunk = act(twists[domain_ncs+cx], vec_chunk, cx)
    end

    # Add vector chunk to the result chunk.
    res_chunk = reshape(view(res, jx:jx+codomain_chunkdim-1), codomain_chunkdims...)
    res_chunk .= addeq!.(res_chunk, vec_chunk)
  end
  return res
end
