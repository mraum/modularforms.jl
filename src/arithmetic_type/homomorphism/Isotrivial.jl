################################################################################
# auxiliary
################################################################################

function _extended_orbits(
    rho::ArithType{G,TwistedPerm{T}}
   ) where {G,T}
  # This gives a vector of six tuples
  #   (orbit, orbit_init,
  #    stabilizer_gens, stabilizer_twists,
  #    orbit_gens, orbit_twists)
  extended_orbits(gens(group(rho)), generator_images(rho))
end

function _extended_orbits(
    rho::IndArithType{G,H,T}
   ) where {G,H,T}
   # We assume that the trivial coset isone and is in position 1.
   [(collect(1:permsize(image_parent(rho))), 1,
     gens(base_group(rho)),
     [base_type(rho)(g) for g in gens(base_group(rho))],
     coset_representatives(rho),
     [one(twist_parent(image_parent(rho))) for _ in 1:permsize(image_parent(rho))]
   )]
end

################################################################################
# orbit chains for direct products of permutations
################################################################################

function _orbit_chains(
    rho::ArithType{G,TwistedPerm{T}};
    provide_stab = true :: Bool
   ) where {G,T}
  _orbit_chains(TensorArithmeticType([rho]); provide_stab = provide_stab)
end

function _orbit_chains(
    rho::TensorArithType{G,TwistedPerm{T}};
    provide_stab = true :: Bool
   ) where {G,T}
  grp_gens = gens(group(rho))

  # A free group with one letter for each generator of the group. We will have
  # to revert all words, since GAP uses actions from the right, and we use
  # actions from the left.
  fgrp = GAP.Globals.FreeGroup(GAP.julia_to_gap(
             [GAP.julia_to_gap(join(
                 'a' .+ digits(ix, base=26, pad=ndigits(length(grp_gens), base=26))
                 ))
              for ix in 1:length(grp_gens)]
             ))
  grp_gap = fgrp
  # TODO: provide gen_relation_words = missing for all groups and remove this
  # try statement.
  try
    fgens = GAP.Globals.GeneratorsOfGroup(fgrp)
    # We revert the order of the word to accommodate GAP;s right actions.
    grp_gap = fgrp /
        GAP.julia_to_gap([
            prod((fgens[gx]^ex for (gx,ex) in grel))
            for grel in reverse(gen_relation_words(group(rho)))
            ])
  catch
  end

  sigmas = [sigma for sigma in components(rho)
            if image_parent(sigma) isa TwistedPermGroup]
  permsizes = [permsize(image_parent(sigma)) for sigma in sigmas]
  permshifts = pushfirst!(accumulate(+,permsizes[1:end-1]),0)
  symgrp = GAP.Globals.SymmetricGroup(sum(permsizes))
  rho_gap = GAP.Globals.GroupHomomorphismByImagesNC(
                grp_gap, symgrp,
                GAP.julia_to_gap([
                    GAP.Globals.PermList(GAP.julia_to_gap(
                        vcat((p.+s for (p,s) in zip(map(perms,gs), permshifts))...)
                        ))
                    for gs in zip(map(generator_images, sigmas)...)
                    ])
                )

  OrbitChainIterator{G}(
      group(rho), grp_gens, grp_gap,
      permsizes, permshifts,
      rho_gap,
      provide_stab
      )
end

function Base.iterate(iter::OrbitChainIterator{G}) where G
  iterate(iter, OrbitChainIteratorState{G}(length(iter.permsizes)))
end

function Base.iterate(
    iter::OrbitChainIterator{G},
    state::OrbitChainIteratorState{G}
   ) where G
  # We terminate the iteration on sx0 == -1.
  if state.sx0 == -1
    return nothing
  end

  # grp: Previous stabilizer in the chain.
  # grp_gens_gap: Generators of grp as GAP objects.
  # grp_gens: Generators of grp in iter.grp_jl.

  # The case state.sx0 == 0 occurs only during initialization.
  grp = state.sx0 >= 2 ? state.stab[state.sx0-1] : GAP.Globals.Image(iter.rho_gap)

  nsigmas = length(iter.permsizes)
  for sx in (state.sx0 != 0 ? state.sx0 : 1):nsigmas
    if sx == state.sx0
      orbits_init = state.orbits_init[sx]
    else
      # orbits: The orbits of grp_img wrt the natural action of the image of sigma.
      # We need to force GAP to use the correct action here, since it discards
      # the information that grp_img is a subgroup of symgrps[sx]
      sigma_set = collect(iter.permshifts[sx]+1:iter.permshifts[sx]+iter.permsizes[sx])
      orbits = map(Vector{Int}, map(GAP.gap_to_julia,
                   GAP.gap_to_julia(GAP.Globals.Orbits(
                       grp, GAP.julia_to_gap(sigma_set)
                       ))
                  ))
      orbits_init = map(first, orbits)
      state.orbits_init[sx] = orbits_init
    end

    oi = pop!(orbits_init)
    state.orbit_init[sx] = oi

    stab = GAP.Globals.Stabilizer(grp, oi)
    state.stab[sx] = stab

    right_coset_representatives = GAP.Globals.RightTransversal(grp, stab)

    orbit_cosets = G[]
    state.orbit_cosets[sx] = orbit_cosets
    for cx in 1:GAP.Globals.Length(right_coset_representatives)
      c_gap = GAP.Globals.PreImagesRepresentative(
                  iter.rho_gap, right_coset_representatives[cx])
      # ExtRepOfObj format: The element c is represented by a list of integers
      # which correspond to words in the generators of grp. In odd positions,
      # the entries of this list refer to the 1-based index of generators, the
      # succeeding integer is the exponent of that generator.
      cword = GAP.Globals.ExtRepOfObj(c_gap)

      if GAP.Globals.Length(cword) == 0
        push!(orbit_cosets, one(iter.grp))
        continue
      end

      # We revert the order of the word to accommodate GAP;s right actions.
      c = iter.grp_gens[cword[1]]^cword[1+1]
      for wx in 3:2:length(cword)
        c = iter.grp_gens[cword[wx]]^cword[wx+1] * c
      end
      push!(orbit_cosets, c)
    end

    grp = stab
  end

  # We unrole the chain from the back until we find an orbit that is not yet
  # exhausted.
  sx0 = nsigmas
  while sx0 != 0 && isempty(state.orbits_init[sx0])
    sx0 -= 1
  end
  # The value sx0 == 0 would restart the whole iteration, but sx0 == -1 by
  # convention terminates it.
  state.sx0 = sx0 != 0 ? sx0 : -1


  if iter.provide_stab
    stab_gens_gap = GAP.Globals.GeneratorsOfGroup(
                        GAP.Globals.PreImage(iter.rho_gap, state.stab[end]))
    stab_gens = G[]
    for gx in 1:GAP.Globals.Length(stab_gens_gap)
      g = stab_gens_gap[gx]
      # ExtRepOfObj format: The element g is represented by a list of integers
      # which correspond to words in the generators of grp. In odd positions,
      # the entries of this list refer to the 1-based index of generators, the
      # succeeding integer is the exponent of that generator.
      gword = GAP.Globals.ExtRepOfObj(g)

      if GAP.Globals.Length(gword) == 0
        continue
      end
      # We revert the order of the word to accommodate GAP;s right actions.
      g = iter.grp_gens[gword[1]]^gword[1+1]
      for wx in 3:2:length(gword)
        g = iter.grp_gens[gword[wx]]^gword[wx+1] * g
      end
      push!(stab_gens, g)
    end
  end

  return ((state.orbit_init .- iter.permshifts, copy(state.orbit_cosets),
           iter.provide_stab ? stab_gens : missing),
          state)
end

################################################################################
# trivial twists
################################################################################

function _isotrivial_subtype(
    rho::ArithType{G,TwistedPerm{TrivialGroupElem}}
   ) where G
  return ((oi,ocs) for ((oi,),(ocs,),_) in _orbit_chains(rho; provide_stab = false))
end

################################################################################
# twists by roots of unity
################################################################################

function _isotrivial_subtype(
    rho::ArithType{G,TwistedPerm{RootOfUnity}}
   ) where G
  function has_trivstab(oi, stab_gens)
    rho_oi = component_stabilizer_restriction(rho, oi)
    return all(isone(rho_oi(g)) for g in stab_gens)
  end
  return ((oi,ocs)
          for ((oi,), (ocs,), stab_gens) in _orbit_chains(rho)
          if has_trivstab(oi, stab_gens))
end

################################################################################
# twists by matrices
################################################################################

function _isotrivial_subtype(
    rho :: ArithType{G,I};
    generators :: Union{Nothing, Vector{GG}} = nothing
   ) where {
    G, GG, GW,
    I <: Union{MatrixGroupElem, WeilTransformation{GW}}
   }
  if isnothing(generators)
    generators = gens(group(rho))
  end
  mat = vcat((matrix(rho(g))-1 for g in generators)...)
  (rk,basismat) = nullspace(mat)
  return (basismat[:,jx] for jx in 1:ncols(basismat))
end

function _isotrivial_subtype(
    rho::ArithType{G,TwistedPerm{T}}
   ) where {
    G, GW,
    T <: Union{MatrixGroupElem, WeilTransformation{GW}}
   }
  ((oi,ocs, v)
   for ((oi,),(ocs,), stab_gens) in  _orbit_chains(rho)
   for v in _isotrivial_subtype(component_stabilizer_restriction(rho, oi);
                                generators = stab_gens)
  )
end

################################################################################
# tensor products of arithmetic types without twists
################################################################################

function _isotrivial_subtype(
    rho::TensorArithType{G,TrivialGroupElem};
    generators :: Union{Nothing, Vector{GG}} = nothing
   ) where {G, GG}
  return true
end

function _isotrivial_subtype(
    rho::TensorArithType{G,RootOfUnity};
    generators :: Union{Nothing, Vector{GG}} = nothing
   ) where {G, GG}
  if isnothing(generators)
    generators = gens(group(rho))
  end
  return all(isone(reduce(tensor_product, (sigma(g) for sigma in components(rho))))
             for g in generators)
end

#This was broken in the absence of `coefftower` or `coeffring`
#because twist_parent(image_parent(...)) is invalid here.
#
# TODO: Can't one just subsume this under usual _isotrivial_subtype? This would
# rely on the evaluation for TensorArithType, but this is implemented.
# REMARK: Subsuming was much slower. See issue #119.
function _isotrivial_subtype(
    rho        :: TensorArithType{G,I},
    coefftower :: MuInfTower,
    coeffring  :: Ring;
    generators :: Union{Nothing, Vector{GG}} = nothing,
  ) where {
      G, GG, GW,
      I <: Union{MatrixGroupElem, WeilTransformation{GW}}
     }
  if isnothing(generators)
    generators = gens(group(rho))
  end
  mat = vcat((reduce(kronecker_product,
                     (matrix(coefftower, coeffring, sigma(g))
                      for sigma in components(rho)))
              - 1
             for g in generators)...)
  (rk,basismat) = nullspace(mat)
  return (basismat[:,jx] for jx in 1:ncols(basismat))
end

################################################################################
# tensor products of arithmetic types with twists
################################################################################

# TODO: For the metaplectic group, we can first take the kernel of the central
# action. This allows us to restrict to subgroups of SL2Z, where we have
# FareySymbol to compute subgroups.
function _isotrivial_subtype(
    rho::TensorArithType{G,TwistedPerm{T}}
   ) where {
    G, T <: Union{TrivialGroupElem,RootOfUnity} 
   }
  ncs = length(components(rho))
  stab_restriction_components = Vector{ArithType}(undef, ncs)
  perm_component_indices = Int[]
  for (sx,sigma) in enumerate(components(rho))
    if image_parent(sigma) isa TwistedPermGroup
      push!(perm_component_indices, sx)
    else
      stab_restriction_components[sx] = sigma
    end
  end
  npcs = length(perm_component_indices)
  perm_component_types = [Dict{Int,ArithType}() for _ in 1:npcs]

  function has_trivstab(oi, stab_gens)
    for (ox,rx) in enumerate(perm_component_indices)
      oiox = oi[ox]
      perm_component_types_ox = perm_component_types[ox]
      if !haskey(perm_component_types_ox, oiox)
        perm_component_types_ox[oiox] =
            component_stabilizer_restriction(components(rho)[rx], oiox)
      end
      stab_restriction_components[rx] = perm_component_types_ox[oiox]
    end
    sigma = TensorArithmeticType(stab_restriction_components;
                                 image_parent = twist_parent(image_parent(rho)),
                                 check = false)
    return _isotrivial_subtype(sigma; generators = stab_gens)
  end
  return ((oi,ocs)
          for (oi,ocs,stab_gens) in _orbit_chains(rho)
          if has_trivstab(oi,stab_gens))
end
function _isotrivial_subtype(
    rho::TensorArithType{G,TwistedPerm{T}}
   ) where {
    G, GW,
    T <: Union{MatrixGroupElem,WeilTransformation{GW}}
   }
  coefftower = coefficient_ring_tower(twist_parent(image_parent(rho)))
  coeffring = coefficient_ring(twist_parent(image_parent(rho)))
  
  ncs = length(components(rho))
  stab_restriction_components = Vector{ArithType}(undef, ncs)
  perm_component_indices = Int[]
  for (sx,sigma) in enumerate(components(rho))
    if image_parent(sigma) isa TwistedPermGroup
      push!(perm_component_indices, sx)
    else
      stab_restriction_components[sx] = sigma
    end
  end
  npcs = length(perm_component_indices)
  perm_component_types = [Dict{Int,ArithType}() for _ in 1:npcs]

  function stab_restrict_type(oelt)
    for (ox,rx) in enumerate(perm_component_indices)
      oelt_ox = oelt[ox]
      perm_component_types_ox = perm_component_types[ox]
      if !haskey(perm_component_types_ox, oelt_ox)
        perm_component_types_ox[oelt_ox] =
            component_stabilizer_restriction(components(rho)[rx], oelt_ox)
      end
      stab_restriction_components[rx] = perm_component_types_ox[oelt_ox]
    end
    sigma = TensorArithmeticType(stab_restriction_components;
                                 image_parent = twist_parent(image_parent(rho)),
                                 check = false)
    return sigma
  end
  return ((oi,ocs,v)
          for (oi,ocs,stab_gens) in _orbit_chains(rho)
          for v in _isotrivial_subtype(stab_restrict_type(oi),
                                       coefftower,
                                       coeffring;
                                       generators = stab_gens))
end
