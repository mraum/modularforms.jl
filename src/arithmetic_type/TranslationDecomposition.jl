################################################################################
# properties
################################################################################

length(o :: TranslationOrbit) = length(o.orbit)

dim(o :: TranslationOrbit) = length(o.orbit) * length(o.components)

# The coordinate of initial element in the orbit and the jx-th component.
function representative(o :: TranslationOrbit, component_index :: Int = 1)
  (o.orbit[1]-1) * o.component_dim + o.components[component_index]
end

# The coordinate of element in the orbit and the jx-th component that are not
# the representatives.
function nonrepresentatives(o :: TranslationOrbit, component_index :: Int = 1)
  (o.orbit[2:end].-1) .* o.component_dim .+ o.components[component_index]
end

function nonrepresentatives_with_transitions(
    o :: TranslationOrbit,
    component_index :: Int = 1
   )
  zip(nonrepresentatives(o, component_index), o.orbit_transitions[2:end])
end

# The twist by which T^N acts, where N is the length of the orbit.
twist(o :: TranslationOrbit) = o.twist

# The minimal shift of covariant Fourier expansions.
function fourier_expansion_base_shift(o :: TranslationOrbit)
  Rational{Int}(exponent(twist(o))) // length(o)
end

# The shift associated with the whole orbit
function fourier_expansion_orbit_shift(o :: TranslationOrbit)
  Rational{Int}(exponent(twist(o)))
end

fourier_expansion_shifts(o :: TranslationOrbit) = o.fourier_expansion_shifts

fourier_coefficient_twists(o :: TranslationOrbit) = o.fourier_coefficient_twists

function number_of_fourier_coefficients(
    o   :: TranslationOrbit,
    prc :: Union{Int,Rational{Int}}
   )
  iprc = floor(Int,prc)
  fprc = prc - iprc
  return iprc*length(o) + count(<(fprc), fourier_expansion_shifts(o))
end

dim(tdecomp :: TranslationDecomposition) = sum(map(dim,tdecomp.orbits))

pivots_with_shift(tdecomp :: TranslationDecomposition) = tdecomp.pivots_with_shift

representatives(tdecomp :: TranslationDecomposition) = map(representative, tdecomp)

function number_of_fourier_coefficients(
    rho :: ArithType{G,I},
    prc :: Union{Int,Rational{Int}}
   ) where {G, I}
  number_of_fourier_coefficients(translation_decomposition(rho), prc)
end

function number_of_fourier_coefficients(
    tdecomp :: TranslationDecomposition,
    prc     :: Union{Int,Rational{Int}}
   )
  iprc = floor(Int,prc)
  fprc = prc - iprc
  return iprc*dim(tdecomp) + count(((_,sh),) -> sh<fprc, pivots_with_shift(tdecomp))
end

################################################################################
# getindex and iteration
################################################################################

eltype(:: Type{TranslationDecomposition}) = TranslationOrbit

length(tdecomp :: TranslationDecomposition) = length(tdecomp.orbits)

iterate(tdecomp :: TranslationDecomposition) = iterate(tdecomp.orbits)

iterate(tdecomp :: TranslationDecomposition, state) = iterate(tdecomp.orbits, state)

getindex(tdecomp :: TranslationDecomposition, ix) = getindex(tdecomp.orbits, ix)

################################################################################
# creation
################################################################################

function TranslationOrbit(
    orbit :: Vector{Int}
   )
  TranslationOrbit(orbit, one(MuInf))
end

function TranslationOrbit(
    orbit :: Vector{Int},
    twist :: TrivialGroupElem
   )
  TranslationOrbit(orbit, one(MuInf))
end

function TranslationOrbit(
    orbit :: Vector{Int},
    twist :: RootOfUnity
    )
  sh = Rational{Int}(exponent(twist))//length(orbit)
  fourier_expansion_shifts = [sh + n//length(orbit) for n in 0:length(orbit)-1]

  # The twisted of the Fourier coefficients in the fx-th element of the orbit
  # equals MuInf(sh)^fx, where sh is the shift. To interprete this, we need to
  # consider a covariant Fourier expansion, i.e.
  #   f(\tau) = \rho(T^-1) f(\tau+1).
  # When we advance the orbit via T^-1, we have to multiply by MuInf(sh). For
  # this reason, translation orbits are constructed from the inverse T as
  # opposed to T itself.
  # See, for example, the translation decomposition for SL2Z types.
  fourier_coefficient_twists =
      [MuInf(sh*fx)
       for sh in fourier_expansion_shifts, fx in 1:length(orbit)-1]

  return TranslationOrbit(orbit, [TrivialGroupElem() for _ in orbit], 1, [1], twist,
                          fourier_expansion_shifts, fourier_coefficient_twists)
end

function TranslationOrbit(
    orbit       :: Vector{Int},
    transitions :: Vector{RootOfUnity},
    twist       :: RootOfUnity
    )
  sh = Rational{Int}(exponent(twist))//length(orbit)
  fourier_expansion_shifts = [sh + n//length(orbit) for n in 0:length(orbit)-1]

  # See the case without transition for some explanation of this.
  fourier_coefficient_twists =
      [MuInf(sh*fx) * transitions[fx+1]
       for sh in fourier_expansion_shifts, fx in 1:length(orbit)-1]

  return TranslationOrbit(orbit, transitions, 1, [1], twist,
                          fourier_expansion_shifts, fourier_coefficient_twists)
end

function translation_decomposition(::TrivialGroupElem)
  TranslationDecomposition([TranslationOrbit([1])], [(1,0//1)])
end

function translation_decomposition(a::TwistedPerm{TrivialGroupElem})
  orbits = TranslationOrbit[]

  unprocessed = Base.Set{Int}(1:permsize(a))
  while !isempty(unprocessed)
    ix = pop!(unprocessed)

    orbit = Int[ix]

    (jx,tw) = act(a, ix)
    while jx != ix
      push!(orbit, jx)
      delete!(unprocessed, jx)
      (jx,tw) = act(a, (jx,tw))
    end

    push!(orbits, TranslationOrbit(orbit, tw))
  end

  pivots_with_shift = sort!([(ox,sh)
                             for (ox,o) in enumerate(orbits)
                             for sh in fourier_expansion_shifts(o)];
                            by = ((a,b),) -> (b,a)
                           )


  return TranslationDecomposition(orbits, pivots_with_shift)
end

function translation_decomposition(a::TwistedPerm{RootOfUnity})
  orbits = TranslationOrbit[]

  unprocessed = Base.Set{Int}(1:permsize(a))
  while !isempty(unprocessed)
    ix = pop!(unprocessed)

    orbit = Int[ix]
    transitions = RootOfUnity[one(MuInf)]

    (jx,tw) = act(a, ix)
    while jx != ix
      push!(orbit, jx)
      push!(transitions, tw)
      delete!(unprocessed, jx)
      (jx,tw) = act(a, (jx,tw))
    end

    push!(orbits, TranslationOrbit(orbit, transitions, tw))
  end

  pivots_with_shift = sort!([(ox,sh)
                             for (ox,o) in enumerate(orbits)
                             for sh in fourier_expansion_shifts(o)];
                            by = ((a,b),) -> (b,a)
                           )


  return TranslationDecomposition(orbits, pivots_with_shift)
end

################################################################################
# twists of Fourier expansion
################################################################################

function deflate(
    f       :: Vector,
    tdecomp :: TranslationDecomposition
   ) where {
    G, I
   }
  f[representatives(tdecomp)]
end

function deflate_fourier_expansion(
    f   :: Vector{F},
    rho :: ArithType{G,I},
    prc :: Union{Int,Rational{Int}}
   ) where {
    R, F <: PuiseuxSeriesElem{R},
    G, I
   }
  deflate_fourier_expansion(f, translation_decomposition(rho), prc)
end

@doc Markdown.doc"""
> Let `f` be a vector of Puiseux series that is covariant under translations
> with resepect to `\rho`. Return a vector holding coefficients of the
> components of `f` that are representatives for the translation orbits.
"""
function deflate_fourier_expansion(
    f       :: Vector{F},
    tdecomp :: TranslationDecomposition,
    prc     :: Union{Int,Rational{Int}}
   ) where {
    R, F <: PuiseuxSeriesElem{R},
    G, I
   }
  # NOTE: Pivots with shift are ordered by shifts. This means that the
  # resulting vector corresponds to Fourier indices in increasing order.
  return [coeff(f[fx],sh+n)
          for n in 0:ceil(Int,prc)-1
          for (fx,sh) in pivots_with_shift(tdecomp)
          if sh+n < prc]
end

function inflate(
    fourier_expansion_represenatives :: Vector{F},
    tdecomp                          :: TranslationDecomposition,
    coefficient_ring_tower           :: MuInfTower
   ) where {
    R, F <: PuiseuxSeriesElem{R}
   }
  fourier_expansion = Vector{F}(undef, dim(tdecomp))
  for (f,orbit) in zip(fourier_expansion_represenatives, tdecomp)
    twist_fourier_expansion!(fourier_expansion, f, orbit, coefficient_ring_tower)
  end
  return fourier_expansion
end

function twist_fourier_expansion!(
    fourier_expansions     :: Vector{P},
    f                      :: P,
    orbit                  :: TranslationOrbit,
    coefficient_ring_tower :: MuInfTower
   ) where {
    R, P <: PuiseuxSeriesElem{R}
   }
  # As long as we assume that transitions are roots of unity, acting
  # diagonally, we can simply expand the twist for each of the components
  # separately.
  if length(orbit.components) != 1
    error("not implemented")
  end


  fourier_expansions[representative(orbit)] = f


  len = length(orbit)
  if len == 1
    return
  end

  fl             = f.data
  coeffs         = fl.coeffs
  fring          = parent(f)
  flaurent_ring  = laurent_ring(fring)

  f_scale   = f.scale
  fl_length = fl.length
  fl_prec   = fl.prec
  fl_val    = fl.val
  fl_scale  = fl.scale

  shift    = fourier_expansion_base_shift(orbit)
  fctwists = fourier_coefficient_twists(orbit)

  # A Puiseux series is stored as a rescaled Laurent series. The ix-th entry of
  # the coefficient vector stores the coefficient of the term of exponent
  #   (fl_val + (ix-1)*fl_scale) // f_scale

  # To obtain the corresponding index of twists, we have to first subtract
  # the base shift of the orbit and then multiply by its length. Adding one
  # yields 1-based indices as opposed to 0-based indices.
  #   (((fl_val + (ix-1)*fl_scale) // f_scale - shift) * len) % len + 1

  # In order to avoid the evalutation of this expression for each ix, we
  # compute the value at ix=1
  #   ((fl_val // f_scale - shift) * len) % len + 1.
  # and then increment by
  #   (fl_scale * len) // f_scale
  txstart = mod1(Int((fl_val // f_scale - shift) * len) + 1, len)
  # If f consists of only one term, then the following quotient might not be
  # exact. But then it is never used, either.
  txinc = mod(div(fl_scale * len, f_scale), len)

  for (ix,vx) in enumerate(nonrepresentatives(orbit))
    coeffs_twisted = similar(coeffs)
    tx = txstart
    for cx in 1:fl_length
      coeffs_twisted[cx] = act(fctwists[tx,ix], coeffs[cx],
                               coefficient_ring_tower)
      tx = mod1(tx+txinc, len)
    end
    fourier_expansions[vx] =
        fring(flaurent_ring(coeffs_twisted, fl_length, fl_prec, fl_val, fl_scale),
              f_scale)
  end

  return fourier_expansion
end

function inflate_fourier_expansion(
    v      :: Vector{R},
    rho    :: ArithType{G,I},
    fering :: PR,
    prc    :: Union{Int, Rational{Int}}
   ) where {
    R,
    PR <: Union{Generic.PuiseuxSeriesRing{R}, Generic.PuiseuxSeriesField{R}},
    G, I
   }
  inflate_fourier_expansion(v, translation_decomposition(rho),
      fering, prc)
end

function inflate_fourier_expansion(
    v       :: Vector{R},
    tdecomp :: TranslationDecomposition,
    fering  :: PR,
    prc     :: Union{Int,Rational{Int}}
   ) where {
    R,
    PR <: Union{Generic.PuiseuxSeriesRing{R}, Generic.PuiseuxSeriesField{R}}
   }
  # We first recover the Puiseux series for the orbit representatives.

  # This inverts the reordering performed during deflation.
  fcoeffs = [Vector{elem_type(base_ring(fering))}(undef, number_of_fourier_coefficients(o,prc))
             for o in tdecomp]
  ix = 1
  findices = fill(1, length(tdecomp))
  for n in 0:ceil(Int,prc)-1
    for (fx,sh) in pivots_with_shift(tdecomp)
      n+sh < prc || continue
      fcoeffs[fx][findices[fx]] = v[ix]
      ix += 1
      findices[fx] += 1
    end
  end

  # The coefficients for an orbit o with shifts s(0) ... s(n-1) should yield an
  # expansion of the form
  #   * q^s(0) + * q^s(1) + ... + * q^s(n-1) + * q^(1+s(0)) + ... + O(q^prc)

  # We have s(i) = s(0) + i/#o. The denominator in the exponent is
  # d = lcm(den(s(0))l den(s(1)))). This is equal to #o times the denominator
  # of the total orbit shift . After rescaling by d, we have a Laurent series
  # of the form
  #   * q^(d*s(0)) + * q^(d*s(1)) + ... + O(q^(d*prc))

  # The valuation of this is d*s(0) and the precision d*prc. The scale of the
  # series is d*(s(1)-s(0)) = d / #o. The valuation also equals the numerator
  # of the total orbit shift. The scale also is the denominator of the total
  # orbit shift.

  felaurentring = laurent_ring(fering)

  return [let
            num = numerator(fourier_expansion_orbit_shift(o))
            den = denominator(fourier_expansion_orbit_shift(o))
            len = length(o)
            fering(felaurentring(fcs,
                        length(fcs),           # length
                        ceil(Int,prc*len*den), # precision
                        num,                   # valuation
                        den                    # scale for Laurent series
                       ),
                    len*den                    # scale for Puiseux series
                   )
          end
          for (o,fcs) in zip(tdecomp,fcoeffs)]
end
