################################################################################
# Hecke operators for general arithmetic types
################################################################################

function hecke(n::Union{Int,fmpz}, rho::ArithType{G,I}) where {G,I}
  hecke(hecke_matrices(n, group(rho)), rho)
end

function hecke_new(n::Union{Int,fmpz}, rho::ArithType{G,I}) where {G,I}
  hecke(hecke_new_matrices(n, group(rho)), rho)
end

function hecke(hecke_mats::Vector{fmpz_mat}, rho::ArithType{G,I}) where {G,I}
  rhogp = group(rho)
  hecke_mat_indices = Dict{eltype(hecke_mats),Int}(
                        m => mx for (mx,m) in enumerate(hecke_mats))

  ix = length(hecke_mats)

  iparent = TwistedPermGroup(image_parent(rho), ix)

  nperms = Vector{Int}(undef, ix)
  ntwists = Vector{elem_type(twist_parent(iparent))}(undef, ix)
  
  gen_images = elem_type(iparent)[]
  sizehint!(gen_images, ngens(rhogp))
  for g in gens(rhogp)
    for (mx,m) in enumerate(hecke_mats)
      (gg,mm) = hecke_transformation(m, inv(g))
      nperms[mx] = hecke_mat_indices[mm]
      ntwists[mx] = rho(inv(gg))
    end
    push!(gen_images, iparent(copy(nperms), copy(ntwists)))
  end

  return ArithmeticType(rhogp, iparent, gen_images)
end

function hecke(hecke_mats::Vector{fmpz_mat}, rho::ArithType{G,TwistedPerm{T}}) where {G,T}
  rhogp = group(rho)
  hecke_mat_indices = Dict{eltype(hecke_mats),Int}(
                        m => mx for (mx,m) in enumerate(hecke_mats))

  ix = length(hecke_mats)
  sz = permsize(image_parent(rho))

  iparent = TwistedPermGroup(twist_parent(image_parent(rho)), ix*sz)

  nperms = Vector{Int}(undef, ix*sz)
  ntwists = Vector{elem_type(twist_parent(iparent))}(undef, ix*sz)
  
  gen_images = elem_type(iparent)[]
  sizehint!(gen_images, ngens(rhogp))
  for g in gens(rhogp)
    for (mx,m) in enumerate(hecke_mats)
      (gg,mm) = hecke_transformation(m, inv(g))
      mmx = hecke_mat_indices[mm]
      im = rho(inv(gg))
      nperms[1+(mx-1)*sz:mx*sz] = [px+(mmx-1)*sz for px in perms(im)]
      ntwists[1+(mx-1)*sz:mx*sz] = twists(im)
    end
    push!(gen_images, iparent(copy(nperms), copy(ntwists)))
  end

  return ArithmeticType(rhogp, iparent, gen_images)
end

################################################################################
# pullbacks associated with Hecke operators
################################################################################

function hecke_new_pullback(n::Union{Int,fmpz}, gp::SL2ZSubgroup)
  gpn = intersect(gp, Gamma0SL2Z(n))
  return map_from_func(g -> gp(SL2Z(g.ga.a,g.ga.b*n,divexact(g.ga.c,n),g.ga.d)), gpn, gp)
end

function hecke_new_pullback(n::Union{Int,fmpz}, gp::SL2ZSubgroupCover)
  # We use that the cocycle for multiplication with [n 0; 0 1] and its
  # inverse is trivial.
  cocycletag(gp) == mp1z_cocycle || error("not implemented")
  gpn = intersect(gp, Gamma0SL2Z(n))
  return map_from_func(g -> gp(SL2Z(g.ga.a,g.ga.b*n,divexact(g.ga.c,n),g.ga.d),g.om), gpn, gp)
end

"""
Pullback along the conjugation by [n 0; 0 1]. After induction this
yields an isomorphism to the Hecke image.
"""
function hecke_new_pullback(n::Union{Int,fmpz}, rho::ArithType{G,I}) where {
    G <: Union{SL2ZElement,SL2ZSubgroupCoverElem},I
   }
  sigma = pullback(hecke_new_pullback(n,group(rho)), rho)
  _copy_level!(sigma, rho)
  lvl = get_attribute(sigma, :level)
  if !isnothing(lvl)
    set_attribute!(sigma, :level => lcm(n,lvl))
  end
  return sigma
end
