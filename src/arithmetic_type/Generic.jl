################################################################################
# properties
################################################################################

group(a::GenArithType{G,I}) where {G,I} = a.group

image_parent(a::GenArithType{G,I}) where {G,I} = a.image_parent

function level(rho::GenArithType{G,I}) where {G <: SL2ZElement,I}
  lvl = get_attribute(rho, :level)
  if !isnothing(lvl)
    return lvl
  end

  gp = group(rho)

  # TODO: Transition of all N by incrementing does not reflect the
  # expected runtime of principal_subgroup(N). Can we factor N and if it
  # contains a high prime power, postpone it?
  # TODO: we can also add an optional argument that gives all primes
  # involved in order to avoid transition of "wrong" ones
  lvl = 1
  while true
    if all(g in gp && isone(rho(g))
           for g in gens(GammaSL2Z(lvl)))
      set_attribute!(rho, :level => lvl)
      return lvl
    end
    lvl += 1
  end
end

function level(rho::GenArithType{G,I}) where {G <: SL2ZSubgroupCoverElem,I}
  lvl = get_attribute(rho, :level)
  if !isnothing(lvl)
    return lvl
  end
  
  gp = group(rho)
  bgp = base_group(gp)

  gpcenterext = centerext(gp)
  rhocenterext = I[one(image_parent(rho))]
  for cx in 1:length(gpcenterext)
    g = gp(one(SL2Z), unitvec(Int, length(gpcenterext), cx))
    h = one(gp)
    rhocenterextorig = copy(rhocenterext)
    for hx in 1:gpcenterext[cx]-1
      h = g*h
      append!(rhocenterext, map(a -> rho(h)*a, rhocenterextorig))
    end
  end

  lvl = 1
  while true
    if all(g in bgp && rho(gp(g)) in rhocenterext
           for g in gens(GammaSL2Z(lvl)))
      set_attribute!(rho, :level => lvl)
      return lvl
    end
    lvl += 1
  end
end

function generator_images(rho::GenArithType{G,I}) where {G,I}
  rho.gen_images
end

################################################################################
# creation
################################################################################

function ArithmeticType(
    group::Group,
    image_parent::Group,
    gen_images::Vector{I};
    check::Bool = true
   ) where I
  GenericType(group, image_parent, gen_images; check = check)
end

function GenericType(
    group::Group,
    image_parent::Group,
    gen_images::Vector{I};
    check::Bool = true
   ) where I
  if check
    for g in gen_images
      if parent(g) != image_parent
        throw(DomainError(g, "not contained in image parent"))
      end
    end
  end
  return GenArithType{elem_type(group),I}(group, image_parent, gen_images)
end

function GenericType(rho::ArithType{G,I}) where {G,I}
  sigma = GenArithType{G,I}(group(rho), image_parent(rho),
              [rho(g) for g in gens(group(rho))])
  _copy_level!(sigma, rho)
  return sigma
end

function GenericType(rho::ArithType{G,I}, image_parent::Group) where {G,I}
  sigma = GenericType(group(rho), image_parent,
              [image_parent(rho(g)) for g in gens(group(rho))];
              check = false)
  _copy_level!(sigma, rho)
  return sigma
end
