weil_type(group::G, m::I) where {G,I <: Integer} = weil_type(group, fill(m,1,1))

function weil_type(group::G, m::Matrix{I}) where {G,I <: Integer}
  weil_type(group, Zlattice(gram = matrix(ZZ, m)))
end

function weil_type(group::SL2ZCover, lattice::Hecke.ZLat)
  weil_image = WeilImage(group, lattice)
  rho = ArithmeticType(group, weil_image, map(weil_image, gens(group)))

  # NOTE: The level of a genus in Hecke is not the level of a discrimiant form
  # in the sense of Scheithauer etc. The problem occurs at 2 only. Hecke
  # defines it as the denomimnator of the Gram matrix for the bilinear form.
  # Scheithauer as the denominator of the upper triangular Gram matrix for the
  # quadratic form.
  invgram = inv(gram_matrix(lattice))
  for i in 2:rank(lattice), j in 1:i-1
    invgram[j,i] *= 2
    invgram[i,j] = 0
  end
  lvl = denominator(QQ(1//2)*invgram)

  set_attribute!(rho, :level => Int(lvl))

  return rho
end

function dedekind_eta_type()
  rho = ArithmeticType(Mp1Z, MuInf, [MuInf(-1//8), MuInf(1//24)])
  set_attribute!(rho, :level => 24)
  return rho
end

isweil(a::ArithType{G,I}) where {G,I} = false
isweil(::ArithType{G,WeilTransformation{G}}) where G = true
