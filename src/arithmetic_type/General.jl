################################################################################
# auxiliary
################################################################################

function _copy_level!(
    b::ArithType,
    as::ArithType...
   )
  @assert !isempty(as)
  aslvl = Vector{Int}(undef, length(as))
  for (ax,a) in enumerate(as)
    if a isa TrivialArithType
      alvl = level(a)
      if ismissing(alvl)
        return
      end
      aslvl[ax] = alvl
    elseif a isa IndArithType || a isa ResArithType
      alvl = get_attribute(base_type(a), :level)
      if isnothing(alvl)
        return
      end
      aslvl[ax] = alvl
    else
      alvl = get_attribute(a, :level)
      if isnothing(alvl)
        return
      end
      aslvl[ax] = alvl
    end
  end
  set_attribute!(b, :level => lcm(aslvl))
  return
end

################################################################################
# properties
################################################################################

dim(a::ArithType{G,I}) where {G,I} = dim(image_parent(a))

generator_images(rho::ArithType{G,I}) where {G,I} = map(rho, gens(group(rho)))

################################################################################
# comparison
################################################################################

function ==(
    a::ArithType{G,I},
    b::ArithType{G,I}
   ) where {G,I}
  group(a) == group(b) && image_parent(a) == image_parent(b) &&
      generator_images(a) == generator_images(b)
end

function isone(a::ArithType{G,I}) where {G,I}
  all(isone(b) for b in generator_images(a))
end

################################################################################
# display
################################################################################

function show(io::IO, a::ArithType{G,I}) where {G,I}
  if isempty(generator_images(a))
    println(io, "ρ[]")
    return
  end

  println(io, "ρ[")
  show(io, generator_images(a)[1])
  for g in generator_images(a)[2:end]
    println(io, ",")
    show(io, g)
  end
  print(io, "]")
end

################################################################################
# arithmetic and tensor products
################################################################################

function conj(a::ArithType{G,I}) where {G,I}
  b = ArithmeticType(group(a), image_parent(a),
          map(conj, generator_images(a)))
  _copy_level!(b, a)
  return b
end

function dual(a::ArithType{G,I}) where {G,I}
  b = ArithmeticType(group(a), dual(image_parent(a)),
          map(dual, generator_images(a)))
  _copy_level!(b, a)
  return b
end

################################################################################
# evaluation
################################################################################

function (rho::ArithType{G1,I})(g::G2) where {G1,G2,I}
  gen_word(rho, word_in_gens(group(rho)(g)))
end

function gen_word(
    rho::ArithType{G,I},
    word::Vector{Tuple{Int,Int}}
   ) where {G,I}
  if isempty(word)
    return one(image_parent(rho))
  end
  gimgs = generator_images(rho)
  res = let (wx,we) = word[1]; gimgs[wx]^we end

  #WARNING: It might be tempting to instead use inplace arithmetic here, i. e.
  #mul!, but it introduces aliasing that eventually yields the wrong results!
  for (wx,we) in word[2:end]
    res *= gimgs[wx]^we
  end
  return res
end

function act(
    rho::ArithType{G1,TwistedPerm{T}},
    g::G2,
    ix::Int
   ) where {G1,G2,T}
  act_gen_word(rho, word_in_gens(group(rho)(g)), ix)
end

function act_perm(
    rho::ArithType{G1,TwistedPerm{T}},
    g::G2,
    ix::Int
   ) where {G1,G2,T}
  _act_perm_gen_word(rho, word_in_gens(group(rho)(g)), ix)
end

function act_gen_word(
    rho::ArithType{G,TwistedPerm{T}},
    word::Vector{Tuple{Int,Int}},
    ix::Int
   ) where {G,T}
  _act_gen_word(
      generator_images(rho),
      map(inv, generator_images(rho)),
      word,
      ix, one(twist_parent(image_parent(rho))))
end

function act_perm_gen_word(
    rho::ArithType{G,TwistedPerm{T}},
    word::Vector{Tuple{Int,Int}},
    ix::Int
   ) where {G,T}
  _act_perm_gen_word(
      generator_images(rho),
      map(inv, generator_images(rho)),
      word, ix)
end

################################################################################
# evaluation : auxiliary
################################################################################

function _act_gen_word(
    generator_images::Vector{TwistedPerm{T}},
    generator_images_inv::Vector{TwistedPerm{T}},
    word::Vector{Tuple{Int,Int}},
    ix::Int,
    tw::T
   ) where T
  for (wx,we) in reverse!(word)
    if we > 0
      g = generator_images[wx]
    else # we < 0
      g = generator_images_inv[wx]
      we = -we
    end
    for _ in 1:we
      (ix,tw) = act(g, (ix,tw))
    end
  end
  return (ix,tw)
end

function _act_perm_gen_word(
    generator_images::Vector{TwistedPerm{T}},
    generator_images_inv::Vector{TwistedPerm{T}},
    word::Vector{Tuple{Int,Int}},
    ix::Int
   ) where T
  for (wx,we) in reverse!(word)
    if we > 0
      g = generator_images[wx]
    else # we < 0
      g = generator_images_inv[wx]
      we = -we
    end
    for _ in 1:we
      ix = act_perm(g, ix)
    end
  end
  return ix
end

################################################################################
# stabilizers of components for twisted permutations
################################################################################

@doc Markdown.doc"""
    component_stabilizer(rho::ArithType{G,TwistedPerm{T}}, ix::Int) where {G, T}

  Compute the stabilizer of the ix-th component of the image of $\rho$.
"""
function component_stabilizer(
    rho::ArithType{G,TwistedPerm{T}},
    ix::Int
   ) where {
    G, T
   }
  component_stabilizer(TensorArithmeticType([rho]), [ix])
end

@doc Markdown.doc"""
    component_stabilizer(rho::TensorArithType{G,I}, ixs::Vector{Int}) where {G,I}

  Compute the stabilizer of the ixs-th component of the image of $\rho$.
"""
function component_stabilizer(
    rho::TensorArithType{G,I},
    ixs::Vector{Int}
   ) where {
    G, I
   }
  if _issl2z(group(rho))
    if group(rho) isa SL2ZFullGroup
      stab_flag = PermStabGL2Z_MODULE.permutation_stabilizer_sl2z_iselement(
                      [map(perms, tuple(generator_images(sigma)...))
                       for sigma in components(rho)],
                      ixs)
    else
      (s,t) = gens(SL2Z)
      stab_flag = PermStabGL2Z_MODULE.permutation_stabilizer_sl2z_iselement(
                      [(perm(rho(s)), perm(rho(t)))],
                      ixs)
    end
    return SL2ZSubgroup(FareySymbolSL2Z_MODULE.FareySymbolSL2Z(
               g -> let (p,n) = stab_flag(g); p end,
               g -> let (p,n) = stab_flag(g); p || n end
               ))
  else
    grp = group(rho)
    gen_imgs = [generator_images(sigma) for sigma in components(rho)]
    gen_imgs_inv = [map(inv, gs) for gs in gen_imgs]
    return subgroup(grp,
        g -> all(_act_perm_gen_word(gs, gs_inv, word_in_gens(grp(g)), ix) == ix
                 for (gs, gs_inv, ix) in zip(gen_imgs, gen_imgs_inv, ixs)))
  end
end

################################################################################
# restriction
################################################################################

function restriction(gp::Group, rho::ArithType{G,I}) where {G,I}
  ResArithType(gp, rho)
end

function component_restriction(
    grp::Group,
    rho::ArithType{G,TwistedPerm{T}},
    ix::Int;
    check :: Bool = true
   ) where {
    G, T
   }
  tws = T[]
  for g in gens(grp)
    (jx,tw) = act(rho,g,ix)
    if check && jx != ix
      throw(DomainError(grp, "does not stabilize component $ix"))
    end
    push!(tws,tw)
  end
  return ArithmeticType(grp, twist_parent(image_parent(rho)), collect(tws))
end

function component_restriction(
    grp::Group,
    rho::TensorArithType{G,TwistedPerm{T}},
    ixs::Vector{Int};
    check :: Bool = true
   ) where {
    G, T
   }
  reduce(tensor_product,
      (component_restriction(grp, sigma, ix; check = check)
       for (sigma,ix) in zip(components(rho),ixs)))
end

function component_stabilizer_restriction(
    rho::ArithType{G,TwistedPerm{T}},
    ix::Int
   ) where {
    G, T
   }
  component_restriction(component_stabilizer(rho, ix), rho, ix;
                        check = false)
end

function component_stabilizer_restriction(
    rho::TensorArithType{G,TwistedPerm{T}},
    ixs::Vector{Int}
   ) where {
    G, T
   }
  component_restriction(component_stabilizer(rho, ixs), rho, ixs;
                        check = false)
end
