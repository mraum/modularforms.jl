module MoonshineLikeType

using Nemo, AbstractAlgebra, Hecke, ModularForms
using Hecke: ZZ
import Base

export rand_moonshine_like_type


struct GrpCommFinGen <: AbstractAlgebra.Group
  abgrp :: GrpAbFinGen
end

struct GrpCommFinGenElem <: AbstractAlgebra.GroupElem
  elt :: GrpAbFinGenElem
  parent :: GrpCommFinGen
end

AbstractAlgebra.parent_type(::GrpCommFinGenElem) = GrpCommFinGen
AbstractAlgebra.elem_type(::GrpCommFinGen) = GrpCommFinGenElem
AbstractAlgebra.parent(a::GrpCommFinGenElem) = a.parent

Base.deepcopy_internal(a::GrpCommFinGenElem, stackdict::IdDict) = parent(a)(Base.deepcopy_internal(a.elt, stackdict))

(parent::GrpCommFinGen)(a::GrpAbFinGenElem) = GrpCommFinGenElem(a, parent)
AbstractAlgebra.gens(grp::GrpCommFinGen) = map(grp, gens(grp.abgrp))
Base.:*(a::GrpCommFinGenElem, b::GrpCommFinGenElem) = parent(a)(a.elt+b.elt)
Base.inv(a::GrpCommFinGenElem) = parent(a)(-a.elt)

unwrap(grp::GrpCommFinGen) = grp.abgrp
unwrap(a::GrpCommFinGenElem) = a.elt


function rand_moonshine_like_type(eldivs::Vector{Int}, twist_level::Int)
  grp = abelian_group(diagonal_matrix(Vector{ZZRingElem}(eldivs)))
  grpsz = length(grp)
  grp2 = [(g,h) for g in grp for h in grp];

  # We construct the action of S and T on pairs of elements of grp
  permS = Vector{Int}( indexin([(h,-g) for (g,h) in grp2], grp2) );
  permT = Vector{Int}( indexin([(g,g+h) for (g,h) in grp2], grp2) );

  # We construct an abstract twist group and then determine relations
  tgrp = GrpCommFinGen( abelian_group( diagonal_matrix(Hecke.ZZ(twist_level), 2*grpsz^2) ) )
  tpgrp = TwistedPermGroup(tgrp, grpsz^2)
  tpS = tpgrp(collect(permS), gens(tgrp)[1:grpsz^2]);
  tpT = tpgrp(collect(permT), gens(tgrp)[1+grpsz^2:2*grpsz^2]);
  
  relS = ModularForms.twists(tpS^4);
  relST = ModularForms.twists((tpS*tpT)^3 * tpS^2);

  # The quotient by all relations gives the group of all possible twists
  tgrp_relquo,psi = quo(unwrap(tgrp), map(unwrap, vcat(relS, relST)));

  # We pick a random element from the dual to obtain a twist by MuInf
  dtgrp,phi = Hecke.dual(tgrp_relquo);
  tw = phi(rand(dtgrp))

  twist_component(a::Int) = MuInf(lift(tw( psi(gen(unwrap(tgrp),a)) )))
  tinfpgrp = TwistedPermGroup(MuInf, grpsz^2)
  tpS = tinfpgrp(collect(permS), map(twist_component, 1:grpsz^2));
  tpT = tinfpgrp(collect(permT), map(twist_component, 1+grpsz^2:2*grpsz^2));

  level = Int(exponent(grp) * twist_level)
  return ArithmeticType(SL2Z, tinfpgrp, [tpS,tpT]; level=16);
end

end
