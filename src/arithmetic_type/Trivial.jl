################################################################################
# properties
################################################################################

group(a::TrivialArithType{G,I}) where {G,I} = a.group

image_parent(a::TrivialArithType{G,I}) where {G,I} = a.image_parent

dim(a::TrivialArithType{G,I}) where {G,I} = dim(image_parent(a))

level(rho::TrivialArithType{G,I}) where {G,I} = level(group(rho))

################################################################################
# comparison
################################################################################

function ==(a::TrivialArithType{G,I}, b::TrivialArithType{G,I}) where {G,I}
  group(a) == group(b) && image_parent(a) == image_parent(b)
end

isone(::TrivialArithType{G,I}) where {G,I} = true

################################################################################
# display
################################################################################

function show(io::IO, rho::TrivialArithType{G,I}) where {G,I}
  print(io, "𝟙")
end

################################################################################
# creation
################################################################################

TrivialArithmeticType(group::G) where G = TrivialArithmeticType(group, TrivialGroup())

function TrivialArithmeticType(group::G, image_parent::I) where {G <: Group, I <: Group}
  TrivialArithType{elem_type(group),elem_type(image_parent)}(group, image_parent)
end

################################################################################
# conversion
################################################################################

function GenArithType(rho::TrivialArithType{G,I}) where {G,I}
  ArithmeticType(group(rho), image_parent(rho),
                 [one(image_parent(rho)) for g in gens(group(rho))])
end

################################################################################
# arithmetic
################################################################################

function tensor_product(a::TrivialArithType{G,I}, b::TrivialArithType{G,I}) where {G,I}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  TrivialArithmeticType(group(a), tensor_product(image_parent(a), image_parent(b)))
end

function dual(a::TrivialArithType{G,I}) where {G,I}
  TrivialArithmeticType(group(a), dual(image_parent(a)))
end

# tensor products with other types

function tensor_product(a::TrivialArithType{G,I}, b::ArithType{G,I}) where {G,I}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  aone = one(image_parent(a))
  ArithmeticType(group(a), tensor_product(image_parent(a), image_parent(b)),
                 [tensor_product(aone, b(g)) for g in gens(group(a))])
end

function tensor_product(b::ArithType{G,I}, a::TrivialArithType{G,I}, ) where {G,I}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  return b
end

################################################################################
# evaluation
################################################################################

(rho::TrivialArithType{G,I})(::G) where {G,I} = one(image_parent(rho))

function (rho::ArithType{G1,TrivialGroupElem})(g::G2) where {G1,G2}
  one(image_parent(rho))
end

function gen_word(
    rho::ArithType{G,TrivialGroupElem},
    word::Vector{Tuple{Int,Int}}
   ) where {G}
  one(image_parent(rho))
end
