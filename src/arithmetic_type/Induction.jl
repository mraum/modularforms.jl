################################################################################
# creation
################################################################################

function IndArithType{G,H,T}(
    grp       :: Group,
    iparent   :: Group,
    rho       :: ArithType{H,T},
    leftright :: Symbol
   ) where {G, H, T}
  if leftright == :left
    coset_reps = left_coset_representatives(grp, group(rho))
  elseif leftright == :rightinv
    coset_reps = rightinv_coset_representatives(grp, group(rho))
  elseif leftright == :right
    coset_reps = right_coset_representatives(grp, group(rho))
  elseif leftright == :leftinv
    coset_reps = leftinv_coset_representatives(grp, group(rho))
  else
    throw(DomainError(leftright,
        "must be :left, :right, :leftinv, or :rightinv"))
  end
  return IndArithType{G,H,T}(grp, iparent, rho, leftright, coset_reps)
end

function induction(
    grp :: Group,
    rho :: ArithType{G,I}
   ) where {G, I}
  left_induction(grp, rho)
end

function left_induction(
    grp :: Group,
    rho :: ArithType{G,I}
   ) where {G, I}
  coset_reps = left_coset_representatives(grp, group(rho))
  iparent = TwistedPermGroup(image_parent(rho), length(coset_reps))
  sigma = IndArithType{elem_type(grp),G,I}(
              grp, iparent, rho, :left, coset_reps)
  _copy_level!(sigma, rho)
  return sigma
end

function right_induction(
    grp :: Group,
    rho :: ArithType{G,I}
   ) where {G, I}
  coset_reps = right_coset_representatives(grp, group(rho))
  iparent = TwistedPermGroup(image_parent(rho), length(coset_reps))
  sigma = IndArithType{elem_type(grp),G,I}(
              grp, iparent, rho, :right, coset_reps)
  _copy_level!(sigma, rho)
  return sigma
end

################################################################################
# properties
################################################################################

group(rho::IndArithType{G,H,T}) where {G,H,T} = rho.group

base_group(rho::IndArithType{G,H,T}) where {G,H,T} = group(base_type(rho))

base_type(rho::IndArithType{G,H,T}) where {G,H,T} = rho.base_type

image_parent(rho::IndArithType{G,H,T}) where {G,H,T} = rho.image_parent

coset_representatives(rho::IndArithType{G,H,T}) where {G,H,T} = rho.coset_reps

level(rho::IndArithType{G,H,T}) where {G,H,T} = level(base_type(rho))

################################################################################
# arithmetic and tensor products
################################################################################

function conj(rho::IndArithType{G,H,T}) where {G,H,T}
  return IndArithType{G,H,T}(
             group(rho), image_parent(rho),
             conj(base_type(rho)),
             rho.leftright, rho.coset_reps)
end

function dual(rho::IndArithType{G,H,T}) where {G,H,T}
  if rho.leftright == :left
    leftright = :leftinv
  elseif rho.leftright == :rightinv
    leftright = :right
  elseif rho.leftright == :right
    leftright = :rightinv
  elseif rho.leftright == :leftinv
    leftright = :left
  else
    throw(DomainError(leftright,
        "must be :left, :right, :leftinv, or :rightinv"))
  end
  return IndArithType{G,H,T}(group(rho), dual(image_parent(rho)),
                             dual(base_type(rho)), leftright)
end

################################################################################
# evaluation
################################################################################

(rho::IndArithType{G1,H,T})(g::G2) where {G1,G2,H,T} = rho(group(rho)(g))

function (rho::IndArithType{G,H,T})(g::G) where {G,H,T}
  n = length(coset_representatives(rho))
  ps = Vector{Int}(undef, n)
  ts = Vector{T}(undef, n)

  for (hx,h) in enumerate(rho.coset_reps)
    if rho.leftright == :left
      (_ , ps[hx], k) = left_coset_transformation_with_index(
                            base_group(rho), g*h)
      ts[hx] = base_type(rho)(k)
    elseif rho.leftright == :rightinv
      (_ , ps[hx], k) = rightinv_coset_transformation_with_index(
                            base_group(rho), g*h)
      ts[hx] = base_type(rho)(k)
    elseif rho.leftright == :right
      (k, _ , ps[hx]) = right_coset_transformation_with_index(
                            base_group(rho), h*inv(g))
      ts[hx] = base_type(rho)(inv(k))
    elseif rho.leftright == :leftinv
      (k, _ , ps[hx]) = leftinv_coset_transformation_with_index(
                            base_group(rho), h*inv(g))
      ts[hx] = base_type(rho)(inv(k))
    else
      throw(DomainError(leftright,
          "must be :left, :right, :leftinv, or :rightinv"))
    end
  end

  return image_parent(rho)(ps, ts; check = false)
end

function gen_word(
    rho::IndArithType{G,H,I},
    word::Vector{Tuple{Int,Int}}
   ) where {G,H,I}
  rho(gen_word(group(rho), word))
end

function act(
    rho::IndArithType{G1,H,T},
    g::G2,
    ix::Int
   ) where {G1,G2,H,T}
  act(rho(g), ix)
end

function act_perm(
    rho::IndArithType{G1,H,T},
    g::G2,
    ix::Int
   ) where {G1,G2,H,T}
  act_perm(rho(g), ix)
end

function act_gen_word(
    rho::IndArithType{G,H,T},
    word::Vector{Tuple{Int,Int}},
    ix::Int
   ) where {G,H,T}
  act(gen_word(rho,word), ix)
end

function act_perm_gen_word(
    rho::IndArithType{G,H,T},
    word::Vector{Tuple{Int,Int}},
    ix::Int
   ) where {G,H,T}
  act_perm(gen_word(rho,word), ix)
end

################################################################################
# stabilizers of components for twisted permutations
################################################################################

function component_stabilizer(
    rho::IndArithType{G,H,T},
    ix::Int
   ) where {
    G, H, T
   }
  if ix == 1
    return base_group(rho)
  elseif rho.leftright == :left || rho.leftright == :rightinv
    return right_conjugate(base_group(rho), coset_representatives(rho)[ix])
  elseif rho.leftright == :right || rho.leftright == :leftinv
    return left_conjugate(base_group(rho), coset_representatives(rho)[ix])
  else
    throw(DomainError(leftright,
        "must be :left, :right, :leftinv, or :rightinv"))
  end
end

function component_restriction(
    grp::Group,
    rho::IndArithType{G,H,T},
    ix::Int;
    check :: Bool = true
   ) where {
    G, H, T
   }
  h = coset_representatives(rho)[ix]
  hinv = inv(g)
  if check
    # NOTE: We check that the conjugates of the generators of grp are in the
    # base group of rho, since we otherwise have to compute the conjucates of
    # the base group.
    if rho.leftright == :left || rho.leftright == :rightinv
      if any(hinv*g*h in base_group(rho) for g in gens(grp))
        throw(DomainError(grp, "does not stabilize component $ix"))
      end
    elseif rho.leftright == :right || rho.leftright == :leftinv
      if any(h*g*hinv in base_group(rho) for g in gens(grp))
        throw(DomainError(grp, "does not stabilize component $ix"))
      end
    else
      throw(DomainError(leftright,
          "must be :left, :right, :leftinv, or :rightinv"))
    end
  end
  sigma = component_stabilizer_restriction(rho, ix)
  return restriction(grp, sigma)
end

function component_stabilizer_restriction(
    rho::IndArithType{G,H,T},
    ix::Int
   ) where {
    G <: SL2ZCoverElement, H <: SL2ZCoverElement, T
   }
  # TODO: we have the invariant that the first element of every set of coset
  # representatives is the identity
  if ix == 1
    return base_type(rho)
  elseif rho.leftright == :left || rho.leftright == :rightinv
    return right_pullback(coset_representatives(rho)[ix],
                          elem_type(base_group(rho)),
                          base_type(rho))
  elseif rho.leftright == :right || rho.leftright == :leftinv
    return left_pullback(coset_representatives(rho)[ix],
                         elem_type(base_group(rho)), 
                         base_type(rho))
  else
    throw(DomainError(leftright,
       "must be :left, :right, :leftinv, or :rightinv"))
  end
end
