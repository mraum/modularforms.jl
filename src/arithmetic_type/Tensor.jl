################################################################################
# properties
################################################################################

group(a::TensorArithType{G,I}) where {G,I} = a.group

image_parent(a::TensorArithType{G,I}) where {G,I} = a.image_parent

dim(a::TensorArithType{G,I}) where {G,I} = dim(image_parent(a))

function components(a::TensorArithType{G,I}) where {G,I}
  a.components
end

function level(a::TensorArithType{G,I}) where {G,I}
  lvl = get_attribute(a, :level)
  if !isnothing(lvl)
    return lvl
  end

  lvl = lcm([level(b) for b in components(a)])
  set_attribute!(a, :level => lvl)
  return lvl
end

function generator_images(::TensorArithType{G,I}) where {G,I}
  error("For efficiency generator images of tensor types should not be computed directly. If you need this function, first apply GenArithType.")
end

################################################################################
# comparison
################################################################################

function ==(
    a::TensorArithType{G,I},
    b::TensorArithType{G,I}
   ) where {G,I}
  if isempty(components(a)) && isempty(components(b))
    return group(a) == group(b) && image_parent(a) == image_parent(b)
  else
    return components(a) == components(b)
  end
end

################################################################################
# display
################################################################################

function show(io::IO, a::TensorArithType{G,I}) where {G,I}
  @assert !isempty(components(a))
  show(io, components(a)[1])
  for b in components(a)[2:end]
    println(io,"")
    println(io,"⊗")
    show(io, b)
  end
end

################################################################################
# creation
################################################################################

function TensorArithmeticType(
    components::Vector;
    image_parent :: Union{Nothing,Group} = nothing,
    check::Bool = false
   )
  if isempty(components)
    throw(DomainError(components, "list of components cannot be empty"))
  end
  grp = group(components[1])

  if check
    for c in components[1:end]
      if !(c isa ArithType)
        throw(DomainError(c, "components must be arithmetic types"))
      end
    end
    for c in components[2:end]
      if group(c) != grp
        throw(DomainError(c, "incompatible group"))
      end
    end
  end

  if isnothing(image_parent)
    image_parent = reduce(tensor_product,
                          [ModularForms.image_parent(c) for c in components])
  end

  components_flattened = []
  for c in components
    if c isa TensorArithType
      append!(components_flattened, ModularForms.components(c))
    else
      push!(components_flattened, c)
    end
  end

  rho = TensorArithType{elem_type(grp), elem_type(image_parent)}(
            grp, image_parent, components_flattened)
  _copy_level!(rho, components...)

  return rho
end

function tensor_product(
    a::TensorArithType{G,Ia},
    b::TensorArithType{G,Ib}
   ) where {G,Ia,Ib}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  iparent = tensor_product(image_parent(a), image_parent(b))
  rho = TensorArithType{G,elem_type(iparent)}(
            group(a), iparent, vcat(components(a), components(b)))
  _copy_level!(rho, a, b)
  return rho
end

function tensor_product(
    a::TensorArithType{G,Ia},
    b::ArithType{G,Ib}
   ) where {G,Ia,Ib}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  iparent = tensor_product(image_parent(a), image_parent(b))
  rho = TensorArithType{G,elem_type(iparent)}(
            group(a), iparent, push!(copy(components(a)), b))
  _copy_level!(rho, a, b)
  return rho
end

function tensor_product(
    a::ArithType{G,Ia},
    b::TensorArithType{G,Ib}
   ) where {G,Ia,Ib}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  iparent = tensor_product(image_parent(a), image_parent(b))
  rho = TensorArithType{G,elem_type(iparent)}(
            group(a), iparent, insert!(copy(components(b)),1,a))
  _copy_level!(rho, a, b)
  return rho
end

function tensor_product(
    a::ArithType{G,Ia},
    b::ArithType{G,Ib}
   ) where {G,Ia,Ib}
  group(a) == group(b) || throw(DomainError((a,b), "incompatible groups"))
  iparent = tensor_product(image_parent(a), image_parent(b))
  rho = TensorArithType{G,elem_type(iparent)}(
            group(a), iparent, [a,b])
  _copy_level!(rho, a, b)
  return rho
end

function ^(a :: ArithType{G,I}, n :: Int) where {G,I}
  if n <= 0
    throw(DomainError(n, "only positive exponents allowed"))
  end
  if n == 1
    return a
  end
  b = a ⊗ a
  for _ in 3:n
    b = b ⊗ a
  end
  return b
end

################################################################################
# conversion
################################################################################

function GenArithType(a::TensorArithType{G,I}) where {G,I}
  gen_images = reshape(vcat([generator_images(c) for c in components(a)]...),
                       ngens(group(a)), length(components(a)))
  b = GenArithType{G,I}(group(a), image_parent(a),
          [reduce(tensor_product, gen_images[gx,:]) for gx in 1:ngens(group(a))])
  _copy_level!(b, a)
  return b
end

################################################################################
# arithmetic
################################################################################

function conj(rho::TensorArithType{G,I}) where {G,I}
  sigma = TensorArithType{G,I}(group(rho), image_parent(rho),
              [conj(c) for c in components(rho)])
  _copy_level!(sigma, rho)
  return sigma
end

function dual(rho::TensorArithType{G,I}) where {G,I}
  iparent = dual(image_parent(rho))
  sigma = TensorArithType{G,elem_type(iparent)}(group(rho), iparent,
              [dual(c) for c in components(rho)])
  _copy_level!(sigma, rho)
  return sigma
end

################################################################################
# evaluation
################################################################################

function (rho::TensorArithType{G1,I})(g::G2) where {G1,G2,I}
  word = word_in_gens(group(rho)(g))
  return reduce(tensor_product, [gen_word(c, word) for c in components(rho)])
end

################################################################################
# tensor product of vectors
################################################################################

function (rho::TensorArithType{G,I})(
    v::Vector{A},
    vs::Vector{A}...
   ) where {
    G,I, A <: RingElement
   }
  if !all(dim(image_parent(c)) == 1 ||
          (image_parent(c) isa TwistedPermGroup && dim(twist_parent(image_parent(c))) == 1)
          for c in components(rho)) &&
     !any(image_parent(c) isa TwistedPermGroup
          for c in components(rho))
    # TODO: we shift all permutations to the front in these cases
    error("not implemented")
  end

  return reduce(kronecker_product, vs, init=v)
end
