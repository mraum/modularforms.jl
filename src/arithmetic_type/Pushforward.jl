################################################################################
# creation
################################################################################

@doc Markdown.doc"""
    pushforward(f::Map{D,C}, rho::ArithType{G,I}) where {D,C,G,I}

> Pushforward of an arithmetic type $G \rightarrow \mathrm{GL}(V)$ via a
> homomorphisms $\mathrm{GL}(V) \rightarrow \mathrm{GL}(W)$.
"""
function pushforward(f::Map{D,C}, rho::ArithType{G,I}) where {D,C,G,I}
  if domain(f) != image_parent(rho)
    throw(DomainError(domain(f), "incompatible parents"))
  end
  return ArithmeticType(group(rho), codomain(f),
             map(f, generator_images(rho)))
end
