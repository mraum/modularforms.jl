################################################################################
# properties
################################################################################

group(a::DirichletType{G}) where {G} = a.group

image_parent(a::DirichletType{G}) where {G} = MuInf

character(rho::DirichletType{G}) where {G} = rho.chi

function level(rho::DirichletType{G}) where {G}
  # In the case of Dirichlet characters the level of the
  # group must subsume the modulus of the character to
  # guarantee the co-primality.
  level(group(rho))
end

################################################################################
# creation
################################################################################

function ArithmeticType(grp::SL2ZSubgroup, chi::DirichletCharacter)
  DirichletType(grp, chi)
end

################################################################################
# arithmetic and tensor products
################################################################################

function conj(rho::DirichletType{G}) where {G}
  ArithmeticType(group(rho), conj(character(rho)))
end

dual(rho::DirichletType{G}) where {G} = conj(rho)

################################################################################
# restriction and pullback
################################################################################

function restriction(
    grp :: SL2ZGroup,
    rho :: DirichletType{G}
   ) where {
    G <: SL2ZElement
   }
  DirichletType(grp, character(rho))
end

function left_pullback(g::G, rho::DirichletType{G}) where {G <: SL2ZElement}
  if parent(g) != group(rho)
    throw(DomainError(g, join("when pulling back in an ambient group, ",
                              "the domain group has to be passed as an argument")))
  end
  return rho
end

function right_pullback(g::G, rho::DirichletType{G}) where {G <: SL2ZElement}
  if parent(g) != group(rho)
    throw(DomainError(g, join("when pulling back in an ambient group, ",
                              "the domain group has to be passed as an argument")))
  end
  return rho
end

################################################################################
# evaluation
################################################################################

function (rho::DirichletType{G1})(g::G2) where {G1,G2}
  rho(group(rho)(g))
end

function (rho::DirichletType{G})(g::SL2ZElem) where {G <: SL2ZElement}
  character(rho)(g.d)
end

function (rho::DirichletType{G})(g::SL2ZSubgroupElem) where {G <: SL2ZElement}
  rho(SL2Z(g))
end

function gen_word(
    rho::DirichletType{G},
    word::Vector{Tuple{Int,Int}}
   ) where {G,I}
  rho(gen_word(group(rho),word))
end
