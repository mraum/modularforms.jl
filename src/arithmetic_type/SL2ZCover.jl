################################################################################
################################################################################
# arithmetic types
################################################################################
################################################################################

################################################################################
# induction to groups
################################################################################

function induction(rho::AT) where {
    G <: SL2ZSubgroupCoverElem, I,
    AT <: ArithType{G,I}
   }
  induction(change_base_group(SL2Z, group(rho)), rho)
end

function left_induction(rho::AT) where {
    G <: SL2ZSubgroupCoverElem, I,
    AT <: ArithType{G,I}
   }
  left_induction(change_base_group(SL2Z, group(rho)), rho)
end

function right_induction(rho::AT) where {
    G <: SL2ZSubgroupCoverElem, I,
    AT <: ArithType{G,I}
   }
  right_induction(change_base_group(SL2Z, group(rho)), rho)
end

################################################################################
# pullbacks to cover groups
################################################################################

function pullback(gp::SL2ZSubgroupCover, rho::A) where
    {G <: SL2ZElement, I,
     A <: ArithType{G,I}
    }
  base_group(gp) == group(rho) || throw(DomainError(gp, "can only pullback to cover"))
  sigma = ArithmeticType(gp, image_parent(rho),
              [rho(group(rho)(base_group_element(g))) for g in gens(gp)])
  _copy_level!(sigma, rho)
  return sigma
end
