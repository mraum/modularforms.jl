################################################################################
# properties
################################################################################

group(a::ResArithType{G,H,I}) where {G,H,I} = a.group

base_type(a::ResArithType{G,H,I}) where {G,H,I} = a.restricted_type

base_group(a::ResArithType{G,H,I}) where {G,H,I} = group(base_type(a))

image_parent(a::ResArithType{G,H,I}) where {G,H,I} = image_parent(base_type(a))

dim(a::ResArithType{G,H,I}) where {G,H,I} = dim(base_type(a))

function level(rho::ResArithType{G,H,I}) where {G,H,I}
  lcm(level(group(rho)), level(base_type(rho)))
end

function generator_images(rho::ResArithType{G,H,I}) where {G,H,I}
  if ismissing(rho.gen_images)
    rho.gen_images = map(rho, gens(group(rho)))
  end
  return rho.gen_images
end

################################################################################
# comparison
################################################################################

function ==(
    a::ResArithType{G,H,I},
    b::ResArithType{G,H,I}
   ) where {G,H,I}
  group(a) == group(b) && base_type(a) == base_type(b)
end

function isone(a::ResArithType{G,H,I}) where {G,H,I}
  isone(base_type(a))
end

################################################################################
# display
################################################################################

function show(io::IO, a::ResArithType{G,H,I}) where {G,H,I}
  show(io, "Res_")
  show(io, group(a))
  println(io, "(")
  show(io, base_type(a))
  println(io, ")")
end

################################################################################
# restriction
################################################################################

function restriction(grp::Group, rho::ResArithType{G,H,I}) where {G,H,I}
  if grp === group(rho)
    return rho
  else
    return ResArithType(grp, base_type(rho))
  end
end

################################################################################
# conversion
################################################################################

function GenericType(rho::ResArithType{G,H,I}) where {G,H,I}
  sigma = ArithmeticType(group(rho), image_parent(rho), 
              [rho(g) for g in gens(group(rho))])
  gplvl = level(gp)
  rholvl = get_attribute(base_type(rho), :level)
  if !ismissing(gplvl) && !isnothing(rholvl)
    set_attribute!(sigma, :level => lcm(gplvl,rholvl))
  end
  return sigma
end

################################################################################
# arithmetic and tensor products
################################################################################

function dual(a::ResArithType{G,H,I}) where {G,H,I}
  ResArithType(group(a), dual(base_type(a)))
end

################################################################################
# evaluation
################################################################################

function (rho::ResArithType{G1,H,I})(g::G2) where {G1,G2,H,I}
  base_type(rho)(g)
end

function mul(
    rho::ResArithType{G1,H,TwistedPerm{T}},
    g::G2,
    ix::Int
   ) where {G1,G2,H,T}
  mul(base_type(rho), g, ix)
end

function mul_perm(
    rho::ResArithType{G1,H,TwistedPerm{T}},
    g::G2,
    ix::Int
   ) where {G1,G2,H,T}
  mul_perm(base_type(rho), g, ix)
end
