################################################################################
# creation
################################################################################

function jacobi_type(gj::JacobiGroup{GT,ET}, rho::AT) where {GT,ET,I,AT <: ArithType{GT,I}}
  base_group(gj) == group(rho) || error("incompatible base group")
  return ArithmeticTypeFamily(
      gj, image_parent(rho),
      vcat(generator_images(rho),
           fill(one(image_parent(rho)),
                ngens(extension_group(gj)))
          )
     )
end

################################################################################
# restriction to torsion points
################################################################################

function torsion_point_restriction(
  N::Int,
  rho::ArithType{JacobiGroup{G,HeisenbergRkGenGroupElem{fmpz}},I}
  ) where {G,I}
  return torsion_point_restriction(N, N, rho)
end

function torsion_point_restriction(
  N::Int, NZ::Int,
  rho::ArithType{JacobiGroup{G,HeisenbergRkGenGroupElem{fmpz}},I}
  ) where {G,I}
  # TODO: isolate this in a module
  error("update this method to new framework")
  ## NZ the order of the action of the center
  N > 0 || throw(ArgumentError("negative index of torsion points"))
  Nfmpz = fmpz(N)
  NZfmpz = fmpz(NZ)

  jacobi_group = group(rho)
  base_group = jacobi_group.base_group
  genus = genus(jacobi_group.extension_group)
  rank = rank(jacobi_group.extension_group)


  gen_perms = [Dict{Int,Int}() for gx in 1:ngens(base_group)]
  gen_twists = [Dict{Int,I}() for gx in 1:ngens(base_group)]

  for (tpix,tp) in enumerate(torsion_points(N,NZ,genus,rank))
    for (gix,g) in enumerate(gens(base_group))
      (tpg_int, tpg_frac) = fldmod(tp*g,Nfmpz, NZfmpz)
      gen_perms[gix][tpix] = torsion_point_index(N,NZ,tpg_frac)
      gen_twists[gix][tpix] = rho(jacobi_group(tpg_int...))
    end
  end

  tps = torsion_points_size(N,NZ,genus,rank)
  twperm_parent = TwistedPermutationParent{I}(image_parent(rho))
  return ArithmeticType(
      jacobi_group, twperm_parent,
      [twperm_parent([perms[ix] for ix in 1:tps],
                            [twists[ix] for ix in 1:tps])
              for (perms,twists) in zip(gen_perms, gen_twists)]
     )
end

################################################################################
# torsion points: internals
################################################################################

mutable struct TorsionPoint
  la::Array{fmpz,2} ## represents rationals with denominator N
  mu::Array{fmpz,2} ## represents rationals with denominator N
  ka::Array{fmpz,2} ## represents rationals with denominator NZ
end


function fldmod(tp::TorsionPoint, N::fmpz, NZ::fmpz)
  la_fldmod = [fldmod(a,N) for a in tp.la]
  la_int = first.(la_fldmod)
  la_frac = last.(la_fldmod)

  mu_fldmod = [fldmod(a,N) for a in tp.mu]
  mu_int = first.(mu_fldmod)
  mu_frac = last.(mu_fldmod)

  ka_fldmod = [fldmod(a,NZ) for a in tp.ka]
  ka_int = first.(ka_fldmod)
  ka_frac = last.(ka_fldmod)

  ((la_int,mu_int,ka_int), TorsionPoint(la_frac,mu_frac,ka_frac))
end

*(tp::TorsionPoint, ga::SL2ZElem) =
  TorsionPoint(ga.a .* tp.la .+ ga.c .* tp.mu, ga.b .* tp.la .+ ga.d .* tp.mu, tp.ka)

*(tp::TorsionPoint, ga::SL2ZSubgroupElem) = tp * ga.gamma

*(tp::TorsionPoint, ga::SL2ZSubgroupCoverElem) = tp * ga.gamma


function torsion_points_size(N::Int, NZ::Int, genus::Int, rank::Int)
  N^(2*genus*rank) * NZ^(rank*rank)
end

function torsion_point_index(N::Int, NZ::Int, tp::TorsionPoint)
  ix = 1
  Npw = 1
  for (v,step) in [(tp.la,N),(tp.mu,N),(tp.ka,NZ)]
    for a in v
      ix += Npw*a
      Npw *= step
    end
  end
  return ix
end

function torsion_points(N::Int, NZ::Int, genus::Int, rank::Int)
  (TorsionPoint(lamuka...)
   for lamuka in @task torsion_points_task_(N, NZ, genus, rank)
  )
end

function torsion_points_task_(N::Int, NZ::Int, genus::Int, rank::Int)
  (N > 0 && NZ > 0 && mod(N,NZ) == 0 && genus > 0 && rank > 0) ||
    throw("incorrect arguments")

  for la in @task array_enumeration_task_(N, (genus,rank))
    for mu in @task array_enumeration_task_(N, (genus,rank))
      for ka in @task array_enumeration_task_(NZ, (rank,rank))
        produce((la,mu,ka))
      end
    end
  end
end

function array_enumeration_task_(N::Int, sizes::Tuple{Int})
  size = prod(sizes)
  v = fill(zero(ZZ), size)

  ix = 1
  while ix != size + 1
    if v[ix] == N
      v[ix] = zero(ZZ)
      ix += 1
    else
      v[ix] += one(ZZ)
      ix = 1
      produce(reshape(v, sizes))
    end
  end
end
