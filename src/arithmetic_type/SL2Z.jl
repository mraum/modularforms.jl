################################################################################
# creation
################################################################################

function ArithmeticType(
    group::SL2ZCover,
    image_parent::Group,
    gen_images::Vector{I};
    check::Bool = true,
    level::Union{Nothing,Int} = nothing
   ) where I
  rho = GenericType(group, image_parent, gen_images; check = check)
  if !isnothing(level)
    if check
      for g in gens(intersect(group,GammaSL2Z(level)))
        if !isone(rho(g))
          throw(DomainError(g, "type not trivel on element $g of level $level"))
        end
      end
    end
    set_attribute!(rho, :level, level)
  end
  return rho
end

function standard_type(group::SL2ZGroup)
  matgroup = MatrixGroup(QQ, 2)
  return ArithmeticType(group, matgroup,
             [matgroup(map(QQ,matrix(g))) for g in gens(group)])
end

function ArithmeticType(chi::DirichletCharacter)
  ArithmeticType(Gamma0SL2Z(modulus(chi)), chi)
end

function induction(rho::AT) where {
    G <: SL2ZElement, I,
    AT <: ArithType{G,I}
   }
  induction(SL2Z, rho)
end

function left_induction(rho::AT) where {
    G <: SL2ZElement, I,
    AT <: ArithType{G,I}
   }
  left_induction(SL2Z, rho)
end

function right_induction(rho::AT) where {
    G <: SL2ZElement, I,
    AT <: ArithType{G,I}
   }
  right_induction(SL2Z, rho)
end

################################################################################
# translation decomposition
################################################################################

function translation_decomposition(rho::ArithType{G,I}) where {
    G <: SL2ZElement,
    I
   }
  os = get_attribute(rho, :translation_decomposition)
  if isnothing(os)
    # We need to base the translation orbits on the inverse of the translation
    # action T, since the slash invariance implies
    #   f(\tau) = inv(rho(T)) f(T \tau).
    # In other word, advancing one step in the orbit of SL2Z(1,-1,0,1)
    # corresponds to multiplication by e(n), where n is the Fourier expansion
    # index.
    os = translation_decomposition(rho(SL2Z(1,-1,0,1)))
    set_attribute!(rho, :translation_decomposition => os)
  end
  return os
end

################################################################################
# evaluation
################################################################################

function (rho::IndArithType{SL2ZElem,H,T})(g::SL2ZElem) where {H,T}
  n = length(coset_representatives(rho))
  ps = Vector{Int}(undef, n)
  ts = Vector{T}(undef, n)

  for (hx,h) in enumerate(rho.coset_reps)
    if rho.leftright == :left
      (_ , ps[hx], k) = left_coset_transformation_with_index(
                            base_group(rho), g*h)
      ts[hx] = base_type(rho)(k)
    elseif rho.leftright == :rightinv
      (_ , ps[hx], k) = rightinv_coset_transformation_with_index(
                            base_group(rho), g*h)
      ts[hx] = base_type(rho)(k)
    elseif rho.leftright == :right
      tmp = SL2ZElem()
      (k, _ , ps[hx]) = right_coset_transformation_with_index(
                            base_group(rho), h*inv!(tmp,g))
      ts[hx] = base_type(rho)(group(rho)(inv!(tmp,SL2Z(k)); check = false))
    elseif rho.leftright == :leftinv
      tmp = SL2ZElem()
      (k, _ , ps[hx]) = leftinv_coset_transformation_with_index(
                            base_group(rho), h*inv!(tmp,g))
      ts[hx] = base_type(rho)(group(rho)(inv!(tmp,SL2Z(k)); check = false))
    else
      throw(DomainError(leftright,
          "must be :left, :right, :leftinv, or :rightinv"))
    end
  end

  return image_parent(rho)(ps, ts; check = false)
end

################################################################################
# random
################################################################################

rand_char_perm_type(group::SL2ZGroup, complexity) = rand_char_perm_type(GLOBAL_RNG, group, complexity)

rand_char_perm_type(r::AbstractRNG, group::SL2ZGroup, complexity::Int) = rand_char_perm_type(r, group, 1:complexity)

function rand_char_perm_type(r::AbstractRNG, group::SL2ZGroup, complexity::UnitRange{Int})
  chi = rand(r, DirichletCharacter, complexity)
  rhochi = ArithmeticType(chi)
  # TODO: if necessary use restriction(intersection(group(rhochi),group),rhochi)
  _issl2z(group) || error("not implemented")
  return induction(group,rhochi)
end
