################################################################################
################################################################################
# type functions
################################################################################
################################################################################

parent_type(::Type{WeilTransformation{G}}) where G = WeilImage{G}

parent(a::WeilTransformation{G}) where G = a.parent

elem_type(::WeilImage{G}) where G = WeilTransformation{G}

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

function ==(a::WeilImage{G}, b::WeilImage{G}) where G
  a.preimage_parent == b.preimage_parent && a.matrix_type == b.matrix_type
end

################################################################################
# properties
################################################################################

preimage_parent(a::WeilImage{G}) where G = a.preimage_parent

dim(a::WeilImage{G}) where G = dim(a.matrix_type)

coefficient_ring_tower(a::WeilImage{G}) where G = QQabTower

coefficient_ring(a::WeilImage{G}) where G = QQab

################################################################################
# display
################################################################################

function show(io::IO, a::WeilImage{G}) where G
  # TODO: insert discrimiant form, once vvmf is available
  print(io, "ρ_")
  show(io, a.lattice)
end

################################################################################
# creation
################################################################################

WeilImage(group::G, m::I) where {G,I <: Integer} = WeilImage(group, fill(m,1,1))

function WeilImage(group::G, m::Matrix{I}) where {G,I <: Integer}
  WeilImage(group, Zlattice(gram = matrix(ZZ, m)))
end

function WeilImage(
    gp::SL2ZCover,
    lattice::Hecke.ZLat
   )
  gp in [SL2Z, Mp1Z] || error("not implemented")
  if isodd(rank(lattice)) && gp != Mp1Z
    throw(DomainError(lattice, "Weil types for odd rank lattice not defined over SL(2,Z)"))
  end

  disc = discriminant_group(lattice)
  disc_elms = collect(disc)
  sz = length(disc)

  e(a::Hecke.QmodnZElem) = e(lift(a))
  ehalf(a::Hecke.QmodnZElem) = e(lift(a)//2)
  e(a::fmpq) = root_of_unity(QQab, Int(denominator(a)))^Int(numerator(a))

  T = [i==j ? ehalf(Hecke.quadratic_product(disc_elms[i])) : zero(QQab)
       for i = 1:sz, j = 1:sz]
  sc = sum(ehalf(inv(Hecke.quadratic_product(v))) for v in disc) // sz
  S =  [sc*e(inv(v*w)) for v in disc, w in disc]

  WeilImage{elem_type(gp)}(gp, lattice,
     MatrixType(gp, [S, T]),
     MatrixType(gp, map(conj, [S, T])))
end

################################################################################
# arithmetic
################################################################################

function *(a::WeilImage{G}, b::WeilImage{G}) where G
  a == b || throw(DomainError((a,b), "multiplication only for identical parents"))
  return a
end

# ad-hoc arithmetic on arrays

function *(a::WeilImage{G}, b::Array{R}) where {G, R <: RingElement}
  matrix_unit(a) * b
end

################################################################################
# tensor product
################################################################################

_WeilImage_dual_cache = Dict{WeilImage,WeilImage}()

function dual(a::WeilImage{G}) where G
  if haskey(_WeilImage_dual_cache, a)
    return _WeilImage_dual_cache[a]
  end
  b = WeilImage{G}(a.preimage_parent, rescale(a.lattice,-1), a.matrix_type_dual, a.matrix_type)
  _WeilImage_dual_cache[a] = b
  return b
end

function tensor_product(a::WeilImage{G}, b::WeilImage{G}) where G
  MatrixGroup(QQab, dim(a)*dim(b))
end

function tensor_product(a::TrivialGroup, b::WeilImage{G}) where G
  b
end

function tensor_product(b::WeilImage{G}, a::TrivialGroup) where G
  b
end

function tensor_product(a::RootOfUnityGroup, b::WeilImage{G}) where G
  MatrixGroup(QQab, dim(b))
end

function tensor_product(b::WeilImage{G}, a::RootOfUnityGroup) where G
  MatrixGroup(QQab, dim(b))
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

function ==(a::WeilTransformation{G}, b::WeilTransformation{G}) where G
  parent(a) == parent(b) && a.preimage == b.preimage
end

function isone(a::WeilTransformation{G}) where G
  isone(preimage(a)) || isone(matrix(a))
end

################################################################################
# properties
################################################################################

dim(a::WeilTransformation{G}) where G = dim(parent(a))

preimage(a::WeilTransformation{G}) where G = a.preimage

################################################################################
# display
################################################################################

function show(io::IO, a::WeilTransformation{G}) where G
  show(io, matrix(a))
end

################################################################################
# creation
################################################################################

function (weilimage::WeilImage{G})() where G
  one(weilimage)
end

function (weilimage::WeilImage{G})(a::WeilTransformation{G}) where G
  parent(a) == weilimage || throw(DomainError("cannot change parent of Weil transformations"))
  deepcopy(a)
end

function (weilimage::WeilImage{G})(a::G) where G
  WeilTransformation(weilimage, a)
end

one(weilimage::WeilImage{G}) where G = weilimage(one(preimage_parent(weilimage)))

copy(a::WeilTransformation{G}) where G = parent(a)(preimage(a))

function deepcopy_internal(a::WeilTransformation{G}, stackdict::IdDict) where G
  return parent(a)(deepcopy_internal(a.preimage, stackdict))
end

################################################################################
# conversion
################################################################################

matrix(a::WeilTransformation{G}) where G = matrix(QQab, a)

matrix(::QQabField, a::WeilTransformation{G}) where G = matrix(matrix_unit(a))

matrix(CC::AcbField, a::WeilTransformation{G}) where G = matrix(CC, matrix(matrix_unit(a)))

function matrix(f::MuInfTower, fFF::Field, a::WeilTransformation{G}) where G
  matrix(f,fFF,matrix_unit(a))
end

function matrix_unit(a::WeilTransformation{G}) where G
  return (parent(a).matrix_type)(preimage(a))
end

function tr(a::WeilTransformation{G}) where G
  return tr(matrix_unit(a))
end

function (g::MatrixGroup{cf_elem})(a::WeilTransformation{G}) where G
  if g == image_parent(parent(a).matrix_type)
    return matrix_unit(a)
  end
  return MatrixGroupElem{cf_elem}(g, matrix(a))
end

################################################################################
# arithmetic
################################################################################

function *(a::WeilTransformation{G}, b::WeilTransformation{G}) where G
  parent(a) == parent(b) || throw(DomainError("incompatible parents"))
  parent(a)(preimage(a)*preimage(b))
end

function mul!(
    c::WeilTransformation{G},
    a::WeilTransformation{G},
    b::WeilTransformation{G}
   ) where G
  if parent(c) != parent(a) || parent(c) != parent(b)
    throw(DomainError((parent(c), parent(a), parent(b)), "incompatible parents"))
  end
  c.preimage = mul!(c.preimage, preimage(a), preimage(b))
  return c
end

Base.inv(a::WeilTransformation{G}) where G = parent(a)(inv(preimage(a)))

function ^(a::WeilTransformation{G}, n::Int) where G
  if n < 0
    power_by_squaring(inv(a),-n)
  else
    power_by_squaring(a,n)
  end
end

# ad-hoc

function *(a::RootOfUnity, b::WeilTransformation{G}) where G
  return a * matrix_unit(b)
end

function *(a::WeilTransformation{G}, b::RootOfUnity) where G
  return matrix_unit(a) * b
end

################################################################################
# actions
################################################################################

# action on itself

act(a::WeilTransformation{G}, b::WeilTransformation{G}) where {G} = a * b
# action on vectors

function act(a::WeilTransformation{G}, b::Vector{F}) where {G,F}
  mul_classical(matrix(parent(first(b)),a),b)
end

################################################################################
# tensor product
################################################################################

dual(a::WeilTransformation{G}) where G = dual(parent(a))(preimage(a))

function tensor_product(a::WeilTransformation{G}, b::WeilTransformation{G}) where G
  tensor_product(matrix_unit(a), matrix_unit(b))
end

function tensor_product(a::TrivialGroupElem, b::WeilTransformation{G}) where G
  b
end

function tensor_product(b::WeilTransformation{G}, a::TrivialGroupElem) where G
  b
end

function tensor_product(a::RootOfUnity, b::WeilTransformation{G}) where G
  tensor_product(a, matrix_unit(b))
end

function tensor_product(b::WeilTransformation{G}, a::RootOfUnity) where G
  tensor_product(matrix_unit(b), a)
end
