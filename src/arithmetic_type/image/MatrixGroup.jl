################################################################################
################################################################################
# type functions
################################################################################
################################################################################

parent_type(::Type{MatrixGroupElem{T}}) where T = MatrixGroup{T}

parent(a::MatrixGroupElem{T}) where T = a.parent

elem_type(::MatrixGroup{T}) where T = MatrixGroupElem{T}

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

function ==(a::MatrixGroup{T}, b::MatrixGroup{T}) where T
  return matrix_parent(a) == matrix_parent(b) &&
      coefficient_ring_tower(a) == coefficient_ring_tower(b)
end

################################################################################
# properties
################################################################################

matrix_parent(a::MatrixGroup{T}) where T = a.matrix_parent

# We need to include this specialization, since compile time otherwise
# increases dramatically.
matrix_parent(a::MatrixGroup{fmpq})::Nemo.FmpqMatSpace= a.matrix_parent

dim(a::MatrixGroup{T}) where T = nrows(matrix_parent(a))

coefficient_ring_tower(a::MatrixGroup{T}) where T = a.coefftower

base_ring(a::MatrixGroup{T}) where T = base_ring(matrix_parent(a))

coefficient_ring(a::MatrixGroup{T}) where T = base_ring(a)

################################################################################
# display
################################################################################

function show(io::IO, a::MatrixGroup{T}) where T
  print(io,"GL($(dim(a)),")
  show(io, base_ring(a))
  print(io,")")
end

################################################################################
# creation
################################################################################

function MatrixGroup(ring::R, size::Int) where R
  MatrixGroup{elem_type(ring)}(MatrixSpace(ring, size), cyclotomic_tower(ring))
end

function MatrixGroup(coefftower::MuInfTower, ring::R, size::Int) where R
  ring in coefftower || throw(DomainError(ring, "ring must lie in coefficient tower"))
  MatrixGroup{elem_type(ring)}(MatrixSpace(ring, size), coefftower)
end

################################################################################
# arithmetic
################################################################################

function *(a::MatrixGroup{T}, b::MatrixGroup{T}) where T
  a == b && return a

  dim(a) == dim(b) || throw(DomainError((a,b), "incompatible sizes"))
  return MatrixSpace(base_ring(a)*base_ring(b), dim(a))
end

################################################################################
# actions
################################################################################

# action on itself

act(a::MatrixGroupElem{T}, b::MatrixGroupElem{T}) where {T} = a * b

# action on arrays

function act(a::MatrixGroupElem{T}, b::Vector{R}) where {T, R <: RingElement}
  dim(a) == length(b) || throw(DomainError(b, "incompatible vector length"))
  mul_classical(matrix(a), b)
end

function act(a::MatrixGroupElem{T}, b::Matrix{R}) where {T, R <: RingElement}
  dim(a) == size(b,1) || throw(DomainError(b, "incompatible matrix size"))
  res = Matrix{R}(undef, size(b)...)
  for jx in 1:size(b,2)
    res[:,jx] .= mul_classical(matrix(a), b[:,jx])
  end
  return res
end

# Act on the first dimension of b.
function act(a::MatrixGroupElem{T}, b::AbstractArray{R}) where {T, R <: RingElement}
  reshape(act(a, reshape(b, size(b,1), prod(size(b)[2:end]))),
          size(b)...)
end

# Act on the ix dimension of b.
function act(a::MatrixGroupElem{T}, b::AbstractArray{R}, ix::Int) where {T, R <: RingElement}
  # We rotate dimensions between 1 and ix instead of swapping them. This should
  # be better for memory locally.
  b_rot, perm = rotatedims_with_perm(b, ix)
  b_rot = act(a, b_rot)
  return permutedims(b_rot, invperm(perm))
end

################################################################################
# tensor products
################################################################################

function dual(a::MatrixGroup{T}) where {T <: Union{fmpq, cf_elem}}
  a
end

function tensor_product(a::MatrixGroup{T}, b::MatrixGroup{T}) where T
  if base_ring(a) == base_ring(b)
    return MatrixGroup(base_ring(a), dim(a)*dim(b))
  else
    return MatrixGroup(base_ring(a)*base_ring(b), dim(a)*dim(b))
  end
end

function tensor_product(a::MatrixGroup{T}, b::TrivialGroup) where T
  a
end

function tensor_product(b::TrivialGroup, a::MatrixGroup{T}) where T
  a
end

function tensor_product(a::MatrixGroup{T}, b::RootOfUnityGroup) where T
  a
end

function tensor_product(b::RootOfUnityGroup, a::MatrixGroup{T}) where T
  a
end

################################################################################
# random
################################################################################

function rand(
    ::Type{MatrixGroup},
    coeffring::R,
    complexity
   ) where {R <: Ring}
  rand(GLOBAL_RNG, MatrixGroup, coeffring, complexity)
end

function rand(
    r::AbstractRNG,
    ::Type{MatrixGroup},
    coeffring::R,
    complexity::Int
   ) where {R <: Ring}
  rand(r, MatrixGroup, coeffring, 1:complexity)
end

function rand(
    r::AbstractRNG,
    ::Type{MatrixGroup},
    coeffring::R,
    complexity::UnitRange{Int}
   ) where {R <: Ring}
  msize = rand(r, complexity)
  return MatrixGroup(coeffring, msize)
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

==(a::MatrixGroupElem{T}, b::MatrixGroupElem{T}) where T = matrix(a) == matrix(b)

isone(a::MatrixGroupElem{T}) where T = isone(matrix(a))

################################################################################
# properties
################################################################################

base_ring(a::MatrixGroupElem{T}) where T = base_ring(parent(a))

matrix(a::MatrixGroupElem{T}) where T = a.matrix

# We need to include this specialization, since compile time otherwise
# increases dramatically.
matrix(a::MatrixGroupElem{fmpq})::Nemo.fmpq_mat = a.matrix

dim(a::MatrixGroupElem{T}) where T = dim(a.parent)

getindex(a::MatrixGroupElem{T}, ix::Int64, jx::Int64) where T = getindex(matrix(a), ix, jx)

################################################################################
# display
################################################################################

function show(io::IO, a::MatrixGroupElem{T}) where T
  show(io, matrix(a))
end

################################################################################
# creation and conversion
################################################################################

function (parent::MatrixGroup{T})() where T
  one(parent)
end

function (parent::MatrixGroup{T})(a::Array{T,2}) where T
  parent(matrix_parent(parent)(a))
end

one(parent::MatrixGroup{T}) where T = parent(one(matrix_parent(parent)))

copy(a::MatrixGroupElem{T}) where T = parent(a)(copy(matrix(a)))

function deepcopy_internal(a::MatrixGroupElem{T}, stackdict::IdDict) where T
  return parent(a)(deepcopy_internal(matrix(a), stackdict))
end

################################################################################
# conversion
################################################################################

function (parent::MatrixGroup{T})(a::MatrixGroupElem{T}) where T
  parent(matrix(a))
end

function (parent::MatrixGroup{T})(a::MatElem{T}) where T
  MatrixGroupElem{T}(parent, matrix_parent(parent)(a))
end

(parent::MatrixGroup{T})(a::T) where T = parent(matrix_parent(parent)(a))

(parent::MatrixGroup{cf_elem})(a::RootOfUnity) = parent(QQab(a))

function matrix(f::MuInfTower, fFF::Field, a::MatrixGroupElem{T}) where T
  f == coefficient_ring_tower(parent(a)) || throw(DomainError("incompatibe tower"))
  fFF in f || throw(DomainError("field not in tower"))
  if fFF == coefficient_ring(parent(a))
    return matrix(a)
  else
    conv = map_from_cyclotomic_tower(f, coefficient_ring(parent(a)), fFF)
    return change_base_ring(conv, matrix(a); parent = fFF)
  end
end

tr(a::MatrixGroupElem{T}) where {T <: RingElem} = tr(matrix(a))

################################################################################
# arithmetic
################################################################################

function *(a::MatrixGroupElem{T}, b::MatrixGroupElem{T}) where T
  if parent(a) != parent(b)
    throw(DomainError((parent(a),parent(b)), "incompatible parents"))
  end
  return parent(a)(matrix(a)*matrix(b))
end

function mul!(
    c::MatrixGroupElem{T},
    a::MatrixGroupElem{T},
    b::MatrixGroupElem{T}
   ) where T
  if parent(c) != parent(a) || parent(c) != parent(b)
    throw(DomainError((parent(c),parent(a),parent(b)), "incompatible parents"))
  end
  c.matrix = mul!(matrix(c), matrix(a), matrix(b))
  return c
end

function Base.inv(a::MatrixGroupElem{T}) where T
  parent(a)(inv(matrix(a)))
end

function ^(a::MatrixGroupElem{T}, n::Int) where T
  if n < 0
    power_by_squaring(inv(a),-n)
  else
    power_by_squaring(a,n)
  end
end

function conj(a::MatrixGroupElem{T}) where T
  # TODO: use conjucation relative to the coefficient tower
  parent(a)(conj(matrix(a)))
end

# action of roots of unity on cf_elem

*(::Type{MatrixGroupElem{cf_elem}}, ::Type{RootOfUnity}) = MatrixGroupElem{cf_elem}
*(::Type{RootOfUnity}, ::Type{MatrixGroupElem{cf_elem}}) = MatrixGroupElem{cf_elem}

function *(a::MatrixGroupElem{cf_elem}, b::RootOfUnity)
  parent(a)(matrix(a) * coefficient_ring_tower(parent(a))(coefficient_ring(parent(a)), b))
end

function *(b::RootOfUnity, a::MatrixGroupElem{cf_elem})
  parent(a)(coefficient_ring_tower(parent(a))(coefficient_ring(parent(a)), b) * matrix(a))
end

# actions on matrices and vectors

*(a::MatrixGroupElem{T}, b::Vector{T}) where T = mul_classical(matrix(a), b)

*(a::Vector{T}, b::MatrixGroupElem{T}) where T = mul_classical(a, matrix(b))

*(a::MatrixGroupElem{T}, b::MatElem{T}) where T = matrix(a) * b

*(a::MatElem{T}, b::MatrixGroupElem{T}) where T = a * matrix(b)

################################################################################
# tensor product
################################################################################

function dual(a::MatrixGroupElem{T}) where T
  parent(a)(inv(transpose(matrix(a))))
end

function tensor_product(
    a::MatrixGroupElem{T},
    b::MatrixGroupElem{T}
   ) where T
  tensor_product(parent(a), parent(b))(
      kronecker_product(matrix(a),matrix(b)))
end

function tensor_product(
    a::TrivialGroupElem,
    b::MatrixGroupElem{T}
   ) where T
  b
end

function tensor_product(
    b::MatrixGroupElem{T},
    a::TrivialGroupElem
   ) where T
  b
end

function tensor_product(
    a::RootOfUnity,
    b::MatrixGroupElem{T}
   ) where T
  a = coefficient_ring_tower(parent(b))(coefficient_ring(parent(b)), a)
  parent(b)(a*matrix(b))
end

function tensor_product(
    b::MatrixGroupElem{T},
    a::RootOfUnity
   ) where T
  a = coefficient_ring_tower(parent(b))(coefficient_ring(parent(b)), a)
  parent(b)(matrix(b)*a)
end

################################################################################
# random
################################################################################

function rand(
    parent::MatrixGroup{T},
    complexity
   ) where {T <: RingElem}
  rand(GLOBAL_RNG, parent, complexity)
end

function rand(
    r::AbstractRNG,
    parent::MatrixGroup{T},
    complexity
   ) where {T <: RingElem}
  while true
    m = rand(matrix_parent(parent), complexity)
    ## FIXME: this is only correct over division rings
    iszero(det(m)) || return parent(m)
  end
end
