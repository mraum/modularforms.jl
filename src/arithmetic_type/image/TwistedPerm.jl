################################################################################
################################################################################
# type functions
################################################################################
################################################################################

parent_type(::Type{TwistedPerm{T}}) where T = TwistedPermGroup{T}

parent(a::TwistedPerm{T}) where T = a.parent

elem_type(::Type{TwistedPermGroup{T}}) where T = TwistedPerm{T}

################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

twist_parent(a::TwistedPermGroup{T}) where T = a.twist_parent

permsize(a::TwistedPermGroup{T}) where T = a.permsize

dim(a::TwistedPermGroup{T}) where T = permsize(a) * dim(twist_parent(a))

base_group(a::TwistedPermGroup{T}) where T = twist_parent(a)

################################################################################
# display
################################################################################

function show(io::IO, a::TwistedPermGroup{T}) where T
  print(io,"(")
  show(io, twist_parent(a))
  print(io,")≀S_$(permsize(a))")
end

################################################################################
# comparison
################################################################################

function ==(a::TwistedPermGroup{T}, b::TwistedPermGroup{T}) where T
  permsize(a) == permsize(b) && a.twist_parent == b.twist_parent
end

################################################################################
# creation
################################################################################

function TwistedPermGroup(twist_parent::T, permsize::Int) where {T <: Group}
  TwistedPermGroup{elem_type(twist_parent)}(twist_parent, permsize)
end

################################################################################
# arithmetic
################################################################################

function *(a::TwistedPermGroup{T}, b::TwistedPermGroup{T}) where T
  a == b || throw(DomainError((a,b), "incompatible parents"))
end

function tensor_product(
    a::TwistedPermGroup{A},
    b::TwistedPermGroup{B}
   ) where
    {G,
     R,
     A <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{R}, WeilTransformation{G}},
     B <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{R},WeilTransformation{G}}
   }
  tparent = twist_parent(a) ⊗ twist_parent(b)
  TwistedPermGroup{elem_type(tparent)}(tparent, permsize(a)*permsize(b))
end

function tensor_product(
    a::A,
    b::TwistedPermGroup{B}
   ) where
    {G,
     R,
     A <: Union{TrivialGroup, RootOfUnityGroup,
                MatrixGroup{R}, WeilImage{G}},
     B <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{R},WeilTransformation{G}}
   }
  tparent = a ⊗ twist_parent(b)
  TwistedPermGroup{elem_type(tparent)}(tparent, permsize(b))
end

function tensor_product(
    a::TwistedPermGroup{A},
    b::B
   ) where
    {G,
     R,
     A <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{R}, WeilTransformation{G}},
     B <: Union{TrivialGroup, RootOfUnityGroup,
                MatrixGroup{R},WeilImage{G}}
   }
  tparent = twist_parent(a) ⊗ b
  TwistedPermGroup{elem_type(tparent)}(tparent, permsize(a))
end

function dual(a::TwistedPermGroup{T}) where {G,T}
  TwistedPermGroup(dual(twist_parent(a)), permsize(a))
end

# ad-hoc products involving MatrixGroup

function *(a::Ta, b::TwistedPermGroup{Tb}) where {
    Ta <: Union{TrivialGroup, RootOfUnityGroup,
                MatrixGroup{cf_elem}},
    Tb <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{cf_elem}}
    }
  TwistedPermGroup(a*twist_parent(b), permsize(b))
end

function *(a::TwistedPermGroup{Ta}, b::Tb) where {
    Ta <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem{cf_elem}},
    Tb <: Union{TrivialGroup, RootOfUnityGroup,
                MatrixGroup{cf_elem}}
    }
  TwistedPermGroup(twist_parent(a)*b, permsize(a))
end

# ad-hoc products involving WeilTransformation

function *(a::Ta, b::TwistedPermGroup{Tb}) where {
    G <: SL2ZCoverElement,
    Ta <: Union{TrivialGroup, RootOfUnityGroup, WeilImage{G}},
    Tb <: Union{TrivialGroupElem, RootOfUnity, WeilTransformation{G}}
    }
  TwistedPermGroup(a*twist_parent(b), permsize(b))
end

function *(a::TwistedPermGroup{Ta}, b::Tb) where {
    G <: SL2ZCoverElement,
    Ta <: Union{TrivialGroupElem, RootOfUnity, WeilTransformation{G}},
    Tb <: Union{TrivialGroup, RootOfUnityGroup, WeilImage{G}}
    }
  TwistedPermGroup(twist_parent(a)*b, permsize(a))
end

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

twist_parent(a::TwistedPerm{T}) where T = parent(a).twist_parent

permsize(a::TwistedPerm{T}) where T = permsize(parent(a))

dim(a::TwistedPerm{T}) where T = dim(parent(a))

perms(a::TwistedPerm{T}) where T = a.perms

twists(a::TwistedPerm{T}) where T = a.twists

permsinv(a::TwistedPerm{T}) where T = a.permsinv

twistsinv(a::TwistedPerm{T}) where T = a.twistsinv

function order(a::TwistedPerm{T}) where T
  ord = 1
  a = copy(a)
  for ix in 1:permsize(a)
    jx = perms(a)[ix]
    if jx != ix
      cyclen = 1
      while jx != ix
        jx = perms(a)[jx]
        cyclen += 1
      end
      ord *= cyclen
      a = a^cyclen
    end
  end
  return ord * lcm(order.(twists(a)))
end

function tr(a::TwistedPerm{T}) where T
  t = zero(QQ)
  for ix in 1:permsize(a)
    if perms(a)[ix] == ix
      t += tr(twists(a)[ix])
    end
  end
  return t
end

################################################################################
# display
################################################################################

function show(io::IO, a::TwistedPerm{T}) where T
  println(io,"( ")
  for (ix,jx,t) in zip(1:permsize(a),perms(a),twists(a))
    print(io,"$ix→")
    show(io,t)
    print(io,"≀$jx ")
  end
  print(io,")")
end

################################################################################
# comparison
################################################################################

function ==(a::TwistedPerm{T}, b::TwistedPerm{T}) where T
  parent(a) == parent(b) && perms(a) == perms(b) && twists(a) == twists(b)
end

function isone(a::TwistedPerm{T}) where T
  all(p == i for (i,p) in enumerate(perms(a))) && all(isone,a.twists)
end

################################################################################
# creation
################################################################################

function (tpg::TwistedPermGroup{T})() where T
  one(tpg)
end

function (tpg::TwistedPermGroup{T})(
    perms::Vector{Int};
    check::Bool = true
   ) where T
  if check
    if permsize(tpg) != length(perms)
      throw(DomainError(perms, "incompatible number of permutations"))
    end
  end
  tpg(perms, fill(one(twist_parent(tpg)), permsize(tpg)); check=false)
end

function (tpg::TwistedPermGroup{T})(
    perms::Vector{Int},
    twists::Vector{T};
    check::Bool = true
   ) where T
  if check
    if permsize(tpg) != length(perms)
      throw(DomainError(perms, "incompatible number of permutations"))
    end
    if permsize(tpg) != length(twists)
      throw(DomainError(twists, "incompatible number of twists"))
    end
    for tw in twists
      if parent(tw) != twist_parent(tpg)
        throw(DomainError(tw, "incompatible parent of twist"))
      end
    end
  end

  permsinv = invperm(perms)
  return TwistedPerm{T}(tpg, perms, twists, permsinv, map(inv,twists)[permsinv])
end

function (tpg::TwistedPermGroup{T})(
    perms::Vector{Int},
    twists::Vector{T},
    permsinv::Vector{Int},
    twistsinv::Vector{T};
    check::Bool = true
   ) where T
  if check
    if permsize(tpg) != length(perms)
      throw(DomainError(perms, "incompatible number of permutations"))
    end
    if permsize(tpg) != length(twists)
      throw(DomainError(twists, "incompatible number of twists"))
    end
    if permsize(tpg) != length(permsinv)
      throw(DomainError(twistsinv, "incompatible number of inverse twists"))
    end
    if permsize(tpg) != length(twistsinv)
      throw(DomainError(twistsinv, "incompatible number of inverse twists"))
    end
    for tw in twists
      if parent(tw) != twist_parent(tpg)
        throw(DomainError(tw, "incompatible parent of twist"))
      end
    end
    for tw in twistsinv
      if parent(tw) != twist_parent(tpg)
        throw(DomainError(tw, "incompatible parent of inverse twist"))
      end
    end
  end
  return TwistedPerm{T}(tpg, perms, twists, permsinv, twistsinv)
end

function one(tpg::TwistedPermGroup{T}) where T
  TwistedPerm{T}(tpg,
      collect(1:permsize(tpg)),
      fill(one(twist_parent(tpg)), permsize(tpg)),
      collect(1:permsize(tpg)),
      fill(one(twist_parent(tpg)), permsize(tpg))
      )
end

function copy(a::TwistedPerm{T}) where T
  parent(a)(
      copy(perms(a)), copy(twists(a)),
      copy(permsinv(a)), copy(twistsinv(a));
      check = false)
end

function deepcopy_internal(a::TwistedPerm{T}, stackdict::IdDict) where T
  twists = Array{T}(undef, permsize(a))
  twistsinv = Array{T}(undef, permsize(a))
  for ix in 1:permsize(a)
     twists[ix] = deepcopy_internal(a.twists[ix], stackdict)
     twistsinv[ix] = deepcopy_internal(a.twistsinv[ix], stackdict)
  end
  return parent(a)(copy(a.perms), twists, copy(a.permsinv), twistsinv; check = false)
end

function cyclic_permutation(gp::TwistedPermGroup{T}) where T
  gp(push!(collect(2:permsize(gp)),1))
end

################################################################################
# conversion
################################################################################

function (tpg::TwistedPermGroup{T1})(a::TwistedPerm{T2}) where {T1,T2}
  permsize(tpg) < permsize(a) && error("incompatible permutation size")

  nperms = vcat(a.perms, (permsize(a)+1):permsize(tpg))
  ntwists = vcat(map(twist_parent(tpg),a.twists),
                 fill(one(tpg), permsize(tpg)-permsize(a)))
  npermsinv = vcat(a.permsinv, (permsize(a)+1):permsize(tpg))
  ntwistsinv = vcat(map(twist_parent(tpg),a.twistsinv),
                 fill(one(tpg), permsize(tpg)-permsize(a)))

  return TwistedPerm{T1}(tpg, nperms, ntwists, npermsinv, ntwistsinv)
end

function (tpg::TwistedPermGroup{T})(a::T) where T
  permsize(tpg) == 1 || error("permutation size must equal 1")

  return TwistedPerm{T}(tpg, [1], [a], [1], [inv(a)])
end

function (mspace::AbstractAlgebra.Generic.MatSpace{cf_elem})(a::TwistedPerm{T}) where
    {T <: Union{TrivialGroupElem, RootOfUnity}}
  nrows(mspace) == ncols(mspace) || error("matrix space must be square")
  permsize(a) <= nrows(mspace) || error("incompatible size")

  m = zero(mspace)
  for jx in 1:permsize(a)
    m[a.perms[jx],jx] = QQab(a.twists[jx])
  end
  for jx in permsize(a)+1:nrows(mspace)
    m[jx,jx] = one(QQab)
  end

  return m
end

function (mspace::AbstractAlgebra.Generic.MatSpace{cf_elem})(a::TwistedPerm{T}) where
    {G, T <: Union{MatrixGroupElem{cf_elem}, WeilTransformation{G}}}
  nrows(mspace) == ncols(mspace) || error("matrix space must be square")

  wdim = dim(twist_parent(a))
  permsize(a)*wdim <= nrows(mspace) || error("incompatible size")

  m = zero(mspace)
  for jx in 1:permsize(a)
    mt = matrix(QQab, a.twists[jx])
    for ixx in 1:wdim, jxx in 1:wdim
      m[(a.perms[jx]-1)*wdim+ixx,(jx-1)*wdim+jxx] = mt[ixx,jxx]
    end
  end
  for jx in permsize(a)*wdim+1:nrows(mspace)
    m[jx,jx] = one(QQab)
  end

  return m
end

function matrix(ring::QQabField, a::TwistedPerm{T}) where T
  MatrixSpace(QQab, dim(a), dim(a))(a)
end

function (g::MatrixGroup{cf_elem})(a::TwistedPerm{T}) where T
  MatrixGroupElem{cf_elem}(g,matrix_parent(g)(a))
end

################################################################################
# arithmetic
################################################################################

function *(a::TwistedPerm{T}, b::TwistedPerm{T}) where {T <: GroupElem}
  parent(a) == parent(b) || throw(DomainError((a,b), "incompatible parents"))

  permsab = perms(a)[perms(b)]
  twistsab = twists(a)[perms(b)] .* twists(b)
  permsinvab = permsinv(b)[permsinv(a)]
  twistsinvab = twistsinv(b)[permsinv(a)] .* twistsinv(a)

  return parent(a)(permsab, twistsab, permsinvab, twistsinvab; check = false)
end

function mul!(c::TwistedPerm{T}, a::TwistedPerm{T}, b::TwistedPerm{T}) where {T <: GroupElem}
  parent(a) == parent(b) == parent(c) || throw(DomainError((c,a,b), "incompatible parents"))

  c.twists .= twists(a)[perms(b)] .* twists(b)
  c.twistsinv .= twistsinv(b)[permsinv(a)] .* twistsinv(a)
  c.perms .= perms(a)[perms(b)]
  c.permsinv .= permsinv(b)[permsinv(a)]
  return c
end

function Base.inv(a::TwistedPerm{T}) where T
  return parent(a)(permsinv(a), twistsinv(a), perms(a), twists(a); check=false)
end

function ^(a::TwistedPerm{T}, n::Int) where {T <: GroupElem}
  if n == 0
    return one(parent(a))
  elseif n < 0
    return power_by_squaring(inv(a), -n)
  else
    return power_by_squaring(a, n)
  end
end

# ad-hoc products

function *(a::TwistedPerm{Ta}, b::Tb) where {
    Ta <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation},
    Tb <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation}
    }
  par = parent(a)*parent(b)
  binv = inv(b)
  par(perms(a), [t*b for t in twists(a)], permsinv(a), [binv*t for t in twistsinv(a)])
end

function *(a::Ta, b::TwistedPerm{Tb}) where {
    G <: SL2ZCoverElement,
    Ta <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation},
    Tb <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation}
    }
  par = parent(a)*parent(b)
  ainv = inv(a)
  par(perms(a), [a*t for t in twists(a)], permsinv(a), [t*ainv for t in twistsinv(a)])
end

################################################################################
# actions
################################################################################

# action on itself

act(a::TwistedPerm{T}, b::TwistedPerm{T}) where {T} = a * b

# action on component indices

function act(a::TwistedPerm{T}, ix::Int) where {T}
  return (perms(a)[ix], twists(a)[ix])
end

function act_perm(a::TwistedPerm{T}, ix::Int) where {T}
  return perms(a)[ix]
end

function act(a::TwistedPerm{T}, ixt::Tuple{Int,R}) where {T,R}
  (ix,it) = ixt
  return (perms(a)[ix], act(twists(a)[ix], it))
end

function act(a::TwistedPerm{T}, b::Dict{Int,R}) where {T,R}
  ab = Dict{Int,R}()
  for (ix,it) in b
    if ix <= permsize(a)
      ab[perms(a)[ix]] = act(twists(a)[ix], it)
    else
      ab[ix] = it
    end
  end
  return ab
end

function act(
    a::TwistedPerm{T},
    b::Vector{R}
   ) where {
    T <: Union{TrivialGroupElem, RootOfUnity},
    R <: RingElem
   }
  permsize(a) <= length(b) || error("incompatible size")

  ab = Vector{R}(undef, length(b))
  for (ix,it) in enumerate(b[1:permsize(a)])
    ab[perms(a)[ix]] = act(twists(a)[ix], it)
  end
  for ix in permsize(a)+1:length(b)
    ab[ix] = b[ix]
  end

  return ab
end

function act(
    a::TwistedPerm{T},
    b::Vector{R}
   ) where {
    R <: RingElem,
    T <: Union{MatrixGroupElem, WeilTransformation}
   }
  wdim = dim(twist_parent(parent(a)))
  permsize(a)*wdim <= length(b) || error("incompatible size")

  ab = Vector{R}(undef, length(b))
  for ax in 1:permsize(a)
    ixx = (perms(a)[ax]-1)*wdim
    abs = act(twists(a)[ax], b[(ax-1)*wdim+1:ax*wdim])
    for ix in 1:wdim
      ab[ix + ixx] = abs[ix]
    end
  end
  for ix in permsize(a)*wdim+1:length(b)
    ab[ix] = b[ix]
  end

  return ab
end

################################################################################
# tensor products
################################################################################

function dual(a::TwistedPerm{T}) where {G,T}
  return dual(parent(a))(perms(a), map(dual, twists(a)),
                         permsinv(a), map(dual, twistsinv(a)))
end

function tensor_product(a::TwistedPerm{A}, b::TwistedPerm{B}) where
    {G,
     A <: Union{TrivialGroupElem, RootOfUnity, WeilTransformation{G}},
     B <: Union{TrivialGroupElem, RootOfUnity, WeilTransformation{G}}}
  nperms = [(ap-1)*permsize(b) + bp for ap in perms(a) for bp in perms(b)]
  ntwists = [at⊗bt for at in twists(a) for bt in twists(b)]
  npermsinv = [(ap-1)*permsize(b) + bp for ap in permsinv(a) for bp in permsinv(b)]
  ntwistsinv = [at⊗bt for at in twistsinv(a) for bt in twistsinv(b)]

  tensor_product(parent(a), parent(b))(nperms, ntwists, npermsinv, ntwistsinv)
end

function tensor_product(a::TwistedPerm{Ta}, b::Tb) where {
    Ta <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation},
    Tb <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation}
    }
  par = tensor_product(parent(a),parent(b))
  par(perms(a), [t⊗b for t in twists(a)],
      permsinv(a), [t⊗b for t in twistsinv(a)])
end

function tensor_product(a::Ta, b::TwistedPerm{Tb}) where {
    Ta <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation},
    Tb <: Union{TrivialGroupElem, RootOfUnity,
                MatrixGroupElem,
                WeilTransformation}
    }
  par = tensor_product(parent(a),parent(b))
  par = tensor_product(parent(a),parent(b))
  par(perms(b), [a⊗t for t in twists(b)],
      permsinv(b), [a⊗t for t in twistsinv(b)])
end

################################################################################
# random
################################################################################

rand(gp::TwistedPermGroup{T}, complexity...) where T = rand(GLOBAL_RNG, gp, complexity...)

function rand(r::AbstractRNG, gp::TwistedPermGroup{T}, complexity...) where T
  gp(randperm(permsize(gp)),
     [rand(r, twist_parent(gp), complexity...) for tx in 1:permsize(gp)];
     check = false)
end
