################################################################################
# properties
################################################################################

function coefficient_ring(stg::SymTransformationGroup)
  stg.coeffring
end

function group(stg::SymTransformationGroup)
  stg.group
end

function degree(stg::SymTransformationGroup)
  stg.degree
end

function isdual(stg::SymTransformationGroup)
  stg.isdual
end

function polyring(stg::SymTransformationGroup)
  stg.polyring
end

function dualpolyring(stg::SymTransformationGroup)
  stg.polyring
end

function elem(st::SymTransformation)
  st.group_elem
end

function parent(st::SymTransformation)
  st.parent
end


################################################################################
# creation
################################################################################

function one(stg::SymTransformationGroup{G}) where G <: GroupElem
  SymTransformation{G}(stg, one(group(stg)))
end

function (stg::SymTransformationGroup{G})(a::G) where G <: GroupElem
  SymTransformation{G}(stg, a)
end

copy(a::SymTransformation{G}) where G = parent(a)(elem(a))

function deepcopy_internal(a::SymTransformation{G}, stackdict::IdDict) where G
  return parent(a)(deepcopy_internal(elem(a), stackdict))
end

################################################################################
# comparison
################################################################################

function ==(a::SymTransformationGroup{G}, b::SymTransformationGroup{G}) where G
  coefficient_ring(a) == coefficient_ring(b) && group(a) == group(b) && degree(a) == degree(b) &&
     isdual(a) == isdual(b)
end

function ==(a::SymTransformation{G}, b::SymTransformation{G}) where G
  parent(a) == parent(b) && elem(a) == elem(b)
end

function isone(a::SymTransformation{G}) where G
  isone(elem(a))
end

################################################################################
# arithmetic
################################################################################

function *(a::SymTransformation{G}, b::SymTransformation{G}) where G
  parent(a) == parent(b) || throw(DomainError("incompatible parents"))
  parent(a)(elem(a)*elem(b))
end

function mul!(
    c::SymTransformation{G},
    a::SymTransformation{G},
    b::SymTransformation{G}
   ) where G
  if parent(c) != parent(a) || parent(c) != parent(b)
    throw(DomainError((parent(c), parent(a), parent(b)), 
                      "incompatible parents"))
  end
  #In Weil transformation, this is instead
  #c.group_elem = mul!(c.group_elem, a.group_elem, b.group_elem)
  mul!(c.group_elem, a.group_elem, b.group_elem)
end

Base.inv(a::SymTransformation{G}) where G = parent(a)(inv(elem(a)))

function ^(a::SymTransformation{G}, n::Int) where G
  if n < 0
    power_by_squaring(inv(a),-n)
  else
    power_by_squaring(a,n)
  end
end

################################################################################
# dual helper
################################################################################

function vector_dual(stg::SymTransformationGroup{G}, b::PolyElem) where G
  if !isdual(stg)
    #sym^d(τ) -> sym^d(X)*, τ^i |-> (-1)^i (d choose i)^{-1} (X^{d-i})*
    parent(b) == polyring(stg) || degree(b) <= degree(stg) || 
      throw(DomainError("incompatible parents"))
    R = dualpolyring(stg)
    d = degree(stg)
    Y = gen(R)
    return sum([coeff(b,n)*(-1)^n*binomial(d,n)^(-1)*Y^(n-i) for n=0:d])
  else
    #sym^d(X)* -> sym^d(τ), (X^i)* |-> (-1)^{d-i} (d choose i) τ^{d-i} 
    parent(b) == dualpolyring(stg) || degree(b) <= degree(stg) || 
      throw(DomainError("incompatible parents"))
    R = polyring(stg)
    d = degree(stg)
    return sum([coeff(b,n)*(-1)^(n-1)*binomial(d,n)*X^(n-i) for n=0:d])
  end
end

################################################################################
# actions
################################################################################

# action on itself
act(a::SymTransformation{G}, b::SymTransformation{G}) where {G} = a * b

# action on corresponding vector space
function act(M::SymTransformation{G}, b::PolyElem) where G <: SL2ZFullGroup
  stg = parent(M)
  if !isdual(stg)
    parent(b) == polyring(stg) || degree(b) <= degree(stg) || 
      throw(DomainError("incompatible parents"))
    R = polyring(stg)
    d = degree(stg)
    X = gen(R)
    ga = elem(M)
    a,b,c,d = ga.a, ga.b, ga.c, ga.d
    return sum([coeff(b,n)*(d*X-b)^n*(-c*X+a)^(d-n) for n=0:d])
  else
    #recall that symd(X)*(γ)(p) = v(symd(τ)(γ)(v^{-1}(p)))
    #map from X* to τ
    dual!(stg)
    #act via symd(τ)(M)
    p = act(stg(M),vector_dual(stg, b))
    #map from τ to X*
    dual!(stg)
    return vector_dual(stg, p)
  end
end

function act(M::SymTransformation{G}, b::PolyElem) where G <: SL2ZSubgroup
  stg = parent(M)
  if !isdual(stg)
    parent(b) == polyring(stg) || degree(b) <= degree(stg) || 
      throw(DomainError("incompatible parents"))
    R = polyring(stg)
    d = degree(stg)
    X = gen(R)
    ga = elem(M)
    a,b,c,d = ga.ga.a, ga.ga.b, ga.ga.c, ga.ga.d
    return sum([coeff(b,n)*(d*X-b)^n*(-c*X+a)^(d-n) for n=0:d])
  else
    #recall that symd(X)*(γ)(p) = v(symd(τ)(γ)(v^{-1}(p)))
    #map from X* to τ
    dual!(stg)
    #act via symd(τ)(M)
    p = act(stg(M),vector_dual(stg, b))
    #map from τ to X*
    dual!(stg)
    return vector_dual(stg, p)
  end
end

################################################################################
# dual
################################################################################

function dual!(a::SymTransformation{G}) where G
  stg = parent(a)
  stg.isdual = stg.isdual ⊻ true
end

function dual(a::SymTransformation{G}) where G
  stg = parent(a)
  stg_dual = SymTransformationGroup{G}(coefficient_ring(stg), group(stg), degree(stg),
                                       polyring(stg),dualpolyring(stg),isdual(stg))
  dual!(stg_dual)
  return stg_dual
end

function dual!(stg::SymTransformationGroup{G}) where G
  stg.isdual = stg.isdual ⊻ true
end

function dual(stg::SymTransformationGroup{G}) where G
  stg_dual = SymTransformationGroup{G}(coefficient_ring(stg), group(stg), degree(stg),
                                       polyring(stg),dualpolyring(stg),isdual(stg))
  return stg_dual
end
