################################################################################
################################################################################
# parent functions
################################################################################
################################################################################

################################################################################
# properties
################################################################################

dim(::TrivialGroup) = 1

################################################################################
# comparison
################################################################################

==(::TrivialGroup, ::TrivialGroup) = true

################################################################################
# display
################################################################################

function show(io::IO, ::TrivialGroup)
  print(io, "G𝟙")
end

################################################################################
# arithmetic
################################################################################

*(a::TrivialGroup, ::TrivialGroup) = a

################################################################################
# tensor products
################################################################################

dual(a::TrivialGroup) = a

tensor_product(a::TrivialGroup, ::TrivialGroup) = a

################################################################################
################################################################################
# element functions
################################################################################
################################################################################

################################################################################
# comparison
################################################################################

==(::TrivialGroupElem, ::TrivialGroupElem) = true

isone(::TrivialGroupElem) = true

################################################################################
# display
################################################################################

function show(io::IO, ::TrivialGroupElem)
  print(io, "1")
end

################################################################################
# creation
################################################################################

one(::TrivialGroup) = TrivialGroupElem()

(::TrivialGroupElem)() = TrivialGroupElem()

copy(a::TrivialGroupElem) = a

deepcopy_internal(a::TrivialGroupElem, stackdict::IdDict) = a

################################################################################
# conversion
################################################################################

(parent::QQabField)(::TrivialGroupElem) = one(parent)

matrix(::QQabField, ::TrivialGroupElem) = identity_matrix(QQab, 1)

matrix(CC::AcbField, ::TrivialGroupElem) = identity_matrix(CC, 1)

function matrix(f::MuInfTower, fFF::Field, ::TrivialGroupElem)
  fFF in f || throw(DomainError("field not in tower"))
  identity_matrix(fFF, 1)
end

# traces of homomorphism

tr(::TrivialGroupElem) = one(ZZ)

################################################################################
# arithmetic
################################################################################

(*)(a::TrivialGroupElem, ::TrivialGroupElem) = a

function mul!(c::TrivialGroupElem, ::TrivialGroupElem, ::TrivialGroupElem)
  return c
end

Base.inv(a::TrivialGroupElem) = a

^(a::TrivialGroupElem, ::Int) = a

################################################################################
# actions
################################################################################

# action on itself

act(a::TrivialGroupElem, b::TrivialGroupElem) = a * b

# action on ring elements

function act(::TrivialGroupElem, a::RingElement)
  deepcopy(a)
end

function act!(c::R, ::TrivialGroupElem, a::R) where {
    R <: RingElement
   }
  set!(c,a)
end

function addacteq!(c::R, ::TrivialGroupElem, a::R) where {
    R <: RingElement
   }
  addeq!(c,a)
end

# action arrays

function act(::TrivialGroupElem, a::Vector{R}) where {R <: RingElement}
  length(a) == 1 || throw(DomainError(a, "incompatible vector length"))
  deepcopy(a)
end

function act(::TrivialGroupElem, a::AbstractArray{R}) where {R <: RingElement}
  size(a,1) == 1 || throw(DomainError(a, "incompatible array size"))
  deepcopy(a)
end

function act(
    ::TrivialGroupElem,
    a::AbstractArray{R},
    ix::Int
   ) where {R <: RingElement}
  size(a,ix) == 1 || throw(DomainError(a, "incompatible array size"))
  deepcopy(a)
end

################################################################################
# tensor products
################################################################################

dual(a::TrivialGroupElem) = a

tensor_product(a::TrivialGroupElem, ::TrivialGroupElem) = a

################################################################################
# random
################################################################################

rand(gp::TrivialGroup) = TrivialGroupElem()

rand(r::AbstractRNG, gp::TrivialGroup) = TrivialGroupElem()
