using BinaryBuilder, Pkg
import Pkg.Types: VersionSpec

name = "ModularForms"
version = v"0.1.0"

if haskey(ENV, "MODULAR_FORMS_JLL_SRC")
  println("using local source $(ENV["MODULAR_FORMS_JLL_SRC"])")
  sources = [DirectorySource(ENV["MODULAR_FORMS_JLL_SRC"])]
else
  sources = [GitSource("https://gitlab.com/mraum/ModularForms_jll_src.git",
                       "4d81d41e625fde3e36dc86df5061313a8447cacd")]
end

# Bash recipe for building across all platforms
script = raw"""
cd $WORKSPACE/srcdir
if [[ ${target} == *musl* ]]; then
   export CFLAGS=-D_GNU_SOURCE
fi
./configure --prefix=$prefix --disable-static --enable-shared --with-gmp=$prefix --with-mpfr=$prefix --with-flint=$prefix --with-arb=$prefix CC=gcc ${extraflags}
make -j${nproc}
make install LIBDIR=$(basename ${libdir})
"""

# These are the platforms we will build for by default, unless further
# platforms are passed in on the command line
platforms = filter(Sys.islinux, supported_platforms())
# When uploading to Yggdrasil we want to activate more supported platforms, but
# when testing, we only cover Linux.
# platforms = supported_platforms(; exclude=Sys.iswindows)

# The products that we will ensure are always built
products = [
    LibraryProduct("libmodularforms_jll", :libmodularforms_jll)
]

# Dependencies that must be installed before this package can be built
dependencies = [
    Dependency(PackageSpec(name="Arb_jll"), compat = "~200.2300"),
    Dependency(PackageSpec(name="FLINT_jll"), compat = "^200.900")
]

# Build the tarballs, and possibly a `build.jl` as well.
build_tarballs(ARGS, name, version, sources, script, platforms, products, dependencies;
               julia_compat="^1.6", preferred_gcc_version=v"11")
